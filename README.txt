
- Credits -
Adubbz: Terrablender 
TelepathicGrunt: Structures
ModdingLegacy: Music ticker
xevinaly: Dynamic light
konwboj: Tumbleweed

- Current features -
*Shaking up gathering/crafting to rely on gold, wood, flint, saplings, grass tufts, etc
*Hazardous darkness (Charlie)
*Basic/Improved Farm utility blocks
*Turf blocks (natural and artifical ones)
*Hammer/Pitchfork
*Campfires/Fire Pits
*Recipe blueprints (many crafting recipes are locked behind these, they function exactly like they do in DS)
*Dynamic Light
*"Crops" such as grass tufts and berry bushes
*Tumbleweed mob
*Deciduous forests/rockyland/creepy forests/lumpy forests/marshes biomes
