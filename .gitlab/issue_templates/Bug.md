/////////Use this template to submit issues and use 'X' in checkboxes as with the examples below. Omit any text surrounded with slashes, so this too. Before submitting, put the Operating System line as the first line of the submitted issue.////////

**Operating System:**  
- [ ] Windows (32-bit)
- [ ] Windows (64-bit)
- [ ] OSX (64-bit)
- [ ] Linux (32-bit)
- [ ] Linux (64-bit)

**Select Release Build Branch:** 
- [ ] Stable
- [ ] Experimental (this is the only one for now, Stable will come once Experimental hits 0.0.9)

**Select Mod Version:**
- Empty for now, ignore/omit until 0.0.2 (assume 0.0.1 for now)

**Select MC Version:**
- Empty for now, ignore/omit (assume 1.12.2 for now)

**Select Bug Category:**  
- [ ] Audio
- [ ] Graphical
- [ ] Lighting
- [ ] Controls
- [ ] Mechanics/Features (if doesn't fit elsewhere)
- [ ] Entities (non-mob)
- [ ] Mobs
- [ ] Items
- [ ] Blocks
- [ ] Tile Entities
- [ ] Inventory/Crafting
- [ ] World/Worldgen
- [ ] Player

**Bug Status:**
- [ ] Gamebreaking/Crashes/Softlocks/Progression Softlocks/etc
- [ ] Critical, while not gamebreaking or crash inducing, may majorly impact functionality, playability, etc
- [ ] Non-critical (no major negative or functional impact, just an ignorable quirk, visual/audio bug or otherwise anomalous behavior)

**Description (describe accurately and as best as you can):**  
////////Include some standard expected info, provide screenshots if needed and so examples of said standard info: were you able to reproduce the bug multiple times, is the bug an edge-case, does it break Minecraft (in a negative context), cause a freeze/hang-up, cause a softlock, does it cause a crash, etc.////////