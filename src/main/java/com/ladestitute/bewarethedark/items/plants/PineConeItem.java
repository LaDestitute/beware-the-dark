package com.ladestitute.bewarethedark.items.plants;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class PineConeItem extends Item {

    public PineConeItem(Properties properties)
    {
        super(properties.stacksTo(40));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("I can hear a tiny tree inside it, trying to get out."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}
