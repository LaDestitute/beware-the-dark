package com.ladestitute.bewarethedark.items.plants;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class BerryBushItem extends Item {

    public BerryBushItem(Item.Properties properties)
    {
        super(properties.stacksTo(10));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("I think those are the edible kind."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}

