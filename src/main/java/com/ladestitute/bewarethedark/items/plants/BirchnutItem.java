package com.ladestitute.bewarethedark.items.plants;

import com.ladestitute.bewarethedark.registries.ItemInit;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ambient.Bat;
import net.minecraft.world.entity.animal.*;
import net.minecraft.world.entity.animal.horse.Donkey;
import net.minecraft.world.entity.animal.horse.Horse;
import net.minecraft.world.entity.animal.horse.Llama;
import net.minecraft.world.entity.animal.horse.Mule;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class BirchnutItem extends Item {

    public BirchnutItem(Item.Properties properties)
    {
        super(properties.stacksTo(40));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("There's definitely something inside there."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResult interactLivingEntity(ItemStack stack, Player p_111207_2_, LivingEntity living, InteractionHand hand) {
        ItemStack manurestack = new ItemStack(ItemInit.MANURE.get());
        ItemEntity manure = new ItemEntity(living.level, living.position().x, living.position().y, living.position().z, manurestack);
        ItemStack guanostack = new ItemStack(ItemInit.GUANO.get());
        ItemEntity guano = new ItemEntity(living.level, living.position().x, living.position().y, living.position().z, guanostack);

        if(living instanceof Cow ||
                living instanceof Sheep ||
                living instanceof Pig ||
                living instanceof Horse ||
                living instanceof Donkey ||
                living instanceof Mule ||
                living instanceof MushroomCow ||
                living instanceof Panda ||
                living instanceof Llama)
        {
            stack.shrink(1);
            living.level.addFreshEntity(manure);
        }
        if(living instanceof Bat)
        {
            stack.shrink(1);
            living.level.addFreshEntity(guano);
        }
        return super.interactLivingEntity(stack, p_111207_2_, living, hand);
    }
}

