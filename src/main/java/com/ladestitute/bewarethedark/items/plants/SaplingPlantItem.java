package com.ladestitute.bewarethedark.items.plants;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class SaplingPlantItem extends Item {

    public SaplingPlantItem(Item.Properties properties)
    {
        super(properties.stacksTo(10));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Baby trees are so cute!"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}


