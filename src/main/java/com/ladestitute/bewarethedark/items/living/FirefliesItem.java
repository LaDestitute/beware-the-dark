package com.ladestitute.bewarethedark.items.living;

import com.ladestitute.bewarethedark.registries.ItemInit;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.UseAnim;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;

import java.util.List;

public class FirefliesItem extends Item {

    public FirefliesItem(Item.Properties properties) {
        super(properties.stacksTo(40));
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level p_41432_, Player p_41433_, InteractionHand p_41434_) {
        for (ItemStack cost1 : p_41433_.getInventory().items) {
            if (cost1.getItem() == ItemInit.MINER_HAT.get()) {
                cost1.setDamageValue(cost1.getDamageValue()-9240);
                break;
            }
        }
        p_41433_.getMainHandItem().shrink(1);

        return super.use(p_41432_, p_41433_, p_41434_);
    }

}