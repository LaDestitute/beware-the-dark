package com.ladestitute.bewarethedark.items.consumables;

import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.UseAnim;
import net.minecraft.world.level.Level;

import java.util.List;

public class HealingSalveItem extends Item {
    // A salve made from rocks, ashes and a spider gland; heals 1.5 hearts

    public HealingSalveItem(Item.Properties properties) {
        super(properties.stacksTo(20));
    }

    @Override
    public UseAnim getUseAnimation(ItemStack stack) {
        return UseAnim.EAT;
    }

    @Override
    public int getUseDuration(ItemStack stack)
    {
        return 10;
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Disinfectant for cuts and abrasions."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level p_77659_1_, Player player, InteractionHand p_77659_3_) {
        if(player.level.isClientSide)
        {
            int health = (int) (player.getHealth() + 6);
            if (health > 20) {
                player.setHealth(20);

            } else {
                player.setHealth(health);
            }
        }
        player.getMainHandItem().shrink(1);
        return super.use(p_77659_1_, player, p_77659_3_);
    }
}

