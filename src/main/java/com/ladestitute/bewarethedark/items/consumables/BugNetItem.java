package com.ladestitute.bewarethedark.items.consumables;

import com.ladestitute.bewarethedark.entities.mobs.passive.ButterflyEntity;
import com.ladestitute.bewarethedark.entities.mobs.passive.FireflyEntity;
import com.ladestitute.bewarethedark.registries.ItemInit;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.animal.Bee;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.List;

public class BugNetItem extends Item {

    public BugNetItem(Item.Properties properties) {
        super(properties.stacksTo(1).durability(10));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Catch bugs."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResult interactLivingEntity(ItemStack p_41398_, Player p_41399_, LivingEntity p_41400_, InteractionHand p_41401_) {
        if(p_41400_ instanceof FireflyEntity)
        {
            ItemHandlerHelper.giveItemToPlayer(p_41399_, ItemInit.FIREFLIES.get().getDefaultInstance());
            p_41398_.hurtAndBreak(1, p_41399_, (p_220045_0_) -> {
                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
            });
            p_41399_.level.setBlockAndUpdate(p_41400_.blockPosition(), Blocks.AIR.defaultBlockState());
            p_41400_.discard();
        }
        if(p_41400_ instanceof ButterflyEntity)
        {
            ItemHandlerHelper.giveItemToPlayer(p_41399_, ItemInit.BUTTERFLY.get().getDefaultInstance());
            p_41398_.hurtAndBreak(1, p_41399_, (p_220045_0_) -> {
                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
            });
            p_41400_.discard();
        }
        if(p_41400_ instanceof Bee)
        {
            ItemHandlerHelper.giveItemToPlayer(p_41399_, ItemInit.BEE.get().getDefaultInstance());
            p_41398_.hurtAndBreak(1, p_41399_, (p_220045_0_) -> {
                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
            });
            p_41400_.discard();
        }
        return super.interactLivingEntity(p_41398_, p_41399_, p_41400_, p_41401_);
    }
}
