package com.ladestitute.bewarethedark.items.utility;

import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class TorchItem extends Item {

    public TorchItem(Item.Properties properties)
    {
        super(properties.stacksTo(1).durability(3750));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("A portable light source."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public boolean shouldCauseReequipAnimation(ItemStack oldStack, ItemStack newStack, boolean slotChanged) {
        return false;
    }

    @Override
    public void inventoryTick(ItemStack stack, Level worldIn, Entity entityIn, int p_77663_4_, boolean isSelected) {
        Player entity = (Player) entityIn;
        ItemStack heldItem = entity.getMainHandItem();
        ItemStack heldItemoffhand = entity.getOffhandItem();
        if(heldItem.getItem() == this && worldIn.isRaining() && worldIn.canSeeSky(entityIn.blockPosition()))
        {
            entity.getMainHandItem().hurtAndBreak(2, entity, (p_220045_0_) -> {
                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
            });
        }
        if(heldItem.getItem() == this && !worldIn.isRaining())
        {
            entity.getMainHandItem().hurtAndBreak(1, entity, (p_220045_0_) -> {
                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
            });
        }
        if(heldItemoffhand.getItem() == this && worldIn.isRaining() && worldIn.canSeeSky(entityIn.blockPosition()))
        {
            entity.getOffhandItem().hurtAndBreak(2, entity, (p_220045_0_) -> {
                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
            });
        }
        if(heldItemoffhand.getItem() == this && !worldIn.isRaining())
        {
            entity.getOffhandItem().hurtAndBreak(1, entity, (p_220045_0_) -> {
                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
            });
        }

        super.inventoryTick(stack, worldIn, entityIn, p_77663_4_, isSelected);
    }
}
