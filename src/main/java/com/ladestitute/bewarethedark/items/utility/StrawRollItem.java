package com.ladestitute.bewarethedark.items.utility;

import com.ladestitute.bewarethedark.btdcapabilities.sanity.PlayerSanityProvider;
import com.ladestitute.bewarethedark.network.ClientboundPlayerSanityUpdateMessage;
import com.ladestitute.bewarethedark.network.NetworkingHandler;
import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Explosion;
import net.minecraft.world.level.ExplosionDamageCalculator;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.properties.BedPart;
import net.minecraftforge.fml.LogicalSide;

import java.util.List;

public class StrawRollItem extends Item {

    public StrawRollItem(Item.Properties properties) {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Sleep through the night."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level p_49516_, Player p_49518_, InteractionHand p_49519_) {
        if (!p_49516_.isClientSide && !p_49516_.isDay())
        {
            p_49518_.startSleeping(p_49518_.blockPosition().above());
            if(p_49516_ instanceof ServerLevel)
            {
                ((ServerLevel) p_49516_).setDayTime(23999);
            }
            p_49518_.getFoodData().setFoodLevel(p_49518_.getFoodData().getFoodLevel()-10);
            p_49518_.getMainHandItem().shrink(1);
            p_49518_.getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
                      sanity.addSanity(4);
                });

            }
        return super.use(p_49516_, p_49518_, p_49519_);
    }

}