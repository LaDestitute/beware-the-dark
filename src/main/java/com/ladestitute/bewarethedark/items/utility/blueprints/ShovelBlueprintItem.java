package com.ladestitute.bewarethedark.items.utility.blueprints;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.registries.SoundInit;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class ShovelBlueprintItem extends Item {
    // A blueprint for an item or block. Unlocks crafting recipe for said item on right-click and consumes the blueprint.

    public ShovelBlueprintItem(Item.Properties properties)
    {
        super(properties.stacksTo(1));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("It's scientific!"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level p_77659_1_, Player playerIn, InteractionHand handIn) {
        ResourceLocation[] recipe = new ResourceLocation[2];
        recipe[0] = new ResourceLocation(BTDMain.MOD_ID, "tools/portableshovel");
        recipe[1] = new ResourceLocation(BTDMain.MOD_ID, "tools/flint_shovel");
        playerIn.playSound(SoundInit.CRAFT_UNLOCK.get(), 0.5f, 1f);
        playerIn.awardRecipesByKey(recipe);
//        playerIn.getCapability(BTDCustomPlayerdataCapabilityProvider.BTDPLAYERDATA).ifPresent(h ->
//        {
//            h.unlockshovelrecipe(1);
//        });
        playerIn.getMainHandItem().shrink(1);

        return super.use(p_77659_1_, playerIn, handIn);
    }
}
