package com.ladestitute.bewarethedark.items.utility;

import com.ladestitute.bewarethedark.containers.BackpackContainer;
import com.ladestitute.bewarethedark.util.backpack.BackpackData;
import com.ladestitute.bewarethedark.util.backpack.BackpackManager;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.*;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.*;
import net.minecraft.world.level.Level;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.network.NetworkHooks;

import java.util.List;
import java.util.UUID;

public class BackpackItem extends ArmorItem {
    public BackpackItem(ArmorMaterial materialIn, EquipmentSlot slot, Properties builder) {
        super(materialIn, slot, builder);
    }

    public int getInventorySize(ItemStack stack) {
        return 24;
    }

    public IItemHandler getInventory(ItemStack stack) {
        if(stack.isEmpty())
            return null;
        ItemStackHandler stackHandler = new ItemStackHandler(getInventorySize(stack));
        stackHandler.deserializeNBT(stack.getOrCreateTag().getCompound("inventory"));
        return stackHandler;
    }

    public static BackpackData getData(ItemStack stack) {
        if (!(stack.getItem() instanceof BackpackItem))
            return null;
        UUID uuid;
        CompoundTag tag = stack.getOrCreateTag();
        if (!tag.contains("UUID")) {
            uuid = UUID.randomUUID();
            tag.putUUID("UUID", uuid);
        } else
            uuid = tag.getUUID("UUID");
        return BackpackManager.get().getOrCreateBackpack(uuid);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level worldIn, Player playerIn, InteractionHand handIn) {
        if (!worldIn.isClientSide) {
            ItemStack backpack = playerIn.getItemInHand(handIn);
            BackpackData data = BackpackItem.getData(backpack);
            NetworkHooks.openScreen(((ServerPlayer) playerIn), new SimpleMenuProvider( (windowId, playerInventory, playerEntity) ->
                    new BackpackContainer(windowId, playerInventory, data.getHandler()), backpack.getHoverName()));
        }
        return new InteractionResultHolder<>(InteractionResult.SUCCESS, playerIn.getItemInHand(handIn));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Carry more stuff."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}