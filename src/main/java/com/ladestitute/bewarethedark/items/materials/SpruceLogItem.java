package com.ladestitute.bewarethedark.items.materials;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.Level;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class SpruceLogItem extends Item {

    public SpruceLogItem(Item.Properties properties)
    {
        super(properties.stacksTo(20));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("It's big, it's heavy and it's wood"));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}
