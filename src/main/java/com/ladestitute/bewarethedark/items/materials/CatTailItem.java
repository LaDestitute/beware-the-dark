package com.ladestitute.bewarethedark.items.materials;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class CatTailItem extends Item {

    public CatTailItem(Item.Properties properties)
    {
        super(properties.stacksTo(40));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("I think these were its genitalia."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}

