package com.ladestitute.bewarethedark.items.tools;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class HammerItem extends Item {

    public HammerItem(Item.Properties properties)
    {
        super(properties.stacksTo(1).durability(75));
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Smashes all kinds of things."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}

