package com.ladestitute.bewarethedark.items.tools.flint;

import net.minecraft.network.chat.Component;
import net.minecraft.world.item.*;
import net.minecraft.world.level.Level;

import java.util.List;

public class FlintShovelItem extends ShovelItem {
    public FlintShovelItem(Tier tier, int attackDamageIn, float attackSpeedIn, Item.Properties builder) {
        super(tier, attackDamageIn, attackSpeedIn, builder);
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Dig up all sorts of things."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}
