package com.ladestitute.bewarethedark.items.dress;

import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class MinerHatItem extends ArmorItem {
    public MinerHatItem(ArmorMaterial materialIn, EquipmentSlot slot, Properties builder) {
        super(materialIn, slot, builder);
    }

    @Override
    public void onArmorTick(ItemStack stack, Level level, Player player) {
        stack.hurtAndBreak(1, (LivingEntity) player, (p_220045_0_) -> {
            p_220045_0_.broadcastBreakEvent(EquipmentSlot.HEAD);
        });
        super.onArmorTick(stack, level, player);
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Light up the night with your noggin."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }
}

