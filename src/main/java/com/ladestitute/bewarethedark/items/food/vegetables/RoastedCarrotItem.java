package com.ladestitute.bewarethedark.items.food.vegetables;

import com.ladestitute.bewarethedark.registries.ItemInit;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ambient.Bat;
import net.minecraft.world.entity.animal.*;
import net.minecraft.world.entity.animal.horse.Donkey;
import net.minecraft.world.entity.animal.horse.Horse;
import net.minecraft.world.entity.animal.horse.Llama;
import net.minecraft.world.entity.animal.horse.Mule;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.UseAnim;
import net.minecraft.world.level.Level;

import java.util.List;

public class RoastedCarrotItem extends Item {

    public RoastedCarrotItem(Item.Properties properties)
    {
        super(properties.stacksTo(40));
    }

    @Override
    public UseAnim getUseAnimation(ItemStack stack) {
        return UseAnim.EAT;
    }

    @Override
    public int getUseDuration(ItemStack stack)
    {
        return 10;
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Mushy."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public InteractionResultHolder<ItemStack> use(Level p_77659_1_, Player player, InteractionHand p_77659_3_) {
        if(player.level.isClientSide)
        {
            int health = (int) (player.getHealth() + 0);
            if (health > 20) {
                player.setHealth(20);

            } else {
                player.setHealth(health);
            }
        }
        return super.use(p_77659_1_, player, p_77659_3_);
    }

    @Override
    public InteractionResult interactLivingEntity(ItemStack stack, Player p_111207_2_, LivingEntity living, InteractionHand hand) {
        ItemStack manurestack = new ItemStack(ItemInit.MANURE.get());
        ItemEntity manure = new ItemEntity(living.level, living.position().x, living.position().y, living.position().z, manurestack);
        ItemStack guanostack = new ItemStack(ItemInit.GUANO.get());
        ItemEntity guano = new ItemEntity(living.level, living.position().x, living.position().y, living.position().z, guanostack);

        if(living instanceof Cow ||
                living instanceof Sheep ||
                living instanceof Pig ||
                living instanceof Horse ||
                living instanceof Donkey ||
                living instanceof Mule ||
                living instanceof MushroomCow ||
                living instanceof Panda ||
                living instanceof Llama)
        {
            stack.shrink(1);
            living.level.addFreshEntity(manure);
        }
        if(living instanceof Bat)
        {
            stack.shrink(1);
            living.level.addFreshEntity(guano);
        }
        return super.interactLivingEntity(stack, p_111207_2_, living, hand);
    }
}

