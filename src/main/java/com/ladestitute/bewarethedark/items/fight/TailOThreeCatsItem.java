package com.ladestitute.bewarethedark.items.fight;

import com.google.common.collect.HashMultimap;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.ladestitute.bewarethedark.entities.mobs.neutral.CatcoonEntity;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.animal.*;
import net.minecraft.world.entity.animal.goat.Goat;
import net.minecraft.world.entity.animal.horse.Llama;
import net.minecraft.world.entity.animal.horse.TraderLlama;
import net.minecraft.world.entity.monster.*;
import net.minecraft.world.entity.monster.piglin.Piglin;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.common.ForgeMod;

import java.util.List;
import java.util.Random;
import java.util.UUID;

public class TailOThreeCatsItem extends SwordItem {
    private static final UUID ATTACK_REACH = UUID.fromString("a4c45206-3f2f-4588-910e-af679c3312f6");
    private final Multimap<Attribute, AttributeModifier> defaultModifiers;

    public TailOThreeCatsItem(Tier tier, int attackDamageIn, float attackSpeedIn, Properties builder) {
        super(tier, attackDamageIn, attackSpeedIn, builder);
        ImmutableMultimap.Builder<Attribute, AttributeModifier> extra = ImmutableMultimap.builder();
        extra.put(ForgeMod.REACH_DISTANCE.get(), new AttributeModifier(ATTACK_REACH, "Attack reach", 2f, AttributeModifier.Operation.ADDITION));
        this.defaultModifiers = extra.build();
    }

    public Multimap<Attribute, AttributeModifier> getDefaultAttributeModifiers(EquipmentSlot p_43274_) {
        return p_43274_ == EquipmentSlot.MAINHAND ? this.defaultModifiers : super.getDefaultAttributeModifiers(p_43274_);
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Dish out some constructive feedback."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public boolean hurtEnemy(ItemStack p_77644_1_, LivingEntity p_77644_2_, LivingEntity p_77644_3_) {
        p_77644_1_.hurtAndBreak(1, p_77644_3_, (p_220045_0_) -> {
            p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
        });

        double radius = 3;
        AABB axisalignedbb = new AABB(p_77644_2_.blockPosition()).inflate(radius).expandTowards(0.0D, p_77644_2_.level.getMaxBuildHeight(), 0.0D);
        List<LivingEntity> list = p_77644_2_.level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
        for (LivingEntity targets : list) {
                if (targets instanceof Bee) {
                    ((Bee) targets).stopBeingAngry();
                }
                if (targets instanceof Wolf) {
                    ((Wolf) targets).stopBeingAngry();
                }
                if (targets instanceof IronGolem) {
                    ((IronGolem) targets).stopBeingAngry();
                }
                if (targets instanceof PolarBear) {
                    ((PolarBear) targets).stopBeingAngry();
                }
                if (targets instanceof EnderMan) {
                    ((EnderMan) targets).stopBeingAngry();
                }
                if (targets instanceof ZombifiedPiglin) {
                    ((ZombifiedPiglin) targets).stopBeingAngry();
                }
        }



        return super.hurtEnemy(p_77644_1_, p_77644_2_, p_77644_3_);
    }
}


