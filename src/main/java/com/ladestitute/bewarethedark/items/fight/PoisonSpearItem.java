package com.ladestitute.bewarethedark.items.fight;

import com.ladestitute.bewarethedark.util.config.BTDConfig;
import net.minecraft.network.chat.Component;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class PoisonSpearItem extends SwordItem {

    public PoisonSpearItem(Tier tier, int attackDamageIn, float attackSpeedIn, Properties builder) {
        super(tier, attackDamageIn, attackSpeedIn, builder);
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Jab'em with a sick stick."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public boolean hurtEnemy(ItemStack p_77644_1_, LivingEntity p_77644_2_, LivingEntity p_77644_3_) {
        p_77644_1_.hurtAndBreak(1, p_77644_3_, (p_220045_0_) -> {
            p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
        });
        p_77644_2_.addEffect(new MobEffectInstance(MobEffects.POISON, 40, 2));
        if(BTDConfig.getInstance().dont_starve_style_poison()) {
            p_77644_2_.addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, 40, 0));
            p_77644_2_.addEffect(new MobEffectInstance(MobEffects.WEAKNESS, 40, 0));
        }
        return super.hurtEnemy(p_77644_1_, p_77644_2_, p_77644_3_);
    }
}

