package com.ladestitute.bewarethedark.items.fight;

import com.ladestitute.bewarethedark.entities.projectile.ElectricDartEntity;
import com.ladestitute.bewarethedark.entities.projectile.FireDartEntity;
import com.ladestitute.bewarethedark.registries.ItemInit;
import net.minecraft.network.chat.Component;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResultHolder;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

import java.util.List;

public class ElectricDartItem extends Item {

    public ElectricDartItem(Item.Properties properties) {
        super(properties.stacksTo(20));
    }

    public InteractionResultHolder<ItemStack> use(Level p_41432_, Player player, InteractionHand p_41434_) {
        player.getCooldowns().addCooldown(this, 10);
        if(!player.level.isClientSide) {
            //Create a new instance of our entity
            ElectricDartEntity dart = new ElectricDartEntity(player, player.level);
            //This sets the entity to the item we are holding
            ItemStack dartstack = new ItemStack(ItemInit.PROJ_DART.get());
            dart.setItem(dartstack);
            //This is the second most important method here, it sets how the entity shall shoot.
            dart.shootFromRotation(player, player.getXRot(), player.getYRot(), 0.0F, 1.5F, 1F);
            //This is the most important, it actually adds it to the world!
            player.level.addFreshEntity(dart);
        }
        if(!player.isCreative())
        {
            player.getMainHandItem().shrink(1);
        }
        return super.use(p_41432_, player, p_41434_);
    }

    @Override
    public void appendHoverText(ItemStack stack, Level worldIn, List<Component> tooltip, TooltipFlag flagIn) {
        tooltip.add(Component.literal("Spit lightning at your enemies."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

}

