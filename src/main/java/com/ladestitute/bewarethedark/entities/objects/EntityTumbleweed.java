package com.ladestitute.bewarethedark.entities.objects;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.registries.ItemInit;
import com.ladestitute.bewarethedark.registries.SoundInit;
import com.mojang.math.Quaternion;
import it.unimi.dsi.fastutil.ints.Int2ObjectMap;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Registry;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.ListTag;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ChunkMap;
import net.minecraft.server.level.ServerEntity;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.TagKey;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.*;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.vehicle.AbstractMinecart;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.entity.IEntityAdditionalSpawnData;
import net.minecraftforge.network.NetworkHooks;
import net.minecraftforge.registries.ForgeRegistries;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

@SuppressWarnings("EntityConstructor")
public class EntityTumbleweed extends Entity {

    public static final int FADE_TIME = 4 * 20;
    private static final int DESPAWN_RANGE = 110;
    private static final float BASE_SIZE = 3/4f;
    private static final double WIND_X = -1/16f;
    private static final double WIND_Z = -1/16f;

    private static final EntityDataAccessor<Integer> SIZE = SynchedEntityData.defineId(EntityTumbleweed.class, EntityDataSerializers.INT);
    private static final EntityDataAccessor<Boolean> CUSTOM_WIND_ENABLED = SynchedEntityData.defineId(EntityTumbleweed.class, EntityDataSerializers.BOOLEAN);
    private static final EntityDataAccessor<Float> CUSTOM_WIND_X = SynchedEntityData.defineId(EntityTumbleweed.class, EntityDataSerializers.FLOAT);
    private static final EntityDataAccessor<Float> CUSTOM_WIND_Z = SynchedEntityData.defineId(EntityTumbleweed.class, EntityDataSerializers.FLOAT);
    private static final EntityDataAccessor<Boolean> FADING = SynchedEntityData.defineId(EntityTumbleweed.class, EntityDataSerializers.BOOLEAN);

    private int age;
    public int fadeProgress;
    public boolean persistent;
    private double windMod;
    private int lifetime;
    private float angularX, angularZ;
    public float stretch = 1f, prevStretch = 1f;
    private boolean prevOnGround;
    private Vec3 prevMotion = Vec3.ZERO;

    @OnlyIn(Dist.CLIENT)
    public float rot1, rot2, rot3;
    @OnlyIn(Dist.CLIENT)
    public Quaternion quat;
    @OnlyIn(Dist.CLIENT)
    public Quaternion prevQuat;

    public EntityTumbleweed(EntityType<?> type, Level world) {
        super(type, world);

        this.blocksBuilding = true;

        setId(getId());

        if (this.level.isClientSide) {
            initClient();
        }
    }

    @OnlyIn(Dist.CLIENT)
    private void initClient() {
        this.rot1 = 360f * level.random.nextFloat();
        this.rot2 = 360f * level.random.nextFloat();
        this.rot3 = 360f * level.random.nextFloat();

        this.quat = new Quaternion(0, 0, 0, 1);
        this.prevQuat = new Quaternion(0, 0, 0, 1);
    }

    @Override
    protected void defineSynchedData() {
        this.entityData.define(SIZE, 2);
        this.entityData.define(CUSTOM_WIND_ENABLED, false);
        this.entityData.define(CUSTOM_WIND_X, 0f);
        this.entityData.define(CUSTOM_WIND_Z, 0f);
        this.entityData.define(FADING, false);

        refreshDimensions();
    }

    @Override
    protected void addAdditionalSaveData(CompoundTag nbt) {
        nbt.putInt("Size", getSize());
        nbt.putBoolean("CustomWindEnabled", getCustomWindEnabled());
        nbt.putDouble("CustomWindX", getCustomWindX());
        nbt.putDouble("CustomWindZ", getCustomWindZ());
        nbt.putBoolean("Persistent", persistent);

        AABB bb = this.getBoundingBox();
        nbt.put("AABB", this.newDoubleList(bb.minX, bb.minY, bb.minZ, bb.maxX, bb.maxY, bb.maxZ));
    }

    @Override
    protected void readAdditionalSaveData(CompoundTag nbt) {
        if (nbt.contains("Size"))
            this.entityData.set(SIZE, nbt.getInt("Size"));

        this.entityData.set(CUSTOM_WIND_ENABLED, nbt.getBoolean("CustomWindEnabled"));
        this.entityData.set(CUSTOM_WIND_X, nbt.getFloat("CustomWindX"));
        this.entityData.set(CUSTOM_WIND_Z, nbt.getFloat("CustomWindZ"));

        persistent = nbt.getBoolean("Persistent");

        // Fixes server-side collision glitches
        if (nbt.contains("AABB")) {
            ListTag aabb = nbt.getList("AABB", 6);
            setBoundingBox(new AABB(aabb.getDouble(0), aabb.getDouble(1), aabb.getDouble(2), aabb.getDouble(3), aabb.getDouble(4), aabb.getDouble(5)));
        }
    }

    @Override
    public void onSyncedDataUpdated(EntityDataAccessor<?> key) {
        super.onSyncedDataUpdated(key);

        if (key.equals(SIZE))
            refreshDimensions();
    }

    @Override
    public Packet<?> getAddEntityPacket() {
        return NetworkHooks.getEntitySpawningPacket(this);
    }

    @Override
    public EntityDimensions getDimensions(Pose pose) {
        float mcSize = BASE_SIZE + this.getSize() * (1 / 8f);

        // Fixes client-side collision glitches
        if (level.isClientSide)
            mcSize -= 1/2048f;

        return EntityDimensions.scalable(mcSize, mcSize);
    }

    @Override
    public boolean isPickable() {
        return true;
    }

    @Override
    public boolean isPushable() {
        return true;
    }

    @Override
    public void setId(int id) {
        super.setId(id);

        Random rand = new Random(id);

        this.windMod = 1.05 - 0.1 * rand.nextDouble();
        this.lifetime = 2 * 60 * 20 + rand.nextInt(200);
    }

    @Override
    public void tick() {
        super.tick();

        // Fixes some cases of rubber banding
        if (!level.isClientSide && tickCount == 1)
            trackerHack();

        if (this.level.isClientSide) {
            preTickClient();
        }

        if (this.getVehicle() != null) {
            this.setDeltaMovement(Vec3.ZERO);
            return;
        }

        if(this.tickCount >= 6000)
        {
            this.discard();
        }

        if (!this.isInWater())
            this.setDeltaMovement(getDeltaMovement().subtract(0, 0.012, 0));

        prevMotion = this.getDeltaMovement();
        prevOnGround = onGround;

        this.move(MoverType.SELF, getDeltaMovement());

        double windX = getCustomWindEnabled() ? getCustomWindX() : WIND_X * windMod;
        double windZ = getCustomWindEnabled() ? getCustomWindZ() : WIND_Z * windMod;

        if (this.isInWater()) {
            this.setDeltaMovement(getDeltaMovement().multiply(0.95, 1, 0.95));
            this.setDeltaMovement(getDeltaMovement().add(0, 0.02, 0));
            windX = windZ = 0;
        } else if (windX != 0 || windZ != 0) {
            this.setDeltaMovement(windX, getDeltaMovement().y, windZ);
        }

        // Rotate
        if (this.level.isClientSide) {
            tickClient();
        }

        // Bounce on ground
        if (this.onGround) {
            if (windX * windX + windZ * windZ >= 0.05 * 0.05) {
                this.setDeltaMovement(getDeltaMovement().x, Math.max(-prevMotion.y * 0.7, 0.24 - getSize() * 0.02), getDeltaMovement().z);
            } else {
                this.setDeltaMovement(getDeltaMovement().x, -prevMotion.y * 0.7, getDeltaMovement().z);
            }
        }

        // Friction
        this.setDeltaMovement(getDeltaMovement().multiply(0.98, 0.98, 0.98));

        collideWithNearbyEntities();

        if (!this.level.isClientSide) {
            // Age faster when stuck on a wall or in water
            this.age += (horizontalCollision || isInWater()) ? 8 : 1;
            tryDespawn();
        }

        if (isFading()) {
            this.fadeProgress++;

            if (this.fadeProgress > FADE_TIME) {
                discard();
            }
        }
    }

    @OnlyIn(Dist.CLIENT)
    private void preTickClient() {
        prevStretch = stretch;
        stretch *= 1.2f;
        if (stretch > 1f) stretch = 1f;

        this.prevQuat = new Quaternion(this.quat);
    }

    @OnlyIn(Dist.CLIENT)
    private void tickClient() {
        if (prevOnGround != onGround)
            stretch *= 0.75f;

        float motionAngleX = (float)-prevMotion.x / (getBbWidth() * 0.5f);
        float motionAngleZ = (float)prevMotion.z / (getBbWidth() * 0.5f);

        if (onGround) {
            angularX = motionAngleX;
            angularZ = motionAngleZ;
        }

        if (isInWater()) {
            angularX += motionAngleX * 0.2f;
            angularZ += motionAngleZ * 0.2f;
        }

        float resistance = isInWater() ? 0.9f : 0.96f;
        angularX *= resistance;
        angularZ *= resistance;

        Quaternion temp = new Quaternion(angularZ, 0, angularX, false);
        temp.mul(quat);
        quat = temp;
    }

    private void trackerHack() {
        ServerEntity entry = getTrackerEntry();
        try {
            int counter = (int)updateCounter.get(entry);
            if (entry != null && counter == 0)
                updateCounter.set(entry, counter + 30);
        } catch (IllegalAccessException e) {
        }
    }

    private void tryDespawn() {
        if (shouldPersist()) {
            this.age = 0;
            return;
        }

        Player player = this.level.getNearestPlayer(this, -1);
        if (player != null && player.distanceToSqr(this) > DESPAWN_RANGE * DESPAWN_RANGE)
            this.discard();

        if (this.age > this.lifetime && fadeProgress == 0)
            this.entityData.set(FADING, true);
    }

    @Override
    public boolean shouldRenderAtSqrDistance(double distance) {
        return distance < 128 * 128;
    }

    @Override
    public boolean hurt(DamageSource source, float amount) {
        if (this.isInvulnerableTo(source)) {
            return false;
        }

        if (this.isAlive() && !this.level.isClientSide) {
            this.discard();

            SoundType sound = SoundType.GRASS;
            this.playSound(sound.getBreakSound(), (sound.getVolume() + 1.0F) / 2.0F, sound.getPitch() * 0.8F);

            if (source.getEntity() instanceof Player)
                dropItem();
        }

        return true;
    }



    private void dropItem() {
        Random rand = new Random();
        ItemStack grass1 = ItemInit.CUT_GRASS.get().getDefaultInstance();
        ItemStack grass2 = ItemInit.CUT_GRASS.get().getDefaultInstance();
        ItemStack grass3 = ItemInit.CUT_GRASS.get().getDefaultInstance();
        ItemStack twigs1 = ItemInit.TWIGS.get().getDefaultInstance();
        ItemStack twigs2 = ItemInit.TWIGS.get().getDefaultInstance();
        ItemStack twigs3 = ItemInit.TWIGS.get().getDefaultInstance();
        TagKey<Item> tumbleweed_loot_table =
                TagKey.create(Registry.ITEM_REGISTRY, new ResourceLocation(BTDMain.MOD_ID, "tumbleweed_loot_table"));
        int item1rolls = rand.nextInt(100);
        int item2rolls = rand.nextInt(100);
        int item3rolls = rand.nextInt(100);
        int grassortwigs1 = rand.nextInt(2);
        int grassortwigs2 = rand.nextInt(2);
        int grassortwigs3 = rand.nextInt(2);
        if(item1rolls <= 76)
        {
            if(grassortwigs1 == 0)
            {
                ItemEntity itemEntity = new ItemEntity(this.level, this.getX(), this.getY(), this.getZ(), grass1);
                itemEntity.setDeltaMovement(new Vec3(0, 0.2, 0));
                itemEntity.setDefaultPickUpDelay();
                this.level.addFreshEntity(itemEntity);
            }
            if(grassortwigs1 == 1)
            {
                ItemEntity itemEntity = new ItemEntity(this.level, this.getX(), this.getY(), this.getZ(), twigs1);
                itemEntity.setDeltaMovement(new Vec3(0, 0.2, 0));
                itemEntity.setDefaultPickUpDelay();
                this.level.addFreshEntity(itemEntity);
            }
        }
        if(item1rolls > 76)
        {
            ItemStack itemstack = ForgeRegistries.ITEMS.tags().getTag(tumbleweed_loot_table).getRandomElement(this.level.getRandom()).get().getDefaultInstance();
            ItemEntity itemEntity = new ItemEntity(this.level, this.getX(), this.getY(), this.getZ(), itemstack);
            itemEntity.setDeltaMovement(new Vec3(0, 0.2, 0));
            itemEntity.setDefaultPickUpDelay();
            this.level.addFreshEntity(itemEntity);
        }
        if(item2rolls <= 76)
        {
            if(grassortwigs2 == 0)
            {
                ItemEntity itemEntity = new ItemEntity(this.level, this.getX(), this.getY(), this.getZ(), grass2);
                itemEntity.setDeltaMovement(new Vec3(0, 0.2, 0));
                itemEntity.setDefaultPickUpDelay();
                this.level.addFreshEntity(itemEntity);
            }
            if(grassortwigs2 == 1)
            {
                ItemEntity itemEntity = new ItemEntity(this.level, this.getX(), this.getY(), this.getZ(), twigs2);
                itemEntity.setDeltaMovement(new Vec3(0, 0.2, 0));
                itemEntity.setDefaultPickUpDelay();
                this.level.addFreshEntity(itemEntity);
            }
        }
        if(item2rolls > 76)
        {
            ItemStack itemstack = ForgeRegistries.ITEMS.tags().getTag(tumbleweed_loot_table).getRandomElement(this.level.getRandom()).get().getDefaultInstance();
            ItemEntity itemEntity = new ItemEntity(this.level, this.getX(), this.getY(), this.getZ(), itemstack);
            itemEntity.setDeltaMovement(new Vec3(0, 0.2, 0));
            itemEntity.setDefaultPickUpDelay();
            this.level.addFreshEntity(itemEntity);
        }
        if(item3rolls <= 76)
        {
            if(grassortwigs3 == 0)
            {
                ItemEntity itemEntity = new ItemEntity(this.level, this.getX(), this.getY(), this.getZ(), grass3);
                itemEntity.setDeltaMovement(new Vec3(0, 0.2, 0));
                itemEntity.setDefaultPickUpDelay();
                this.level.addFreshEntity(itemEntity);
            }
            if(grassortwigs3 == 1)
            {
                ItemEntity itemEntity = new ItemEntity(this.level, this.getX(), this.getY(), this.getZ(), twigs3);
                itemEntity.setDeltaMovement(new Vec3(0, 0.2, 0));
                itemEntity.setDefaultPickUpDelay();
                this.level.addFreshEntity(itemEntity);
            }
        }
        if(item3rolls > 76)
        {
            ItemStack itemstack = ForgeRegistries.ITEMS.tags().getTag(tumbleweed_loot_table).getRandomElement(this.level.getRandom()).get().getDefaultInstance();
            ItemEntity itemEntity = new ItemEntity(this.level, this.getX(), this.getY(), this.getZ(), itemstack);
            itemEntity.setDeltaMovement(new Vec3(0, 0.2, 0));
            itemEntity.setDefaultPickUpDelay();
            this.level.addFreshEntity(itemEntity);
        }
    }

    @Override
    public boolean skipAttackInteraction(Entity entityIn) {
        return entityIn instanceof Player && this.hurt(DamageSource.playerAttack((Player) entityIn), 0.0F);
    }

    @Override
    protected void playStepSound(BlockPos pos, BlockState blockIn) {
        this.playSound(SoundInit.TUMBLEWEED_BOUNCE.get(), 0.25f,1.0f);
    }

    @Override
    protected boolean repositionEntityAfterLoad() {
        return false;
    }

    private void collideWithNearbyEntities() {
        List<Entity> list = this.level.getEntities(this, this.getBoundingBox().expandTowards(0.2D, 0.0D, 0.2D), Entity::isPushable);

        for (Entity entity : list) {
            if (!this.level.isClientSide && entity instanceof AbstractMinecart && ((AbstractMinecart) entity).getMinecartType() == AbstractMinecart.Type.RIDEABLE && entity.getDeltaMovement().x * entity.getDeltaMovement().x + entity.getDeltaMovement().z * entity.getDeltaMovement().z > 0.01D && entity.getPassengers().isEmpty() && this.getVehicle() == null) {
                this.startRiding(entity);
                this.setDeltaMovement(getDeltaMovement().add(0, 0.25, 0));
                this.hurtMarked = true;
            }

            entity.push(this);
        }
    }

    public void setSize(int size) {
        this.entityData.set(SIZE, size);
    }

    public int getSize() {
        return this.entityData.get(SIZE);
    }

    public double getCustomWindX() {
        return this.entityData.get(CUSTOM_WIND_X);
    }

    public double getCustomWindZ() {
        return this.entityData.get(CUSTOM_WIND_Z);
    }

    public boolean getCustomWindEnabled() {
        return this.entityData.get(CUSTOM_WIND_ENABLED);
    }

    public boolean isFading() {
        return this.entityData.get(FADING);
    }

    public boolean shouldPersist() {
        return persistent || getVehicle() != null;
    }

    private static Field trackedEntityHashTable;
    private static Field entryFieldLazy;
    private static Field updateCounter;

    static {
        trackedEntityHashTable = fieldsOfType(ChunkMap.class, Int2ObjectMap.class)[0];
        trackedEntityHashTable.setAccessible(true);

        // Field found by index, recheck on game updates
        updateCounter = fieldsOfType(ServerEntity.class, int.class)[4];
        updateCounter.setAccessible(true);
    }

    private static Field[] fieldsOfType(Class inClass, Class type){
        return Arrays.stream(inClass.getDeclaredFields()).filter(f -> f.getType() == type).toArray(Field[]::new);
    }

    public ServerEntity getTrackerEntry() {
        ServerEntity entry = null;
        try {
            Object e = ((Int2ObjectMap<?>) trackedEntityHashTable.get(((ServerLevel) level).getChunkSource().chunkMap)).get(getId());
            if (entryFieldLazy == null) {
                entryFieldLazy = fieldsOfType(e.getClass(), ServerEntity.class)[0];
                entryFieldLazy.setAccessible(true);
            }
            entry = (ServerEntity)entryFieldLazy.get(e);
        } catch(IllegalAccessException e){
        }
        return entry;
    }

}