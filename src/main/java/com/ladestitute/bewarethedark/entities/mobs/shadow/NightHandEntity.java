package com.ladestitute.bewarethedark.entities.mobs.shadow;

import com.ladestitute.bewarethedark.blocks.entity.CampFireTileEntity;
import com.ladestitute.bewarethedark.blocks.entity.FirePitTileEntity;
import com.ladestitute.bewarethedark.blocks.utility.BTDCampfireBlock;
import com.ladestitute.bewarethedark.blocks.utility.FirePitBlock;
import com.ladestitute.bewarethedark.client.ClientSanityData;
import com.ladestitute.bewarethedark.entities.mobs.neutral.CatcoonEntity;
import com.ladestitute.bewarethedark.registries.SoundInit;
import net.minecraft.core.BlockPos;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.level.BlockAndTintGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import org.jetbrains.annotations.Nullable;

public class NightHandEntity extends Animal {

    private static final EntityDataAccessor<Integer> FIRES_SNUFFED = SynchedEntityData.defineId(NightHandEntity.class,
            EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> COOLDOWN = SynchedEntityData.defineId(NightHandEntity.class,
            EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> PREV_X = SynchedEntityData.defineId(NightHandEntity.class,
            EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> PREV_Y = SynchedEntityData.defineId(NightHandEntity.class,
            EntityDataSerializers.INT);
    private static final EntityDataAccessor<Integer> PREV_Z = SynchedEntityData.defineId(NightHandEntity.class,
            EntityDataSerializers.INT);

    public NightHandEntity(EntityType<? extends Animal> p_27717_, Level p_27718_) {
        super(p_27717_, p_27718_);
    }

    protected void defineSynchedData() {
        super.defineSynchedData();
        this.entityData.define(FIRES_SNUFFED, 0);
        this.entityData.define(COOLDOWN, 0);
        this.entityData.define(PREV_X, 0);
        this.entityData.define(PREV_Y, 0);
        this.entityData.define(PREV_Z, 0);
    }

    private int cooldown;

    @Override
    public void tick() {
        BlockPos blockPos = this.blockPosition();
        int torchsearchRadius = 5;
        int radius = (int) (Math.floor(2.0) + torchsearchRadius);
        if(this.entityData.get(COOLDOWN) < 0)
        {
            this.entityData.set(COOLDOWN, 0);
        }
        if(this.entityData.get(COOLDOWN) <= 3600 && this.entityData.get(COOLDOWN) > 0)
        {
            cooldown--;
            this.entityData.set(COOLDOWN, cooldown);
        }
        for (int x = -radius; x <= radius; ++x) {
            for (int y = -1; y <= 1; ++y) {
                for (int z = -radius; z <= radius; ++z) {
                    BlockPos shrinePos = new BlockPos(blockPos.getX() + x, blockPos.getY() + y, blockPos.getZ() + z);
                    BlockState blockState = this.level.getBlockState(shrinePos);
                    Block block = blockState.getBlock();
                    if (this.entityData.get(COOLDOWN) == 0 && block instanceof FirePitBlock && this.entityData.get(FIRES_SNUFFED) < 3)
                    {
                        this.entityData.set(PREV_X, Math.round(blockPos.getX()));
                        this.entityData.set(PREV_Y, Math.round(blockPos.getY()));
                        this.entityData.set(PREV_Z, Math.round(blockPos.getZ()));
                        this.absMoveTo(shrinePos.getX(), shrinePos.getY(), shrinePos.getZ(), (float)this.lerpYRot, (float)this.lerpXRot);
                        BlockEntity tileentity = this.level.getBlockEntity(shrinePos);
                        ((FirePitTileEntity)tileentity).fuel = Math.round(((FirePitTileEntity) tileentity).fuel/2);
                        this.entityData.set(FIRES_SNUFFED, this.entityData.get(FIRES_SNUFFED)+1);
                        this.teleportTo(this.entityData.get(PREV_X), this.entityData.get(PREV_Y), this.entityData.get(PREV_Z));
                        cooldown=3600;
                        this.entityData.set(COOLDOWN, cooldown);
                    }
                    else if (this.entityData.get(COOLDOWN) == 0 && block instanceof BTDCampfireBlock && this.entityData.get(FIRES_SNUFFED) < 3)
                    {
                        this.entityData.set(PREV_X, Math.round(blockPos.getX()));
                        this.entityData.set(PREV_Y, Math.round(blockPos.getY()));
                        this.entityData.set(PREV_Z, Math.round(blockPos.getZ()));
                        this.absMoveTo(shrinePos.getX(), shrinePos.getY(), shrinePos.getZ(), (float)this.lerpYRot, (float)this.lerpXRot);
                        BlockEntity tileentity = this.level.getBlockEntity(shrinePos);
                        ((CampFireTileEntity)tileentity).fuel = Math.round(((CampFireTileEntity) tileentity).fuel/2);
                        this.entityData.set(FIRES_SNUFFED, this.entityData.get(FIRES_SNUFFED)+1);
                        this.teleportTo(this.entityData.get(PREV_X), this.entityData.get(PREV_Y), this.entityData.get(PREV_Z));
                        cooldown=3600;
                        this.entityData.set(COOLDOWN, cooldown);
                    }
                }

            }
        }

        if (ClientSanityData.getPlayerSanity() >= 15) {
            discard();
        }
        super.tick();
    }

    @Nullable
    @Override
    public AgeableMob getBreedOffspring(ServerLevel p_146743_, AgeableMob p_146744_) {
        return null;
    }

    public static AttributeSupplier.Builder createAttributes() {
        return Mob.createMobAttributes().add(Attributes.MAX_HEALTH, 15D);
    }

    public static boolean checkSpawnRules(EntityType<NightHandEntity> entityType, LevelAccessor level, MobSpawnType spawnType, BlockPos pos, RandomSource random) {
        return checkAnimalSpawnRules(entityType, level, spawnType, pos, random) && getSanityLevel(level, pos);
    }

    protected static boolean getSanityLevel(BlockAndTintGetter p_186210_, BlockPos p_186211_) {
        return ClientSanityData.getPlayerSanity() <= 14 && p_186210_.getRawBrightness(p_186211_, 0) <= 7;
    }

    @Override
    public boolean causeFallDamage(float p_147187_, float p_147188_, DamageSource p_147189_) {
        return false;
    }
}
