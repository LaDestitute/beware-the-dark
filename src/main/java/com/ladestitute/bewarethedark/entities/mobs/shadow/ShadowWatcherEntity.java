package com.ladestitute.bewarethedark.entities.mobs.shadow;

import com.ladestitute.bewarethedark.client.ClientSanityData;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.level.BlockAndTintGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import org.jetbrains.annotations.Nullable;

public class ShadowWatcherEntity extends Animal {

    public ShadowWatcherEntity(EntityType<? extends Animal> p_27717_, Level p_27718_) {
        super(p_27717_, p_27718_);
        this.isPushable();
    }

    @Override
    public void aiStep() {
        if(!this.level.isNight())
        {
            this.kill();
        }

        super.aiStep();
    }

    @Override
    public void tick() {
        if (ClientSanityData.getPlayerSanity() >= 13) {
            discard();
        }
        super.tick();
    }

    @Override
    public boolean isPushable() {
        return false;
    }

    @Nullable
    @Override
    public AgeableMob getBreedOffspring(ServerLevel p_146743_, AgeableMob p_146744_) {
        return null;
    }

    public static AttributeSupplier.Builder createAttributes() {
        return Mob.createMobAttributes().add(Attributes.MAX_HEALTH, 5D);
    }

    public static boolean checkSpawnRules(EntityType<ShadowWatcherEntity> entityType, LevelAccessor level, MobSpawnType spawnType, BlockPos pos, RandomSource random) {
        return checkAnimalSpawnRules(entityType, level, spawnType, pos, random) && getSanityLevel(level, pos);
    }

    protected static boolean getSanityLevel(BlockAndTintGetter p_186210_, BlockPos p_186211_) {
        return ClientSanityData.getPlayerSanity() <= 12 && p_186210_.getRawBrightness(p_186211_, 0) <= 7;
    }

    @Override
    public boolean causeFallDamage(float p_147187_, float p_147188_, DamageSource p_147189_) {
        return false;
    }
}

