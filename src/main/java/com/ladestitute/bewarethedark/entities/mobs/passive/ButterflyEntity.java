package com.ladestitute.bewarethedark.entities.mobs.passive;

import com.ladestitute.bewarethedark.registries.EntityInit;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.*;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.control.FlyingMoveControl;
import net.minecraft.world.entity.ai.control.LookControl;
import net.minecraft.world.entity.ai.goal.AvoidEntityGoal;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.navigation.FlyingPathNavigation;
import net.minecraft.world.entity.ai.navigation.PathNavigation;
import net.minecraft.world.entity.ai.util.AirAndWaterRandomPos;
import net.minecraft.world.entity.ai.util.AirRandomPos;
import net.minecraft.world.entity.ai.util.HoverRandomPos;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.animal.FlyingAnimal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockAndTintGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.DoublePlantBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.phys.Vec3;

import javax.annotation.Nullable;
import java.util.EnumSet;
import java.util.Optional;
import java.util.function.Predicate;

@SuppressWarnings("EntityConstructor")
public class ButterflyEntity extends Animal implements FlyingAnimal {
    public static final int TICKS_PER_FLAP = Mth.ceil(1.4959966F);
    @Nullable
    BlockPos savedFlowerPos;
    int remainingCooldownBeforeLocatingNewFlower = Mth.nextInt(this.random, 20, 60);
    ButterflyEntity.ButterflyPollinateGoal butterflyPollinateGoal;
    private ButterflyEntity.ButterflyGoToKnownFlowerGoal goToKnownFlowerGoal;
    private int underWaterTicks;

    public ButterflyEntity(EntityType<? extends Animal> p_27717_, Level p_27718_) {
        super(p_27717_, p_27718_);
        this.moveControl = new FlyingMoveControl(this, 20, true);
        this.lookControl = new LookControl(this);
        this.setPathfindingMalus(BlockPathTypes.DANGER_FIRE, -1.0F);
        this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);
        this.setPathfindingMalus(BlockPathTypes.WATER_BORDER, 16.0F);
        this.setPathfindingMalus(BlockPathTypes.COCOA, -1.0F);
        this.setPathfindingMalus(BlockPathTypes.FENCE, -1.0F);
    }

    public float getWalkTargetValue(BlockPos p_27788_, LevelReader p_27789_) {
        return p_27789_.getBlockState(p_27788_).isAir() ? 10.0F : 0.0F;
    }

    protected void registerGoals() {
        this.goalSelector.addGoal(4, new ButterflyEntity.ButterflyAvoidEntityGoal<>(this, Player.class, 8.0F, 2.2D, 2.2D));
        this.butterflyPollinateGoal = new ButterflyEntity.ButterflyPollinateGoal();
        this.goalSelector.addGoal(4, this.butterflyPollinateGoal);
        this.goToKnownFlowerGoal = new ButterflyEntity.ButterflyGoToKnownFlowerGoal();
        this.goalSelector.addGoal(6, this.goToKnownFlowerGoal);
        this.goalSelector.addGoal(8, new ButterflyEntity.ButterflyWanderGoal());
        this.goalSelector.addGoal(9, new FloatGoal(this));
    }

    public void addAdditionalSaveData(CompoundTag p_27823_) {
        super.addAdditionalSaveData(p_27823_);

        if (this.hasSavedFlowerPos()) {
            p_27823_.put("FlowerPos", NbtUtils.writeBlockPos(this.getSavedFlowerPos()));
        }
    }

    public void readAdditionalSaveData(CompoundTag p_27793_) {

        this.savedFlowerPos = null;
        if (p_27793_.contains("FlowerPos")) {
            this.savedFlowerPos = NbtUtils.readBlockPos(p_27793_.getCompound("FlowerPos"));
        }

        super.readAdditionalSaveData(p_27793_);
    }

    void pathfindRandomlyTowards(BlockPos p_27881_) {
        Vec3 vec3 = Vec3.atBottomCenterOf(p_27881_);
        int i = 0;
        BlockPos blockpos = this.blockPosition();
        int j = (int)vec3.y - blockpos.getY();
        if (j > 2) {
            i = 4;
        } else if (j < -2) {
            i = -4;
        }

        int k = 6;
        int l = 8;
        int i1 = blockpos.distManhattan(p_27881_);
        if (i1 < 15) {
            k = i1 / 2;
            l = i1 / 2;
        }

        Vec3 vec31 = AirRandomPos.getPosTowards(this, k, l, i, vec3, (double)((float)Math.PI / 10F));
        if (vec31 != null) {
            this.navigation.setMaxVisitedNodesMultiplier(0.5F);
            this.navigation.moveTo(vec31.x, vec31.y, vec31.z, 1.0D);
        }
    }

    @Nullable
    public BlockPos getSavedFlowerPos() {
        return this.savedFlowerPos;
    }

    public boolean hasSavedFlowerPos() {
        return this.savedFlowerPos != null;
    }

    protected void customServerAiStep() {
        if (this.isInWaterOrBubble()) {
            ++this.underWaterTicks;
        } else {
            this.underWaterTicks = 0;
        }

        if (this.underWaterTicks > 20) {
            this.hurt(DamageSource.DROWN, 1.0F);
        }

    }

    boolean isTooFarAway(BlockPos p_27890_) {
        return !this.closerThan(p_27890_, 32);
    }

    public static AttributeSupplier.Builder createAttributes() {
        return Mob.createMobAttributes().add(Attributes.MAX_HEALTH, 1D).add(Attributes.FLYING_SPEED, 0.6F).add(Attributes.MOVEMENT_SPEED, 0.3F);
    }

    protected PathNavigation createNavigation(Level p_27815_) {
        FlyingPathNavigation flyingpathnavigation = new FlyingPathNavigation(this, p_27815_) {
            public boolean isStableDestination(BlockPos p_27947_) {
                return !this.level.getBlockState(p_27947_.below()).isAir();
            }

            public void tick() {

                    super.tick();

            }
        };
        flyingpathnavigation.setCanOpenDoors(false);
        flyingpathnavigation.setCanFloat(false);
        flyingpathnavigation.setCanPassDoors(true);
        return flyingpathnavigation;
    }

    public boolean isFood(ItemStack p_27895_) {
        return p_27895_.is(ItemTags.FLOWERS);
    }

    boolean isFlowerValid(BlockPos p_27897_) {
        return this.level.isLoaded(p_27897_) && this.level.getBlockState(p_27897_).is(BlockTags.FLOWERS);
    }

    protected void playStepSound(BlockPos p_27820_, BlockState p_27821_) {
    }

    protected SoundEvent getAmbientSound() {
        return null;
    }

    protected SoundEvent getHurtSound(DamageSource p_27845_) {
        return SoundEvents.BEE_HURT;
    }

    protected SoundEvent getDeathSound() {
        return SoundEvents.BEE_DEATH;
    }

    protected float getSoundVolume() {
        return 0.4F;
    }

    public ButterflyEntity getBreedOffspring(ServerLevel p_148760_, AgeableMob p_148761_) {
        return EntityInit.BUTTERFLY.get().create(p_148760_);
    }

    protected float getStandingEyeHeight(Pose p_27804_, EntityDimensions p_27805_) {
        return p_27805_.height * 0.5F;
    }

    public boolean causeFallDamage(float p_148750_, float p_148751_, DamageSource p_148752_) {
        return false;
    }

    protected void checkFallDamage(double p_27754_, boolean p_27755_, BlockState p_27756_, BlockPos p_27757_) {
    }

    public boolean isFlapping() {
        return this.isFlying() && this.tickCount % TICKS_PER_FLAP == 0;
    }

    public boolean isFlying() {
        return !this.onGround;
    }

    public boolean hurt(DamageSource p_27762_, float p_27763_) {
        if (this.isInvulnerableTo(p_27762_)) {
            return false;
        } else {

            return super.hurt(p_27762_, p_27763_);
        }
    }

    public MobType getMobType() {
        return MobType.ARTHROPOD;
    }

    @Deprecated // FORGE: use jumpInFluid instead
    protected void jumpInLiquid(TagKey<Fluid> p_204061_) {
        this.jumpInLiquidInternal();
    }

    private void jumpInLiquidInternal() {
        this.setDeltaMovement(this.getDeltaMovement().add(0.0D, 0.01D, 0.0D));
    }

    @Override
    public void jumpInFluid(net.minecraftforge.fluids.FluidType type) {
        this.jumpInLiquidInternal();
    }

    public Vec3 getLeashOffset() {
        return new Vec3(0.0D, 0.5F * this.getEyeHeight(), this.getBbWidth() * 0.2F);
    }

    boolean closerThan(BlockPos p_27817_, int p_27818_) {
        return p_27817_.closerThan(this.blockPosition(), p_27818_);
    }

    abstract class BaseButterflyGoal extends Goal {
        public abstract boolean canButterflyUse();

        public abstract boolean canButterflyContinueToUse();

        public boolean canUse() {
            return this.canButterflyUse();
        }

        public boolean canContinueToUse() {
            return this.canButterflyContinueToUse() ;
        }
    }

    public class ButterflyGoToKnownFlowerGoal extends BaseButterflyGoal {
        int travellingTicks = ButterflyEntity.this.level.random.nextInt(10);

        ButterflyGoToKnownFlowerGoal() {
            this.setFlags(EnumSet.of(Goal.Flag.MOVE));
        }

        public boolean canButterflyUse() {
            return ButterflyEntity.this.savedFlowerPos != null && !ButterflyEntity.this.hasRestriction() &&
                    ButterflyEntity.this.isFlowerValid(ButterflyEntity.this.savedFlowerPos) &&
                    !ButterflyEntity.this.closerThan(ButterflyEntity.this.savedFlowerPos, 2);
        }

        public boolean canButterflyContinueToUse() {
            return this.canButterflyUse();
        }

        public void start() {
            this.travellingTicks = 0;
            super.start();
        }

        public void stop() {
            this.travellingTicks = 0;
            ButterflyEntity.this.navigation.stop();
            ButterflyEntity.this.navigation.resetMaxVisitedNodesMultiplier();
        }

        public void tick() {
            if (ButterflyEntity.this.savedFlowerPos != null) {
                ++this.travellingTicks;
                if (this.travellingTicks > this.adjustedTickDelay(600)) {
                    ButterflyEntity.this.savedFlowerPos = null;
                } else if (!ButterflyEntity.this.navigation.isInProgress()) {
                    if (ButterflyEntity.this.isTooFarAway(ButterflyEntity.this.savedFlowerPos)) {
                        ButterflyEntity.this.savedFlowerPos = null;
                    } else {
                        ButterflyEntity.this.pathfindRandomlyTowards(ButterflyEntity.this.savedFlowerPos);
                    }
                }
            }
        }
    }

    class ButterflyPollinateGoal extends ButterflyEntity.BaseButterflyGoal {
        private final Predicate<BlockState> VALID_POLLINATION_BLOCKS = (p_28074_) -> {
            if (p_28074_.hasProperty(BlockStateProperties.WATERLOGGED) && p_28074_.getValue(BlockStateProperties.WATERLOGGED)) {
                return false;
            } else if (p_28074_.is(BlockTags.FLOWERS)) {
                if (p_28074_.is(Blocks.SUNFLOWER)) {
                    return p_28074_.getValue(DoublePlantBlock.HALF) == DoubleBlockHalf.UPPER;
                } else {
                    return true;
                }
            } else {
                return false;
            }
        };
        private int successfulPollinatingTicks;
        private int lastSoundPlayedTick;
        private boolean pollinating;
        @Nullable
        private Vec3 hoverPos;
        private int pollinatingTicks;

        ButterflyPollinateGoal() {
            this.setFlags(EnumSet.of(Goal.Flag.MOVE));
        }

        public boolean canButterflyUse() {
            if (ButterflyEntity.this.remainingCooldownBeforeLocatingNewFlower > 0) {
                return false;
            } else if (ButterflyEntity.this.level.isRaining()) {
                return false;
            } else {
                Optional<BlockPos> optional = this.findNearbyFlower();
                if (optional.isPresent()) {
                    ButterflyEntity.this.savedFlowerPos = optional.get();
                    ButterflyEntity.this.navigation.moveTo(
                            (double)ButterflyEntity.this.savedFlowerPos.getX() + 0.5D,
                            (double)ButterflyEntity.this.savedFlowerPos.getY() + 0.5D,
                            (double)ButterflyEntity.this.savedFlowerPos.getZ() + 0.5D, 1.2F);
                    return true;
                } else {
                    ButterflyEntity.this.remainingCooldownBeforeLocatingNewFlower =
                            Mth.nextInt(ButterflyEntity.this.random, 20, 60);
                    return false;
                }
            }
        }

        public boolean canButterflyContinueToUse() {
            if (!this.pollinating) {
                return false;
            } else if (!ButterflyEntity.this.hasSavedFlowerPos()) {
                return false;
            } else if (ButterflyEntity.this.level.isRaining()) {
                return false;
            } else if (this.hasPollinatedLongEnough()) {
                return ButterflyEntity.this.random.nextFloat() < 0.2F;
            } else if (ButterflyEntity.this.tickCount % 20 == 0 &&
                    !ButterflyEntity.this.isFlowerValid(ButterflyEntity.this.savedFlowerPos)) {
                ButterflyEntity.this.savedFlowerPos = null;
                return false;
            } else {
                return true;
            }
        }

        private boolean hasPollinatedLongEnough() {
            return this.successfulPollinatingTicks > 400;
        }

        public void start() {
            this.successfulPollinatingTicks = 0;
            this.pollinatingTicks = 0;
            this.lastSoundPlayedTick = 0;
            this.pollinating = true;
        }

        public void stop() {

            this.pollinating = false;
            ButterflyEntity.this.navigation.stop();
            ButterflyEntity.this.remainingCooldownBeforeLocatingNewFlower = 200;
        }

        public boolean requiresUpdateEveryTick() {
            return true;
        }

        public void tick() {
            ++this.pollinatingTicks;
            if (this.pollinatingTicks > 600) {
                ButterflyEntity.this.savedFlowerPos = null;
            } else {
                Vec3 vec3 = Vec3.atBottomCenterOf(ButterflyEntity.this.savedFlowerPos)
                        .add(0.0D, (double)0.6F, 0.0D);
                if (vec3.distanceTo(ButterflyEntity.this.position()) > 1.0D) {
                    this.hoverPos = vec3;
                    this.setWantedPos();
                } else {
                    if (this.hoverPos == null) {
                        this.hoverPos = vec3;
                    }

                    boolean flag = ButterflyEntity.this.position().distanceTo(this.hoverPos) <= 0.1D;
                    boolean flag1 = true;
                    if (!flag && this.pollinatingTicks > 600) {
                        ButterflyEntity.this.savedFlowerPos = null;
                    } else {
                        if (flag) {
                            boolean flag2 = ButterflyEntity.this.random.nextInt(25) == 0;
                            if (flag2) {
                                this.hoverPos = new Vec3(vec3.x() +
                                        (double)this.getOffset(), vec3.y(), vec3.z() +
                                        (double)this.getOffset());
                                ButterflyEntity.this.navigation.stop();
                            } else {
                                flag1 = false;
                            }

                            ButterflyEntity.this.getLookControl().setLookAt(vec3.x(), vec3.y(), vec3.z());
                        }

                        if (flag1) {
                            this.setWantedPos();
                        }

                        ++this.successfulPollinatingTicks;
                        if (ButterflyEntity.this.random.nextFloat() < 0.05F && this.successfulPollinatingTicks
                                > this.lastSoundPlayedTick + 60) {
                            this.lastSoundPlayedTick = this.successfulPollinatingTicks;
                            ButterflyEntity.this.playSound(SoundEvents.BEE_POLLINATE, 1.0F, 1.0F);
                        }

                    }
                }
            }
        }

        private void setWantedPos() {
            ButterflyEntity.this.getMoveControl().setWantedPosition(this.hoverPos.x(),
                    this.hoverPos.y(), this.hoverPos.z(), (double)0.35F);
        }

        private float getOffset() {
            return (ButterflyEntity.this.random.nextFloat() * 2.0F - 1.0F) * 0.33333334F;
        }

        private Optional<BlockPos> findNearbyFlower() {
            return this.findNearestBlock(this.VALID_POLLINATION_BLOCKS, 5.0D);
        }

        private Optional<BlockPos> findNearestBlock(Predicate<BlockState> p_28076_, double p_28077_) {
            BlockPos blockpos = ButterflyEntity.this.blockPosition();
            BlockPos.MutableBlockPos blockpos$mutableblockpos = new BlockPos.MutableBlockPos();

            for(int i = 0; (double)i <= p_28077_; i = i > 0 ? -i : 1 - i) {
                for(int j = 0; (double)j < p_28077_; ++j) {
                    for(int k = 0; k <= j; k = k > 0 ? -k : 1 - k) {
                        for(int l = k < j && k > -j ? j : 0; l <= j; l = l > 0 ? -l : 1 - l) {
                            blockpos$mutableblockpos.setWithOffset(blockpos, k, i - 1, l);
                            if (blockpos.closerThan(blockpos$mutableblockpos, p_28077_) && p_28076_.test(ButterflyEntity.this.level.getBlockState(blockpos$mutableblockpos))) {
                                return Optional.of(blockpos$mutableblockpos);
                            }
                        }
                    }
                }
            }

            return Optional.empty();
        }
    }

    class ButterflyWanderGoal extends Goal {
        ButterflyWanderGoal() {
            this.setFlags(EnumSet.of(Goal.Flag.MOVE));
        }

        public boolean canUse() {
            return ButterflyEntity.this.navigation.isDone() &&
                    ButterflyEntity.this.random.nextInt(10) == 0;
        }

        public boolean canContinueToUse() {
            return ButterflyEntity.this.navigation.isInProgress();
        }

        public void start() {
            Vec3 vec3 = this.findPos();
            if (vec3 != null) {
                ButterflyEntity.this.navigation.moveTo(ButterflyEntity.this.navigation.createPath(new BlockPos(vec3), 1), 1.0D);
            }

        }

        @Nullable
        private Vec3 findPos() {
            Vec3 vec3;
                vec3 = ButterflyEntity.this.getViewVector(0.0F);


            int i = 8;
            Vec3 vec32 = HoverRandomPos.getPos(ButterflyEntity.this, 8, 7, vec3.x, vec3.z, ((float)Math.PI / 2F), 3, 1);
            return vec32 != null ? vec32 : AirAndWaterRandomPos.getPos(ButterflyEntity.this, 8, 4, -2, vec3.x, vec3.z, (double)((float)Math.PI / 2F));
        }
    }

    @Override
    public void tick() {
        if(this.level.getDayTime() >= 11415)
        {
            this.discard();
        }
        super.tick();
    }

    static class ButterflyAvoidEntityGoal<T extends LivingEntity> extends AvoidEntityGoal<T> {

        public ButterflyAvoidEntityGoal(ButterflyEntity p_29743_, Class<T> p_29744_, float p_29745_, double p_29746_, double p_29747_) {
            super(p_29743_, p_29744_, p_29745_, p_29746_, p_29747_);
        }

        public boolean canUse() {
            return super.canUse();
        }
    }

    //Dedicated spawn rules, useful if you want more complex spawning rules
    public static boolean checkSpawnRules(EntityType<ButterflyEntity> entityType, LevelAccessor level, MobSpawnType spawnType, BlockPos pos, RandomSource random) {
        return checkAnimalSpawnRules(entityType, level, spawnType, pos, random) && getLightLevelForButterfly(level, pos);
    }

    protected static boolean getLightLevelForButterfly(BlockAndTintGetter p_186210_, BlockPos p_186211_) {
        return p_186210_.getRawBrightness(p_186211_, 0) >= 9;
    }

}

