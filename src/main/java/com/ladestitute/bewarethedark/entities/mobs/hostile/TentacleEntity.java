package com.ladestitute.bewarethedark.entities.mobs.hostile;

import com.ladestitute.bewarethedark.client.ClientSanityData;
import com.ladestitute.bewarethedark.entities.mobs.neutral.CatcoonEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.TerrorbeakEntity;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.util.RandomSource;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.level.BlockAndTintGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.phys.AABB;

import java.util.List;

public class TentacleEntity extends Monster {
    private static final EntityDataAccessor<Integer> ATTACK_COOLDOWN = SynchedEntityData.defineId(TentacleEntity.class, EntityDataSerializers.INT);

    public TentacleEntity(EntityType<? extends Monster> p_33002_, Level p_33003_) {
        super(p_33002_, p_33003_);
        this.isPushable();
        this.fireImmune();
    }

    protected void defineSynchedData() {
        super.defineSynchedData();
        this.entityData.define(ATTACK_COOLDOWN, 40);
    }

    public void addAdditionalSaveData(CompoundTag p_30418_) {
        super.addAdditionalSaveData(p_30418_);
    }

    public void readAdditionalSaveData(CompoundTag p_30402_) {
        super.readAdditionalSaveData(p_30402_);
    }

    @Override
    public boolean isPushable() {
        return false;
    }

    @Override
    public boolean fireImmune() {
        return true;
    }

    private int cooldown = 40;

    @Override
    public void tick() {
        double radius = 2;
        AABB axisalignedbb = new AABB(this.blockPosition()).inflate(radius).expandTowards(0.0D, this.level.getMaxBuildHeight(), 0.0D);
        List<LivingEntity> list = this.level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
        if(this.entityData.get(ATTACK_COOLDOWN) == 0)
        {
            for (LivingEntity targets : list) {
                if(targets != this) {
                    targets.hurt(DamageSource.mobAttack(this), (float) this.getAttributeValue(Attributes.ATTACK_DAMAGE));
                }
            }
            this.entityData.set(ATTACK_COOLDOWN, cooldown);
        }
        else cooldown--;
        this.entityData.set(ATTACK_COOLDOWN, cooldown);
        super.tick();
    }

    public static AttributeSupplier.Builder createAttributes() {
        return Monster.createMonsterAttributes().add(Attributes.MAX_HEALTH, 32.0D).add(Attributes.ATTACK_DAMAGE, 4.0D).add(Attributes.KNOCKBACK_RESISTANCE, 1.0D);
    }

    //Spawning rules
    public static boolean checkSpawnRules(EntityType<TentacleEntity> entityType, ServerLevelAccessor level, MobSpawnType spawnType, BlockPos pos, RandomSource random) {
        return checkMonsterSpawnRules(entityType, level, spawnType, pos, random);
    }

}
