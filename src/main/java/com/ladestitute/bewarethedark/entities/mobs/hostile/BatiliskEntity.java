package com.ladestitute.bewarethedark.entities.mobs.hostile;

import com.ladestitute.bewarethedark.registries.ItemInit;
import net.minecraft.core.BlockPos;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.*;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.control.MoveControl;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.raid.Raider;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.BlockAndTintGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.phys.Vec3;

import java.util.EnumSet;

public class BatiliskEntity extends Monster {
    protected static final EntityDataAccessor<Byte> DATA_FLAGS_ID = SynchedEntityData.defineId(BatiliskEntity.class, EntityDataSerializers.BYTE);
    private static final int FLAG_IS_CHARGING = 1;
    public int guanoTime = this.random.nextInt(1500) + 1500;

    public BatiliskEntity(EntityType<? extends Monster> p_33984_, Level p_33985_) {
        super(p_33984_, p_33985_);
        this.moveControl = new BatiliskEntity.BatiliskEntityMoveControl(this);
    }

    public void aiStep() {
        super.aiStep();
        if (!this.level.isClientSide && this.isAlive() && --this.guanoTime <= 0) {
            this.playSound(SoundEvents.CHICKEN_EGG, 1.0F, (this.random.nextFloat() - this.random.nextFloat()) * 0.2F + 1.0F);
            this.spawnAtLocation(ItemInit.GUANO.get());
            this.gameEvent(GameEvent.ENTITY_PLACE);
            this.guanoTime = this.random.nextInt(1500) + 1500;
        }

    }

    public void tick() {
        if(this.level.getDayTime() <= 11413)
        {
            this.discard();
        }
        super.tick();
        this.setNoGravity(true);
    }

    public void move(MoverType p_33997_, Vec3 p_33998_) {
        super.move(p_33997_, p_33998_);
        this.checkInsideBlocks();
    }

    protected void registerGoals() {
        super.registerGoals();
        this.goalSelector.addGoal(0, new FloatGoal(this));
        this.goalSelector.addGoal(4, new BatiliskEntity.BatiliskEntityChargeAttackGoal());
        this.goalSelector.addGoal(8, new BatiliskEntity.BatiliskEntityRandomMoveGoal());
        this.goalSelector.addGoal(9, new LookAtPlayerGoal(this, Player.class, 3.0F, 1.0F));
        this.goalSelector.addGoal(10, new LookAtPlayerGoal(this, Mob.class, 8.0F));
        this.targetSelector.addGoal(1, (new HurtByTargetGoal(this, Raider.class)).setAlertOthers());
        this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, Player.class, true));
    }

    public static AttributeSupplier.Builder createAttributes() {
        return Monster.createMonsterAttributes().add(Attributes.MAX_HEALTH, 6.0D).add(Attributes.ATTACK_DAMAGE, 2.0D).add(Attributes.FLYING_SPEED, 0.15f);
    }

    protected void defineSynchedData() {
        super.defineSynchedData();
        this.entityData.define(DATA_FLAGS_ID, (byte)0);
    }

    private boolean getBatiliskEntityFlag(int p_34011_) {
        int i = this.entityData.get(DATA_FLAGS_ID);
        return (i & p_34011_) != 0;
    }

    private void setBatiliskEntityFlag(int p_33990_, boolean p_33991_) {
        int i = this.entityData.get(DATA_FLAGS_ID);
        if (p_33991_) {
            i |= p_33990_;
        } else {
            i &= ~p_33990_;
        }

        this.entityData.set(DATA_FLAGS_ID, (byte)(i & 255));
    }

    public boolean isCharging() {
        return this.getBatiliskEntityFlag(1);
    }

    public void setIsCharging(boolean p_34043_) {
        this.setBatiliskEntityFlag(1, p_34043_);
    }

    protected SoundEvent getAmbientSound() {
        return SoundEvents.VEX_AMBIENT;
    }

    protected SoundEvent getDeathSound() {
        return SoundEvents.VEX_DEATH;
    }

    protected SoundEvent getHurtSound(DamageSource p_34023_) {
        return SoundEvents.VEX_HURT;
    }

    class BatiliskEntityChargeAttackGoal extends Goal {
        public BatiliskEntityChargeAttackGoal() {
            this.setFlags(EnumSet.of(Goal.Flag.MOVE));
        }

        public boolean canUse() {
            LivingEntity livingentity = BatiliskEntity.this.getTarget();
            if (livingentity != null && livingentity.isAlive() && !BatiliskEntity.this.getMoveControl().hasWanted() && BatiliskEntity.this.random.nextInt(reducedTickDelay(7)) == 0) {
                return BatiliskEntity.this.distanceToSqr(livingentity) > 4.0D;
            } else {
                return false;
            }
        }

        public boolean canContinueToUse() {
            return BatiliskEntity.this.getMoveControl().hasWanted() && BatiliskEntity.this.isCharging() && BatiliskEntity.this.getTarget() != null && BatiliskEntity.this.getTarget().isAlive();
        }

        public void start() {
            LivingEntity livingentity = BatiliskEntity.this.getTarget();
            if (livingentity != null) {
                Vec3 vec3 = livingentity.getEyePosition();
                BatiliskEntity.this.moveControl.setWantedPosition(vec3.x, vec3.y, vec3.z, 1.0D);
            }

            BatiliskEntity.this.setIsCharging(true);
            BatiliskEntity.this.playSound(SoundEvents.VEX_CHARGE, 1.0F, 1.0F);
        }

        public void stop() {
            BatiliskEntity.this.setIsCharging(false);
        }

        public boolean requiresUpdateEveryTick() {
            return true;
        }

        public void tick() {
            LivingEntity livingentity = BatiliskEntity.this.getTarget();
            if (livingentity != null) {
                if (BatiliskEntity.this.getBoundingBox().intersects(livingentity.getBoundingBox())) {
                    BatiliskEntity.this.doHurtTarget(livingentity);
                    BatiliskEntity.this.setIsCharging(false);
                } else {
                    double d0 = BatiliskEntity.this.distanceToSqr(livingentity);
                    if (d0 < 9.0D) {
                        Vec3 vec3 = livingentity.getEyePosition();
                        BatiliskEntity.this.moveControl.setWantedPosition(vec3.x, vec3.y, vec3.z, 1.0D);
                    }
                }

            }
        }
    }

    class BatiliskEntityMoveControl extends MoveControl {
        public BatiliskEntityMoveControl(BatiliskEntity p_34062_) {
            super(p_34062_);
        }

        public void tick() {
            if (this.operation == MoveControl.Operation.MOVE_TO) {
                Vec3 vec3 = new Vec3(this.wantedX - BatiliskEntity.this.getX(), this.wantedY - BatiliskEntity.this.getY(), this.wantedZ - BatiliskEntity.this.getZ());
                double d0 = vec3.length();
                if (d0 < BatiliskEntity.this.getBoundingBox().getSize()) {
                    this.operation = MoveControl.Operation.WAIT;
                    BatiliskEntity.this.setDeltaMovement(BatiliskEntity.this.getDeltaMovement().scale(0.5D));
                } else {
                    BatiliskEntity.this.setDeltaMovement(BatiliskEntity.this.getDeltaMovement().add(vec3.scale(this.speedModifier * 0.05D / d0)));
                    if (BatiliskEntity.this.getTarget() == null) {
                        Vec3 vec31 = BatiliskEntity.this.getDeltaMovement();
                        BatiliskEntity.this.setYRot(-((float)Mth.atan2(vec31.x, vec31.z)) * (180F / (float)Math.PI));
                        BatiliskEntity.this.yBodyRot = BatiliskEntity.this.getYRot();
                    } else {
                        double d2 = BatiliskEntity.this.getTarget().getX() - BatiliskEntity.this.getX();
                        double d1 = BatiliskEntity.this.getTarget().getZ() - BatiliskEntity.this.getZ();
                        BatiliskEntity.this.setYRot(-((float)Mth.atan2(d2, d1)) * (180F / (float)Math.PI));
                        BatiliskEntity.this.yBodyRot = BatiliskEntity.this.getYRot();
                    }
                }

            }
        }
    }

    class BatiliskEntityRandomMoveGoal extends Goal {
        public BatiliskEntityRandomMoveGoal() {
            this.setFlags(EnumSet.of(Goal.Flag.MOVE));
        }

        public boolean canUse() {
            return !BatiliskEntity.this.getMoveControl().hasWanted() && BatiliskEntity.this.random.nextInt(reducedTickDelay(7)) == 0;
        }

        public boolean canContinueToUse() {
            return false;
        }

        public void tick() {
            BlockPos blockpos = BatiliskEntity.this.blockPosition();

            for(int i = 0; i < 3; ++i) {
                BlockPos blockpos1 = blockpos.offset(BatiliskEntity.this.random.nextInt(15) - 7, BatiliskEntity.this.random.nextInt(11) - 5, BatiliskEntity.this.random.nextInt(15) - 7);
                if (BatiliskEntity.this.level.isEmptyBlock(blockpos1)) {
                    BatiliskEntity.this.moveControl.setWantedPosition((double)blockpos1.getX() + 0.5D, (double)blockpos1.getY() + 0.5D, (double)blockpos1.getZ() + 0.5D, 0.25D);
                    if (BatiliskEntity.this.getTarget() == null) {
                        BatiliskEntity.this.getLookControl().setLookAt((double)blockpos1.getX() + 0.5D, (double)blockpos1.getY() + 0.5D, (double)blockpos1.getZ() + 0.5D, 180.0F, 20.0F);
                    }
                    break;
                }
            }

        }
    }

    public boolean causeFallDamage(float p_148750_, float p_148751_, DamageSource p_148752_) {
        return false;
    }

    protected void checkFallDamage(double p_27754_, boolean p_27755_, BlockState p_27756_, BlockPos p_27757_) {
    }

    public static boolean checkSpawnRules(EntityType<BatiliskEntity> entityType, ServerLevelAccessor level, MobSpawnType spawnType, BlockPos pos, RandomSource random) {
        return checkMonsterSpawnRules(entityType, level, spawnType, pos, random) && getLightLevelForMob(level, pos);
    }

    protected static boolean getLightLevelForMob(BlockAndTintGetter p_186210_, BlockPos p_186211_) {
        return p_186210_.getRawBrightness(p_186211_, 0) <= 7;
    }
}
