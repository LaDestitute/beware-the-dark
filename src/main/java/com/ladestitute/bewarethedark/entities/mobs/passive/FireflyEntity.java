package com.ladestitute.bewarethedark.entities.mobs.passive;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.*;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.animal.FlyingAnimal;
import net.minecraft.world.level.BlockAndTintGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import org.jetbrains.annotations.Nullable;

@SuppressWarnings("EntityConstructor")
public class FireflyEntity extends Animal implements FlyingAnimal {

    public FireflyEntity(EntityType<? extends Animal> p_21803_, Level p_21804_) {
        super(p_21803_, p_21804_);
        this.isPushable();
        this.isInvulnerable();
        this.isAttackable();
        this.isInvisible();
    }

    @Override
    public boolean isPushable() {
        return false;
    }

    @Override
    public boolean isInvulnerable() {
        return true;
    }

    @Override
    public boolean isAttackable() {
        return false;
    }

    @Override
    public boolean isInvisible() {
        return false;
    }

    public static AttributeSupplier.Builder createAttributes() {
        return Mob.createMobAttributes().add(Attributes.MAX_HEALTH, 10D);
    }

    @Nullable
    @Override
    public AgeableMob getBreedOffspring(ServerLevel p_146743_, AgeableMob p_146744_) {
        return null;
    }

    public MobType getMobType() {
        return MobType.ARTHROPOD;
    }

    @Override
    public void aiStep() {
        if(!this.level.isNight())
        {
            this.kill();
        }

        super.aiStep();
    }

    //Dedicated spawn rules, useful if you want more complex spawning rules
    public static boolean checkSpawnRules(EntityType<FireflyEntity> entityType, LevelAccessor level, MobSpawnType spawnType, BlockPos pos, RandomSource random) {
        return checkAnimalSpawnRules(entityType, level, spawnType, pos, random) && getLightLevelForFirefly(level, pos);
    }

    protected static boolean getLightLevelForFirefly(BlockAndTintGetter p_186210_, BlockPos p_186211_) {
        return p_186210_.getRawBrightness(p_186211_, 0) <= 8;
    }

    @Override
    public boolean causeFallDamage(float p_147187_, float p_147188_, DamageSource p_147189_) {
        return false;
    }

    @Override
    public boolean isFlying() {
        return true;
    }
}
