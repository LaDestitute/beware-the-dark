package com.ladestitute.bewarethedark.util.datagen;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.registries.FoodInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import net.minecraft.core.Registry;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.BlockTagsProvider;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.data.ExistingFileHelper;

public class BTDItemTagGen extends ItemTagsProvider {
    public BTDItemTagGen(DataGenerator p_126530_, BlockTagsProvider p_126531_) {
        super(p_126530_, p_126531_);
    }

    public static final TagKey<Item> CROCK_POT_INPUTS = ItemTags.create(new ResourceLocation(BTDMain.MOD_ID,
            "crock_pot_inputs"));

    @Override
    protected void addTags() {
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.GLOW_BERRIES);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.APPLE);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.BEEF);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.BEETROOT);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.BLUE_ICE);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.CACTUS);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.CARROT);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.CHICKEN);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.CHORUS_FRUIT);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.COCOA_BEANS);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.COD);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.COOKED_BEEF);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.COOKED_CHICKEN);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.COOKED_COD);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.COOKED_MUTTON);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.COOKED_PORKCHOP);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.COOKED_RABBIT);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.COOKED_SALMON);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.DRIED_KELP);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.EGG);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.HONEY_BOTTLE);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.HONEYCOMB);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.ICE);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.KELP);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.MELON_SLICE);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.MUTTON);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.PACKED_ICE);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.PORKCHOP);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.POTATO);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.PUMPKIN);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.RABBIT);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.SALMON);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.STICK);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.SUGAR);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(Items.TURTLE_EGG);
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(ItemInit.TWIGS.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(ItemInit.TWIGS.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.SMALL_JERKY.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.JERKY.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.DRIED_SEAWEED.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.MEAT.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.MONSTER_JERKY.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.MONSTER_MEAT.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.MORSEL.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.FISH.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.ROASTED_BIRCHNUT.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.ROASTED_CARROT.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.ROASTED_BERRIES.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.COOKED_MEAT.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.COOKED_FISH.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.COOKED_MORSEL.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.COOKED_EGG.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.BERRIES.get());
        this.tag(create(new ResourceLocation(BTDMain.MOD_ID, "crock_pot_inputs")))
                .add(FoodInit.COOKED_MONSTER_MEAT.get());
    }

    private static TagKey<Item> create(ResourceLocation pName) {
        return TagKey.create(Registry.ITEM_REGISTRY, pName);
    }

    public static class BlockTagsDataGen extends BlockTagsProvider {

        public BlockTagsDataGen(DataGenerator generatorIn, ExistingFileHelper existingFileHelper) {
            super(generatorIn, BTDMain.MOD_ID, existingFileHelper);
        }

        @Override
        protected void addTags() {
        }
    }
}
