package com.ladestitute.bewarethedark.util.datagen;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.util.datagen.BTDItemTagGen;
import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.data.event.GatherDataEvent;


@Mod.EventBusSubscriber(modid = BTDMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class BTDDataGen {

    //First, make sure you have datagen setup in your build.gradle, in the last part of runs configuration section
    //after client and server runs is where it is, make sure the modid matches yours
    // sourceSets.main.resources { srcDir 'src/generated/resources' } should also be after it as expected
    //To generate tags/recipes/etc provided by generators, run the "runData" gradle task
    //generated content will be placed in src/generated/resources by default
    //for entity tags, put the generated tags folders in src/main/resources/data/modid/tags/entity_types
    //for item tags, put the generated tags folders in src/main/resources/data/modid/tags/items
    //for block tags, put the generated tags folders in src/main/resources/data/modid/tags/blocks
    @SubscribeEvent
    public static void datagen(GatherDataEvent event) {
        DataGenerator generator = event.getGenerator();
        ExistingFileHelper existingFileHelper = event.getExistingFileHelper();
        BTDItemTagGen.BlockTagsDataGen blockTagsProvider = new BTDItemTagGen.BlockTagsDataGen(event.getGenerator(), existingFileHelper);

        event.getGenerator().addProvider(event.includeServer(),
                new BTDItemTagGen(generator, blockTagsProvider));


    }


}
