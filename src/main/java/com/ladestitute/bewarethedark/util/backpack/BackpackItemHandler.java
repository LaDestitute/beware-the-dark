package com.ladestitute.bewarethedark.util.backpack;

import net.minecraft.core.NonNullList;
import net.minecraft.world.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;

public class BackpackItemHandler extends ItemStackHandler {
    public BackpackItemHandler(int size) {
        super(size);
    }

    @Override
    protected void onContentsChanged(int slot) {
        BackpackManager.get().setDirty();
    }

}
