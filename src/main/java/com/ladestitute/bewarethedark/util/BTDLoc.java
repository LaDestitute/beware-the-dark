package com.ladestitute.bewarethedark.util;

import com.ladestitute.bewarethedark.BTDMain;
import net.minecraft.resources.ResourceLocation;

public class BTDLoc {
    public static ResourceLocation createLoc(String name) {
        return new ResourceLocation(BTDMain.MOD_ID, name);
    }
}
