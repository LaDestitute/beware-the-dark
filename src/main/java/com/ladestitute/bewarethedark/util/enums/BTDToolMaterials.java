package com.ladestitute.bewarethedark.util.enums;

import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.crafting.Ingredient;

public enum BTDToolMaterials implements Tier {

    FLINT(1F, 5f, 100, 1, 5, Blocks.AIR.asItem()),
    LUXURIANT(2.0F, 7f, 400, 2, 10, Blocks.AIR.asItem()),
    SPEAR(3.4F, 2F, 150, 3, 0, Blocks.AIR.asItem()),
    TENTACLE_SPIKE(5.1F, 2F, 100, 3, 0, Blocks.AIR.asItem()),
    TAIL_O_THREE_CATS(2.72F, 2F, 90, 3, 0, Blocks.AIR.asItem()),
    PITCHFORK(1.7F, 12F, 400, 3, 0, Blocks.AIR.asItem());
    private float attackDmg, efficiency;
    private int durability, harvestLevel, enchantabillity;
    private Item repairMaterial;
    private BTDToolMaterials(float attackDmg, float efficiency, int durability, int harvestLevel, int enchantability, Item repairMaterial) {
        this.attackDmg = attackDmg; // Wood/Gold: 0F, Stone: 1F, Iron: 2F, Diamond: 3F, Netherite: 4F
        this.efficiency = efficiency; // Wood: 2F, Stone: 4F, Iron: 6F, Diamond: 8F, Netherite: 9F, Gold: 12F
        this.durability = durability; // Gold: 32, Wood: 59, Stone: 131, Iron: 250, Diamond: 1561, Netherite: 2031
        this.harvestLevel = harvestLevel; // Wood/Gold: 0, Stone: 1, Iron: 2, Diamond: 3, Netherite: 4
        this.enchantabillity = enchantability; // Stone: 5, Diamond: 10, Iron: 14, Wood/Netherite: 15, Gold: 22,
        this.repairMaterial = repairMaterial;
    }

    @Override
    public int getUses() {
        return this.durability;
    }

    @Override
    public float getSpeed() {
        return this.efficiency;
    }

    @Override
    public float getAttackDamageBonus() {
        return this.attackDmg;
    }

    @Override
    public int getLevel() {
        return this.harvestLevel;
    }

    @Override
    public int getEnchantmentValue() {
        return this.enchantabillity;
    }

    @Override
    public Ingredient getRepairIngredient() {
        return Ingredient.of(this.repairMaterial);
    }

}
