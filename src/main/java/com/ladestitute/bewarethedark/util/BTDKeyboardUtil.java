package com.ladestitute.bewarethedark.util;

import com.ladestitute.bewarethedark.BTDMain;
import net.minecraft.client.KeyMapping;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.common.Mod;
import org.lwjgl.glfw.GLFW;

@SuppressWarnings("NoTranslation")
@Mod.EventBusSubscriber(modid = BTDMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class BTDKeyboardUtil {

    public static KeyMapping OPEN_BACKPACK =
            new KeyMapping("open_back", GLFW.GLFW_KEY_B, "key.category.bewarethedark");


}

