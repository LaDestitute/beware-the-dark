package com.ladestitute.bewarethedark.util.recipes;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.ladestitute.bewarethedark.BTDMain;
import net.minecraft.core.NonNullList;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.*;

public class CrockPotRecipe implements Recipe<SimpleContainer> {
    private final ResourceLocation id;
    private final ItemStack output;
    private final NonNullList<Ingredient> recipeItems;
    private final int time;

    // !!!
    public CrockPotRecipe(ResourceLocation id, ItemStack output,
                                  NonNullList<Ingredient> recipeItems, int time) {
        this.id = id;
        this.output = output;
        this.recipeItems = recipeItems;
        this.time = time;
    }

    @Override
    public boolean matches(SimpleContainer pContainer, net.minecraft.world.level.Level pLevel) {
        if(pLevel.isClientSide()) {
            return false;
        }

        if(recipeItems.get(0).test(pContainer.getItem(0)) &&
                recipeItems.get(1).test(pContainer.getItem(1))
                && recipeItems.get(2).test(pContainer.getItem(2))) {
            return recipeItems.get(3).test(pContainer.getItem(3));
        }

        return false;
    }

    public int getTime() {
        return time;
    }

    @Override
    public ItemStack assemble(SimpleContainer pContainer) {
        return output;
    }

    @Override
    public boolean canCraftInDimensions(int pWidth, int pHeight) {
        return true;
    }

    @Override
    public ItemStack getResultItem() {
        return output.copy();
    }

    @Override
    public ResourceLocation getId() {
        return id;
    }

    @Override
    public RecipeSerializer<?> getSerializer() {
        return Serializer.INSTANCE;
    }

    @Override
    public RecipeType<?> getType() {
        return Type.INSTANCE;
    }

    // !!!
    public static class Type implements RecipeType<CrockPotRecipe> {
        private Type() { }
        public static final Type INSTANCE = new Type();
        public static final String ID = "crock_pot";
    }

    // !!!
    public static class Serializer implements RecipeSerializer<CrockPotRecipe> {
        public static final Serializer INSTANCE = new Serializer();
        public static final ResourceLocation ID = new ResourceLocation(BTDMain.MOD_ID,"crock_pot");

        @Override
        // !!!
        public CrockPotRecipe fromJson(ResourceLocation id, JsonObject json) {
            ItemStack output = ShapedRecipe.itemStackFromJson(GsonHelper.getAsJsonObject(json, "output"));

            JsonArray ingredients = GsonHelper.getAsJsonArray(json, "ingredients");
            NonNullList<Ingredient> inputs = NonNullList.withSize(4, Ingredient.EMPTY);
            int time = GsonHelper.getAsInt(json, "time");

            for (int i = 0; i < inputs.size(); i++) {
                inputs.set(i, Ingredient.fromJson(ingredients.get(i)));
            }

            // !!!
            return new CrockPotRecipe(id, output, inputs, time);
        }

        @Override
        // !!!
        public CrockPotRecipe fromNetwork(ResourceLocation id, FriendlyByteBuf buf) {
            NonNullList<Ingredient> inputs = NonNullList.withSize(buf.readInt(), Ingredient.EMPTY);

            for (int i = 0; i < inputs.size(); i++) {
                inputs.set(i, Ingredient.fromNetwork(buf));
            }
            int time = buf.readVarInt();

            ItemStack output = buf.readItem();
            // !!!
            return new CrockPotRecipe(id, output, inputs, time);
        }

        @Override
        // !!!
        public void toNetwork(FriendlyByteBuf buf, CrockPotRecipe recipe) {
            buf.writeInt(recipe.getIngredients().size());
            buf.writeVarInt(recipe.getTime());
            for (Ingredient ing : recipe.getIngredients()) {
                ing.toNetwork(buf);
            }
            buf.writeItemStack(recipe.getResultItem(), false);
        }


        @SuppressWarnings("unchecked") // Need this wrapper, because generics
        private static <G> Class<G> castClass(Class<?> cls) {
            return (Class<G>)cls;
        }
    }
}
