package com.ladestitute.bewarethedark.util.glm;

import com.google.common.base.Suppliers;
import com.google.gson.JsonObject;
import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.registries.BlockInit;
import com.ladestitute.bewarethedark.registries.FoodInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import com.mojang.serialization.Codec;
import com.mojang.serialization.codecs.RecordCodecBuilder;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.parameters.LootContextParams;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraftforge.common.loot.IGlobalLootModifier;
import net.minecraftforge.common.loot.LootModifier;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

public class DropModifier {
    public static final DeferredRegister<Codec<? extends IGlobalLootModifier>> GLM = DeferredRegister.create(ForgeRegistries.Keys.GLOBAL_LOOT_MODIFIER_SERIALIZERS, BTDMain.MOD_ID);
    public static final RegistryObject<Codec<? extends IGlobalLootModifier>> BEWARETHEDARK_DROPS = GLM.register("bewarethedark_drops", BlockDropModifier.CODEC);

    public static class BlockDropModifier extends LootModifier {
        public static final Supplier<Codec<BlockDropModifier>> CODEC = Suppliers.memoize(() ->
                RecordCodecBuilder.create(inst -> codecStart(inst).apply(inst, BlockDropModifier::new)));

        public BlockDropModifier(LootItemCondition[] lootConditions) {
            super(lootConditions);
        }

        @Nonnull
        @Override
        protected ObjectArrayList<ItemStack> doApply(ObjectArrayList<ItemStack> generatedLoot, LootContext context) {
            if (context.hasParam(LootContextParams.BLOCK_STATE)) {
                BlockState state = context.getParamOrNull(LootContextParams.BLOCK_STATE);
                Block block = state.getBlock();
                if(block == Blocks.GRAVEL)
                {
                    generatedLoot.clear();
                    Random rand = new Random();
                    int chance = rand.nextInt(101);
                    if(chance <= 50)
                    {
                        generatedLoot.add(new ItemStack(Items.FLINT, 1));
                    }
                    else generatedLoot.add(new ItemStack(Blocks.GRAVEL, 1));
                }
                if(block == Blocks.TALL_GRASS||block == Blocks.GRASS)
                {
                    generatedLoot.clear();
                    Random rand = new Random();
                    int chance = rand.nextInt(101);
                    if(chance <= 25)
                    {
                        generatedLoot.add(new ItemStack(ItemInit.CUT_GRASS.get(), 1));
                    }
                }
                if (block == Blocks.OAK_LOG||
                        block == Blocks.ACACIA_LOG||
                        block == Blocks.SPRUCE_LOG||
                        block == Blocks.DARK_OAK_LOG||
                        block == Blocks.JUNGLE_LOG||
                        block == Blocks.BIRCH_LOG||
                        block == Blocks.STRIPPED_OAK_LOG||
                        block == Blocks.STRIPPED_ACACIA_LOG||
                        block == Blocks.STRIPPED_SPRUCE_LOG||
                        block == Blocks.STRIPPED_DARK_OAK_LOG||
                        block == Blocks.STRIPPED_JUNGLE_LOG||
                        block == Blocks.STRIPPED_BIRCH_LOG) {
                    if(BTDConfig.getInstance().replacevanilladrops()) {
                        generatedLoot.clear();
                        generatedLoot.add(new ItemStack(ItemInit.SPRUCE_LOG.get(), 1));
                    }

                }
                //
                if (block == Blocks.BRAIN_CORAL||
                        block == Blocks.BUBBLE_CORAL||
                        block == Blocks.FIRE_CORAL||
                        block == Blocks.HORN_CORAL||
                        block == Blocks.TUBE_CORAL||
                        block == Blocks.DEAD_BRAIN_CORAL||
                        block == Blocks.DEAD_BUBBLE_CORAL||
                        block == Blocks.DEAD_FIRE_CORAL||
                        block == Blocks.DEAD_HORN_CORAL||
                        block == Blocks.DEAD_TUBE_CORAL||
                        block == Blocks.BRAIN_CORAL_FAN||
                        block == Blocks.BUBBLE_CORAL_FAN||
                        block == Blocks.FIRE_CORAL_FAN||
                        block == Blocks.HORN_CORAL_FAN||
                        block == Blocks.TUBE_CORAL_FAN||
                        block == Blocks.DEAD_BRAIN_CORAL_FAN||
                        block == Blocks.DEAD_BUBBLE_CORAL_FAN||
                        block == Blocks.DEAD_FIRE_CORAL_FAN||
                        block == Blocks.DEAD_HORN_CORAL_FAN||
                        block == Blocks.DEAD_TUBE_CORAL_FAN) {
                    if(BTDConfig.getInstance().replacevanilladrops()) {
//                        generatedLoot.clear();
//                        generatedLoot.add(new ItemStack(ItemInit.CORAL.get(), 1));
                    }

                }
                //
                if (block == Blocks.OAK_LEAVES||
                        block == Blocks.ACACIA_LEAVES||
                        block == Blocks.DARK_OAK_LEAVES||
                        block == Blocks.JUNGLE_LEAVES||
                        block == Blocks.SPRUCE_LEAVES||
                block == BlockInit.EVERGREEN_LEAVES.get()) {
                    if(BTDConfig.getInstance().pineconesassaplings()) {
                        Random rand = new Random();
                        int pineconechance = rand.nextInt(100);
                        int twigschance = rand.nextInt(100);
                        int bonustwigschance = rand.nextInt(2);
                        if(BTDConfig.getInstance().replacevanilladrops()) {
                        generatedLoot.clear();
                        if (pineconechance <= 25) {
                                generatedLoot.add(new ItemStack(ItemInit.PINE_CONE.get(), 1));

                        }
                        if (twigschance <= 12) {
                            if (bonustwigschance == 0) {
                                generatedLoot.add(new ItemStack(ItemInit.TWIGS.get(), 1));
                            }
                            if (bonustwigschance == 1) {
                                generatedLoot.add(new ItemStack(ItemInit.TWIGS.get(), 1));
                                generatedLoot.add(new ItemStack(ItemInit.TWIGS.get(), 1));
                            }
                            }
                        }
                    }
                }
                //
                if (block == Blocks.BIRCH_LEAVES ||
                        block == BlockInit.GREEN_DECIDUOUS_LEAVES.get()||
                        block == BlockInit.ORANGE_DECIDUOUS_LEAVES.get()||
                        block == BlockInit.RED_DECIDUOUS_LEAVES.get()||
                        block == BlockInit.YELLOW_DECIDUOUS_LEAVES.get()) {
                    if(BTDConfig.getInstance().pineconesassaplings()) {
                        Random rand = new Random();
                        int pineconechance = rand.nextInt(100);
                        int twigschance = rand.nextInt(100);
                        int bonustwigschance = rand.nextInt(2);
                        if(BTDConfig.getInstance().replacevanilladrops()) {
                        generatedLoot.clear();
                        if (pineconechance <= 25) {
                            generatedLoot.add(new ItemStack(ItemInit.BIRCHNUT.get(), 1));
                        }
                        if (twigschance <= 12) {
                            if (bonustwigschance == 0) {
                                generatedLoot.add(new ItemStack(ItemInit.TWIGS.get(), 1));
                            }
                            if (bonustwigschance == 1) {
                                generatedLoot.add(new ItemStack(ItemInit.TWIGS.get(), 1));
                                generatedLoot.add(new ItemStack(ItemInit.TWIGS.get(), 1));
                            }
                        }
                        }
                    }
                }
                //
                if (block == Blocks.DEAD_BUSH) {
                    if(BTDConfig.getInstance().pineconesassaplings()) {
                        Random rand = new Random();
                        int twigschance = rand.nextInt(100);
                        int bonustwigschance = rand.nextInt(2);
                        if(BTDConfig.getInstance().replacevanilladrops()) {
                            generatedLoot.clear();
                            if (twigschance <= 49) {
                                if (bonustwigschance == 0) {
                                    generatedLoot.add(new ItemStack(ItemInit.TWIGS.get(), 1));
                                }
                                if (bonustwigschance == 1) {
                                    generatedLoot.add(new ItemStack(ItemInit.TWIGS.get(), 1));
                                    generatedLoot.add(new ItemStack(ItemInit.TWIGS.get(), 1));
                                }
                            }
                        }
                    }
                }
                //
                if (block == Blocks.DANDELION||block == Blocks.POPPY||block == Blocks.BLUE_ORCHID||
                        block == Blocks.AZURE_BLUET||block == Blocks.LILY_OF_THE_VALLEY||
                        block == Blocks.CORNFLOWER||block == Blocks.OXEYE_DAISY||
                        block == Blocks.ALLIUM||block == Blocks.WHITE_TULIP||
                        block == Blocks.PINK_TULIP||block == Blocks.ORANGE_TULIP||
                        block == Blocks.RED_TULIP) {
                    if(BTDConfig.getInstance().replacevanilladrops()) {
                        generatedLoot.clear();
                        generatedLoot.add(new ItemStack(FoodInit.PETALS.get(), 1));
                    }
                }
                if (block == Blocks.SUNFLOWER||block == Blocks.LILAC
                ||block == Blocks.PEONY||block == Blocks.ROSE_BUSH) {
                    if(BTDConfig.getInstance().replacevanilladrops()) {
                        generatedLoot.clear();
                        generatedLoot.add(new ItemStack(FoodInit.PETALS.get(), 2));
                    }
                }
            }
            return generatedLoot;
        }

        @Override
        public Codec<? extends IGlobalLootModifier> codec() {
            return BEWARETHEDARK_DROPS.get();
        }
    }
}
