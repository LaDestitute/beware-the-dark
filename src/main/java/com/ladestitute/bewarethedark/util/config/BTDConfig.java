package com.ladestitute.bewarethedark.util.config;

import net.minecraftforge.common.ForgeConfigSpec;
import org.apache.commons.lang3.tuple.Pair;

public class BTDConfig {
    private static final BTDConfig INSTANCE;

    public static final ForgeConfigSpec SPEC;

    static {
        ForgeConfigSpec.Builder builder = new ForgeConfigSpec.Builder();

        Pair<BTDConfig, ForgeConfigSpec> specPair =
                new ForgeConfigSpec.Builder().configure(BTDConfig::new);
        INSTANCE = specPair.getLeft();

        SPEC = specPair.getRight();
    }

    //General
    private final ForgeConfigSpec.BooleanValue allowphantomstospawn;
    private final ForgeConfigSpec.BooleanValue allowdynamiclight;
    private final ForgeConfigSpec.BooleanValue mobsdropgenericmeat;
    private final ForgeConfigSpec.BooleanValue pineconesassaplings;
    private final ForgeConfigSpec.BooleanValue enablerecipeprototyping;
    private final ForgeConfigSpec.BooleanValue terrablender_replace_vanilla_biomes;
    private final ForgeConfigSpec.IntValue terrablender_biome_weight;
    private final ForgeConfigSpec.BooleanValue replacevanilladrops;
    private final ForgeConfigSpec.BooleanValue allow_cavegen_for_mod_biomes;
    private final ForgeConfigSpec.BooleanValue dont_starve_style_fishing;
    private final ForgeConfigSpec.BooleanValue replace_title_music;
    private final ForgeConfigSpec.BooleanValue dont_starve_style_hunger;
    private final ForgeConfigSpec.IntValue hunger_tick;
    private final ForgeConfigSpec.BooleanValue pumpkin_lantern_long_perish_time;
    private final ForgeConfigSpec.BooleanValue enable_sanity;
    private final ForgeConfigSpec.BooleanValue enable_sanity_gamma;
    private final ForgeConfigSpec.BooleanValue enable_sanity_fog;
    private final ForgeConfigSpec.BooleanValue enable_sanity_screenshake;
    private final ForgeConfigSpec.BooleanValue dont_starve_style_poison;

    private BTDConfig(ForgeConfigSpec.Builder configSpecBuilder) {
        //General
        allowphantomstospawn = configSpecBuilder
                .comment("Whether Phantoms are allowed to spawn with the mod, false by default")
                .define("allowphantomstospawn", false);
        allowdynamiclight = configSpecBuilder
                .comment("Whether non-client dynamic light is enabled (newly re-implemented in 1.19.2, warning, experimental and may cause FPS loss! Use Forge performance mods if enabled)")
                .define("allowdynamiclight", true);
        mobsdropgenericmeat = configSpecBuilder
                .comment("Whether passive mobs drop generic meat instead of pork/beef/etc")
                .define("mobsdropgenericmeat", true);
        pineconesassaplings = configSpecBuilder
                .comment("Whether leaves drop pinecones instead of saplings")
                .define("pineconesassaplings", true);
        enablerecipeprototyping = configSpecBuilder
                .comment("Whether to enable the game-rule limited crafting, which will force the player to unlock recipes via the Science Machine/Alchemy Engine and Blueprints")
                .define("enablerecipeprototyping", true);
        replacevanilladrops = configSpecBuilder
                .comment("Whether to replace vanilla drops of blocks such as logs instead")
                .define("replacevanilladrops", true);
        terrablender_biome_weight = configSpecBuilder
                .comment("Weight for the mod biomes generated using Terrablender")
                .defineInRange("terrablender_biome_weight", 10, 1, 15);
        terrablender_replace_vanilla_biomes = configSpecBuilder
                .comment("Whether mod biomes will replace vanilla biomes occasionally, keeping this enabled makes the vanilla-bug of mod biomes generating underwater less likely")
                .define("terrablender_replace_vanilla_biomes", true);
        allow_cavegen_for_mod_biomes = configSpecBuilder
                .comment("Whether mod biomes will generate with caves")
                .define("allow_cavegen_for_mod_biomes", false);
        dont_starve_style_fishing = configSpecBuilder
                .comment("Whether fishing returns only fish and nothing else")
                .define("dont_starve_style_fishing", true);
        replace_title_music = configSpecBuilder
                .comment("Whether to replace vanilla title music with Don't Starve Together's title theme")
                .define("replace_title_music", true);
        dont_starve_style_hunger = configSpecBuilder
                .comment("Whether to mirror DS-style hunger; no saturation, residual-idle hunger drain and nerfed hunger-values for non crock pot foods")
                .define("dont_starve_style_hunger", true);
        hunger_tick = configSpecBuilder
                .comment("How long in ticks for each time the player's hunger drains even while idle (1200 = 1 Minute)")
                .defineInRange("hunger_tick", 2400, 600, 4800);
        pumpkin_lantern_long_perish_time = configSpecBuilder
                .comment("Whether Pumpkin Lanterns take much longer to perish when placed (like in DST); true sets it to their longer perish-time (7.1 days), false sets it to their original DS-version perish time of 1.3 days")
                .define("pumpkin_lantern_long_perish_time", false);
        enable_sanity = configSpecBuilder
                .comment("Whether the mod's sanity system is enabled, disabling this make make future content difficult or near-impossible to obtain or complete")
                .define("enable_sanity", true);
        enable_sanity_gamma = configSpecBuilder
                .comment("Whether increased gamma effects (brightness) are enabled for being under or at 75% sanity")
                .define("enable_sanity_gamma", true);
        enable_sanity_fog = configSpecBuilder
                .comment("Whether greyscale fog effects are enabled for being under or at 75% sanity")
                .define("enable_sanity_fog", true);
        enable_sanity_screenshake = configSpecBuilder
                .comment("Whether screen shake effects are enabled for being under or at 75% sanity")
                .define("enable_sanity_screenshake", true);
        dont_starve_style_poison = configSpecBuilder
                .comment("Whether slowness and weakness is included with the poison effect to simulate how it functions in Don't Starve")
                .define("dont_starve_style_poison", true);

    }

    public static BTDConfig getInstance() {
        return INSTANCE;
    }
    // Query Operations

    //General
    public boolean allowphantomstospawn() { return allowphantomstospawn.get(); }
    public boolean allowdynamiclight() { return allowdynamiclight.get(); }
    public boolean mobsdropgenericmeat() { return mobsdropgenericmeat.get(); }
    public boolean pineconesassaplings() { return pineconesassaplings.get(); }
    public boolean replacevanilladrops() { return replacevanilladrops.get(); }
    public boolean enablerecipeprototyping() { return enablerecipeprototyping.get(); }
    public int terrablender_biome_weight() { return terrablender_biome_weight.get(); }
    public boolean terrablender_replace_vanilla_biomes() { return terrablender_replace_vanilla_biomes.get(); }
    public boolean allow_cavegen_for_mod_biomes() { return allow_cavegen_for_mod_biomes.get(); }
    public boolean dont_starve_style_fishing() { return dont_starve_style_fishing.get(); }
    public boolean replace_title_music() { return replace_title_music.get(); }
    public boolean dont_starve_style_hunger() { return dont_starve_style_hunger.get(); }
    public int hunger_tick() { return hunger_tick.get(); }
    public boolean pumpkin_lantern_long_perish_time() { return pumpkin_lantern_long_perish_time.get(); }
    public boolean enable_sanity() { return enable_sanity.get(); }
    public boolean enable_sanity_gamma() { return enable_sanity_gamma.get(); }
    public boolean enable_sanity_fog() { return enable_sanity_fog.get(); }
    public boolean enable_sanity_screenshake() { return enable_sanity_screenshake.get(); }
    public boolean dont_starve_style_poison() { return dont_starve_style_poison.get(); }

    public void changeallowphantomstospawn(boolean newValue) {
        allowphantomstospawn.set(newValue);
    }
    public void changeallowdynamiclight(boolean newValue) {
        allowdynamiclight.set(newValue);
    }
    public void changemobsdropgenericmeat(boolean newValue) {
        mobsdropgenericmeat.set(newValue);
    }
    public void changepineconesassaplings(boolean newValue) {
        pineconesassaplings.set(newValue);
    }
    public void changeenablerecipeprototyping(boolean newValue) {
        enablerecipeprototyping.set(newValue);
    }
    public void changereplacevanilladrops(boolean newValue) {
        replacevanilladrops.set(newValue);
    }
    public void changeterrablenderweight(int newValue) {terrablender_biome_weight.set(newValue);}
    public void changeterrablendereplacebiomes(boolean newValue) {
        terrablender_replace_vanilla_biomes.set(newValue);
    }
    public void changeallow_cavegen_for_mod_biomes(boolean newValue) {allow_cavegen_for_mod_biomes.set(newValue);}
    public void changedont_starve_style_fishing(boolean newValue) {dont_starve_style_fishing.set(newValue);}
    public void changereplace_title_music(boolean newValue) {replace_title_music.set(newValue);}
    public void changedont_starve_style_hunger(boolean newValue) {dont_starve_style_hunger.set(newValue);}
    public void changehungertick(int newValue) {hunger_tick.set(newValue);}
    public void changepumpkin_lantern_long_perish_time(boolean newValue) {pumpkin_lantern_long_perish_time.set(newValue);}
    public void changeenable_sanity(boolean newValue) {enable_sanity.set(newValue);}
    public void changeenable_sanity_gamma(boolean newValue) {enable_sanity_gamma.set(newValue);}
    public void changeenable_sanity_fog(boolean newValue) {enable_sanity_fog.set(newValue);}
    public void changeenable_sanity_screenshake(boolean newValue) {enable_sanity_screenshake.set(newValue);}
    public void changedont_starve_style_poison(boolean newValue) {dont_starve_style_poison.set(newValue);}


    public void save() {
        SPEC.save();
    }

}
