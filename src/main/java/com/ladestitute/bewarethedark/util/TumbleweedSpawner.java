package com.ladestitute.bewarethedark.util;

import com.ladestitute.bewarethedark.entities.objects.EntityTumbleweed;
import com.ladestitute.bewarethedark.registries.EntityInit;
import com.ladestitute.bewarethedark.registries.SpecialBlockInit;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.Random;

public class TumbleweedSpawner {

   @SubscribeEvent
   public void attemptspawn(TickEvent.PlayerTickEvent event) {
       BlockPos blockPos = event.player.blockPosition();
       int searchRadius = 15;
       int radius = (int) (Math.floor(1) + searchRadius);

       for (int x = -radius; x <= radius; ++x) {
           for (int y = -1; y <= 1; ++y) {
               for (int z = -radius; z <= radius; ++z) {
                   BlockPos weedPos = new BlockPos(blockPos.getX() + x, blockPos.getY() + y, blockPos.getZ() + z);
                   BlockState blockState = event.player.level.getBlockState(weedPos);
                   Block block = blockState.getBlock();
                   if (block == SpecialBlockInit.TUMBLE_SPAWNER.get())
                   {
                       EntityTumbleweed tumbleweed = new EntityTumbleweed(EntityInit.TUMBLEWEED.get(), event.player.level);
                       tumbleweed.setPos(weedPos.getX(), weedPos.getY(), weedPos.getZ());
                       Random rand1 = new Random();
                       int movement1 = rand1.nextInt(3);
                       int movement2 = rand1.nextInt(3);
                       tumbleweed.setDeltaMovement(movement1+1, 0, movement2+1);


                       int spawnchance = rand1.nextInt(800);

                       if(spawnchance < 1)
                       {
                           event.player.level.addFreshEntity(tumbleweed);
                       }
                   }
               }
           }
       }
   }

}
