package com.ladestitute.bewarethedark.world.feature.tree;

import com.ladestitute.bewarethedark.registries.FeatureInit;
import net.minecraft.core.Holder;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.block.grower.AbstractTreeGrower;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;

//A simple tree grower class we associate with our treeinit trees
public class RedBirchnutTreeGrower extends AbstractTreeGrower
{
    @Override
    protected Holder<? extends ConfiguredFeature<?, ?>> getConfiguredFeature(RandomSource p_225546_1_, boolean p_225546_2_)
    {
        return FeatureInit.RED_BIRCHNUT;
    }
}
