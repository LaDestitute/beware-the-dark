package com.ladestitute.bewarethedark.world.feature;

import com.ladestitute.bewarethedark.registries.SpecialBlockInit;
import com.mojang.serialization.Codec;
import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.SimpleBlockConfiguration;

public class RedMushroomFeature extends Feature<SimpleBlockConfiguration> {
    public RedMushroomFeature(Codec<SimpleBlockConfiguration> config) {
        super(config);
    }

    @Override
    public boolean place(FeaturePlaceContext<SimpleBlockConfiguration> context) {
        SimpleBlockConfiguration config = context.config();
        WorldGenLevel level = context.level();
        RandomSource random = context.random();
        BlockPos origin = context.origin();
        BlockState state = config.toPlace().getState(random, origin);
        if (state.canSurvive(level, origin)) {
            if (random.nextInt(10) != 0) {
                level.setBlock(origin.below(), SpecialBlockInit.RED_MUSHROOM_SPAWNER.get().defaultBlockState(), 2);
            }

            return true;
        } else {
            return false;
        }
    }
}
