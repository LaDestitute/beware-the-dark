package com.ladestitute.bewarethedark.world.biome;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.registries.BiomeInit;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;

import java.util.function.Supplier;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
public class BTDBiomes
{
    protected static DeferredRegister<Biome> BIOME_REGISTER = DeferredRegister.create(Registry.BIOME_REGISTRY,
            BTDMain.MOD_ID);

    public static void registerBiomes()
    {
        register(BiomeInit.ROCKYLAND, BTDOverworldBiomes::rockyland);
        register(BiomeInit.CREEPY_FOREST, BTDOverworldBiomes::creepy_forest);
        register(BiomeInit.LUMPY_FOREST, BTDOverworldBiomes::lumpy_forest);
        register(BiomeInit.DECIDUOUS_FOREST, BTDOverworldBiomes::deciduous_forest);
        register(BiomeInit.MARSH, BTDOverworldBiomes::marsh);
        register(BiomeInit.GRAVEYARD, BTDOverworldBiomes::graveyard);
    }

    public static RegistryObject<Biome> register(ResourceKey<Biome> key, Supplier<Biome> biomeSupplier)
    {
        return BIOME_REGISTER.register(key.location().getPath(), biomeSupplier);
    }
}
