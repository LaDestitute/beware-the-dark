package com.ladestitute.bewarethedark.world.biome;

import com.ladestitute.bewarethedark.registries.EntityInit;
import com.ladestitute.bewarethedark.registries.SoundInit;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import net.minecraft.data.worldgen.BiomeDefaultFeatures;
import net.minecraft.sounds.Music;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.level.biome.*;

import javax.annotation.Nullable;

public class BTDOverworldBiomes
{
    @Nullable
    private static final Music NORMAL_MUSIC = null;

    protected static int calculateSkyColor(float color)
    {
        float $$1 = color / 3.0F;
        $$1 = Mth.clamp($$1, -1.0F, 1.0F);
        return Mth.hsvToRgb(0.62222224F - $$1 * 0.05F, 0.5F + $$1 * 0.1F, 1.0F);
    }

    private static Biome biome(Biome.Precipitation precipitation, float temperature, float downfall, MobSpawnSettings.Builder spawnBuilder, BiomeGenerationSettings.Builder biomeBuilder, @Nullable Music music)
    {
        return biome(precipitation, temperature, downfall, 4159204, 329011, spawnBuilder, biomeBuilder, music);
    }

    private static Biome biome(Biome.Precipitation precipitation, float temperature, float downfall, int waterColor, int waterFogColor, MobSpawnSettings.Builder spawnBuilder, BiomeGenerationSettings.Builder biomeBuilder, @Nullable Music music)
    {
        return (new Biome.BiomeBuilder()).precipitation(precipitation).temperature(temperature).downfall(downfall).specialEffects((new BiomeSpecialEffects.Builder()).waterColor(waterColor).waterFogColor(waterFogColor).fogColor(12638463).skyColor(calculateSkyColor(temperature)).ambientMoodSound(AmbientMoodSettings.LEGACY_CAVE_SETTINGS).backgroundMusic(music).build()).mobSpawnSettings(spawnBuilder.build()).generationSettings(biomeBuilder.build()).build();
    }

    private static void globalOverworldGeneration(BiomeGenerationSettings.Builder builder)
    {
        if(BTDConfig.getInstance().allow_cavegen_for_mod_biomes()) {
            BiomeDefaultFeatures.addDefaultCarversAndLakes(builder);
        }
        BiomeDefaultFeatures.addDefaultCrystalFormations(builder);

        BiomeDefaultFeatures.addDefaultMonsterRoom(builder);
            BiomeDefaultFeatures.addDefaultUndergroundVariety(builder);
    }

    //If you are adding vanilla BiomeDefaultFeatures in your custom biomes, they must
    //follow the exact feature order Mojang uses in OverworldBiomes or your mod will crash
    public static Biome rockyland()
    {
        MobSpawnSettings.Builder spawnBuilder = new MobSpawnSettings.Builder();
        BiomeDefaultFeatures.commonSpawns(spawnBuilder);
        BiomeGenerationSettings.Builder biomeBuilder = new BiomeGenerationSettings.Builder();
        globalOverworldGeneration(biomeBuilder);
        BiomeDefaultFeatures.addDefaultSoftDisks(biomeBuilder);
        return biome(Biome.Precipitation.RAIN, 1.5F, 0.5F,
                spawnBuilder, biomeBuilder, NORMAL_MUSIC);
    }

    public static Biome creepy_forest()
    {
        MobSpawnSettings.Builder spawnBuilder = new MobSpawnSettings.Builder();
        BiomeDefaultFeatures.commonSpawns(spawnBuilder);
        BiomeGenerationSettings.Builder biomeBuilder = new BiomeGenerationSettings.Builder();
        globalOverworldGeneration(biomeBuilder);
        BiomeDefaultFeatures.addDefaultSoftDisks(biomeBuilder);
        return biome(Biome.Precipitation.RAIN, 0.65F, 0.8F,
                spawnBuilder, biomeBuilder, NORMAL_MUSIC);
    }

    public static Biome lumpy_forest()
{
    MobSpawnSettings.Builder spawnBuilder = new MobSpawnSettings.Builder();
    BiomeDefaultFeatures.commonSpawns(spawnBuilder);
    BiomeGenerationSettings.Builder biomeBuilder = new BiomeGenerationSettings.Builder();
    globalOverworldGeneration(biomeBuilder);
    BiomeDefaultFeatures.addDefaultSoftDisks(biomeBuilder);
    return biome(Biome.Precipitation.RAIN, 0.65F, 0.8F,
            spawnBuilder, biomeBuilder, NORMAL_MUSIC);
}

    public static Biome deciduous_forest()
    {
        MobSpawnSettings.Builder spawnBuilder = new MobSpawnSettings.Builder();
        BiomeGenerationSettings.Builder biomeBuilder = new BiomeGenerationSettings.Builder();
        globalOverworldGeneration(biomeBuilder);
        BiomeDefaultFeatures.addDefaultSoftDisks(biomeBuilder);
        return biome(Biome.Precipitation.RAIN, 0.65F, 0.6F,
                spawnBuilder, biomeBuilder, NORMAL_MUSIC);
    }

    public static Biome marsh()
    {
        MobSpawnSettings.Builder spawnBuilder = new MobSpawnSettings.Builder();
        BiomeDefaultFeatures.commonSpawns(spawnBuilder);
        BiomeGenerationSettings.Builder biomeBuilder = new BiomeGenerationSettings.Builder();
        // no lakes for marshes, they got ponds instead
        BiomeDefaultFeatures.addDefaultSoftDisks(biomeBuilder);
        return biome(Biome.Precipitation.RAIN, 0.7F, 0.65F,
                spawnBuilder, biomeBuilder, NORMAL_MUSIC);
    }

    public static Biome graveyard()
    {
        MobSpawnSettings.Builder spawnBuilder = new MobSpawnSettings.Builder();
        BiomeDefaultFeatures.commonSpawns(spawnBuilder);
        BiomeGenerationSettings.Builder biomeBuilder = new BiomeGenerationSettings.Builder();
        globalOverworldGeneration(biomeBuilder);
        BiomeDefaultFeatures.addDefaultSoftDisks(biomeBuilder);
        return biome(Biome.Precipitation.RAIN, 0.65F, 0.8F,
                spawnBuilder, biomeBuilder, NORMAL_MUSIC);
    }

}
