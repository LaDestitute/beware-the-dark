package com.ladestitute.bewarethedark.world.biome;

import com.ladestitute.bewarethedark.registries.BiomeInit;
import com.ladestitute.bewarethedark.registries.BlockInit;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.SurfaceRules;

public class BTDSurfaceRules
{
    private static final SurfaceRules.ConditionSource AT_OR_ABOVE_WATER = SurfaceRules.waterBlockCheck(-1, 0);

    private static final SurfaceRules.RuleSource ROCKYLAND_SURFACE = SurfaceRules.sequence(
            SurfaceRules.ifTrue(AT_OR_ABOVE_WATER, SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR,
                    SurfaceRules.state(BlockInit.ROCKY_TURF.get().defaultBlockState()))),
            SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, SurfaceRules.state(
                    BlockInit.ROCKY_TURF.get().defaultBlockState())));

    private static final SurfaceRules.RuleSource ROCKYLAND = SurfaceRules.ifTrue(
            SurfaceRules.isBiome(BiomeInit.ROCKYLAND),
            SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.steep(),
                    SurfaceRules.state(Blocks.DIRT.defaultBlockState())), ROCKYLAND_SURFACE));

    private static final SurfaceRules.RuleSource CREEPY_FOREST_SURFACE = SurfaceRules.sequence(
            SurfaceRules.ifTrue(AT_OR_ABOVE_WATER, SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR,
                    SurfaceRules.state(BlockInit.FOREST_TURF.get().defaultBlockState()))),
            SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, SurfaceRules.state(
                    BlockInit.FOREST_TURF.get().defaultBlockState())));

    private static final SurfaceRules.RuleSource CREEPY_FOREST = SurfaceRules.ifTrue(
            SurfaceRules.isBiome(BiomeInit.CREEPY_FOREST),
            SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.steep(),
                    SurfaceRules.state(Blocks.DIRT.defaultBlockState())), CREEPY_FOREST_SURFACE));

    private static final SurfaceRules.RuleSource LUMPY_FOREST_SURFACE = SurfaceRules.sequence(
            SurfaceRules.ifTrue(AT_OR_ABOVE_WATER, SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR,
                    SurfaceRules.state(BlockInit.FOREST_TURF.get().defaultBlockState()))),
            SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, SurfaceRules.state(
                    BlockInit.FOREST_TURF.get().defaultBlockState())));

    private static final SurfaceRules.RuleSource LUMPY_FOREST = SurfaceRules.ifTrue(
            SurfaceRules.isBiome(BiomeInit.LUMPY_FOREST),
            SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.steep(),
                    SurfaceRules.state(Blocks.DIRT.defaultBlockState())), LUMPY_FOREST_SURFACE));

    private static final SurfaceRules.RuleSource DECIDUOUS_FOREST_SURFACE = SurfaceRules.sequence(
            SurfaceRules.ifTrue(AT_OR_ABOVE_WATER, SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR,
                    SurfaceRules.state(BlockInit.DECIDUOUS_TURF.get().defaultBlockState()))),
            SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, SurfaceRules.state(
                    BlockInit.DECIDUOUS_TURF.get().defaultBlockState())));

    private static final SurfaceRules.RuleSource DECIDUOUS_FOREST = SurfaceRules.ifTrue(
            SurfaceRules.isBiome(BiomeInit.DECIDUOUS_FOREST),
            SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.steep(),
                    SurfaceRules.state(Blocks.DIRT.defaultBlockState())), DECIDUOUS_FOREST_SURFACE));

    private static final SurfaceRules.RuleSource MARSH_SURFACE = SurfaceRules.sequence(
            SurfaceRules.ifTrue(AT_OR_ABOVE_WATER, SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR,
                    SurfaceRules.state(BlockInit.MARSH_TURF.get().defaultBlockState()))),
            SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, SurfaceRules.state(
                    BlockInit.MARSH_TURF.get().defaultBlockState())));

    private static final SurfaceRules.RuleSource MARSH = SurfaceRules.ifTrue(
            SurfaceRules.isBiome(BiomeInit.MARSH),
            SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.steep(),
                    SurfaceRules.state(Blocks.DIRT.defaultBlockState())), MARSH_SURFACE));

    private static final SurfaceRules.RuleSource GRAVEYARD_SURFACE = SurfaceRules.sequence(
            SurfaceRules.ifTrue(AT_OR_ABOVE_WATER, SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR,
                    SurfaceRules.state(BlockInit.FOREST_TURF.get().defaultBlockState()))),
            SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, SurfaceRules.state(
                    Blocks.DIRT.defaultBlockState())));

    private static final SurfaceRules.RuleSource GRAVEYARD = SurfaceRules.ifTrue(
            SurfaceRules.isBiome(BiomeInit.GRAVEYARD),
            SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.steep(),
                    SurfaceRules.state(Blocks.DIRT.defaultBlockState())), GRAVEYARD_SURFACE));

    private static final SurfaceRules.RuleSource OVERWORLD =
            SurfaceRules.ifTrue(SurfaceRules.abovePreliminarySurface(),
                    SurfaceRules.sequence(ROCKYLAND, CREEPY_FOREST, LUMPY_FOREST, DECIDUOUS_FOREST, MARSH, GRAVEYARD));



    public static final SurfaceRules.RuleSource OVERWORLD_SURFACE_RULES =
            SurfaceRules.sequence(OVERWORLD);
}
