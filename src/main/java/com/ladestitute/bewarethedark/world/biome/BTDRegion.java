package com.ladestitute.bewarethedark.world.biome;

import com.ladestitute.bewarethedark.registries.BiomeInit;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import com.mojang.datafixers.util.Pair;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.biome.Climate;
import terrablender.api.Region;
import terrablender.api.RegionType;

import java.util.function.Consumer;

public class BTDRegion extends Region {
    public BTDRegion(ResourceLocation name, RegionType type, int weight) {
        super(name, type, weight);
    }

    @Override
    public void addBiomes(Registry<Biome> registry, Consumer<Pair<Climate.ParameterPoint, ResourceKey<Biome>>> mapper) {
        this.addModifiedVanillaOverworldBiomes(mapper, builder -> {
            builder.replaceBiome(Biomes.BADLANDS, BiomeInit.ROCKYLAND);
            builder.replaceBiome(Biomes.SWAMP, BiomeInit.MARSH);
            builder.replaceBiome(Biomes.BIRCH_FOREST, BiomeInit.DECIDUOUS_FOREST);
            builder.replaceBiome(Biomes.ERODED_BADLANDS, BiomeInit.ROCKYLAND);
            builder.replaceBiome(Biomes.WOODED_BADLANDS, BiomeInit.ROCKYLAND);
            builder.replaceBiome(Biomes.OLD_GROWTH_BIRCH_FOREST, BiomeInit.DECIDUOUS_FOREST);
            builder.replaceBiome(Biomes.OLD_GROWTH_SPRUCE_TAIGA, BiomeInit.CREEPY_FOREST);
            builder.replaceBiome(Biomes.FLOWER_FOREST, BiomeInit.CREEPY_FOREST);
            builder.replaceBiome(Biomes.FOREST, BiomeInit.CREEPY_FOREST);
            builder.replaceBiome(Biomes.TAIGA, BiomeInit.CREEPY_FOREST);
            builder.replaceBiome(Biomes.DARK_FOREST, BiomeInit.CREEPY_FOREST);
            builder.replaceBiome(Biomes.WINDSWEPT_FOREST, BiomeInit.LUMPY_FOREST);
            builder.replaceBiome(Biomes.OLD_GROWTH_PINE_TAIGA, BiomeInit.LUMPY_FOREST);
            builder.replaceBiome(Biomes.TAIGA, BiomeInit.GRAVEYARD);
            builder.replaceBiome(Biomes.PLAINS, BiomeInit.GRAVEYARD);
            builder.replaceBiome(Biomes.WINDSWEPT_GRAVELLY_HILLS, BiomeInit.ROCKYLAND);
            builder.replaceBiome(Biomes.STONY_SHORE, BiomeInit.ROCKYLAND);
        });
    }


}
