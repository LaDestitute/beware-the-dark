package com.ladestitute.bewarethedark.network;

import com.ladestitute.bewarethedark.BTDMain;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraftforge.network.NetworkDirection;
import net.minecraftforge.network.NetworkRegistry;
import net.minecraftforge.network.PacketDistributor;
import net.minecraftforge.network.simple.SimpleChannel;

import java.util.function.Function;

public class NetworkingHandler {
    public static SimpleChannel INSTANCE;

    private static int packetId = 0;
    private static int id() {
        return packetId++;
    }

    public static void register() {
        SimpleChannel net = NetworkRegistry.ChannelBuilder
                .named(new ResourceLocation(BTDMain.MOD_ID, "messages"))
                .networkProtocolVersion(() -> "1.0")
                .clientAcceptedVersions(s -> true)
                .serverAcceptedVersions(s -> true)
                .simpleChannel();

        INSTANCE = net;

        net.messageBuilder(ItemStackRenderUpdatePacket.class, id(), NetworkDirection.PLAY_TO_CLIENT)
                .decoder(ItemStackRenderUpdatePacket::new)
                .encoder(ItemStackRenderUpdatePacket::toBytes)
                .consumerMainThread(ItemStackRenderUpdatePacket::handle)
                .add();

        net.messageBuilder(CrockPotStackRenderUpdatePacket.class, id(), NetworkDirection.PLAY_TO_CLIENT)
                .decoder(CrockPotStackRenderUpdatePacket::new)
                .encoder(CrockPotStackRenderUpdatePacket::toBytes)
                .consumerMainThread(CrockPotStackRenderUpdatePacket::handle)
                .add();

        net.messageBuilder(ClientboundPlayerSanityUpdateMessage.class, id(), NetworkDirection.PLAY_TO_CLIENT)
                .decoder(ClientboundPlayerSanityUpdateMessage::new)
                .encoder(ClientboundPlayerSanityUpdateMessage::toBytes)
                .consumerMainThread(ClientboundPlayerSanityUpdateMessage::handle)
                .add();

    }




    public static <MSG> void sendToServer(MSG message) {
        INSTANCE.sendToServer(message);
    }

    public static <MSG> void sendToPlayer(MSG message, ServerPlayer player) {
        INSTANCE.send(PacketDistributor.PLAYER.with(() -> player), message);
    }

    public static <MSG> void sendToClients(MSG message) {
        INSTANCE.send(PacketDistributor.ALL.noArg(), message);
    }
}
