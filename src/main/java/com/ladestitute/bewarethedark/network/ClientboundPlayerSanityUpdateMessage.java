package com.ladestitute.bewarethedark.network;

import com.ladestitute.bewarethedark.client.ClientSanityData;
import net.minecraft.network.FriendlyByteBuf;
import net.minecraftforge.network.NetworkEvent;

import java.util.function.Supplier;

public class ClientboundPlayerSanityUpdateMessage {
    private final int firstspawn;
    private final int sanity;
    private final int nightdrain;
    private final int raindrain;
    private final int spiderdrain;
    private final int darknessdrain;
    private final int ghostdrain;
    private final int shadowcreaturedrain;
    private final int poisondrain;
    private final int garlandtimer;

    public ClientboundPlayerSanityUpdateMessage(int firstspawn, int sanity, int nightdrain,
                                                int raindrain, int spiderdrain, int darknessdrain,
                                                int ghostdrain, int shadowcreaturedrain, int poisondrain,
                                                int garlandtimer) {
        this.firstspawn = firstspawn;
        this.sanity = sanity;
        this.nightdrain = nightdrain;
        this.raindrain = raindrain;
        this.spiderdrain = spiderdrain;
        this.darknessdrain = darknessdrain;
        this.ghostdrain = ghostdrain;
        this.shadowcreaturedrain = shadowcreaturedrain;
        this.poisondrain = poisondrain;
        this.garlandtimer = garlandtimer;
    }

    public ClientboundPlayerSanityUpdateMessage(FriendlyByteBuf buf) {
        this.firstspawn = buf.readInt();
        this.sanity = buf.readInt();
        this.nightdrain = buf.readInt();
        this.raindrain = buf.readInt();
        this.spiderdrain = buf.readInt();
        this.darknessdrain = buf.readInt();
        this.ghostdrain = buf.readInt();
        this.shadowcreaturedrain = buf.readInt();
        this.poisondrain = buf.readInt();
        this.garlandtimer = buf.readInt();
    }

    public void toBytes(FriendlyByteBuf buf) {
        buf.writeInt(firstspawn);
        buf.writeInt(sanity);
        buf.writeInt(nightdrain);
        buf.writeInt(raindrain);
        buf.writeInt(spiderdrain);
        buf.writeInt(darknessdrain);
        buf.writeInt(ghostdrain);
        buf.writeInt(shadowcreaturedrain);
        buf.writeInt(poisondrain);
        buf.writeInt(garlandtimer);
    }

    public boolean handle(Supplier<NetworkEvent.Context> supplier) {
        NetworkEvent.Context context = supplier.get();
        context.enqueueWork(() -> {
            // HERE WE ARE ON THE CLIENT!
            ClientSanityData.set(firstspawn, sanity, nightdrain, raindrain, spiderdrain, darknessdrain, ghostdrain, shadowcreaturedrain, poisondrain, garlandtimer);
        });
        return true;
    }
}

