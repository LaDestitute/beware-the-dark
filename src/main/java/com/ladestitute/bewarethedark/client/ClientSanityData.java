package com.ladestitute.bewarethedark.client;

public class ClientSanityData {
    private static int firstspawn;
    private static int playerSanity;
    private static int nightdrain;
    private static int raindrain;
    private static int spiderdrain;
    private static int darknessdrain;
    private static int ghostdrain;
    private static int poisondrain;
    private static int shadowcreaturedrain;
    private static int garlandtimer;

    public static void set(int firstspawn, int sanity, int nightdrain, int raindrain, int spiderdrain, int darknessdrain, int ghostdrain, int poisondrain, int shadowcreaturedrain, int garlandtimer) {
        ClientSanityData.firstspawn = firstspawn;
        ClientSanityData.playerSanity = sanity;
        ClientSanityData.nightdrain = nightdrain;
        ClientSanityData.raindrain = raindrain;
        ClientSanityData.spiderdrain = spiderdrain;
        ClientSanityData.darknessdrain = darknessdrain;
        ClientSanityData.ghostdrain = ghostdrain;
        ClientSanityData.poisondrain = poisondrain;
        ClientSanityData.shadowcreaturedrain = shadowcreaturedrain;
        ClientSanityData.garlandtimer = garlandtimer;
    }

    public static int getPlayerSanity() {
        return playerSanity;
    }


}
