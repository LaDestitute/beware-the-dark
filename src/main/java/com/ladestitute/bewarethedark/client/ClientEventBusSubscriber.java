package com.ladestitute.bewarethedark.client;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.blocks.entity.renderer.CrockPotBlockEntityRenderer;
import com.ladestitute.bewarethedark.blocks.entity.renderer.DryingRackBlockEntityRenderer;
import com.ladestitute.bewarethedark.client.model.*;
import com.ladestitute.bewarethedark.client.render.*;
import com.ladestitute.bewarethedark.client.ui.SanityHUDOverlay;
import com.ladestitute.bewarethedark.registries.*;
import com.ladestitute.bewarethedark.util.BTDKeyboardUtil;
import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.ThrownItemRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.client.event.RegisterGuiOverlaysEvent;
import net.minecraftforge.client.event.RegisterKeyMappingsEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

@Mod.EventBusSubscriber(modid = BTDMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientEventBusSubscriber {


    @SubscribeEvent
    public static void onStaticClientSetup(FMLClientSetupEvent event) {
        event.setPhase(EventPriority.HIGH);
        ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.FIRE_PIT.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.CAMP_FIRE.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.DRYING_RACK.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.CROCK_POT.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.HOLLOW_STUMP.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.SINKHOLE.get(), RenderType.translucent());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.WOOD_LANTERN.get(), RenderType.translucent());
//        ItemBlockRenderTypes.setRenderLayer(BlockInit.EVERGREEN_SAPLING.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.BIRCHNUT_SAPLING_ORANGE.get(), RenderType.cutout());
       ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.BIRCHNUT_SAPLING_RED.get(), RenderType.cutout());
       ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.BIRCHNUT_SAPLING_YELLOW.get(), RenderType.cutout());
       ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.BIRCHNUT_SAPLING_GREEN.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.BERRY_BUSH.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.SAPLING_PLANT.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.GRASS_TUFT.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.MARSH_POND_PLANT.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.SPIKY_BUSH.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(SpecialBlockInit.REEDS.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.RED_MUSHROOM.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.GREEN_MUSHROOM.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.BLUE_MUSHROOM.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.LIGHT_FLOWER.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.CAVE_FERN_VARIANT_ONE.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.CAVE_FERN_VARIANT_TWO.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.CAVE_FERN_VARIANT_THREE.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.CAVE_FERN_VARIANT_FOUR.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.CAVE_FERN_VARIANT_FIVE.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.CAVE_FERN_VARIANT_SIX.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.CAVE_FERN_VARIANT_SEVEN.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.CAVE_FERN_VARIANT_EIGHT.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.CAVE_FERN_VARIANT_NINE.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.CAVE_FERN_VARIANT_TEN.get(), RenderType.cutout());
        ItemBlockRenderTypes.setRenderLayer(BlockInit.EVERGREEN_SAPLING.get(), RenderType.cutout());
    }

    @Mod.EventBusSubscriber(modid = BTDMain.MOD_ID, value = Dist.CLIENT, bus = Mod.EventBusSubscriber.Bus.MOD)
    public static class ClientModBusEvents {

        @SubscribeEvent
        public static void registerRenderers(final EntityRenderersEvent.RegisterRenderers event) {
            event.registerBlockEntityRenderer(BlockEntityInit.DRYING_RACK.get(),
                    DryingRackBlockEntityRenderer::new);
            event.registerBlockEntityRenderer(BlockEntityInit.CROCK_POT.get(),
                    CrockPotBlockEntityRenderer::new);
        }

        @SubscribeEvent
        public static void onKeyRegister(RegisterKeyMappingsEvent event) {
            event.register(BTDKeyboardUtil.OPEN_BACKPACK);
        }
    }


    @SubscribeEvent
    public static void layerDefinitions(EntityRenderersEvent.RegisterLayerDefinitions event) {
        event.registerLayerDefinition(RenderTumbleweed.MAIN_LAYER, TumbleweedModel::createLayer);
        event.registerLayerDefinition(FireflyModel.LAYER_LOCATION, FireflyModel::createBodyLayer);
        event.registerLayerDefinition(ButterflyModel.LAYER_LOCATION, ButterflyModel::createBodyLayer);
        event.registerLayerDefinition(JungleButterflyModel.LAYER_LOCATION, JungleButterflyModel::createBodyLayer);
        event.registerLayerDefinition(MandrakeModel.LAYER_LOCATION, MandrakeModel::createBodyLayer);
        event.registerLayerDefinition(GhostModel.LAYER_LOCATION, GhostModel::createBodyLayer);
        event.registerLayerDefinition(CatcoonModel.LAYER_LOCATION, CatcoonModel::createBodyLayer);
        event.registerLayerDefinition(MrSkittsModel.LAYER_LOCATION, MrSkittsModel::createBodyLayer);
        event.registerLayerDefinition(NightHandModel.LAYER_LOCATION, NightHandModel::createBodyLayer);
        event.registerLayerDefinition(ShadowWatcherModel.LAYER_LOCATION, ShadowWatcherModel::createBodyLayer);
        event.registerLayerDefinition(PassiveCrawlingHorrorModel.LAYER_LOCATION, PassiveCrawlingHorrorModel::createBodyLayer);
        event.registerLayerDefinition(HostileCrawlingHorrorModel.LAYER_LOCATION, HostileCrawlingHorrorModel::createBodyLayer);
        event.registerLayerDefinition(TerrorbeakModel.LAYER_LOCATION, TerrorbeakModel::createBodyLayer);
        event.registerLayerDefinition(TentacleModel.LAYER_LOCATION, TentacleModel::createBodyLayer);
        event.registerLayerDefinition(BatiliskModel.LAYER_LOCATION, BatiliskModel::createBodyLayer);
    }

    @SubscribeEvent
    public static void entityRenderers(EntityRenderersEvent.RegisterRenderers event) {
        event.registerEntityRenderer(EntityInit.TUMBLEWEED.get(), RenderTumbleweed::new);
        event.registerEntityRenderer(EntityInit.FIREFLY.get(), RenderFirefly::new);
        event.registerEntityRenderer(EntityInit.BUTTERFLY.get(), RenderButterfly::new);
        event.registerEntityRenderer(EntityInit.JUNGLE_BUTTERFLY.get(), RenderJungleButterfly::new);
        event.registerEntityRenderer(EntityInit.MANDRAKE.get(), RenderMandrake::new);
        event.registerEntityRenderer(EntityInit.GHOST.get(), RenderGhost::new);
        event.registerEntityRenderer(EntityInit.CATCOON.get(), RenderCatcoon::new);
        event.registerEntityRenderer(EntityInit.MR_SKITTS.get(), RenderMrSkitts::new);
        event.registerEntityRenderer(EntityInit.NIGHT_HAND.get(), RenderNightHand::new);
        event.registerEntityRenderer(EntityInit.SHADOW_WATCHER.get(), RenderShadowWatcher::new);
        event.registerEntityRenderer(EntityInit.PASSIVE_CRAWLING_HORROR.get(), RenderPassiveCrawlingHorror::new);
        event.registerEntityRenderer(EntityInit.HOSTILE_CRAWLING_HORROR.get(), RenderHostileCrawlingHorror::new);
        event.registerEntityRenderer(EntityInit.TERRORBEAK.get(), RenderTerrorbeak::new);
        event.registerEntityRenderer(EntityInit.TENTACLE.get(), RenderTentacle::new);
        event.registerEntityRenderer(EntityInit.BATILISK.get(), RenderBatilisk::new);
        event.registerEntityRenderer(EntityInit.DART.get(),
                ThrownItemRenderer::new);
        event.registerEntityRenderer(EntityInit.FIRE_DART.get(),
                ThrownItemRenderer::new);
        event.registerEntityRenderer(EntityInit.ELECTRIC_DART.get(),
                ThrownItemRenderer::new);
        event.registerEntityRenderer(EntityInit.POISON_DART.get(),
                ThrownItemRenderer::new);
    }

    @SubscribeEvent
    public static void registerGuiOverlays(RegisterGuiOverlaysEvent event) {
        event.registerAboveAll("sanity", SanityHUDOverlay.HUD_SANITY);
    }

}

