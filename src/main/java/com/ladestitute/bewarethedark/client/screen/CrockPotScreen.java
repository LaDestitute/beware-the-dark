package com.ladestitute.bewarethedark.client.screen;

import com.ladestitute.bewarethedark.BTDMain;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.gui.screens.inventory.AbstractContainerScreen;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.player.Inventory;

public class CrockPotScreen extends AbstractContainerScreen<CrockPotMenu> {
    private static final ResourceLocation TEXTURE =
            new ResourceLocation(BTDMain.MOD_ID, "textures/gui/crock_pot_gui.png");

    public CrockPotScreen(CrockPotMenu menu, Inventory playerInventory, Component title) {
        super(menu, playerInventory, title);
        this.imageHeight = 168;
        this.inventoryLabelY = this.imageHeight - 94;
    }

    @Override
    public Component getTitle() {
        return Component.literal("Crock Pot");
    }

    @Override
    public void render(PoseStack poseStack, int mouseX, int mouseY, float partialTick) {
        this.renderBackground(poseStack);
        super.render(poseStack, mouseX, mouseY, partialTick);
        this.renderTooltip(poseStack, mouseX, mouseY);
    }

    @Override
    protected void renderLabels(PoseStack poseStack, int mouseX, int mouseY) {
        Component title = getTitle();
        font.draw(poseStack, title, imageWidth / 2.0F - font.width(title) / 2.0F, (float) titleLabelY, 0x404040);
        font.draw(poseStack, playerInventoryTitle, (float) inventoryLabelX, (float) inventoryLabelY, 0x404040);
    }

    @Override
    protected void renderBg(PoseStack poseStack, float partialTicks, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, TEXTURE);

        // Draw Background
        blit(poseStack, leftPos, topPos, 0, 0, imageWidth, imageHeight);

        // Draw Input Slots
        blit(poseStack, leftPos + 38, topPos + 16, 176, 97, 36, 36);

        // Draw Process Arrow
        blit(poseStack, leftPos + 80, topPos + 44, 176, 63, 24, 17);


        // Draw Output Slots
        blit(poseStack, leftPos + 112, topPos + 39, 176, 133, 26, 26);
    }
}
