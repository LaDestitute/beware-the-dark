package com.ladestitute.bewarethedark.client.render;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.client.model.CatcoonModel;
import com.ladestitute.bewarethedark.client.model.GhostModel;
import com.ladestitute.bewarethedark.entities.mobs.hostile.GhostEntity;
import com.ladestitute.bewarethedark.entities.mobs.neutral.CatcoonEntity;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;

import javax.annotation.Nullable;

public class RenderCatcoon extends MobRenderer<CatcoonEntity, CatcoonModel> {

    private static final ResourceLocation TEXTURE = new ResourceLocation(BTDMain.MOD_ID, "textures/entity/catcoon.png");

    //In +1.18, we now pass a LAYER_LOCATION (see the explanation in the entityModel) and bake it in
    //using the renderer's EntityRendererProvider.Context in the entity's renderer and pass it through to the constructor
    public RenderCatcoon(EntityRendererProvider.Context context) {
        super(context, new CatcoonModel(context.getModelSet().bakeLayer(CatcoonModel.LAYER_LOCATION)), 0.50f);
    }

    @Nullable
    @Override
    public ResourceLocation getTextureLocation(CatcoonEntity entity) {
        return TEXTURE;
    }
}
