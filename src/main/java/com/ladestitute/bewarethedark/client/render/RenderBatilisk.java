package com.ladestitute.bewarethedark.client.render;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.client.model.BatiliskModel;
import com.ladestitute.bewarethedark.client.model.GhostModel;
import com.ladestitute.bewarethedark.entities.mobs.hostile.BatiliskEntity;
import com.ladestitute.bewarethedark.entities.mobs.hostile.GhostEntity;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;

import javax.annotation.Nullable;

public class RenderBatilisk extends MobRenderer<BatiliskEntity, BatiliskModel> {

    private static final ResourceLocation TEXTURE = new ResourceLocation(BTDMain.MOD_ID, "textures/entity/batilisk.png");

    //In +1.18, we now pass a LAYER_LOCATION (see the explanation in the entityModel) and bake it in
    //using the renderer's EntityRendererProvider.Context in the entity's renderer and pass it through to the constructor
    public RenderBatilisk(EntityRendererProvider.Context context) {
        super(context, new BatiliskModel(context.getModelSet().bakeLayer(BatiliskModel.LAYER_LOCATION)), 0.25f);
    }

    @Nullable
    @Override
    public ResourceLocation getTextureLocation(BatiliskEntity entity) {
        return TEXTURE;
    }
}
