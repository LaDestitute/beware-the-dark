package com.ladestitute.bewarethedark.client.render;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.client.model.GhostModel;
import com.ladestitute.bewarethedark.entities.mobs.hostile.GhostEntity;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;

import javax.annotation.Nullable;

public class RenderGhost extends MobRenderer<GhostEntity, GhostModel> {

    private static final ResourceLocation TEXTURE = new ResourceLocation(BTDMain.MOD_ID, "textures/entity/ghost.png");

    //In +1.18, we now pass a LAYER_LOCATION (see the explanation in the entityModel) and bake it in
    //using the renderer's EntityRendererProvider.Context in the entity's renderer and pass it through to the constructor
    public RenderGhost(EntityRendererProvider.Context context) {
        super(context, new GhostModel(context.getModelSet().bakeLayer(GhostModel.LAYER_LOCATION)), 0.25f);
    }

    @Nullable
    @Override
    public ResourceLocation getTextureLocation(GhostEntity entity) {
        return TEXTURE;
    }
}

