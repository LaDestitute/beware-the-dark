package com.ladestitute.bewarethedark.client.render;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.client.model.MandrakeModel;
import com.ladestitute.bewarethedark.entities.mobs.passive.MandrakeEntity;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;

import javax.annotation.Nullable;

public class RenderMandrake extends MobRenderer<MandrakeEntity, MandrakeModel> {

    private static final ResourceLocation TEXTURE = new ResourceLocation(BTDMain.MOD_ID, "textures/entity/mandrake.png");

    //In +1.18, we now pass a LAYER_LOCATION (see the explanation in the entityModel) and bake it in
    //using the renderer's EntityRendererProvider.Context in the entity's renderer and pass it through to the constructor
    public RenderMandrake(EntityRendererProvider.Context context) {
        super(context, new MandrakeModel(context.getModelSet().bakeLayer(MandrakeModel.LAYER_LOCATION)), 0.25f);
    }

    @Nullable
    @Override
    public ResourceLocation getTextureLocation(MandrakeEntity entity) {
        return TEXTURE;
    }
}
