package com.ladestitute.bewarethedark.client.render;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.client.model.GhostModel;
import com.ladestitute.bewarethedark.client.model.TentacleModel;
import com.ladestitute.bewarethedark.entities.mobs.hostile.GhostEntity;
import com.ladestitute.bewarethedark.entities.mobs.hostile.TentacleEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.PassiveCrawlingHorrorEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;

import javax.annotation.Nullable;

public class RenderTentacle extends MobRenderer<TentacleEntity, TentacleModel> {

    private static final ResourceLocation TEXTURE = new ResourceLocation(BTDMain.MOD_ID, "textures/entity/tentacle.png");

    //In +1.18, we now pass a LAYER_LOCATION (see the explanation in the entityModel) and bake it in
    //using the renderer's EntityRendererProvider.Context in the entity's renderer and pass it through to the constructor
    public RenderTentacle(EntityRendererProvider.Context context) {
        super(context, new TentacleModel(context.getModelSet().bakeLayer(TentacleModel.LAYER_LOCATION)), 0.25f);
    }

    @Nullable
    @Override
    public ResourceLocation getTextureLocation(TentacleEntity entity) {
        return TEXTURE;
    }

    @Override
    protected void scale(TentacleEntity p_115314_, PoseStack p_115315_, float p_115316_) {
        p_115315_.scale(2.1f, 2.1f, 2.1f);
        super.scale(p_115314_, p_115315_, p_115316_);
    }
}
