package com.ladestitute.bewarethedark.client.render;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.client.model.MandrakeModel;
import com.ladestitute.bewarethedark.client.model.MrSkittsModel;
import com.ladestitute.bewarethedark.entities.mobs.passive.MandrakeEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.MrSkittsEntity;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.PoseStack;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;
import org.lwjgl.opengl.GL11;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RenderMrSkitts extends MobRenderer<MrSkittsEntity, MrSkittsModel> {

    private static final ResourceLocation TEXTURE = new ResourceLocation(BTDMain.MOD_ID, "textures/entity/shadow_creature.png");

    //In +1.18, we now pass a LAYER_LOCATION (see the explanation in the entityModel) and bake it in
    //using the renderer's EntityRendererProvider.Context in the entity's renderer and pass it through to the constructor
    public RenderMrSkitts(EntityRendererProvider.Context context) {
        super(context, new MrSkittsModel(context.getModelSet().bakeLayer(MrSkittsModel.LAYER_LOCATION)), 0.0f);
    }

    @Nullable
    @Override
    public ResourceLocation getTextureLocation(MrSkittsEntity entity) {
        return TEXTURE;
    }

    @Override
    public void render(@Nonnull MrSkittsEntity entity, float entityYaw, float partialTicks,
                       @Nonnull PoseStack matrixStack, @Nonnull MultiBufferSource bufferIn, int packedLightIn) {
        RenderSystem.enableBlend();
        RenderSystem.blendFunc(GlStateManager.SourceFactor.SRC_ALPHA, GlStateManager.DestFactor.ONE_MINUS_SRC_ALPHA);
        super.render(entity, entityYaw, partialTicks, matrixStack, bufferIn, packedLightIn);
    }

    @Override
    protected void scale(MrSkittsEntity p_115314_, PoseStack p_115315_, float p_115316_) {
        p_115315_.scale(0.7f, 0.7f, 0.7f);
        super.scale(p_115314_, p_115315_, p_115316_);
    }
}

