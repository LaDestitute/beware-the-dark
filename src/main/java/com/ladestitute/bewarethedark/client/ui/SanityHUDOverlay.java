package com.ladestitute.bewarethedark.client.ui;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.client.ClientSanityData;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import com.mojang.blaze3d.systems.RenderSystem;
import net.minecraft.client.Minecraft;
import net.minecraft.client.Options;
import net.minecraft.client.gui.GuiComponent;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.client.gui.overlay.IGuiOverlay;

public class SanityHUDOverlay {
    private static final ResourceLocation FILLED_SANITY = new ResourceLocation(BTDMain.MOD_ID,
            "textures/sanity/filled_sanity.png");
    private static final ResourceLocation EMPTY_SANITY = new ResourceLocation(BTDMain.MOD_ID,
            "textures/sanity/empty_sanity.png");

    public static final IGuiOverlay HUD_SANITY = ((gui, poseStack, partialTick, width, height) -> {
        Minecraft mc = Minecraft.getInstance();
        int x = width / 2;
        int y = height;

        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        assert mc.player != null;

            RenderSystem.setShaderTexture(0, EMPTY_SANITY);

        for(int i = 0; i < 20; i++) {
            assert mc.player != null;
            if(mc.gameMode.canHurtPlayer()) {
                GuiComponent.blit(poseStack, x - 91 + (i * 9), y - 234, 0, 0, 14, 14,
                        14, 14);
            }
        }


            RenderSystem.setShaderTexture(0, FILLED_SANITY);

        for(int i = 0; i < 20; i++) {
            if(ClientSanityData.getPlayerSanity() > i && mc.gameMode.canHurtPlayer()) {
                GuiComponent.blit(poseStack,x - 91 + (i * 9),y - 234,0,0,14,14,
                        14,14);
            } else {
                break;
            }
        }
        //Gamma option and value accessed via AT
        if(ClientSanityData.getPlayerSanity() <= 12 && mc.gameMode.canHurtPlayer() && BTDConfig.getInstance().enable_sanity_gamma())
        {
            Options options = mc.options;
            if (GameSettingsFunctions.getGamma(options) >= 1.0F) {
                GameSettingsFunctions.setGamma(options, 1.0F);
            }
            double brightergamma = Math.round(1.5F % 28.0F + 1.0F);
                GameSettingsFunctions.setGamma(options, brightergamma);
        }
        if(ClientSanityData.getPlayerSanity() >= 13)
        {
            Options options = mc.options;
            options.gamma().set(1.0);
        }
    });

}
