package com.ladestitute.bewarethedark.client.sound;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.sounds.SimpleSoundInstance;
import net.minecraft.client.resources.sounds.SoundInstance;
import net.minecraft.sounds.SoundSource;

public class AudioHooks {
    public static boolean shouldCancelSound(SoundInstance sound) {

            if (sound.getSource() == SoundSource.MUSIC) {
                return BTDMusicTicker.getSituationalMusic() != null && !sound.getLocation().equals(SimpleSoundInstance.forMusic(BTDMusicTicker.getSituationalMusic().getEvent()).getLocation())
                        || (BTDMusicTicker.getCurrentMusic() != null && !sound.getLocation().equals(BTDMusicTicker.getCurrentMusic().getLocation()));
            }

        return false;
    }

    public static void tick() {
        if (!Minecraft.getInstance().isPaused()) {
            BTDMusicTicker.tick();
        }
    }

    public static void stop() {

            BTDMusicTicker.stopPlaying();

    }
}
