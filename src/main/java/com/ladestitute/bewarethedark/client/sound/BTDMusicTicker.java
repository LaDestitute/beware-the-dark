package com.ladestitute.bewarethedark.client.sound;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.registries.SoundInit;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.TitleScreen;
import net.minecraft.client.gui.screens.WinScreen;
import net.minecraft.client.resources.sounds.SimpleSoundInstance;
import net.minecraft.client.resources.sounds.SoundInstance;
import net.minecraft.client.renderer.texture.Tickable;
import net.minecraft.client.sounds.MusicManager;
import net.minecraft.client.sounds.SoundManager;
import net.minecraft.core.Holder;
import net.minecraft.sounds.Music;
import net.minecraft.sounds.Musics;
import net.minecraft.tags.FluidTags;
import net.minecraft.resources.ResourceKey;
import net.minecraft.sounds.SoundSource;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.Biomes;
import net.minecraft.world.level.dimension.LevelStem;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

import javax.annotation.Nullable;
import java.util.Random;

@OnlyIn(Dist.CLIENT)
public class BTDMusicTicker
{
    private static final RandomSource random = RandomSource.create();
    private static final Minecraft minecraft = Minecraft.getInstance();
    private static final MusicManager musicManager = Minecraft.getInstance().getMusicManager();
    @Nullable
    private static SoundInstance currentMusic;
    private static int nextSongDelay = 100;

    public static void tick() {
        Music music = getSituationalMusic();
        if (music != null) {
            if (currentMusic != null) {
                if (!music.getEvent().getLocation().equals(currentMusic.getLocation()) && music.replaceCurrentMusic()) {
                    minecraft.getSoundManager().stop(currentMusic);
                    nextSongDelay = Mth.nextInt(random, 0, music.getMinDelay() / 2);
                }

                if (!minecraft.getSoundManager().isActive(currentMusic)) {
                    currentMusic = null;
                    nextSongDelay = Math.min(nextSongDelay, Mth.nextInt(random, music.getMinDelay(), music.getMaxDelay()));
                }
            }

            nextSongDelay = Math.min(nextSongDelay, music.getMaxDelay());
            if (currentMusic == null && nextSongDelay-- <= 0) {
                startPlaying(music);
            }
        } else {
            currentMusic = null;
            if (nextSongDelay-- <= 0) {
                nextSongDelay = Math.min(Integer.MAX_VALUE, Mth.nextInt(random, 12000, 24000));
            }
        }
    }

    /**
     * Vanilla copy
     * @see MusicManager#startPlaying(Music)
     */
    public static void startPlaying(Music pSelector) {
        musicManager.stopPlaying(); // non-copy, cancels vanilla music if Aether music starts
        currentMusic = SimpleSoundInstance.forMusic(pSelector.getEvent());
        if (currentMusic.getSound() != SoundManager.EMPTY_SOUND) {
            minecraft.getSoundManager().play(currentMusic);
        }
        nextSongDelay = Integer.MAX_VALUE;
    }

    /**
     * Vanilla copy
     * @see MusicManager#stopPlaying()
     */
    public static void stopPlaying() {
        if (currentMusic != null) {
            minecraft.getSoundManager().stop(currentMusic);
            currentMusic = null;
        }
        nextSongDelay += 100;
    }

    @Nullable
    public static SoundInstance getCurrentMusic() {
        return currentMusic;
    }

    public static Music getSituationalMusic() {
        if (!(minecraft.screen instanceof WinScreen)) {
              if(BTDConfig.getInstance().replace_title_music() && minecraft.screen instanceof TitleScreen)
              {
                  return new Music(SoundInit.DST_TITLE.get(), 12000, 24000, true);
              }
              if(minecraft.player != null) {
                  if(minecraft.player.level.dimension() == BTDMain.CAVES_DIMENSION||
                          minecraft.player.getY() <= 64||
                          minecraft.player.level.getBiome(minecraft.player.blockPosition()).is(Biomes.DEEP_DARK)||
                          minecraft.player.level.getBiome(minecraft.player.blockPosition()).is(Biomes.DRIPSTONE_CAVES)||
                          minecraft.player.level.getBiome(minecraft.player.blockPosition()).is(Biomes.LUSH_CAVES))
                  {
                      return new Music(SoundInit.CAVE_WORK.get(), 12000, 24000, true);
                  }
                  if(minecraft.player.level.dimension() == Level.OVERWORLD)
                  {
                      int roll = random.nextInt(2);
                      if (roll == 0) {
                          return new Music(SoundInit.AUTUMN_WORK.get(), 12000, 24000, true);
                      }
                      if (roll == 1) {
                          return new Music(SoundInit.AUTUMN_WORK_DST.get(), 12000, 24000, true);
                      }
                  }
              }


        }
        return null;
    }

}
