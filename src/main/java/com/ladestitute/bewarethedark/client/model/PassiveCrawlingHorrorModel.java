package com.ladestitute.bewarethedark.client.model;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.entities.mobs.shadow.PassiveCrawlingHorrorEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

public class PassiveCrawlingHorrorModel extends EntityModel<PassiveCrawlingHorrorEntity> {
    public static final String BODY = "body";
    // This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
    public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(BTDMain.MOD_ID, "passive_crawling_horror"), BODY);
    private final ModelPart leftFrontLeg;
    private final ModelPart body;
    private final ModelPart rightFrontLeg;
    private final ModelPart leftHindLeg;
    private final ModelPart rightHindLeg;

    public PassiveCrawlingHorrorModel(ModelPart root) {
        super(RenderType::entityTranslucent);
        this.leftFrontLeg = root.getChild("leftFrontLeg");
        this.body = root.getChild("body");
        this.rightFrontLeg = root.getChild("rightFrontLeg");
        this.leftHindLeg = root.getChild("leftHindLeg");
        this.rightHindLeg = root.getChild("rightHindLeg");
    }

    public static LayerDefinition createBodyLayer() {
        MeshDefinition meshdefinition = new MeshDefinition();
        PartDefinition partdefinition = meshdefinition.getRoot();

        PartDefinition leftFrontLeg = partdefinition.addOrReplaceChild("leftFrontLeg", CubeListBuilder.create().texOffs(160, 26).addBox(-4.5F, -1.0F, -1.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(143, 10).addBox(-3.5F, -1.0F, -4.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(153, 30).addBox(-3.5F, -2.0F, -3.1F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(150, 28).addBox(-3.0F, -7.0F, -3.85F, 6.0F, 5.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(70, 28).addBox(-3.0F, -4.0F, -4.9F, 6.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(88, 26).addBox(-3.0F, -7.0F, -4.9F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(92, 16).addBox(2.5F, -3.0F, -2.6F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(93, 29).addBox(-3.5F, -3.0F, -2.6F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(41, 27).addBox(-2.0F, -11.0F, 3.15F, 1.0F, 1.0F, 0.0F, new CubeDeformation(0.0F))
                .texOffs(41, 27).addBox(1.0F, -11.0F, 3.15F, 1.0F, 1.0F, 0.0F, new CubeDeformation(0.0F))
                .texOffs(41, 27).addBox(-1.0F, -11.0F, 0.15F, 2.0F, 1.0F, 0.0F, new CubeDeformation(0.0F))
                .texOffs(41, 27).addBox(-3.0F, -10.0F, -0.85F, 6.0F, 2.0F, 5.0F, new CubeDeformation(0.0F))
                .texOffs(165, 31).addBox(-5.0F, -5.3F, 0.15F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(165, 31).addBox(-5.0F, -5.0F, -0.85F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(165, 31).addBox(-5.0F, -7.3F, 3.15F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(165, 31).addBox(-5.0F, -7.0F, 2.15F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(165, 31).addBox(4.0F, -7.3F, 3.15F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(165, 31).addBox(4.0F, -7.0F, 2.15F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(165, 31).addBox(4.0F, -5.3F, 0.15F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(165, 31).addBox(4.0F, -5.0F, -0.85F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(87, 21).addBox(-4.0F, -8.0F, -1.85F, 8.0F, 6.0F, 7.0F, new CubeDeformation(0.0F))
                .texOffs(232, 18).addBox(1.0F, -5.0F, -4.9F, 1.0F, 1.0F, 0.0F, new CubeDeformation(0.0F))
                .texOffs(232, 18).addBox(-2.0F, -6.0F, -4.9F, 2.0F, 2.0F, 0.0F, new CubeDeformation(0.0F))
                .texOffs(89, 15).addBox(0.0F, -7.0F, -4.9F, 3.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(94, 19).addBox(2.0F, -5.0F, -4.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(89, 30).addBox(0.0F, -5.0F, -4.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(91, 19).addBox(-3.0F, -6.0F, -4.9F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        PartDefinition rightFrontLeg = partdefinition.addOrReplaceChild("rightFrontLeg", CubeListBuilder.create().texOffs(143, 26).addBox(3.5F, -1.0F, -1.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(158, 15).addBox(2.5F, -1.0F, -4.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(140, 29).addBox(2.5F, -2.0F, -3.1F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        PartDefinition leftHindLeg = partdefinition.addOrReplaceChild("leftHindLeg", CubeListBuilder.create().texOffs(162, 30).addBox(-6.5F, -1.0F, 2.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(161, 28).addBox(-5.5F, -2.0F, 3.9F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        PartDefinition rightHindLeg = partdefinition.addOrReplaceChild("rightHindLeg", CubeListBuilder.create().texOffs(175, 31).addBox(5.5F, -1.0F, 2.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(147, 27).addBox(3.5F, -2.0F, 3.9F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        return LayerDefinition.create(meshdefinition, 256, 64);
    }

    @Override
    public void setupAnim(PassiveCrawlingHorrorEntity p_103866_, float p_103867_, float p_103868_, float p_103869_, float p_103870_, float p_103871_) {
        this.rightHindLeg.zRot = (-(float)Math.PI / 4F);
        this.leftHindLeg.zRot = ((float)Math.PI / 4F);
        this.rightFrontLeg.zRot = (-(float)Math.PI / 4F);
        this.leftFrontLeg.zRot = ((float)Math.PI / 4F);
        this.rightHindLeg.yRot = ((float)Math.PI / 4F);
        this.leftHindLeg.yRot = (-(float)Math.PI / 4F);
        this.rightFrontLeg.yRot = (-(float)Math.PI / 4F);
        this.leftFrontLeg.yRot = ((float)Math.PI / 4F);
        float f3 = -(Mth.cos(p_103867_ * 0.6662F * 2.0F + 0.0F) * 0.4F) * p_103868_;
        float f6 = -(Mth.cos(p_103867_ * 0.6662F * 2.0F + ((float)Math.PI * 1.5F)) * 0.4F) * p_103868_;
        float f7 = Math.abs(Mth.sin(p_103867_ * 0.6662F + 0.0F) * 0.4F) * p_103868_;
        float f10 = Math.abs(Mth.sin(p_103867_ * 0.6662F + ((float)Math.PI * 1.5F)) * 0.4F) * p_103868_;
        this.rightHindLeg.yRot += f3;
        this.leftHindLeg.yRot += -f3;
        this.rightFrontLeg.yRot += f6;
        this.leftFrontLeg.yRot += -f6;
        this.rightHindLeg.zRot += f7;
        this.leftHindLeg.zRot += -f7;
        this.rightFrontLeg.zRot += f10;
        this.leftFrontLeg.zRot += -f10;
    }

    @Override
    public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        leftFrontLeg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        body.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        rightFrontLeg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        leftHindLeg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        rightHindLeg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
    }
}