package com.ladestitute.bewarethedark.client.model;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.entities.mobs.hostile.BatiliskEntity;
import com.ladestitute.bewarethedark.entities.mobs.hostile.TentacleEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

public class BatiliskModel extends EntityModel<BatiliskEntity>  {
    public static final String BODY = "body";
    // This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
    public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(BTDMain.MOD_ID, "batilisk"), BODY);
    private final ModelPart leftWing;
    private final ModelPart rightWing;
   // private final ModelPart leftWingTip;
 //   private final ModelPart rightWingTip;
    private final ModelPart bb_main;

    public BatiliskModel(ModelPart root) {
        this.leftWing = root.getChild("leftWing");
        this.rightWing = root.getChild("rightWing");
//        this.leftWingTip = root.getChild("leftWingTip");
//        this.rightWingTip = root.getChild("rightWingTip");
        this.bb_main = root.getChild("bb_main");
    }

    public static LayerDefinition createBodyLayer() {
        MeshDefinition meshdefinition = new MeshDefinition();
        PartDefinition partdefinition = meshdefinition.getRoot();

        PartDefinition leftWing = partdefinition.addOrReplaceChild("leftWing", CubeListBuilder.create().texOffs(0, 0).mirror().addBox(5.0F, -22.0F, 1.5F, 19.0F, 16.0F, 1.0F, new CubeDeformation(0.0F)).mirror(false)
                .texOffs(26, 47).mirror().addBox(6.0F, -23.0F, 0.5F, 17.0F, 16.0F, 1.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(0.0F, 24.0F, 0.0F));

        PartDefinition rightWingTip_r1 = leftWing.addOrReplaceChild("rightWingTip_r1", CubeListBuilder.create().texOffs(34, 0).addBox(-14.0F, 21.0F, 0.0F, 10.0F, 16.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -3.1416F));

        PartDefinition rightWing = partdefinition.addOrReplaceChild("rightWing", CubeListBuilder.create().texOffs(1, 0).addBox(-23.0F, -22.0F, 1.5F, 18.0F, 16.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(24, 47).addBox(-25.0F, -23.0F, 0.5F, 19.0F, 16.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        PartDefinition leftWingTip_r1 = rightWing.addOrReplaceChild("leftWingTip_r1", CubeListBuilder.create().texOffs(33, 0).addBox(7.0F, 21.0F, 0.0F, 10.0F, 16.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, -3.1416F));

        PartDefinition bb_main = partdefinition.addOrReplaceChild("bb_main", CubeListBuilder.create().texOffs(56, 0).addBox(-4.0F, -23.0F, -2.0F, 3.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(56, 0).mirror().addBox(1.0F, -23.0F, -2.0F, 3.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)).mirror(false)
                .texOffs(0, 17).addBox(-6.0F, -19.0F, -3.0F, 12.0F, 13.0F, 7.0F, new CubeDeformation(0.0F))
                .texOffs(42, 18).addBox(-7.0F, -12.0F, 0.0F, 10.0F, 16.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        return LayerDefinition.create(meshdefinition, 64, 64);
    }

    @Override
    public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        leftWing.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        rightWing.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
      //  leftWingTip.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
      //  rightWingTip.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        bb_main.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
    }

    @Override
    public void setupAnim(BatiliskEntity p_102618_, float p_102619_, float p_102620_, float p_102621_, float p_102622_, float p_102623_) {
      //  this.rightWingTip.setPos(0.0F, 0.0F, 0.0F);
     //   this.leftWingTip.setPos(0.0F, 0.0F, 0.0F);
        this.rightWing.yRot = Mth.cos(p_102621_ * 74.48451F * ((float)Math.PI / 180F)) * (float)Math.PI * 0.25F;
        this.leftWing.yRot = -this.rightWing.yRot;
      //  this.rightWingTip.yRot = this.rightWing.yRot * 0.5F;
       // this.leftWingTip.yRot = -this.rightWing.yRot * 0.5F;
    }
}

