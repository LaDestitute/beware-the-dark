package com.ladestitute.bewarethedark.client.model;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.entities.mobs.shadow.ShadowWatcherEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;

public class ShadowWatcherModel extends EntityModel<ShadowWatcherEntity> {
    public static final String BODY = "body";
    // This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
    public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(BTDMain.MOD_ID, "shadow_watcher"), BODY);
    private final ModelPart body;
    private final ModelPart head;

    public ShadowWatcherModel(ModelPart root) {
        super(RenderType::entityTranslucent);
        this.body = root.getChild("body");
        this.head = root.getChild("head");
    }

    public static LayerDefinition createBodyLayer() {
        MeshDefinition meshdefinition = new MeshDefinition();
        PartDefinition partdefinition = meshdefinition.getRoot();

        PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(144, 0).addBox(-5.0F, -102.0F, -5.4587F, 10.0F, 102.0F, 10.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        PartDefinition head = partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(141, 20).addBox(-4.5F, -53.0F, -39.9587F, 9.0F, 4.0F, 9.0F, new CubeDeformation(0.0F))
                .texOffs(94, 39).addBox(-3.5F, -61.0F, -38.9587F, 7.0F, 8.0F, 7.0F, new CubeDeformation(0.0F))
                .texOffs(100, 3).addBox(-0.5F, -56.0F, -42.9587F, 1.0F, 0.0F, 4.0F, new CubeDeformation(0.0F))
                .texOffs(100, 3).addBox(-0.5F, -55.0F, -31.9587F, 1.0F, 0.0F, 6.0F, new CubeDeformation(0.0F))
                .texOffs(100, 3).addBox(-0.5F, -58.0F, -31.9587F, 1.0F, 0.0F, 4.0F, new CubeDeformation(0.0F))
                .texOffs(100, 3).addBox(-0.5F, -61.5F, -32.9587F, 1.0F, 0.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(100, 3).addBox(-0.5F, -52.5F, -41.9587F, 1.0F, 0.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(20, 18).addBox(-2.5F, -63.0F, -37.9587F, 5.0F, 2.0F, 5.0F, new CubeDeformation(0.0F))
                .texOffs(20, 18).addBox(0.0F, -66.0F, -36.4587F, 0.0F, 3.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -29.0F, 35.0F));

        PartDefinition cube_r1 = head.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(88, 15).addBox(-5.5F, -38.0F, -45.5F, 1.0F, 0.0F, 3.0F, new CubeDeformation(0.0F))
                .texOffs(88, 15).addBox(-5.5F, -35.0F, -40.5F, 1.0F, 0.0F, 3.0F, new CubeDeformation(0.0F))
                .texOffs(88, 15).addBox(-5.5F, -36.0F, -53.5F, 1.0F, 0.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(5.0F, 0.0F, -20.4587F, -0.6545F, 0.0F, 0.0F));

        PartDefinition cube_r2 = head.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(100, 3).addBox(-5.5F, -57.5F, -10.5F, 1.0F, 0.0F, 5.0F, new CubeDeformation(0.0F))
                .texOffs(100, 3).addBox(-5.5F, -60.5F, -8.5F, 1.0F, 0.0F, 5.0F, new CubeDeformation(0.0F))
                .texOffs(100, 3).addBox(-5.5F, -54.5F, -11.5F, 1.0F, 0.0F, 5.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(5.0F, 0.0F, -20.4587F, 0.2182F, 0.0F, 0.0F));

        return LayerDefinition.create(meshdefinition, 256, 64);
    }

    @Override
    public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        body.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        head.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
    }

    @Override
    public void setupAnim(ShadowWatcherEntity p_102618_, float p_102619_, float p_102620_, float p_102621_, float p_102622_, float p_102623_) {

    }
}
