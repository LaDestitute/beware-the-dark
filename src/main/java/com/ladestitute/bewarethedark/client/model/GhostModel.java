package com.ladestitute.bewarethedark.client.model;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.entities.mobs.hostile.GhostEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.resources.ResourceLocation;

public class GhostModel extends EntityModel<GhostEntity> {
    public static final String BODY = "body";
    // This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
    public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(BTDMain.MOD_ID, "ghost"), BODY);
    private final ModelPart bb_main;

    public GhostModel(ModelPart root) {
        this.bb_main = root.getChild("bb_main");
    }

    public static LayerDefinition createBodyLayer() {
        MeshDefinition meshdefinition = new MeshDefinition();
        PartDefinition partdefinition = meshdefinition.getRoot();

        PartDefinition bb_main = partdefinition.addOrReplaceChild("bb_main", CubeListBuilder.create().texOffs(6, -6).mirror().addBox(3.0F, -15.0F, -3.0F, 1.0F, 10.0F, 6.0F, new CubeDeformation(0.0F)).mirror(false)
                .texOffs(5, 0).addBox(-3.0F, -15.0F, 3.0F, 6.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(18, 0).addBox(-3.0F, -15.0F, -4.0F, 6.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(50, 0).addBox(-3.0F, -16.0F, -4.0F, 6.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(50, 0).addBox(3.0F, -16.0F, -3.0F, 1.0F, 1.0F, 6.0F, new CubeDeformation(0.0F))
                .texOffs(50, 0).addBox(-4.0F, -16.0F, -3.0F, 1.0F, 1.0F, 6.0F, new CubeDeformation(0.0F))
                .texOffs(48, 0).addBox(-2.0F, -18.0F, -2.0F, 4.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
                .texOffs(0, 7).addBox(3.0F, -5.0F, -2.0F, 1.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
                .texOffs(0, 7).addBox(-4.0F, -5.0F, -2.0F, 1.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
                .texOffs(5, 11).addBox(-2.0F, -5.0F, 3.0F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(0, 11).addBox(-2.0F, -5.0F, -4.0F, 4.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(50, 0).addBox(-3.0F, -16.0F, 3.0F, 6.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(40, 0).addBox(-3.0F, -17.0F, -3.0F, 6.0F, 2.0F, 6.0F, new CubeDeformation(0.0F))
                .texOffs(0, 8).addBox(-4.0F, -4.0F, -1.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(0, 8).addBox(3.0F, -4.0F, -1.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(5, 12).addBox(-1.0F, -4.0F, 3.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(6, 0).addBox(3.0F, -16.0F, 3.0F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(5, 12).addBox(-1.0F, -4.0F, -4.0F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(6, 0).addBox(3.0F, -16.0F, -4.0F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(6, 0).addBox(-4.0F, -16.0F, -4.0F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(6, 0).addBox(-4.0F, -16.0F, 3.0F, 1.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(6, -6).mirror().addBox(-4.0F, -15.0F, -3.0F, 1.0F, 10.0F, 6.0F, new CubeDeformation(0.0F)).mirror(false), PartPose.offset(0.0F, 24.0F, 0.0F));

        return LayerDefinition.create(meshdefinition, 64, 16);
    }

    @Override
    public void setupAnim(GhostEntity p_102618_, float p_102619_, float p_102620_, float p_102621_, float p_102622_, float p_102623_) {

    }

    @Override
    public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        bb_main.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
    }
}