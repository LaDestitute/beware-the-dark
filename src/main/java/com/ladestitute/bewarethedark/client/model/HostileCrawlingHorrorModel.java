package com.ladestitute.bewarethedark.client.model;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.entities.mobs.shadow.HostileCrawlingHorrorEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.PassiveCrawlingHorrorEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

public class HostileCrawlingHorrorModel extends EntityModel<HostileCrawlingHorrorEntity> {
    public static final String BODY = "body";
    // This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
    public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(BTDMain.MOD_ID, "hostile_crawling_horror"), BODY);
    private final ModelPart leftFrontLeg;
    private final ModelPart body;
    private final ModelPart rightFrontLeg;
    private final ModelPart leftHindLeg;
    private final ModelPart rightHindLeg;

    public HostileCrawlingHorrorModel(ModelPart root) {
        super(RenderType::entityTranslucent);
        this.leftFrontLeg = root.getChild("leftFrontLeg");
        this.body = root.getChild("body");
        this.rightFrontLeg = root.getChild("rightFrontLeg");
        this.leftHindLeg = root.getChild("leftHindLeg");
        this.rightHindLeg = root.getChild("rightHindLeg");
    }

    public static LayerDefinition createBodyLayer() {
        MeshDefinition meshdefinition = new MeshDefinition();
        PartDefinition partdefinition = meshdefinition.getRoot();

        PartDefinition leftFrontLeg = partdefinition.addOrReplaceChild("leftFrontLeg", CubeListBuilder.create().texOffs(155, 29).addBox(-4.5F, -1.0F, -1.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(138, 13).addBox(-3.5F, -1.0F, -4.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(148, 33).addBox(-3.5F, -2.0F, -3.1F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(148, 31).addBox(-3.0F, -7.0F, -3.85F, 6.0F, 5.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(160, 36).addBox(-3.0F, -4.0F, -4.9F, 6.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(160, 18).addBox(-3.0F, -7.0F, -4.9F, 3.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(157, 29).addBox(2.5F, -3.0F, -2.6F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(155, 27).addBox(-3.5F, -3.0F, -2.6F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(158, 29).addBox(-2.0F, -11.0F, 3.15F, 1.0F, 1.0F, 0.0F, new CubeDeformation(0.0F))
                .texOffs(152, 31).addBox(1.0F, -11.0F, 3.15F, 1.0F, 1.0F, 0.0F, new CubeDeformation(0.0F))
                .texOffs(149, 37).addBox(-1.0F, -11.0F, 0.15F, 2.0F, 1.0F, 0.0F, new CubeDeformation(0.0F))
                .texOffs(155, 29).addBox(-3.0F, -10.0F, -0.85F, 6.0F, 2.0F, 5.0F, new CubeDeformation(0.0F))
                .texOffs(163, 34).addBox(-5.0F, -5.3F, 0.15F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(163, 34).addBox(-5.0F, -5.0F, -0.85F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(163, 34).addBox(-5.0F, -7.3F, 3.15F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(163, 34).addBox(-5.0F, -7.0F, 2.15F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(163, 34).addBox(4.0F, -7.3F, 3.15F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(163, 34).addBox(4.0F, -7.0F, 2.15F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(163, 34).addBox(4.0F, -5.3F, 0.15F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(163, 34).addBox(4.0F, -5.0F, -0.85F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(152, 26).addBox(-4.0F, -8.0F, -1.85F, 8.0F, 6.0F, 7.0F, new CubeDeformation(0.0F))
                .texOffs(230, 21).addBox(1.0F, -5.0F, -4.9F, 1.0F, 1.0F, 0.0F, new CubeDeformation(0.0F))
                .texOffs(230, 21).addBox(-2.0F, -6.0F, -4.9F, 2.0F, 2.0F, 0.0F, new CubeDeformation(0.0F))
                .texOffs(161, 28).addBox(0.0F, -7.0F, -4.9F, 3.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(147, 25).addBox(2.0F, -5.0F, -4.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(143, 36).addBox(0.0F, -5.0F, -4.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(145, 23).addBox(-3.0F, -6.0F, -4.9F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        PartDefinition rightFrontLeg = partdefinition.addOrReplaceChild("rightFrontLeg", CubeListBuilder.create().texOffs(138, 29).addBox(3.5F, -1.0F, -1.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(153, 18).addBox(2.5F, -1.0F, -4.1F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(135, 32).addBox(2.5F, -2.0F, -3.1F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        PartDefinition rightHindLeg = partdefinition.addOrReplaceChild("rightHindLeg", CubeListBuilder.create().texOffs(170, 34).addBox(5.5F, -1.0F, 2.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(142, 30).addBox(3.5F, -2.0F, 3.9F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        PartDefinition leftHindLeg = partdefinition.addOrReplaceChild("leftHindLeg", CubeListBuilder.create().texOffs(157, 33).addBox(-6.5F, -1.0F, 2.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(156, 31).addBox(-5.5F, -2.0F, 3.9F, 2.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        return LayerDefinition.create(meshdefinition, 256, 64);
    }

    @Override
    public void setupAnim(HostileCrawlingHorrorEntity p_103866_, float p_103867_, float p_103868_, float p_103869_, float p_103870_, float p_103871_) {
        this.rightHindLeg.zRot = (-(float)Math.PI / 4F);
        this.leftHindLeg.zRot = ((float)Math.PI / 4F);
        this.rightFrontLeg.zRot = (-(float)Math.PI / 4F);
        this.leftFrontLeg.zRot = ((float)Math.PI / 4F);
        this.rightHindLeg.yRot = ((float)Math.PI / 4F);
        this.leftHindLeg.yRot = (-(float)Math.PI / 4F);
        this.rightFrontLeg.yRot = (-(float)Math.PI / 4F);
        this.leftFrontLeg.yRot = ((float)Math.PI / 4F);
        float f3 = -(Mth.cos(p_103867_ * 0.6662F * 2.0F + 0.0F) * 0.4F) * p_103868_;
        float f6 = -(Mth.cos(p_103867_ * 0.6662F * 2.0F + ((float)Math.PI * 1.5F)) * 0.4F) * p_103868_;
        float f7 = Math.abs(Mth.sin(p_103867_ * 0.6662F + 0.0F) * 0.4F) * p_103868_;
        float f10 = Math.abs(Mth.sin(p_103867_ * 0.6662F + ((float)Math.PI * 1.5F)) * 0.4F) * p_103868_;
        this.rightHindLeg.yRot += f3;
        this.leftHindLeg.yRot += -f3;
        this.rightFrontLeg.yRot += f6;
        this.leftFrontLeg.yRot += -f6;
        this.rightHindLeg.zRot += f7;
        this.leftHindLeg.zRot += -f7;
        this.rightFrontLeg.zRot += f10;
        this.leftFrontLeg.zRot += -f10;
    }

    @Override
    public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        leftFrontLeg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        body.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        rightFrontLeg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        leftHindLeg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        rightHindLeg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
    }
}
