package com.ladestitute.bewarethedark.client.model;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.entities.mobs.shadow.TerrorbeakEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

public class TerrorbeakModel extends EntityModel<TerrorbeakEntity> {
    public static final String BODY = "body";
    // This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
    public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(BTDMain.MOD_ID, "terrorbeak"), BODY);
    private final ModelPart maw;
    private final ModelPart body;
    private final ModelPart right_hind_leg;
    private final ModelPart left_hind_leg;
    private final ModelPart right_front_leg;
    private final ModelPart left_front_leg;

    public TerrorbeakModel(ModelPart root) {
        this.maw = root.getChild("maw");
        this.body = root.getChild("body");
        this.right_hind_leg = root.getChild("right_hind_leg");
        this.left_hind_leg = root.getChild("left_hind_leg");
        this.right_front_leg = root.getChild("right_front_leg");
        this.left_front_leg = root.getChild("left_front_leg");
    }

    public static LayerDefinition createBodyLayer() {
        MeshDefinition meshdefinition = new MeshDefinition();
        PartDefinition partdefinition = meshdefinition.getRoot();

        PartDefinition maw = partdefinition.addOrReplaceChild("maw", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 24.0F, 4.0F, 0.0F, 1.5272F, 0.0F));

        PartDefinition cube_r1 = maw.addOrReplaceChild("cube_r1", CubeListBuilder.create().texOffs(163, 18).addBox(15.3927F, -10.4124F, 6.5628F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(147, 12).addBox(15.3927F, -8.4124F, 5.5628F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(147, 12).addBox(15.3927F, -7.4124F, 5.5628F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(147, 12).addBox(15.3927F, -9.4124F, 5.5628F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(164, 16).addBox(15.3927F, -6.4124F, 5.5628F, 3.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(156, 27).addBox(14.3927F, -10.4124F, 5.5628F, 1.0F, 5.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.0F, 1.0F, 2.0F, 0.6545F, 0.0F, -0.6109F));

        PartDefinition cube_r2 = maw.addOrReplaceChild("cube_r2", CubeListBuilder.create().texOffs(157, 26).addBox(15.3927F, -7.9773F, -10.7362F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(146, 12).addBox(15.3927F, -5.9773F, -9.7362F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(146, 12).addBox(15.3927F, -6.9773F, -9.7362F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(146, 12).addBox(15.3927F, -4.9773F, -9.7362F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(153, 21).addBox(15.3927F, -3.9773F, -10.7362F, 3.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(158, 15).addBox(14.3927F, -7.9773F, -10.7362F, 1.0F, 5.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.0F, 1.0F, 2.0F, -0.6545F, 0.0F, -0.6109F));

        PartDefinition cube_r3 = maw.addOrReplaceChild("cube_r3", CubeListBuilder.create().texOffs(141, 5).addBox(-7.7544F, -17.8874F, -14.4345F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(168, 18).addBox(-7.7544F, -18.8874F, -14.4345F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(152, 15).addBox(-7.7544F, -19.8874F, -14.4345F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(143, 5).addBox(-7.7544F, -20.8874F, -14.4345F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(142, 10).addBox(-7.7544F, -21.8874F, -14.4345F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(154, 18).addBox(-7.7544F, -23.8874F, -14.4345F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(142, 26).addBox(-7.7544F, -22.8874F, -13.4345F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(150, 18).addBox(-8.7544F, -23.8874F, -14.4345F, 1.0F, 8.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.0F, 1.0F, 2.0F, -0.6545F, 0.0F, 0.6109F));

        PartDefinition cube_r4 = maw.addOrReplaceChild("cube_r4", CubeListBuilder.create().texOffs(139, 16).addBox(-7.7544F, -20.3224F, 10.2611F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(129, 12).addBox(-7.7544F, -21.3224F, 10.2611F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(165, 20).addBox(-7.7544F, -22.3224F, 10.2611F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(145, 15).addBox(-7.7544F, -23.3224F, 10.2611F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(154, 8).addBox(-7.7544F, -24.3224F, 10.2611F, 1.0F, 0.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(149, 24).addBox(-7.7544F, -26.3224F, 9.2611F, 5.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(156, 24).addBox(-7.7544F, -25.3224F, 9.2611F, 1.0F, 7.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(155, 24).addBox(-8.7544F, -26.3224F, 9.2611F, 1.0F, 8.0F, 2.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.0F, 1.0F, 2.0F, 0.6545F, 0.0F, 0.6109F));

        PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(160, 23).addBox(-2.0F, -27.0F, -5.0F, 4.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
                .texOffs(169, 20).addBox(-2.0F, -24.0F, -4.0F, 4.0F, 3.0F, 3.0F, new CubeDeformation(0.0F))
                .texOffs(154, 12).addBox(-6.0F, -18.0F, -4.0F, 4.0F, 3.0F, 7.0F, new CubeDeformation(0.0F))
                .texOffs(144, 22).addBox(2.0F, -18.0F, -4.0F, 4.0F, 3.0F, 7.0F, new CubeDeformation(0.0F))
                .texOffs(154, 30).addBox(2.0F, -22.0F, -4.0F, 5.0F, 4.0F, 4.0F, new CubeDeformation(0.0F))
                .texOffs(212, 21).addBox(4.0F, -21.0F, -5.1F, 2.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(212, 21).addBox(-6.0F, -21.0F, -5.1F, 2.0F, 2.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(145, 14).addBox(-7.0F, -22.0F, -4.0F, 5.0F, 4.0F, 4.0F, new CubeDeformation(0.0F))
                .texOffs(162, 26).addBox(-2.0F, -19.0F, -6.0F, 4.0F, 0.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(158, 23).addBox(0.0F, -21.0F, -5.0F, 0.0F, 4.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(150, 21).addBox(-2.0F, -21.0F, -4.0F, 4.0F, 4.0F, 4.0F, new CubeDeformation(0.0F))
                .texOffs(161, 14).addBox(-2.0F, -17.0F, -2.0F, 4.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
                .texOffs(166, 21).addBox(-1.0F, -6.0F, -1.0F, 2.0F, 1.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(156, 15).addBox(-2.0F, -14.0F, -0.5F, 4.0F, 5.0F, 4.0F, new CubeDeformation(0.0F))
                .texOffs(166, 21).addBox(-2.0F, -9.0F, -2.0F, 4.0F, 3.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        PartDefinition cube_r5 = body.addOrReplaceChild("cube_r5", CubeListBuilder.create().texOffs(139, 16).addBox(-3.0F, -19.2456F, -16.3927F, 2.0F, 0.0F, 7.0F, new CubeDeformation(0.0F))
                .texOffs(151, 23).addBox(-3.0F, -15.2456F, -12.3927F, 2.0F, 0.0F, 7.0F, new CubeDeformation(0.0F))
                .texOffs(162, 30).addBox(-3.0F, -12.2456F, -7.3927F, 2.0F, 0.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(2.0F, 1.0F, 1.0F, -0.6109F, 0.0F, 0.0F));

        PartDefinition right_hind_leg = partdefinition.addOrReplaceChild("right_hind_leg", CubeListBuilder.create().texOffs(163, 21).addBox(2.0F, -7.0F, 2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(149, 18).addBox(4.0F, -5.0F, 4.0F, 1.0F, 5.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(160, 31).addBox(2.0F, -6.0F, 2.0F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        PartDefinition left_hind_leg = partdefinition.addOrReplaceChild("left_hind_leg", CubeListBuilder.create().texOffs(163, 21).addBox(-3.0F, -7.0F, 2.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(160, 31).addBox(-5.0F, -6.0F, 2.0F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
                .texOffs(159, 33).addBox(-5.0F, -5.0F, 4.0F, 1.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        PartDefinition right_front_leg = partdefinition.addOrReplaceChild("right_front_leg", CubeListBuilder.create().texOffs(163, 21).addBox(2.0F, -7.0F, -3.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(160, 31).addBox(2.0F, -6.0F, -5.0F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
                .texOffs(153, 18).addBox(4.0F, -5.0F, -5.0F, 1.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        PartDefinition left_front_leg = partdefinition.addOrReplaceChild("left_front_leg", CubeListBuilder.create().texOffs(163, 21).addBox(-3.0F, -7.0F, -3.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(160, 31).addBox(-5.0F, -6.0F, -5.0F, 3.0F, 1.0F, 3.0F, new CubeDeformation(0.0F))
                .texOffs(168, 27).addBox(-5.0F, -5.0F, -5.0F, 1.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        return LayerDefinition.create(meshdefinition, 256, 64);
    }

    @Override
    public void setupAnim(TerrorbeakEntity p_102463_, float p_102464_, float p_102465_, float p_102466_, float p_102467_, float p_102468_) {
        this.right_hind_leg.xRot = Mth.cos(p_102464_ * 0.6662F) * 1.4F * p_102465_;
        this.left_hind_leg.xRot = Mth.cos(p_102464_ * 0.6662F + (float)Math.PI) * 1.4F * p_102465_;
        this.right_front_leg.xRot = Mth.cos(p_102464_ * 0.6662F + (float)Math.PI) * 1.4F * p_102465_;
        this.left_front_leg.xRot = Mth.cos(p_102464_ * 0.6662F) * 1.4F * p_102465_;
    }

    @Override
    public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        maw.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        body.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        right_hind_leg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        left_hind_leg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        right_front_leg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        left_front_leg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
    }
}
