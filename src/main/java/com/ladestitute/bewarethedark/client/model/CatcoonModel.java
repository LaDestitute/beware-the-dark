package com.ladestitute.bewarethedark.client.model;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.entities.mobs.hostile.GhostEntity;
import com.ladestitute.bewarethedark.entities.mobs.neutral.CatcoonEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.ModelUtils;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.resources.ResourceLocation;

public class CatcoonModel extends EntityModel<CatcoonEntity> {
    public static final String BODY = "body";
    // This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
    public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(BTDMain.MOD_ID, "catcoon"), BODY);
    private final ModelPart body;

    public CatcoonModel(ModelPart root) {
        this.body = root.getChild("body");
    }

    public static LayerDefinition createBodyLayer() {
        MeshDefinition meshdefinition = new MeshDefinition();
        PartDefinition partdefinition = meshdefinition.getRoot();

        PartDefinition body = partdefinition.addOrReplaceChild("body", CubeListBuilder.create(), PartPose.offset(0.0F, 17.0F, 1.0F));

        PartDefinition body_r1 = body.addOrReplaceChild("body_r1", CubeListBuilder.create().texOffs(0, 9).addBox(-4.0F, -7.0F, 3.0F, 8.0F, 10.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(30, 13).addBox(-5.0F, -8.0F, -4.0F, 10.0F, 12.0F, 7.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, 1.5708F, 0.0F, 0.0F));

        PartDefinition head = body.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-3.5F, 0.0F, -3.0F, 7.0F, 3.0F, 5.0F, new CubeDeformation(0.0F))
                .texOffs(38, 7).addBox(-2.5F, -1.0F, -2.0F, 5.0F, 1.0F, 4.0F, new CubeDeformation(0.0F))
                .texOffs(19, 14).addBox(-2.5F, 3.0F, -2.0F, 5.0F, 2.0F, 4.0F, new CubeDeformation(0.0F))
                .texOffs(30, 0).addBox(-4.5F, 3.0F, -3.0F, 9.0F, 1.0F, 5.0F, new CubeDeformation(0.0F))
                .texOffs(0, 1).addBox(-3.0F, -3.0F, -1.0F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F))
                .texOffs(0, 1).addBox(2.0F, -3.0F, -1.0F, 1.0F, 3.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -2.0F, -10.0F));

        PartDefinition tail1 = body.addOrReplaceChild("tail1", CubeListBuilder.create(), PartPose.offset(0.0F, -2.0F, 7.0F));

        PartDefinition tail1_r1 = tail1.addOrReplaceChild("tail1_r1", CubeListBuilder.create().texOffs(3, 23).addBox(3.5F, -12.0F, 5.0F, 1.0F, 1.0F, 8.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 17.0F, 1.0F, 0.9F, -0.3446F, -0.4024F));

        PartDefinition tail2 = tail1.addOrReplaceChild("tail2", CubeListBuilder.create(), PartPose.offsetAndRotation(0.0F, 5.4F, 5.9F, 1.6581F, 0.0F, 0.0F));

        PartDefinition tail4_r1 = tail2.addOrReplaceChild("tail4_r1", CubeListBuilder.create().texOffs(3, 27).addBox(15.5F, -2.9F, 19.4F, 1.0F, 1.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 3.6F, -13.9F, -0.4856F, -0.6082F, 2.7357F));

        PartDefinition tail2_r1 = tail2.addOrReplaceChild("tail2_r1", CubeListBuilder.create().texOffs(13, 24).addBox(0.5F, -7.9F, 20.4F, 1.0F, 1.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(0.0F, 3.6F, -13.9F, 0.0F, -0.1745F, 0.0F));

        PartDefinition backLegL = body.addOrReplaceChild("backLegL", CubeListBuilder.create().texOffs(60, 14).addBox(0.0F, 1.0F, -5.0F, 1.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(1.1F, 1.0F, 6.0F));

        PartDefinition backLegR = body.addOrReplaceChild("backLegR", CubeListBuilder.create().texOffs(60, 14).addBox(-1.0F, 1.0F, -5.0F, 1.0F, 5.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.1F, 1.0F, 6.0F));

        PartDefinition frontLegL = body.addOrReplaceChild("frontLegL", CubeListBuilder.create().texOffs(60, 11).addBox(0.0F, 1.8F, -1.0F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(1.2F, -3.0F, -5.0F));

        PartDefinition frontLegR = body.addOrReplaceChild("frontLegR", CubeListBuilder.create().texOffs(60, 11).addBox(-1.0F, 1.8F, -1.0F, 1.0F, 8.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(-1.2F, -3.0F, -5.0F));

        return LayerDefinition.create(meshdefinition, 64, 32);
    }

    @Override
    public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        body.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
    }

    @Override
    public void setupAnim(CatcoonEntity p_102618_, float p_102619_, float p_102620_, float p_102621_, float p_102622_, float p_102623_) {
        if (p_102618_.lieDownAmount > 0.0F) {
            this.body.getChild("head").zRot = ModelUtils.rotlerpRad(this.body.getChild("head").zRot, -1.2707963F, p_102618_.lieDownAmount);
            this.body.getChild("head").yRot = ModelUtils.rotlerpRad(this.body.getChild("head").yRot, 1.2707963F, p_102618_.lieDownAmount);
            this.body.getChild("frontLegL").xRot = -1.2707963F;
            this.body.getChild("frontLegR").xRot = -0.47079635F;
            this.body.getChild("frontLegR").zRot = -0.2F;
            this.body.getChild("frontLegR").x = -0.2F;
            this.body.getChild("backLegL").xRot = -0.4F;
            this.body.getChild("backLegR").xRot = 0.5F;
            this.body.getChild("backLegR").zRot = -0.5F;
            this.body.getChild("backLegR").x = -0.3F;
            this.body.getChild("backLegR").y = 20.0F;
            this.body.getChild("tail1").xRot = ModelUtils.rotlerpRad(this.body.getChild("tail1").xRot, 0.8F, p_102618_.lieDownAmountTail);
            this.body.getChild("tail2").xRot = ModelUtils.rotlerpRad(this.body.getChild("tail2").xRot, -0.4F, p_102618_.lieDownAmountTail);
        }

        if (p_102618_.relaxStateOneAmount > 0.0F) {
            this.body.getChild("head").xRot = ModelUtils.rotlerpRad(this.body.getChild("head").xRot, -0.58177644F, p_102618_.relaxStateOneAmount);
        }
    }
}
