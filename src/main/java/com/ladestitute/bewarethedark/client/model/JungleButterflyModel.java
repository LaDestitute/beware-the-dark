package com.ladestitute.bewarethedark.client.model;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.entities.mobs.passive.JungleButterflyEntity;
import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

public class JungleButterflyModel extends EntityModel<JungleButterflyEntity> {
    public static final String BODY = "body";
    // This layer location should be baked with EntityRendererProvider.Context in the entity renderer and passed into this model's constructor
    public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation(BTDMain.MOD_ID, "jungle_butterfly"), BODY);
    private final ModelPart bb_main;
    private final ModelPart left;
    private final ModelPart right;

    public JungleButterflyModel(ModelPart root) {
        this.left = root.getChild("left");
        this.right = root.getChild("right");
        this.bb_main = root.getChild("bb_main");
    }

    public static LayerDefinition createBodyLayer() {
        MeshDefinition meshdefinition = new MeshDefinition();
        PartDefinition partdefinition = meshdefinition.getRoot();

        PartDefinition left = partdefinition.addOrReplaceChild("left", CubeListBuilder.create().texOffs(6, 0).addBox(-2.6508F, -9.5F, -1.75F, 2.0F, 0.0F, 3.0F, new CubeDeformation(0.0F))
                .texOffs(-2, 0).addBox(-3.6508F, -9.5F, -0.75F, 1.0F, 0.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(-1, 0).addBox(-4.6508F, -9.5F, -2.75F, 2.0F, 0.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(0, 4).addBox(-3.6508F, -9.5F, 1.25F, 2.0F, 0.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(-3.0F, 24.0F, 0.0F, 0.0F, 0.0F, 0.3491F));

        PartDefinition right = partdefinition.addOrReplaceChild("right", CubeListBuilder.create().texOffs(4, 0).addBox(0.3492F, -8.55F, -1.75F, 2.0F, 0.0F, 3.0F, new CubeDeformation(0.0F))
                .texOffs(0, 0).addBox(2.3492F, -8.55F, -2.75F, 2.0F, 0.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(17, 14).addBox(2.3492F, -8.55F, -0.75F, 1.0F, 0.0F, 2.0F, new CubeDeformation(0.0F))
                .texOffs(-1, 4).addBox(1.3492F, -8.55F, 1.25F, 2.0F, 0.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offsetAndRotation(3.0F, 23.0F, 0.0F, 0.0F, 0.0F, -0.3491F));

        PartDefinition bb_main = partdefinition.addOrReplaceChild("bb_main", CubeListBuilder.create().texOffs(1, 1).addBox(-0.5205F, -9.1F, -1.75F, 1.0F, 1.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 24.0F, 0.0F));

        return LayerDefinition.create(meshdefinition, 13, 6);
    }

    @Override
    public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
        left.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        right.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
        bb_main.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
    }

    @Override
    public void setupAnim(JungleButterflyEntity p_102618_, float p_102619_, float p_102620_, float p_102621_, float p_102622_, float p_102623_) {
        this.right.xRot = 0.0F;
        float f = p_102621_ * 120.32113F * ((float)Math.PI / 180F);
        this.right.yRot = 0.0F;
        this.right.zRot = Mth.cos(f) * (float)Math.PI * 0.15F;
        this.left.xRot = this.right.xRot;
        this.left.yRot = this.right.yRot;
        this.left.zRot = -this.right.zRot;
    }
}
