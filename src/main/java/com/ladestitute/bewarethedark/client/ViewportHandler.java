package com.ladestitute.bewarethedark.client;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.registries.BiomeInit;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import net.minecraft.client.Minecraft;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.ViewportEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = BTDMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.FORGE, value = Dist.CLIENT)
public class ViewportHandler {

    @SubscribeEvent
    public static void onColorFog(ViewportEvent.ComputeFogColor e) {
        Entity viewer = Minecraft.getInstance().getCameraEntity();
        if(viewer != null) {
            if(viewer instanceof LivingEntity) {
                //For full moons, else, slightly desaturate when sanity below 16
                    if(viewer.level.dimension() == Level.OVERWORLD && viewer.level.getMoonPhase() == 0 && viewer.level.getDayTime() >= 13000)
                    {
                        e.setRed(0.08f);
                        e.setBlue(0.50f);
                        e.setGreen(0.20f);
                    }
                    else
                        e.setRed(0.25f);
                        e.setBlue(0.25f);
                        e.setGreen(0.25f);
                    }
            }

    }

    @SubscribeEvent
    public static void onSetupFogDensity(ViewportEvent.RenderFog e) {
        Entity viewer = Minecraft.getInstance().getCameraEntity();
        if(viewer != null) {
            if(BTDConfig.getInstance().enable_sanity_fog() && viewer instanceof LivingEntity && ClientSanityData.getPlayerSanity() <= 15 && Minecraft.getInstance().gameMode.canHurtPlayer()) {
                            e.setCanceled(true);
                            //super thick fog, like at your feet 'you're eating it' fog
     //                  e.setNearPlaneDistance(0.01f);
     //                  e.setFarPlaneDistance(0.07F);
                    //dense fog
     //                  e.setNearPlaneDistance(-8f);
     //                  e.setFarPlaneDistance(30F);
                    //weak fog
     //                  e.setNearPlaneDistance(20F);
     //                  e.setFarPlaneDistance(160f);
                    //another dense fog setup
                    //   e.setNearPlaneDistance(0.1F);
                   //    e.setFarPlaneDistance(0.1F*e.getFarPlaneDistance());
                    //another weak fog example
     //                  e.setNearPlaneDistance(e.getNearPlaneDistance());
     //                  e.setFarPlaneDistance(e.getFarPlaneDistance());
                    //semi-dense fog
                       e.setNearPlaneDistance(20f);
                       e.setFarPlaneDistance(50f);
                    }
            }
        if(viewer instanceof LivingEntity && viewer.level.getBiome(viewer.blockPosition()).is(BiomeInit.GRAVEYARD))
        {
               e.setCanceled(true);
            e.setNearPlaneDistance(10f);
            e.setFarPlaneDistance(60f);
        }
        if(viewer.level.dimension() == Level.OVERWORLD && viewer.level.getMoonPhase() == 0 && viewer.level.getDayTime() >= 13000)
        {
            e.setCanceled(true);
            e.setNearPlaneDistance(10f);
            e.setFarPlaneDistance(60f);
        }
        }

    private static float currentScreenRoll = 0F;

    //More wip
    @SubscribeEvent
    public static void screenshake(ViewportEvent.ComputeCameraAngles event) {
        if (event.getCamera().getEntity() instanceof Player player) {
            if (BTDConfig.getInstance().enable_sanity_screenshake() && ClientSanityData.getPlayerSanity() <= 15 && Minecraft.getInstance().gameMode.canHurtPlayer()) {
                float targetRoll;
                float div = 50F;
                    //Investigate methods of x-rotation to spice the screen shake up?
                    float roll = player.yHeadRot - player.yHeadRotO;
                    if (Math.abs(roll) < 0.0001) {
                        targetRoll = 0F;
                    } else {
                        targetRoll = Math.signum(roll) * 4;
                        div = Math.abs(100F / roll);
                    }
                currentScreenRoll += (targetRoll - currentScreenRoll) / div;
                event.setRoll(currentScreenRoll);
            }
        }
    }

    }

