package com.ladestitute.bewarethedark.btdcapabilities.sanity;

import net.minecraft.nbt.CompoundTag;

public class PlayerSanity {
    private int firstspawn;
    private int sanity;
    private int nightdraintimer;
    private int raintimer;
    private int spiderauratimer;
    private int darknesstimer;
    private int ghostauratimer;
    private int poisontimer;
    private int shadowcreaturetimer;
    private int garlandtimer;
    private final int MIN_SANITY = 0;
    private final int MAX_SANITY = 20;

    public int getMaxSanity() {
        return MAX_SANITY;
    }

    public int getIsFirstSpawn() {
        return firstspawn;
    }

    public void setHasSpawnedOnce(int add) {
        this.firstspawn = add;
    }

    public int getSanity() {
        return sanity;
    }

    public int getNightTimer() {
        return nightdraintimer;
    }

    public int getRainTimer() {
        return raintimer;
    }

    public int getSpiderAuraTimer() {
        return spiderauratimer;
    }

    public int getDarknessTimer() {
        return darknesstimer;
    }

    public int getGhostAuraTimer() {
        return ghostauratimer;
    }

    public int getShadowCreatureTimer() {
        return shadowcreaturetimer;
    }

    public int getPoisonTimer() {
        return poisontimer;
    }

    public int getGarlandTimer() {
        return garlandtimer;
    }


    public void addSanity(int add) {
        this.sanity = Math.min(sanity + add, MAX_SANITY);
    }

    public void addNightTimer(int add) {
        this.nightdraintimer += add;
    }

    public void setNightTimer(int add) {
        this.nightdraintimer = add;
    }

    public void addRainTimer(int add) {
        this.raintimer += add;
    }

    public void setRainTimer(int add) {
        this.raintimer = add;
    }

    public void addSpiderAuraTimer(int add) {
        this.spiderauratimer += add;
    }

    public void setSpiderAuraTimer(int add) {
        this.spiderauratimer = add;
    }

    public void addGhostAuraTimer(int add) {
        this.ghostauratimer += add;
    }

    public void setGhostAuraTimer(int add) {
        this.ghostauratimer = add;
    }

    public void addDarknessTimer(int add) {
        this.darknesstimer += add;
    }

    public void setDarknessTimer(int add) {
        this.darknesstimer = add;
    }

    public void addShadowCreatureTimer(int add) {
        this.shadowcreaturetimer += add;
    }

    public void setShadowCreatureTimer(int add) {
        this.shadowcreaturetimer = add;
    }

    public void addPoisonTimer(int add) {
        this.poisontimer += add;
    }

    public void setPoisonTimer(int add) {
        this.poisontimer = add;
    }

    public void addGarlandTimer(int add) {
        this.garlandtimer += add;
    }

    public void setGarlandTimer(int add) {
        this.garlandtimer = add;
    }

    public void subSanity(int sub) {
        this.sanity = Math.max(sanity - sub, MIN_SANITY);
    }

    public void setSanity(int set) {
        this.sanity = set;
    }

    public void copyFrom(PlayerSanity source) {
        this.sanity = source.sanity;
    }

    public void saveNBTData(CompoundTag nbt) {
        nbt.putInt("sanity", sanity);
        nbt.putInt("nightdraintimer", nightdraintimer);
        nbt.putInt("raintimer", raintimer);
        nbt.putInt("spidertimer", spiderauratimer);
        nbt.putInt("darknesstimer", darknesstimer);
        nbt.putInt("ghosttimer", ghostauratimer);
        nbt.putInt("firstspawn", firstspawn);
    }

    public void loadNBTData(CompoundTag nbt) {
        sanity = nbt.getInt("sanity");
        nightdraintimer = nbt.getInt("nightdraintimer");
        raintimer = nbt.getInt("raintimer");
        spiderauratimer = nbt.getInt("spidertimer");
        darknesstimer = nbt.getInt("darknesstimer");
        ghostauratimer = nbt.getInt("ghosttimer");
        firstspawn = nbt.getInt("firstspawn");
    }
}