package com.ladestitute.bewarethedark.btdcapabilities.customplayerdata;

import net.minecraft.core.Direction;
import net.minecraft.nbt.CompoundTag;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilityProvider;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class CustomPlayerDataProvider implements ICapabilityProvider, INBTSerializable<CompoundTag> {
    public static Capability<CustomPlayerData> CUSTOM_PLAYER_DATA = CapabilityManager.get(new CapabilityToken<CustomPlayerData>() { });

    private CustomPlayerData custom_player_data = null;
    private final LazyOptional<CustomPlayerData> optional = LazyOptional.of(this::createCustomPlayerData);

    private CustomPlayerData createCustomPlayerData() {
        if(this.custom_player_data == null) {
            this.custom_player_data = new CustomPlayerData();
        }

        return this.custom_player_data;
    }

    @Override
    public @NotNull <T> LazyOptional<T> getCapability(@NotNull Capability<T> cap, @Nullable Direction side) {
        if(cap == CUSTOM_PLAYER_DATA) {
            return optional.cast();
        }

        return LazyOptional.empty();
    }

    @Override
    public CompoundTag serializeNBT() {
        CompoundTag nbt = new CompoundTag();
        createCustomPlayerData().saveNBTData(nbt);
        return nbt;
    }

    @Override
    public void deserializeNBT(CompoundTag nbt) {
        createCustomPlayerData().loadNBTData(nbt);
    }
}

