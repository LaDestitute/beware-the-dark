package com.ladestitute.bewarethedark.btdcapabilities.customplayerdata;

import com.ladestitute.bewarethedark.BTDMain;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.common.capabilities.RegisterCapabilitiesEvent;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;

@Mod.EventBusSubscriber(modid = BTDMain.MOD_ID)
public class CustomPlayerDataHandler {

    @SubscribeEvent
    public void registerCaps(RegisterCapabilitiesEvent event) {
        event.register(CustomPlayerData.class);
    }

    @SubscribeEvent
    public static void onAttachCapabilitiesPlayer(AttachCapabilitiesEvent<Entity> event) {
        if(event.getObject() instanceof Player) {
            if(!event.getObject().getCapability(CustomPlayerDataProvider.CUSTOM_PLAYER_DATA).isPresent()) {
                event.addCapability(new ResourceLocation(BTDMain.MOD_ID, "custom_player_data"), new CustomPlayerDataProvider());
            }
        }
    }
}
