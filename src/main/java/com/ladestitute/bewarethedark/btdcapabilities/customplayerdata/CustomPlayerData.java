package com.ladestitute.bewarethedark.btdcapabilities.customplayerdata;

import net.minecraft.nbt.CompoundTag;

public class CustomPlayerData {
    private int hungertick;
    private int poisonimmunitytick;
    private int lightupdatetick;
    private int lanternfuel;

    public int getHungerTick() {
        return hungertick;
    }

    public void addHungerTick(int add) {
        this.hungertick += add;
    }

    public void setHungerTick(int add) {
        this.hungertick = add;
    }

    public int getPoisonImmunityTick() {
        return poisonimmunitytick;
    }

    public void addPoisonImmunityTick(int add) {
        this.poisonimmunitytick += add;
    }

    public void subPoisonImmunityTick(int add) {
        this.poisonimmunitytick -= add;
    }

    public void setPoisonImmunityTick(int add) {
        this.poisonimmunitytick = add;
    }

    public int getLightUpdateTick() {
        return lightupdatetick;
    }

    public void addLightUpdateTick(int add) {
        this.lightupdatetick += add;
    }

    public void subLightUpdateTick(int add) {
        this.lightupdatetick -= add;
    }

    public void setLightUpdateTick(int add) {
        this.lightupdatetick = add;
    }

    public int getLanternFuel() {
        return lanternfuel;
    }

    public void addLanternFuel(int add) {
        this.lanternfuel += add;
    }

    public void subLanternFuel(int add) {
        this.lanternfuel -= add;
    }

    public void setLanternFuel(int add) {
        this.lanternfuel = add;
    }

//    @Override
//    public int gettruedarknessvalue() {
//        return truedarkness;
//    }
//
//    @Override
//    public int getisshovelrecipeunlocked() {
//        return unlockshovelrecipe;
//    }
//
//    @Override
//    public int getispitchforkrecipeunlocked() {
//        return unlockpitchforkrecipe;
//    }
//
//    @Override
//    public int getiscompassrecipeunlocked() {
//        return unlockcompassrecipe;
//    }
//
//    @Override
//    public int getishealingsalverecipeunlocked() {
//        return unlockhealingsalverecipe;
//    }
//
//    @Override
//    public int getisfishingrodrecipeunlocked() {
//        return unlockfishingrodrecipe;
//    }
//
//    @Override
//    public int getisbasicfarmrecipeunlocked() {
//        return unlockbasicfarmrecipe;
//    }
//
//    @Override
//    public int getisimprovedfarmrecipeunlocked() {
//        return unlockimprovedfarmrecipe;
//    }
//
//    @Override
//    public int getisspearrecipeunlocked() {
//        return unlockspearrecipe;
//    }
//
//    @Override
//    public int getislogsuitrecipeunlocked() {
//        return unlocklogsuitrecipe;
//    }
//
//    @Override
//    public int getischestrecipeunlocked() {
//        return unlockchestrecipe;
//    }
//
//    @Override
//    public int getissignrecipeunlocked() {
//        return unlocksignrecipe;
//    }
//
//    @Override
//    public int getisboardsrecipeunlocked() {
//        return unlockboardsrecipe;
//    }
//
//    @Override
//    public int getisroperecipeunlocked() {
//        return unlockroperecipe;
//    }
//
//    @Override
//    public int getiscutstonerecipeunlocked() {
//        return unlockcutstonerecipe;
//    }
//
//    @Override
//    public int getisopulentpickaxerecipeunlocked() {
//        return unlockopulentpickaxerecipe;
//    }
//
//    @Override
//    public int getisluxuryaxerecipeunlocked() {
//        return unlockluxuryaxerecipe;
//    }
//
//    @Override
//    public int getisregalshovelrecipeunlocked() {
//        return unlockregalshovelrecipe;
//    }
//
//    @Override
//    public int getispumpkinlanternrecipeunlocked() {
//        return unlockpumpkinlanternrecipe;
//    }
//
//    @Override
//    public int getiscobblestonesrecipeunlocked() {
//        return unlockcobblestonesrecipe;
//    }
//
//    @Override
//    public int getiswoodenflooringrecipeunlocked() {
//        return unlockwoodenflooringrecipe;
//    }
//
//    @Override
//    public int getispaperrecipeunlocked() {
//        return unlockpaperrecipe;
//    }

    public void saveNBTData(CompoundTag nbt) {
        nbt.putInt("hungertick", hungertick);
    }

    public void loadNBTData(CompoundTag nbt) {
        hungertick = nbt.getInt("hungertick");
    }
}
