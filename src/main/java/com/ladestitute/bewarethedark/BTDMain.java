package com.ladestitute.bewarethedark;

import com.ladestitute.bewarethedark.client.screen.BackpackScreen;
import com.ladestitute.bewarethedark.client.screen.CrockPotScreen;
import com.ladestitute.bewarethedark.client.screen.DryingRackScreen;
import com.ladestitute.bewarethedark.client.sound.BTDMusicTicker;
import com.ladestitute.bewarethedark.events.*;
import com.ladestitute.bewarethedark.network.NetworkingHandler;
import com.ladestitute.bewarethedark.registries.*;
import com.ladestitute.bewarethedark.util.BTDLoc;
import com.ladestitute.bewarethedark.util.TumbleweedSpawner;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import com.ladestitute.bewarethedark.util.glm.DropModifier;
import com.ladestitute.bewarethedark.util.glm.FishingLootModifier;
import com.ladestitute.bewarethedark.world.biome.BTDRegion;
import com.ladestitute.bewarethedark.world.biome.BTDSurfaceRules;
import com.ladestitute.bewarethedark.world.biome.BiomeModifiers;
import com.mojang.serialization.Codec;
import net.minecraft.client.gui.screens.MenuScreens;
import net.minecraft.core.Direction;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.Level;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.loot.IGlobalLootModifier;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber;
import net.minecraftforge.fml.common.Mod.EventBusSubscriber.Bus;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import terrablender.api.RegionType;
import terrablender.api.Regions;
import terrablender.api.SurfaceRuleManager;


@Mod(BTDMain.MOD_ID)
@EventBusSubscriber(modid = BTDMain.MOD_ID, bus = Bus.MOD)
public class BTDMain
{
    public static BTDMain instance;
    public static final String NAME = "Beware the Dark";
    public static final String MOD_ID = "bewarethedark";
    public static final Logger LOGGER = LogManager.getLogger();
    public static ResourceKey<Level> CAVES_DIMENSION;

    public BTDMain()
    {
        instance = this;
        ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, BTDConfig.SPEC, "bewarethedarkconfig.toml");
//        BTDConfig.loadConfig(BTDConfig.SPEC,
//                FMLPaths.CONFIGDIR.get().resolve("bewarethedarkconfig.toml").toString());
        final IEventBus modEventBus = FMLJavaModLoadingContext.get().getModEventBus();

        modEventBus.addListener(this::setup);
        modEventBus.addListener(this::clientSetup);

        DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () ->
        {
          MinecraftForge.EVENT_BUS.register(BTDMusicTicker.class);
        });
        DropModifier.GLM.register(modEventBus);
        MinecraftForge.EVENT_BUS.register(new BTDMobSpawningHandler());
        MinecraftForge.EVENT_BUS.register(new BTDBlockInteractionsHandler());
        MinecraftForge.EVENT_BUS.register(new BTDItemInteractionsHandler());
        MinecraftForge.EVENT_BUS.register(new BTDCharlieHandler());
        MinecraftForge.EVENT_BUS.register(new TumbleweedSpawner());
        MinecraftForge.EVENT_BUS.register(new BTDMainHandler());
        MinecraftForge.EVENT_BUS.register(new BTDHungerHandler());
        MinecraftForge.EVENT_BUS.register(new BTDFoodEffectsHandler());
        MinecraftForge.EVENT_BUS.register(new BTDMobDropsHandler());
        MinecraftForge.EVENT_BUS.register(new BTDUnlockRecipeHandler());
        MinecraftForge.EVENT_BUS.register(new BTDSanityHandler());
        MinecraftForge.EVENT_BUS.register(new DynamicLightHandler());
        /* Register all of our deferred registries from our list/init classes, which get added to the IEventBus */
        // ParticleList.PARTICLES.register(modEventBus);
        EffectInit.register(modEventBus);
        //EffectInit.POTIONS.register(modEventBus);
        //Leaving those above uncommented in case I add particles or effects at some point
        SoundInit.SOUNDS.register(modEventBus);
        BlockInit.BLOCKS.register(modEventBus);
        ItemInit.ITEMS.register(modEventBus);
        FoodInit.FOOD.register(modEventBus);
        SpecialBlockInit.BLOCKS.register(modEventBus);
        BiomeModifiers.register(modEventBus);
        PlacedFeatureInit.register(modEventBus);
        MushroomInit.FEATURES.register(modEventBus);
        MushroomInit.NeapolitanConfiguredFeatures.CONFIGURED_FEATURES.register(modEventBus);
        MushroomInit.NeapolitanPlacedFeatures.PLACED_FEATURES.register(modEventBus);
        BlockEntityInit.register(modEventBus);
        MenuTypesInit.register(modEventBus);
        ContainerInit.CONTAINER_TYPES.register(modEventBus);
        RecipeTypeInit.RECIPE_TYPES.register(modEventBus);
        RecipeTypeInit.SERIALIZERS.register(modEventBus);
        EntityInit.ENTITIES.register(modEventBus);
        IEventBus forgeBus = MinecraftForge.EVENT_BUS;
        GLM.register(modEventBus);
    }

    private void clientSetup(final FMLClientSetupEvent event) {
        MenuScreens.register(MenuTypesInit.DRYING_RACK_MENU.get(), DryingRackScreen::new);
        MenuScreens.register(MenuTypesInit.CROCK_POT_MENU.get(), CrockPotScreen::new);
        MenuScreens.register(ContainerInit.BACKPACK.get(), BackpackScreen::new);
    }

    public static VoxelShape calculateShapes(Direction to, VoxelShape shape) {
        final VoxelShape[] buffer = { shape, Shapes.empty() };

        final int times = (to.get2DDataValue() - Direction.NORTH.get2DDataValue() + 4) % 4;
        for (int i = 0; i < times; i++) {
            buffer[0].forAllBoxes((minX, minY, minZ, maxX, maxY, maxZ) -> buffer[1] = Shapes.or(buffer[1],
                    Shapes.create(1 - maxZ, minY, minX, 1 - minZ, maxY, maxX)));
            buffer[0] = buffer[1];
            buffer[1] = Shapes.empty();
        }

        return buffer[0];
    }

    private static final DeferredRegister<Codec<? extends IGlobalLootModifier>> GLM = DeferredRegister.create(ForgeRegistries.Keys.GLOBAL_LOOT_MODIFIER_SERIALIZERS, MOD_ID);
    public static final RegistryObject<Codec<FishingLootModifier>> FISH_LOOT = GLM.register("fishing", () -> FishingLootModifier.CODEC);

    /* The FMLCommonSetupEvent (FML - Forge Mod Loader) */
    private void setup(final FMLCommonSetupEvent event)
    {
        event.enqueueWork(() -> {
            NetworkingHandler.register();
            Regions.register(new BTDRegion(BTDLoc.createLoc("bewarethedark_region"), RegionType.OVERWORLD,
                    BTDConfig.getInstance().terrablender_biome_weight()));
            SurfaceRuleManager.addSurfaceRules(
                    SurfaceRuleManager.RuleCategory.OVERWORLD, MOD_ID, BTDSurfaceRules.OVERWORLD_SURFACE_RULES);
        });

        CAVES_DIMENSION = ResourceKey.create(Registry.DIMENSION_REGISTRY, new ResourceLocation(BTDMain.MOD_ID, "caves_dimension"));
    }

    // Custom CreativeModeTab tabs
    public static final CreativeModeTab NATURAL_TAB = new CreativeModeTab("btd_natural") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(BlockInit.EVERGREEN_LEAVES.get());
        }
    };

    public static final CreativeModeTab TURF_TAB = new CreativeModeTab("btd_turf") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(BlockInit.FOREST_TURF.get());
        }
    };

    public static final CreativeModeTab TOOLS_TAB = new CreativeModeTab("btd_tools") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.FLINT_PICKAXE.get());
        }
    };

    public static final CreativeModeTab LIGHT_TAB = new CreativeModeTab("btd_light") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.TORCH.get());
        }
    };

    public static final CreativeModeTab SURVIVAL_TAB = new CreativeModeTab("btd_survival") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.ROPE.get());
        }
    };

    public static final CreativeModeTab FOOD_TAB = new CreativeModeTab("btd_food") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(Items.CARROT);
        }
    };

    public static final CreativeModeTab SCIENCE_TAB = new CreativeModeTab("btd_science") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.SHOVEL_BLUEPRINT.get());
        }
    };

    public static final CreativeModeTab FIGHT_TAB = new CreativeModeTab("btd_fight") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.SPEAR.get());
        }
    };

    public static final CreativeModeTab REFINED_TAB = new CreativeModeTab("btd_refined") {
        @Override
        public ItemStack makeIcon() {
            return new ItemStack(ItemInit.BOARDS.get());
        }
    };

    public static final CreativeModeTab MATERIALS_TAB = new CreativeModeTab("btd_materials") {
        @Override
        public ItemStack makeIcon() {
     return new ItemStack(ItemInit.ROCKS.get());
        }
    };

}



