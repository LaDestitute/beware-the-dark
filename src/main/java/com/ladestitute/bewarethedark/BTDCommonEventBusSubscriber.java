package com.ladestitute.bewarethedark;

import com.ladestitute.bewarethedark.entities.mobs.hostile.BatiliskEntity;
import com.ladestitute.bewarethedark.entities.mobs.hostile.GhostEntity;
import com.ladestitute.bewarethedark.entities.mobs.hostile.TentacleEntity;
import com.ladestitute.bewarethedark.entities.mobs.neutral.CatcoonEntity;
import com.ladestitute.bewarethedark.entities.mobs.passive.ButterflyEntity;
import com.ladestitute.bewarethedark.entities.mobs.passive.FireflyEntity;
import com.ladestitute.bewarethedark.entities.mobs.passive.JungleButterflyEntity;
import com.ladestitute.bewarethedark.entities.mobs.passive.MandrakeEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.*;
import com.ladestitute.bewarethedark.registries.EntityInit;
import net.minecraft.world.entity.SpawnPlacements;
import net.minecraft.world.entity.ambient.Bat;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;

@Mod.EventBusSubscriber(modid = BTDMain.MOD_ID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class BTDCommonEventBusSubscriber {

    @SubscribeEvent
    public static void onStaticCommonSetup(FMLCommonSetupEvent event) {
        
      //  MinecraftForge.EVENT_BUS.register(new BTDCustomPlayerdataCapabilityHandler());
        SpawnPlacements.register(EntityInit.FIREFLY.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, FireflyEntity::checkSpawnRules);
        SpawnPlacements.register(EntityInit.BUTTERFLY.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, ButterflyEntity::checkSpawnRules);
        SpawnPlacements.register(EntityInit.JUNGLE_BUTTERFLY.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, JungleButterflyEntity::checkSpawnRules);
        SpawnPlacements.register(EntityInit.MANDRAKE.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, MandrakeEntity::checkSpawnRules);
        SpawnPlacements.register(EntityInit.GHOST.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, GhostEntity::checkSpawnRules);
        SpawnPlacements.register(EntityInit.CATCOON.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, CatcoonEntity::checkSpawnRules);
        SpawnPlacements.register(EntityInit.MR_SKITTS.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, MrSkittsEntity::checkSpawnRules);
        SpawnPlacements.register(EntityInit.NIGHT_HAND.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, NightHandEntity::checkSpawnRules);
        SpawnPlacements.register(EntityInit.SHADOW_WATCHER.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, ShadowWatcherEntity::checkSpawnRules);
        SpawnPlacements.register(EntityInit.PASSIVE_CRAWLING_HORROR.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, PassiveCrawlingHorrorEntity::checkSpawnRules);
        SpawnPlacements.register(EntityInit.HOSTILE_CRAWLING_HORROR.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, HostileCrawlingHorrorEntity::checkSpawnRules);
        SpawnPlacements.register(EntityInit.TERRORBEAK.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, TerrorbeakEntity::checkSpawnRules);
        SpawnPlacements.register(EntityInit.TENTACLE.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, TentacleEntity::checkSpawnRules);
        SpawnPlacements.register(EntityInit.BATILISK.get(), SpawnPlacements.Type.ON_GROUND,
                Heightmap.Types.WORLD_SURFACE, BatiliskEntity::checkSpawnRules);
    }

    //This method is required to give your entity its attributes, not doing so will cause MC to fail to load
    @SubscribeEvent
    public static void registerAttributes(EntityAttributeCreationEvent event) {
        event.put(EntityInit.FIREFLY.get(), FireflyEntity.createAttributes().build());
        event.put(EntityInit.BUTTERFLY.get(), ButterflyEntity.createAttributes().build());
        event.put(EntityInit.JUNGLE_BUTTERFLY.get(), JungleButterflyEntity.createAttributes().build());
        event.put(EntityInit.MANDRAKE.get(), MandrakeEntity.createAttributes().build());
        event.put(EntityInit.GHOST.get(), GhostEntity.createAttributes().build());
        event.put(EntityInit.CATCOON.get(), CatcoonEntity.createAttributes().build());
        event.put(EntityInit.MR_SKITTS.get(), MrSkittsEntity.createAttributes().build());
        event.put(EntityInit.NIGHT_HAND.get(), NightHandEntity.createAttributes().build());
        event.put(EntityInit.SHADOW_WATCHER.get(), ShadowWatcherEntity.createAttributes().build());
        event.put(EntityInit.PASSIVE_CRAWLING_HORROR.get(), PassiveCrawlingHorrorEntity.createAttributes().build());
        event.put(EntityInit.HOSTILE_CRAWLING_HORROR.get(), HostileCrawlingHorrorEntity.createAttributes().build());
        event.put(EntityInit.TERRORBEAK.get(), TerrorbeakEntity.createAttributes().build());
        event.put(EntityInit.TENTACLE.get(), TentacleEntity.createAttributes().build());
        event.put(EntityInit.BATILISK.get(), BatiliskEntity.createAttributes().build());
    }

}

