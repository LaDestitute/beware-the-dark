package com.ladestitute.bewarethedark.registries;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.blocks.building.CarpetedFlooringBlock;
import com.ladestitute.bewarethedark.blocks.building.CobblestonesBlock;
import com.ladestitute.bewarethedark.blocks.building.WoodenFlooringBlock;
import com.ladestitute.bewarethedark.blocks.foodutil.BasicFarmBlock;
import com.ladestitute.bewarethedark.blocks.foodutil.ImprovedFarmBlock;
import com.ladestitute.bewarethedark.blocks.helper.*;
import com.ladestitute.bewarethedark.blocks.natural.BlueMushroomSpawnerBlock;
import com.ladestitute.bewarethedark.blocks.natural.GreenMushroomSpawnerBlock;
import com.ladestitute.bewarethedark.blocks.natural.RedMushroomSpawnerBlock;
import com.ladestitute.bewarethedark.blocks.natural.plant.GrassTuftBlock;
import com.ladestitute.bewarethedark.blocks.natural.plant.*;
import com.ladestitute.bewarethedark.blocks.natural.plant.logs.BirchnutStumpBlock;
import com.ladestitute.bewarethedark.blocks.utility.BTDCampfireBlock;
import com.ladestitute.bewarethedark.blocks.utility.FirePitBlock;
import com.ladestitute.bewarethedark.blocks.utility.ScienceMachineBlock;
import com.ladestitute.bewarethedark.blocks.utility.UnlitFirepitBlock;
import com.ladestitute.bewarethedark.blocks.world.PluggedSinkholeBlock;
import com.ladestitute.bewarethedark.blocks.world.SinkholeBlock;
import com.ladestitute.bewarethedark.world.feature.tree.*;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.material.Material;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class SpecialBlockInit {
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS,
            BTDMain.MOD_ID);

    //Special/util blocks such as dedicated spawners
    public static final RegistryObject<Block> TUMBLE_SPAWNER = BLOCKS.register("tumbleweed_spawner",
            () -> new TumbleweedSpawnerBlock(PropertiesInit.TUMBLEWEEDSPAWNER));
    public static final RegistryObject<Block> FIREFLIES_SPAWNER = BLOCKS.register("fireflies_spawner",
            () -> new FirefliesSpawnerBlock(PropertiesInit.FIREFLYSPAWNER));
    public static final RegistryObject<Block> NIGHTHAND_SPAWNER = BLOCKS.register("nighthand_spawner",
            () -> new NighthandSpawnerBlock(PropertiesInit.NIGHTHANDSPAWNER));
    public static final RegistryObject<Block> MR_SKITTS_SPAWNER = BLOCKS.register("mr_skitts_spawner",
            () -> new MrSkittsSpawnerBlock(PropertiesInit.NIGHTHANDSPAWNER));
    public static final RegistryObject<Block> SHADOW_WATCHER_SPAWNER = BLOCKS.register("shadow_watcher_spawner",
            () -> new ShadowWatcherSpawnerBlock(PropertiesInit.NIGHTHANDSPAWNER));
    public static final RegistryObject<Block> CRAWLING_HORROR_SPAWNER = BLOCKS.register("crawling_horror_spawner",
            () -> new CrawlingHorrorSpawnerBlock(PropertiesInit.NIGHTHANDSPAWNER));
    public static final RegistryObject<Block> TERRORBEAK_SPAWNER = BLOCKS.register("terrorbeak_spawner",
            () -> new TerrorbeakSpawnerBlock(PropertiesInit.NIGHTHANDSPAWNER));
    public static final RegistryObject<Block> TENTACLE_SPAWNER = BLOCKS.register("tentacle_spawner",
            () -> new TentacleSpawnerBlock(PropertiesInit.NIGHTHANDSPAWNER));

    public static final RegistryObject<Block> FIRE_PIT = BLOCKS.register("fire_pit",
            () -> new FirePitBlock(PropertiesInit.HAMMERABLEFIREPIT));
    public static final RegistryObject<Block> FIRE_PIT_OFF = BLOCKS.register("fire_pit_off",
            () -> new UnlitFirepitBlock(PropertiesInit.HAMMERABLEUNLITFIREPIT));
    public static final RegistryObject<Block> CAMP_FIRE = BLOCKS.register("campfire",
           () -> new BTDCampfireBlock(PropertiesInit.HAMMERABLEFIREPIT));

    public static final RegistryObject<Block> BASIC_FARM = BLOCKS.register("basic_farm",
            () -> new BasicFarmBlock(PropertiesInit.HAMMERABLEFARM));
    public static final RegistryObject<Block> IMPROVED_FARM = BLOCKS.register("improved_farm",
            () -> new ImprovedFarmBlock(PropertiesInit.HAMMERABLEFARM));

    public static final RegistryObject<Block> SCIENCE_MACHINE = BLOCKS.register("science_machine",
            () -> new ScienceMachineBlock(PropertiesInit.HAMMERABLESCIENCEMACHINE));

    public static final RegistryObject<Block> BERRY_BUSH = BLOCKS.register("berry_bush",
            () -> new BerryBushBlock(PropertiesInit.BERRY_BUSH));

    public static final RegistryObject<Block> SAPLING_PLANT = BLOCKS.register("blocksapling",
            () -> new SaplingPlantBlock(PropertiesInit.BERRY_BUSH));

    public static final RegistryObject<Block> GRASS_TUFT = BLOCKS.register("grass_tuft",
            () -> new GrassTuftBlock(PropertiesInit.BERRY_BUSH));

    public static final RegistryObject<Block> BIRCHNUT_SAPLING_GREEN = BLOCKS.register("birchnut_sapling_green",
            () -> new SaplingBlock(new GreenBirchnutTreeGrower(), BlockBehaviour.Properties.copy(Blocks.BIRCH_SAPLING)));
    public static final RegistryObject<Block> BIRCHNUT_SAPLING_ORANGE = BLOCKS.register("birchnut_sapling_orange",
            () -> new SaplingBlock(new OrangeBirchnutTreeGrower(), BlockBehaviour.Properties.copy(Blocks.BIRCH_SAPLING)));
    public static final RegistryObject<Block> BIRCHNUT_SAPLING_RED = BLOCKS.register("birchnut_sapling_red",
            () -> new SaplingBlock(new RedBirchnutTreeGrower(), BlockBehaviour.Properties.copy(Blocks.BIRCH_SAPLING)));
    public static final RegistryObject<Block> BIRCHNUT_SAPLING_YELLOW = BLOCKS.register("birchnut_sapling_yellow",
            () -> new SaplingBlock(new YellowBirchnutTreeGrower(), BlockBehaviour.Properties.copy(Blocks.BIRCH_SAPLING)));

    public static final RegistryObject<Block> SPIKY_BUSH = BLOCKS.register("spiky_bush",
            () -> new SpikyBushBlock(PropertiesInit.BERRY_BUSH));

    public static final RegistryObject<Block> REEDS = BLOCKS.register("reeds",
            () -> new ReedsBlock(PropertiesInit.BERRY_BUSH));

    public static final RegistryObject<Block> STUMP = BLOCKS.register("stump",
            () -> new StumpBlock(PropertiesInit.STUMP));
    public static final RegistryObject<Block> BIRCHNUT_STUMP = BLOCKS.register("birchnut_stump",
            () -> new BirchnutStumpBlock(PropertiesInit.STUMP));

    public static final RegistryObject<Block> RED_MUSHROOM_SPAWNER = BLOCKS.register("red_mushroom_spawner",
            () -> new RedMushroomSpawnerBlock(PropertiesInit.MUSHROOM_SPAWNER));

    public static final RegistryObject<Block> GREEN_MUSHROOM_SPAWNER = BLOCKS.register("green_mushroom_spawner",
            () -> new GreenMushroomSpawnerBlock(PropertiesInit.MUSHROOM_SPAWNER));

    public static final RegistryObject<Block> BLUE_MUSHROOM_SPAWNER = BLOCKS.register("blue_mushroom_spawner",
            () -> new BlueMushroomSpawnerBlock(PropertiesInit.MUSHROOM_SPAWNER));

    public static final RegistryObject<Block> LIGHT_SHAFT = BLOCKS.register("light_shaft",
            () -> new LightShaftBlock(PropertiesInit.FIREFLYSPAWNER));

}
