package com.ladestitute.bewarethedark.registries;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.blocks.building.CarpetedFlooringBlock;
import com.ladestitute.bewarethedark.blocks.building.CobblestonesBlock;
import com.ladestitute.bewarethedark.blocks.building.WoodenFlooringBlock;
import com.ladestitute.bewarethedark.blocks.foodutil.CrockPotBlock;
import com.ladestitute.bewarethedark.blocks.foodutil.DryingRackBlock;
import com.ladestitute.bewarethedark.blocks.light.PumpkinLanternBlock;
import com.ladestitute.bewarethedark.blocks.light.WoodLanternBlock;
import com.ladestitute.bewarethedark.blocks.natural.HollowStumpBlock;
import com.ladestitute.bewarethedark.blocks.natural.plant.*;
import com.ladestitute.bewarethedark.blocks.natural.plant.logs.*;
import com.ladestitute.bewarethedark.blocks.turf.*;
import com.ladestitute.bewarethedark.blocks.world.*;
import com.ladestitute.bewarethedark.world.feature.tree.EvergreenTreeGrower;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.*;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.function.Supplier;

public class BlockInit {

    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS,
            BTDMain.MOD_ID);

    //Turfs
    public static final RegistryObject<Block> FOREST_TURF = registerBlock("forest_turf",
            () -> new ForestTurfBlock(PropertiesInit.GRASSTURF), BTDMain.TURF_TAB);
    public static final RegistryObject<Block> ROCKY_TURF = registerBlock("rocky_turf",
            () -> new RockyTurfBlock(PropertiesInit.ROCKYTURF), BTDMain.TURF_TAB);
    public static final RegistryObject<Block> MARSH_TURF = registerBlock("marsh_turf",
            () -> new MarshTurfBlock(PropertiesInit.MARSHTURF), BTDMain.TURF_TAB);
    public static final RegistryObject<Block> DECIDUOUS_TURF = registerBlock("deciduous_turf",
            () -> new DeciduousTurfBlock(PropertiesInit.GRASSTURF), BTDMain.TURF_TAB);
    public static final RegistryObject<Block> BLUE_FUNGAL_TURF = registerBlock("blue_fungal_turf",
            () -> new BlueFungalTurfBlock(PropertiesInit.FUNGAL_TURF), BTDMain.TURF_TAB);
    public static final RegistryObject<Block> RED_FUNGAL_TURF = registerBlock("red_fungal_turf",
            () -> new RedFungalTurfBlock(PropertiesInit.FUNGAL_TURF), BTDMain.TURF_TAB);
    public static final RegistryObject<Block> GREEN_FUNGAL_TURF = registerBlock("green_fungal_turf",
            () -> new GreenFungalTurfBlock(PropertiesInit.FUNGAL_TURF), BTDMain.TURF_TAB);
    public static final RegistryObject<Block> GUANO_TURF = registerBlock("guano_turf",
            () -> new GuanoTurfBlock(PropertiesInit.GUANOTURF), BTDMain.TURF_TAB);
    public static final RegistryObject<Block> COBBLESTONES = registerBlock("cobblestones",
            () -> new CobblestonesBlock(PropertiesInit.STONEFLOOR), BTDMain.TURF_TAB);
    public static final RegistryObject<Block> WOODEN_FLOORING = registerBlock("wooden_flooring",
            () -> new WoodenFlooringBlock(PropertiesInit.WOODFLOOR), BTDMain.TURF_TAB);
    public static final RegistryObject<Block> CARPETED_FLOORING = registerBlock("carpeted_flooring",
            () -> new CarpetedFlooringBlock(PropertiesInit.SOFTFLOOR), BTDMain.TURF_TAB);

    public static final RegistryObject<Block> MARSH_POND = registerBlock("marsh_pond",
            () -> new MarshPondBlock(PropertiesInit.MARSH_POND), BTDMain.NATURAL_TAB);

    //The sapling takes our custom tree as a parameter like below (TreeInit.ORANGE_BIRCHNUT)
    //Don't forget to add your sapling to the sapling block tag

    //Logs use the RotatedPillarBlock class
    public static final RegistryObject<Block> EVERGREEN = registerBlock("evergreen_log",
            () -> new EvergreenLogBlock(PropertiesInit.LUMPY_EVERGREEN), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> LUMPY_EVERGREEN = registerBlock("lumpy_evergreen_log",
            () -> new LumpyEvergreenLogBlock(PropertiesInit.LUMPY_EVERGREEN), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> SPIKY_LOG = registerBlock("spiky_tree_log",
            () -> new SpikyLogBlock(PropertiesInit.LUMPY_EVERGREEN), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> BURNT_LOG = registerBlock("burnt_log",
            () -> new BurntLogBlock(PropertiesInit.BURNT_EVERGREEN), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> RED_MUSHTREE_CAP = registerBlock("red_mushtree_cap",
            () -> new RedMushtreeCapBlock(PropertiesInit.MUSHTREE_CAP), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> GREEN_MUSHTREE_CAP = registerBlock("green_mushtree_cap",
            () -> new GreenMushtreeCapBlock(PropertiesInit.MUSHTREE_CAP), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> BLUE_MUSHTREE_CAP = registerBlock("blue_mushtree_cap",
            () -> new BlueMushtreeCapBlock(PropertiesInit.MUSHTREE_CAP), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> MUSHTREE = registerHiddenBlock("mushtree_log",
            () -> new MushtreeLogBlock(PropertiesInit.MUSHTREE));
    public static final RegistryObject<Block> EVERGREEN_LEAVES = registerBlock("evergreen_leaves",
            () -> new LeavesBlock(PropertiesInit.LEAVES), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> LUMPY_EVERGREEN_LEAVES = registerBlock("lumpy_evergreen_leaves",
            () -> new LeavesBlock(PropertiesInit.LEAVES), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> ORANGE_DECIDUOUS_LEAVES = registerBlock("orange_deciduous_leaves",
            () -> new LeavesBlock(PropertiesInit.LEAVES), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> RED_DECIDUOUS_LEAVES = registerBlock("red_deciduous_leaves",
            () -> new LeavesBlock(PropertiesInit.LEAVES), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> YELLOW_DECIDUOUS_LEAVES = registerBlock("yellow_deciduous_leaves",
            () -> new LeavesBlock(PropertiesInit.LEAVES), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> GREEN_DECIDUOUS_LEAVES = registerBlock("green_deciduous_leaves",
            () -> new LeavesBlock(PropertiesInit.LEAVES), BTDMain.NATURAL_TAB);

    //Plants

    //World objects
    public static final RegistryObject<Block> BOULDER = registerBlock("boulder",
            () -> new BoulderBlock(PropertiesInit.BOULDER), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> GOLD_VEIN_BOULDER = registerBlock("gold_vein_boulder",
            () -> new GoldVeinBoulderBlock(PropertiesInit.BOULDER), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> FLINTESS_BOULDER = registerBlock("flintless_boulder",
            () -> new FlintlessBoulderBlock(PropertiesInit.BOULDER), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> WORLD_ROCKS = registerBlock("world_rocks",
            () -> new WorldRocksBlock(PropertiesInit.WORLDROCK), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> WORLD_FLINT = registerBlock("world_flint",
            () -> new WorldFlintBlock(PropertiesInit.WORLDROCK), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> WORLD_GOLD_NUGGET = registerBlock("world_gold_nugget",
            () -> new WorldGoldNuggetBlock(PropertiesInit.WORLDROCK), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> WORLD_CARROT = registerBlock("world_carrot",
            () -> new CarrotBlock(PropertiesInit.WORLD_PLANT), BTDMain.NATURAL_TAB);

    public static final RegistryObject<Block> MARSH_POND_PLANT = registerBlock("marsh_pond_plant",
            () -> new MarshPondPlantBlock(PropertiesInit.MARSH_POND_PLANT), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> WORLD_SEEDS = registerBlock("world_seeds",
            () -> new SeedsBlock(PropertiesInit.WORLD_PLANT), BTDMain.NATURAL_TAB);

    public static final RegistryObject<Block> WORLD_MANDRAKE = registerBlock("mandrake_plant",
            () -> new MandrakePlantBlock(PropertiesInit.WORLD_PLANT), BTDMain.NATURAL_TAB);

    public static final RegistryObject<Block> GRAVE = registerBlock("grave",
            () -> new GraveBlock(PropertiesInit.GRAVE), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> DUG_GRAVE = registerBlock("dug_grave",
            () -> new DugGraveBlock(PropertiesInit.GRAVE), BTDMain.NATURAL_TAB);

    public static final RegistryObject<Block> HOLLOW_STUMP = registerBlock("hollow_stump",
            () -> new HollowStumpBlock(PropertiesInit.STUMP), BTDMain.NATURAL_TAB);

    //Food Utility
    public static final RegistryObject<Block> DRYING_RACK = registerBlock("drying_rack",
            () -> new DryingRackBlock(PropertiesInit.DRYING_RACK), BTDMain.NATURAL_TAB);
    public static final RegistryObject<Block> CROCK_POT = registerBlock("crock_pot",
            () -> new CrockPotBlock(PropertiesInit.CROCK_POT), BTDMain.NATURAL_TAB);

    public static final RegistryObject<Block> PUMPKIN_LANTERN = registerBlock("pumpkin_lantern",
            () -> new PumpkinLanternBlock(PropertiesInit.PUMPKIN_LANTERN), BTDMain.LIGHT_TAB);

    public static final RegistryObject<Block> WOOD_LANTERN = registerHiddenBlock("wood_lantern",
            () -> new WoodLanternBlock(PropertiesInit.LANTERN));

    public static final RegistryObject<Block> PLUGGED_SINKHOLE = registerBlock("plugged_sinkhole",
            () -> new PluggedSinkholeBlock(PropertiesInit.PLUGGED_SINKHOLE), BTDMain.NATURAL_TAB);

    public static final RegistryObject<Block> SINKHOLE = registerBlock("sinkhole",
            () -> new SinkholeBlock(PropertiesInit.SINKHOLE), BTDMain.NATURAL_TAB);

    public static final RegistryObject<Block> RED_MUSHROOM = registerHiddenBlock("red_mushroom",
            () -> new RedMushroomBlock(PropertiesInit.FLORA));

    public static final RegistryObject<Block> GREEN_MUSHROOM = registerHiddenBlock("green_mushroom",
            () -> new GreenMushroomBlock(PropertiesInit.FLORA));

    public static final RegistryObject<Block> BLUE_MUSHROOM = registerHiddenBlock("blue_mushroom",
            () -> new BlueMushroomBlock(PropertiesInit.FLORA));

    public static final RegistryObject<Block> LIGHT_FLOWER = registerBlock("light_flower",
            () -> new LightFlowerBlock(PropertiesInit.FLORA), BTDMain.NATURAL_TAB);

    public static final RegistryObject<Block> EVERGREEN_SAPLING = registerBlock("evergreen_sapling",
            () -> new EvergreenSaplingBlock(new EvergreenTreeGrower(), BlockBehaviour.Properties.copy(Blocks.SPRUCE_SAPLING)), BTDMain.NATURAL_TAB);

    public static final RegistryObject<Block> CAVE_FERN_VARIANT_ONE = registerHiddenBlock("cave_fern_variant_one",
            () -> new CaveFernBlock(PropertiesInit.FERN));
    public static final RegistryObject<Block> CAVE_FERN_VARIANT_TWO = registerHiddenBlock("cave_fern_variant_two",
            () -> new CaveFernBlock(PropertiesInit.FERN));
    public static final RegistryObject<Block> CAVE_FERN_VARIANT_THREE = registerHiddenBlock("cave_fern_variant_three",
            () -> new CaveFernBlock(PropertiesInit.FERN));
    public static final RegistryObject<Block> CAVE_FERN_VARIANT_FOUR = registerHiddenBlock("cave_fern_variant_four",
            () -> new CaveFernBlock(PropertiesInit.FERN));
    public static final RegistryObject<Block> CAVE_FERN_VARIANT_FIVE = registerHiddenBlock("cave_fern_variant_five",
            () -> new CaveFernBlock(PropertiesInit.FERN));
    public static final RegistryObject<Block> CAVE_FERN_VARIANT_SIX = registerHiddenBlock("cave_fern_variant_six",
            () -> new CaveFernBlock(PropertiesInit.FERN));
    public static final RegistryObject<Block> CAVE_FERN_VARIANT_SEVEN = registerHiddenBlock("cave_fern_variant_seven",
            () -> new CaveFernBlock(PropertiesInit.FERN));
    public static final RegistryObject<Block> CAVE_FERN_VARIANT_EIGHT = registerHiddenBlock("cave_fern_variant_eight",
            () -> new CaveFernBlock(PropertiesInit.FERN));
    public static final RegistryObject<Block> CAVE_FERN_VARIANT_NINE = registerHiddenBlock("cave_fern_variant_nine",
            () -> new CaveFernBlock(PropertiesInit.FERN));
    public static final RegistryObject<Block> CAVE_FERN_VARIANT_TEN = registerHiddenBlock("cave_fern_variant_ten",
            () -> new CaveFernBlock(PropertiesInit.FERN));

    private static <T extends Block> RegistryObject<T> registerBlock(String name, Supplier<T> block, CreativeModeTab tab) {
        RegistryObject<T> toReturn = BLOCKS.register(name, block);
        registerBlockItem(name, toReturn, tab);
        return toReturn;
    }

    private static <T extends Block> RegistryObject<T> registerHiddenBlock(String name, Supplier<T> block) {
        RegistryObject<T> toReturn = BLOCKS.register(name, block);
        return toReturn;
    }

    private static <T extends Block> RegistryObject<Item> registerBlockItem(String name, RegistryObject<T> block,
                                                                            CreativeModeTab tab) {
        return ItemInit.ITEMS.register(name, () -> new BlockItem(block.get(),
                new Item.Properties().tab(tab)));
    }

}
