package com.ladestitute.bewarethedark.registries;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.util.BTDLoc;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.biome.Biome;

import java.util.ArrayList;

public class BiomeInit {
    //Terrablender classes are in the world/biome package
    private static final ArrayList<ResourceKey<Biome>> KEYS = new ArrayList<ResourceKey<Biome>>();

    public static final ResourceKey<Biome> ROCKYLAND = register("rockyland");
    public static final ResourceKey<Biome> DECIDUOUS_FOREST = register("deciduous_forest");
    public static final ResourceKey<Biome> CREEPY_FOREST = register("creepy_forest");
    public static final ResourceKey<Biome> LUMPY_FOREST = register("lumpy_forest");
    public static final ResourceKey<Biome> MARSH = register("marsh");
    public static final ResourceKey<Biome> GRAVEYARD = register("graveyard");
    public static final ResourceKey<Biome> CAVES = register("caves");
    public static final ResourceKey<Biome> SUNKEN_FOREST = register("sunken_forest");
    public static final ResourceKey<Biome> RED_MUSHTREE_FOREST = register("red_mushtree_forest");
    public static final ResourceKey<Biome> GREEN_MUSHTREE_FOREST = register("green_mushtree_forest");
    public static final ResourceKey<Biome> BLUE_MUSHTREE_FOREST = register("blue_mushtree_forest");
    public static final ResourceKey<Biome> CAVE_MARSH = register("cave_marsh");

    public static ArrayList<ResourceKey<Biome>> getAllKeys() {
        return KEYS;
    }

    private static ResourceKey<Biome> register(String name)
    {
        ResourceKey<Biome> key = ResourceKey.create(Registry.BIOME_REGISTRY, BTDLoc.createLoc(name));
        KEYS.add(key);
        return key;
    }



}
