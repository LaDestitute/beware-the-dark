package com.ladestitute.bewarethedark.registries;

import com.google.common.collect.ImmutableList;
import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.world.feature.BlueMushroomFeature;
import com.ladestitute.bewarethedark.world.feature.GreenMushroomFeature;
import com.ladestitute.bewarethedark.world.feature.RedMushroomFeature;
import net.minecraft.core.Direction;
import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicate;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.RandomPatchConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.SimpleBlockConfiguration;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.placement.*;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

import java.util.List;
import java.util.function.Supplier;

public class MushroomInit {
    public static final DeferredRegister<Feature<?>> FEATURES = DeferredRegister.create(ForgeRegistries.FEATURES, BTDMain.MOD_ID);
    public static final RegistryObject<Feature<SimpleBlockConfiguration>> RED_MUSHROOM =
            FEATURES.register("red_mushroom", () -> new RedMushroomFeature(SimpleBlockConfiguration.CODEC));
    public static final RegistryObject<Feature<SimpleBlockConfiguration>> GREEN_MUSHROOM =
            FEATURES.register("green_mushroom", () -> new GreenMushroomFeature(SimpleBlockConfiguration.CODEC));
    public static final RegistryObject<Feature<SimpleBlockConfiguration>> BLUE_MUSHROOM =
            FEATURES.register("blue_mushroom", () -> new BlueMushroomFeature(SimpleBlockConfiguration.CODEC));

    public static final class NeapolitanConfiguredFeatures {
        public static final DeferredRegister<ConfiguredFeature<?, ?>> CONFIGURED_FEATURES = DeferredRegister.create(Registry.CONFIGURED_FEATURE_REGISTRY, BTDMain.MOD_ID);

        public static final RegistryObject<ConfiguredFeature<RandomPatchConfiguration, ?>> PATCH_RED_MUSHROOM = register("patch_red_mushroom", () ->
                new ConfiguredFeature<>(Feature.RANDOM_PATCH, new RandomPatchConfiguration(128, 16, 16,
                PlacementUtils.filtered(RED_MUSHROOM.get(),
                new SimpleBlockConfiguration(BlockStateProvider.simple(SpecialBlockInit.RED_MUSHROOM_SPAWNER.get().defaultBlockState())),
                simplePatchPredicate(List.of(Blocks.GRASS_BLOCK,BlockInit.FOREST_TURF.get(),BlockInit.DECIDUOUS_TURF.get(),BlockInit.RED_FUNGAL_TURF.get()))))));
        public static final RegistryObject<ConfiguredFeature<RandomPatchConfiguration, ?>> PATCH_GREEN_MUSHROOM = register("patch_green_mushroom", () ->
                new ConfiguredFeature<>(Feature.RANDOM_PATCH, new RandomPatchConfiguration(128, 16, 16,
                        PlacementUtils.filtered(GREEN_MUSHROOM.get(),
                                new SimpleBlockConfiguration(BlockStateProvider.simple(SpecialBlockInit.GREEN_MUSHROOM_SPAWNER.get().defaultBlockState())),
                                simplePatchPredicate(List.of(Blocks.GRASS_BLOCK,Blocks.MUD,BlockInit.FOREST_TURF.get(),BlockInit.DECIDUOUS_TURF.get(),BlockInit.MARSH_TURF.get()))))));
        public static final RegistryObject<ConfiguredFeature<RandomPatchConfiguration, ?>> PATCH_BLUE_MUSHROOM = register("patch_blue_mushroom", () ->
                new ConfiguredFeature<>(Feature.RANDOM_PATCH, new RandomPatchConfiguration(128, 16, 16,
                        PlacementUtils.filtered(BLUE_MUSHROOM.get(),
                                new SimpleBlockConfiguration(BlockStateProvider.simple(SpecialBlockInit.BLUE_MUSHROOM_SPAWNER.get().defaultBlockState())),
                                simplePatchPredicate(List.of(Blocks.GRASS_BLOCK,Blocks.MUD,BlockInit.FOREST_TURF.get(),BlockInit.DECIDUOUS_TURF.get(),BlockInit.MARSH_TURF.get()))))));

        private static <FC extends FeatureConfiguration, F extends Feature<FC>> RegistryObject<ConfiguredFeature<FC, ?>> register(String name, Supplier<ConfiguredFeature<FC, F>> feature) {
            return CONFIGURED_FEATURES.register(name, feature);
        }

        private static BlockPredicate simplePatchPredicate(List<Block> matchBlocks) {
            BlockPredicate blockpredicate;
            if (!matchBlocks.isEmpty()) {
                blockpredicate = BlockPredicate.allOf(BlockPredicate.ONLY_IN_AIR_PREDICATE, BlockPredicate.matchesBlocks(Direction.DOWN.getNormal(), matchBlocks));
            } else {
                blockpredicate = BlockPredicate.ONLY_IN_AIR_PREDICATE;
            }

            return blockpredicate;
        }
    }

    public static final class NeapolitanPlacedFeatures {
        public static final DeferredRegister<PlacedFeature> PLACED_FEATURES = DeferredRegister.create(Registry.PLACED_FEATURE_REGISTRY, BTDMain.MOD_ID);

        public static final RegistryObject<PlacedFeature> PATCH_RED_MUSHROOM = register("patch_red_mushroom",
                NeapolitanConfiguredFeatures.PATCH_RED_MUSHROOM, RarityFilter.onAverageOnceEvery(64), InSquarePlacement.spread(),
                PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome());
        public static final RegistryObject<PlacedFeature> PATCH_GREEN_MUSHROOM = register("patch_green_mushroom",
                NeapolitanConfiguredFeatures.PATCH_GREEN_MUSHROOM, RarityFilter.onAverageOnceEvery(64), InSquarePlacement.spread(),
                PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome());
        public static final RegistryObject<PlacedFeature> PATCH_BLUE_MUSHROOM = register("patch_blue_mushroom",
                NeapolitanConfiguredFeatures.PATCH_BLUE_MUSHROOM, RarityFilter.onAverageOnceEvery(64), InSquarePlacement.spread(),
                PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome());

        @SuppressWarnings("unchecked")
        private static RegistryObject<PlacedFeature> register(String name, RegistryObject<? extends ConfiguredFeature<?, ?>> feature, PlacementModifier... placementModifiers) {
            return PLACED_FEATURES.register(name, () -> new PlacedFeature((Holder<ConfiguredFeature<?, ?>>) feature.getHolder().get(), ImmutableList.copyOf(placementModifiers)));
        }
    }
}
