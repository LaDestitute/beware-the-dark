package com.ladestitute.bewarethedark.registries;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.items.consumables.BugNetItem;
import com.ladestitute.bewarethedark.items.consumables.HealingSalveItem;
import com.ladestitute.bewarethedark.items.consumables.SpiderGlandItem;
import com.ladestitute.bewarethedark.items.dress.GarlandItem;
import com.ladestitute.bewarethedark.items.dress.MinerHatItem;
import com.ladestitute.bewarethedark.items.fight.*;
import com.ladestitute.bewarethedark.items.living.BeeItem;
import com.ladestitute.bewarethedark.items.living.ButterflyItem;
import com.ladestitute.bewarethedark.items.living.FirefliesItem;
import com.ladestitute.bewarethedark.items.materials.*;
import com.ladestitute.bewarethedark.items.plants.*;
import com.ladestitute.bewarethedark.items.refined.BoardsItem;
import com.ladestitute.bewarethedark.items.refined.CutStoneItem;
import com.ladestitute.bewarethedark.items.refined.RopeItem;
import com.ladestitute.bewarethedark.items.tools.BTDFishingRodItem;
import com.ladestitute.bewarethedark.items.tools.HammerItem;
import com.ladestitute.bewarethedark.items.tools.PitchforkItem;
import com.ladestitute.bewarethedark.items.tools.flint.FlintAxeItem;
import com.ladestitute.bewarethedark.items.tools.flint.FlintPickaxeItem;
import com.ladestitute.bewarethedark.items.tools.flint.FlintShovelItem;
import com.ladestitute.bewarethedark.items.tools.luxuriant.LuxuryAxeItem;
import com.ladestitute.bewarethedark.items.tools.luxuriant.OpulentPickaxeItem;
import com.ladestitute.bewarethedark.items.tools.luxuriant.RegalShovelItem;
import com.ladestitute.bewarethedark.items.utility.AntiVenomItem;
import com.ladestitute.bewarethedark.items.utility.BackpackItem;
import com.ladestitute.bewarethedark.items.utility.StrawRollItem;
import com.ladestitute.bewarethedark.items.utility.TorchItem;
import com.ladestitute.bewarethedark.items.utility.blueprints.*;
import com.ladestitute.bewarethedark.util.enums.BTDArmorMaterials;
import com.ladestitute.bewarethedark.util.enums.BTDToolMaterials;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.animal.Cat;
import net.minecraft.world.item.*;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ItemInit {

    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS,
            BTDMain.MOD_ID);

    //Materials
    public static final RegistryObject<Item> PINE_CONE = ITEMS.register("pine_cone",
            () -> new PineConeItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> BIRCHNUT = ITEMS.register("birchnut",
            () -> new BirchnutItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> ASHES = ITEMS.register("ashes",
            () -> new AshesItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> TWIGS = ITEMS.register("twigs",
            () -> new TwigsItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> CUT_GRASS = ITEMS.register("cut_grass",
            () -> new CutGrassItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> GUANO = ITEMS.register("guano",
            () -> new GuanoItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> SPRUCE_LOG = ITEMS.register("spruce_log",
            () -> new SpruceLogItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> MANURE = ITEMS.register("manure",
            () -> new ManureItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> NITRE = ITEMS.register("nitre",
            () -> new NitreItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> ROCKS = ITEMS.register("rocks",
            () -> new RocksItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> SILK = ITEMS.register("silk",
            () -> new SilkItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> CUT_REEDS = ITEMS.register("cut_reeds",
            () -> new CutReedsItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> SPIDER_GLAND = ITEMS.register("spider_gland",
            () -> new SpiderGlandItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
//    public static final RegistryObject<Item> PETALS = ITEMS.register("petals",
//            () -> new PetalsItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
//        public static final RegistryObject<Item> FOILAGE = ITEMS.register("foilage",
//            () -> new PetalsItem(new Item.Properties()));
    public static final RegistryObject<Item> TENTACLE_SPOTS = ITEMS.register("tentacle_spots",
            () -> new TentacleSpotsItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> CAT_TAIL = ITEMS.register("cat_tail",
            () -> new CatTailItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> CORAL = ITEMS.register("coral",
            () -> new CoralItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));

    //Plants
    public static final RegistryObject<Item> BERRY_BUSH_ITEM = ITEMS.register("itemberry_bush",
            () -> new BerryBushItem(new Item.Properties().tab(BTDMain.NATURAL_TAB)));
    public static final RegistryObject<Item> SAPLING_PLANT_ITEM = ITEMS.register("itemsapling",
            () -> new SaplingPlantItem(new Item.Properties().tab(BTDMain.NATURAL_TAB)));
    public static final RegistryObject<Item> GRASS_TUFT_ITEM = ITEMS.register("itemgrass_tuft",
            () -> new GrassTuftItem(new Item.Properties().tab(BTDMain.NATURAL_TAB)));
    public static final RegistryObject<Item> SPIKY_BUSH_ITEM = ITEMS.register("itemspiky_bush",
            () -> new SpikyBushItem(new Item.Properties().tab(BTDMain.NATURAL_TAB)));


    public static final RegistryObject<Item> BEE = ITEMS.register("bee_item",
            () -> new BeeItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> BUTTERFLY = ITEMS.register("butterfly",
            () -> new ButterflyItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));
    public static final RegistryObject<Item> JUNGLE_BUTTERFLY = ITEMS.register("jungle_butterfly",
            () -> new ButterflyItem(new Item.Properties().tab(BTDMain.MATERIALS_TAB)));

    //Refined
    public static final RegistryObject<Item> BOARDS = ITEMS.register("boards",
            () -> new BoardsItem(new Item.Properties().tab(BTDMain.REFINED_TAB)));
    public static final RegistryObject<Item> CUT_STONE = ITEMS.register("cut_stone",
            () -> new CutStoneItem(new Item.Properties().tab(BTDMain.REFINED_TAB)));
    public static final RegistryObject<Item> ROPE = ITEMS.register("rope",
            () -> new RopeItem(new Item.Properties().tab(BTDMain.REFINED_TAB)));

    //Fight
    public static final RegistryObject<ArmorItem> LOG_SUIT = ITEMS.register("log_suit",
            () -> new LogSuitItem(BTDArmorMaterials.LOG_SUIT, EquipmentSlot.CHEST,
                    new Item.Properties().tab(BTDMain.FIGHT_TAB)));
    public static final RegistryObject<SwordItem> SPEAR = ITEMS.register("spear",
            () -> new SpearItem(BTDToolMaterials.SPEAR, 0, -2.4f,
                    new Item.Properties().tab(BTDMain.FIGHT_TAB)));
    public static final RegistryObject<Item> PROJ_DART = ITEMS.register("projectile_dart",
            () -> new Item(new Item.Properties()));
    public static final RegistryObject<Item> DART = ITEMS.register("blow_dart",
            () -> new DartItem(new Item.Properties().tab(BTDMain.FIGHT_TAB)));
    public static final RegistryObject<Item> FIRE_DART = ITEMS.register("fire_blow_dart",
            () -> new FireDartItem(new Item.Properties().tab(BTDMain.FIGHT_TAB)));
    public static final RegistryObject<Item> ELECTRIC_DART = ITEMS.register("electric_blow_dart",
            () -> new ElectricDartItem(new Item.Properties().tab(BTDMain.FIGHT_TAB)));
    public static final RegistryObject<Item> POISON_DART = ITEMS.register("poison_blow_dart",
            () -> new PoisonDartItem(new Item.Properties().tab(BTDMain.FIGHT_TAB)));
    public static final RegistryObject<SwordItem> TENTACLE_SPIKE = ITEMS.register("tentacle_spike",
            () -> new TentacleSpikeItem(BTDToolMaterials.TENTACLE_SPIKE, 0, -2.4f,
                    new Item.Properties().tab(BTDMain.FIGHT_TAB)));
    public static final RegistryObject<SwordItem> TAIL_O_THREE_CATS = ITEMS.register("tail_o_three_cats",
            () -> new TailOThreeCatsItem(BTDToolMaterials.TAIL_O_THREE_CATS, 0, -2.4f,
                    new Item.Properties().tab(BTDMain.FIGHT_TAB)));
    public static final RegistryObject<SwordItem> POISON_SPEAR = ITEMS.register("poison_spear",
            () -> new PoisonSpearItem(BTDToolMaterials.SPEAR, 0, -2.4f,
                    new Item.Properties().tab(BTDMain.FIGHT_TAB)));


    //Tools
    public static final RegistryObject<AxeItem> FLINT_AXE = ITEMS.register("flint_axe",
            () -> new FlintAxeItem(BTDToolMaterials.FLINT, 2, 5.0f,
                    new Item.Properties().tab(BTDMain.TOOLS_TAB)));
    public static final RegistryObject<AxeItem> LUXURY_AXE = ITEMS.register("luxury_axe",
            () -> new LuxuryAxeItem(BTDToolMaterials.LUXURIANT, 4, 5.0f,
                    new Item.Properties().tab(BTDMain.TOOLS_TAB)));
    public static final RegistryObject<PickaxeItem> FLINT_PICKAXE = ITEMS.register("flint_pickaxe",
            () -> new FlintPickaxeItem(BTDToolMaterials.FLINT, 2, 5.0f,
                    new Item.Properties().tab(BTDMain.TOOLS_TAB)));
    public static final RegistryObject<PickaxeItem> OPULENT_PICKAXE = ITEMS.register("opulent_pickaxe",
            () -> new OpulentPickaxeItem(BTDToolMaterials.LUXURIANT, 4, 5.0f,
                    new Item.Properties().tab(BTDMain.TOOLS_TAB)));
    public static final RegistryObject<Item> FLINT_SHOVEL = ITEMS.register("flint_shovel",
            () -> new FlintShovelItem(BTDToolMaterials.FLINT, 2, 5.0f,
                    new Item.Properties().tab(BTDMain.TOOLS_TAB)));
    public static final RegistryObject<Item> REGAL_SHOVEL = ITEMS.register("regal_shovel",
            () -> new RegalShovelItem(BTDToolMaterials.LUXURIANT, 4, 5.0f,
                    new Item.Properties().tab(BTDMain.TOOLS_TAB)));
    public static final RegistryObject<Item> PITCHFORK = ITEMS.register("pitchfork",
            () -> new PitchforkItem(BTDToolMaterials.PITCHFORK, 4, 5.0f,
                    new Item.Properties().tab(BTDMain.TOOLS_TAB)));
    public static final RegistryObject<Item> HAMMER = ITEMS.register("hammer",
            () -> new HammerItem(new Item.Properties().tab(BTDMain.TOOLS_TAB)));

    //Light
    public static final RegistryObject<Item> TORCH = ITEMS.register("torchitem",
            () -> new TorchItem(new Item.Properties().tab(BTDMain.LIGHT_TAB)));
    public static final RegistryObject<Item> CAMPFIRE_BLOCK = ITEMS.register("campfire",
            () -> new BlockItem(SpecialBlockInit.CAMP_FIRE.get(), new Item.Properties().tab(BTDMain.LIGHT_TAB)));
    public static final RegistryObject<Item> FIRE_PIT_BLOCK = ITEMS.register("fire_pit",
            () -> new BlockItem(SpecialBlockInit.FIRE_PIT.get(), new Item.Properties().tab(BTDMain.LIGHT_TAB)));
    public static final RegistryObject<Item> FIRE_PIT_OFF_BLOCK = ITEMS.register("fire_pit_off",
            () -> new BlockItem(SpecialBlockInit.FIRE_PIT_OFF.get(), new Item.Properties().tab(BTDMain.LIGHT_TAB)));

    //Survival
    public static final RegistryObject<Item> FISHING_ROD = ITEMS.register("fishing_rod",
            () -> new BTDFishingRodItem(new Item.Properties()));
    public static final RegistryObject<Item> HEALING_SALVE = ITEMS.register("healing_salve",
            () -> new HealingSalveItem(new Item.Properties().tab(BTDMain.SURVIVAL_TAB)));
    public static final RegistryObject<Item> BUG_NET = ITEMS.register("bug_net",
            () -> new BugNetItem(new Item.Properties().tab(BTDMain.SURVIVAL_TAB)));
    public static final RegistryObject<ArmorItem> BACKPACK = ITEMS.register("backpack",
            () -> new BackpackItem(BTDArmorMaterials.BACKPACK, EquipmentSlot.CHEST,
                    new BackpackItem.Properties().tab(BTDMain.SURVIVAL_TAB)));
    public static final RegistryObject<Item> STRAW_ROLL = ITEMS.register("straw_roll",
            () -> new StrawRollItem(new Item.Properties().tab(BTDMain.SURVIVAL_TAB)));
    public static final RegistryObject<ArmorItem> GARLAND = ITEMS.register("garland",
            () -> new GarlandItem(BTDArmorMaterials.GARLAND, EquipmentSlot.HEAD,
                    new Item.Properties().tab(BTDMain.SURVIVAL_TAB)));
    public static final RegistryObject<ArmorItem> MINER_HAT = ITEMS.register("miner_hat",
            () -> new MinerHatItem(BTDArmorMaterials.MINER_HAT, EquipmentSlot.HEAD,
                    new Item.Properties().tab(BTDMain.SURVIVAL_TAB)));
    public static final RegistryObject<Item> KINDLING = ITEMS.register("kindling",
            () -> new Item(new Item.Properties().tab(BTDMain.SURVIVAL_TAB)));

    //Food
    public static final RegistryObject<Item> BASIC_FARM_BLOCK = ITEMS.register("basic_farm",
            () -> new BlockItem(SpecialBlockInit.BASIC_FARM.get(), new Item.Properties().tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> IMPROVED_FARM_BLOCK = ITEMS.register("improved_farm",
            () -> new BlockItem(SpecialBlockInit.IMPROVED_FARM.get(), new Item.Properties().tab(BTDMain.FOOD_TAB)));

    //Science
    public static final RegistryObject<Item> SCIENCE_MACHINE_BLOCK = ITEMS.register("science_machine",
            () -> new BlockItem(SpecialBlockInit.SCIENCE_MACHINE.get(), new Item.Properties().tab(BTDMain.SCIENCE_TAB)));

    //Blueprints
    public static final RegistryObject<Item> ANTI_VENOM_BLUEPRINT = ITEMS.register("anti_venom_blueprint",
            () -> new AntiVenomBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> BACKPACK_BLUEPRINT = ITEMS.register("backpack_blueprint",
            () -> new BackpackBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> BASIC_FARM_BLUEPRINT = ITEMS.register("basic_farm_blueprint",
            () -> new BasicFarmBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> BOARDS_BLUEPRINT = ITEMS.register("boards_blueprint",
            () -> new BoardsBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> BUG_NET_BLUEPRINT = ITEMS.register("bug_net_blueprint",
            () -> new BugNetBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> CARPETED_FLOORING_BLUEPRINT = ITEMS.register("carpeted_flooring_blueprint",
            () -> new CarpetedFlooringBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> CHEST_BLUEPRINT = ITEMS.register("chest_blueprint",
            () -> new ChestBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> COBBLESTONES_BLUEPRINT = ITEMS.register("cobblestones_blueprint",
            () -> new CobblestonesBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> COMPASS_BLUEPRINT = ITEMS.register("compass_blueprint",
            () -> new CompassBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> CUT_STONE_BLUEPRINT = ITEMS.register("cut_stone_blueprint",
            () -> new CutStoneBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> DART_BLUEPRINT = ITEMS.register("dart_blueprint",
            () -> new DartBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> ELECTRIC_DART_BLUEPRINT = ITEMS.register("electric_dart_blueprint",
            () -> new ElectricDartBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> FIRE_DART_BLUEPRINT = ITEMS.register("fire_dart_blueprint",
            () -> new FireDartBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> FISHING_ROD_BLUEPRINT = ITEMS.register("fishing_rod_blueprint",
            () -> new FishingRodBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> GUANO_TURF_BLUEPRINT = ITEMS.register("guano_turf_blueprint",
            () -> new GuanoTurfBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> HAMMER_BLUEPRINT = ITEMS.register("hammer_blueprint",
            () -> new HammerBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> HEALING_SALVE_BLUEPRINT = ITEMS.register("healing_salve_blueprint",
            () -> new HealingSalveBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> IMPROVED_FARM_BLUEPRINT = ITEMS.register("improved_farm_blueprint",
            () -> new ImprovedFarmBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> LANTERN_BLUEPRINT = ITEMS.register("lantern_blueprint",
            () -> new LanternBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> LOG_SUIT_BLUEPRINT = ITEMS.register("log_suit_blueprint",
            () -> new LogSuitBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> LUXURY_AXE_BLUEPRINT = ITEMS.register("luxury_axe_blueprint",
            () -> new LuxuryAxeBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> MINER_HAT_BLUEPRINT = ITEMS.register("miner_hat_blueprint",
            () -> new MinerHatBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> OPULENT_PICKAXE_BLUEPRINT = ITEMS.register("opulent_pickaxe_blueprint",
            () -> new OpulentPickaxeBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> PAPER_BLUEPRINT = ITEMS.register("paper_blueprint",
            () -> new PaperBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> PITCHFORK_BLUEPRINT = ITEMS.register("pitchfork_blueprint",
            () -> new PitchforkBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> POISON_DART_BLUEPRINT = ITEMS.register("poison_dart_blueprint",
            () -> new PoisonDartBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> POISON_SPEAR_BLUEPRINT = ITEMS.register("poison_spear_blueprint",
            () -> new PoisonSpearBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> PUMPKIN_LANTERN_BLUEPRINT = ITEMS.register("pumpkin_lantern_blueprint",
            () -> new PumpkinLanternBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> REGAL_SHOVEL_BLUEPRINT = ITEMS.register("regal_shovel_blueprint",
            () -> new RegalShovelBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> ROPE_BLUEPRINT = ITEMS.register("rope_blueprint",
            () -> new RopeBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> SHOVEL_BLUEPRINT = ITEMS.register("shovel_blueprint",
            () -> new ShovelBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> SIGN_BLUEPRINT = ITEMS.register("sign_blueprint",
            () -> new SignBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> SPEAR_BLUEPRINT = ITEMS.register("spear_blueprint",
            () -> new SpearBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> STRAW_ROLL_BLUEPRINT = ITEMS.register("straw_roll_blueprint",
            () -> new StrawRollBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> TAIL_O_THREE_CATS_BLUEPRINT = ITEMS.register("tail_o_three_cats_blueprint",
            () -> new TailOThreeCatsBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));
    public static final RegistryObject<Item> WOODEN_FLOORING_BLUEPRINT = ITEMS.register("wooden_flooring_blueprint",
            () -> new WoodenFlooringBlueprintItem(new Item.Properties().tab(BTDMain.SCIENCE_TAB)));

        public static final RegistryObject<Item> WOOD_LANTERN = ITEMS.register("wood_lantern",
            () -> new BlockItem(BlockInit.WOOD_LANTERN.get(), new Item.Properties().tab(BTDMain.LIGHT_TAB).stacksTo(1)));


    //Living things
    public static final RegistryObject<Item> FIREFLIES = ITEMS.register("fireflies",
            () -> new FirefliesItem(new Item.Properties().tab(BTDMain.LIGHT_TAB)));


}
