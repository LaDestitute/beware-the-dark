package com.ladestitute.bewarethedark.registries;


import com.google.common.collect.ImmutableList;
import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.blocks.natural.plant.BerryBushBlock;
import com.ladestitute.bewarethedark.blocks.natural.plant.GrassTuftBlock;
import com.ladestitute.bewarethedark.blocks.natural.plant.SaplingPlantBlock;
import com.ladestitute.bewarethedark.blocks.world.GraveBlock;
import java.util.List;
import java.util.function.Supplier;

import com.ladestitute.bewarethedark.world.feature.RedMushroomFeature;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.util.RandomSource;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.CropBlock;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicate;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.RandomPatchConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.SimpleBlockConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;
import net.minecraft.world.level.levelgen.feature.featuresize.TwoLayersFeatureSize;
import net.minecraft.world.level.levelgen.feature.foliageplacers.AcaciaFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.BlobFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.PineFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.SpruceFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.trunkplacers.ForkingTrunkPlacer;
import net.minecraft.world.level.levelgen.feature.trunkplacers.StraightTrunkPlacer;
import net.minecraft.world.level.levelgen.placement.*;
import net.minecraftforge.common.property.Properties;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class FeatureInit {

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> TUMBLEWEED = FeatureUtils.register("tumbleweed",
            Feature.FLOWER, new RandomPatchConfiguration(46, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.TUMBLE_SPAWNER.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> FIREFLIES = FeatureUtils.register("fireflies",
            Feature.FLOWER, new RandomPatchConfiguration(10, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.FIREFLIES_SPAWNER.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> FIREFLIES_DECIDUOUS = FeatureUtils.register("fireflies_deciduous",
            Feature.FLOWER, new RandomPatchConfiguration(15, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.FIREFLIES_SPAWNER.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> NIGHTHAND = FeatureUtils.register("nighthand",
            Feature.FLOWER, new RandomPatchConfiguration(9, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.NIGHTHAND_SPAWNER.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> MR_SKITTS = FeatureUtils.register("mr_skitts",
            Feature.FLOWER, new RandomPatchConfiguration(4, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.MR_SKITTS_SPAWNER.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> SHADOW_WATCHER = FeatureUtils.register("shadow_watcher",
            Feature.FLOWER, new RandomPatchConfiguration(7, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.SHADOW_WATCHER_SPAWNER.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> CRAWLING_HORROR = FeatureUtils.register("crawling_horror",
            Feature.FLOWER, new RandomPatchConfiguration(10, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.CRAWLING_HORROR_SPAWNER.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> TERRORBEAK = FeatureUtils.register("terrorbeak",
            Feature.FLOWER, new RandomPatchConfiguration(5, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.TERRORBEAK_SPAWNER.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> TENTACLE = FeatureUtils.register("tentacle",
            Feature.FLOWER, new RandomPatchConfiguration(240, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.TENTACLE_SPAWNER.get())))));

    /////

    public static final Holder<ConfiguredFeature<TreeConfiguration, ?>> ORANGE_BIRCHNUT =
            FeatureUtils.register("orange_birchnut", Feature.TREE, new TreeConfiguration.TreeConfigurationBuilder(
                    BlockStateProvider.simple(Blocks.BIRCH_LOG),
                    new StraightTrunkPlacer(4, 1, 1),
                    BlockStateProvider.simple(BlockInit.ORANGE_DECIDUOUS_LEAVES.get()),
                    new BlobFoliagePlacer(ConstantInt.of(2), ConstantInt.of(0), 3),
                    new TwoLayersFeatureSize(1, 0, 2))
                    .dirt(BlockStateProvider.simple(SpecialBlockInit.BIRCHNUT_STUMP.get()))
                    .forceDirt()
                    .ignoreVines().build());

    public static final Holder<ConfiguredFeature<TreeConfiguration, ?>> RED_BIRCHNUT =
            FeatureUtils.register("red_birchnut", Feature.TREE, new TreeConfiguration.TreeConfigurationBuilder(
                    BlockStateProvider.simple(Blocks.BIRCH_LOG),
                    new StraightTrunkPlacer(4, 1, 1),
                    BlockStateProvider.simple(BlockInit.RED_DECIDUOUS_LEAVES.get()),
                    new BlobFoliagePlacer(ConstantInt.of(2), ConstantInt.of(0), 3),
                    new TwoLayersFeatureSize(1, 0, 2))
                    .dirt(BlockStateProvider.simple(SpecialBlockInit.BIRCHNUT_STUMP.get()))
                    .forceDirt()
                    .ignoreVines().build());

    public static final Holder<ConfiguredFeature<TreeConfiguration, ?>> YELLOW_BIRCHNUT =
            FeatureUtils.register("yellow_birchnut", Feature.TREE, new TreeConfiguration.TreeConfigurationBuilder(
                    BlockStateProvider.simple(Blocks.BIRCH_LOG),
                    new StraightTrunkPlacer(4, 1, 1),
                    BlockStateProvider.simple(BlockInit.YELLOW_DECIDUOUS_LEAVES.get()),
                    new BlobFoliagePlacer(ConstantInt.of(2), ConstantInt.of(0), 3),
                    new TwoLayersFeatureSize(1, 0, 2))
                            .dirt(BlockStateProvider.simple(SpecialBlockInit.BIRCHNUT_STUMP.get()))
                            .forceDirt()
                    .ignoreVines().build());

    public static final Holder<ConfiguredFeature<TreeConfiguration, ?>> GREEN_BIRCHNUT =
            FeatureUtils.register("green_birchnut", Feature.TREE, new TreeConfiguration.TreeConfigurationBuilder(
                    BlockStateProvider.simple(Blocks.BIRCH_LOG),
                    new StraightTrunkPlacer(4, 1, 1),
                    BlockStateProvider.simple(BlockInit.GREEN_DECIDUOUS_LEAVES.get()),
                    new BlobFoliagePlacer(ConstantInt.of(2), ConstantInt.of(0), 3),
                    new TwoLayersFeatureSize(1, 0, 2))
                    .dirt(BlockStateProvider.simple(SpecialBlockInit.BIRCHNUT_STUMP.get()))
                    .forceDirt()
                    .ignoreVines().build());

    public static final Holder<ConfiguredFeature<TreeConfiguration, ?>> EVERGREEN =
            FeatureUtils.register("evergreen", Feature.TREE, new TreeConfiguration.TreeConfigurationBuilder(
                    BlockStateProvider.simple(BlockInit.EVERGREEN.get()),
                    new StraightTrunkPlacer(4, 1, 1),
                    BlockStateProvider.simple(BlockInit.EVERGREEN_LEAVES.get()),
                    new PineFoliagePlacer(ConstantInt.of(1), ConstantInt.of(1), UniformInt.of(3, 4)),
                    new TwoLayersFeatureSize(1, 0, 2))
                    .dirt(BlockStateProvider.simple(SpecialBlockInit.STUMP.get()))
                    .forceDirt()
                    .ignoreVines().build());

    public static final Holder<ConfiguredFeature<TreeConfiguration, ?>> LUMPY_EVERGREEN =
            FeatureUtils.register("lumpy_evergreen", Feature.TREE, new TreeConfiguration.TreeConfigurationBuilder(
                    BlockStateProvider.simple(BlockInit.LUMPY_EVERGREEN.get()),
                    new StraightTrunkPlacer(4, 1, 1),
                    BlockStateProvider.simple(BlockInit.LUMPY_EVERGREEN_LEAVES.get()),
                    new PineFoliagePlacer(ConstantInt.of(1), ConstantInt.of(1), UniformInt.of(3, 4)),
                    new TwoLayersFeatureSize(1, 0, 2))
                    .dirt(BlockStateProvider.simple(SpecialBlockInit.STUMP.get()))
                    .forceDirt()
                    .ignoreVines().build());


    public static final Holder<ConfiguredFeature<TreeConfiguration, ?>> SPIKY_TREE =
            FeatureUtils.register("spiky_tree", Feature.TREE, new TreeConfiguration.TreeConfigurationBuilder(
                    BlockStateProvider.simple(BlockInit.SPIKY_LOG.get()),
                    new ForkingTrunkPlacer(3, 2, 2),
                    BlockStateProvider.simple(Blocks.AIR),
                    new PineFoliagePlacer(ConstantInt.of(2), ConstantInt.of(1), UniformInt.of(3, 4)),
                    new TwoLayersFeatureSize(1, 0, 1))
                    .dirt(BlockStateProvider.simple(BlockInit.SPIKY_LOG.get()))
                    .forceDirt()
                    .ignoreVines().build());


    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> SAPLING = FeatureUtils.register("sapling",
            Feature.FLOWER, new RandomPatchConfiguration(92, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                                    SpecialBlockInit.SAPLING_PLANT.get().
                                            defaultBlockState().setValue(SaplingPlantBlock.AGE, 7))))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> GRASS_TUFT = FeatureUtils.register("grass_tuft",
            Feature.FLOWER, new RandomPatchConfiguration(184, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.GRASS_TUFT.get().
                                    defaultBlockState().setValue(GrassTuftBlock.AGE, 7))))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> SAVANNA_GRASS_TUFT = FeatureUtils.register("savanna_grass_tuft",
            Feature.FLOWER, new RandomPatchConfiguration(368, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.GRASS_TUFT.get().
                                    defaultBlockState().setValue(GrassTuftBlock.AGE, 7))))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> BERRY_BUSH = FeatureUtils.register("berry_bush",
            Feature.FLOWER, new RandomPatchConfiguration(62, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.BERRY_BUSH.get().
                                    defaultBlockState().setValue(BerryBushBlock.AGE, 7))))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> SAVANNA_BOULDER = FeatureUtils.register("savanna_boulder",
            Feature.FLOWER, new RandomPatchConfiguration(70, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.BOULDER.get())))));
    //62

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> ROCKYLAND_BOULDER = FeatureUtils.register("rockyland_boulder",
            Feature.FLOWER, new RandomPatchConfiguration(92, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.BOULDER.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> ROCKYLAND_GOLD_VEIN_BOULDER = FeatureUtils.register("rockyland_gold_vein_boulder",
            Feature.FLOWER, new RandomPatchConfiguration(92, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.GOLD_VEIN_BOULDER.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> ROCKYLAND_FLINT = FeatureUtils.register("rockyland_flint",
            Feature.FLOWER, new RandomPatchConfiguration(46, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.WORLD_FLINT.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> ROCKYLAND_ROCKS = FeatureUtils.register("rockyland_rocks",
            Feature.FLOWER, new RandomPatchConfiguration(46, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.WORLD_ROCKS.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> DECIDUOUS_SAPLING = FeatureUtils.register("deciduous_sapling",
            Feature.FLOWER, new RandomPatchConfiguration(92, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.SAPLING_PLANT.get().
                                    defaultBlockState().setValue(SaplingPlantBlock.AGE, 7))))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> DECIDUOUS_BERRY_BUSH = FeatureUtils.register("deciduous_berry_bush",
            Feature.FLOWER, new RandomPatchConfiguration(62, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.BERRY_BUSH.get().
                                    defaultBlockState().setValue(BerryBushBlock.AGE, 7))))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> DECIDUOUS_GRASS_TUFT = FeatureUtils.register("deciduous_grass_tuft",
            Feature.FLOWER, new RandomPatchConfiguration(56, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.GRASS_TUFT.get().
                                    defaultBlockState().setValue(GrassTuftBlock.AGE, 7))))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> DECIDUOUS_BOULDER = FeatureUtils.register("deciduous_boulder",
            Feature.FLOWER, new RandomPatchConfiguration(70, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.BOULDER.get())))));
    //46

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> MARSH_SPIKY_BUSH = FeatureUtils.register("marsh_spiky_bush",
            Feature.FLOWER, new RandomPatchConfiguration(62, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.SPIKY_BUSH.get().
                                    defaultBlockState().setValue(CropBlock.AGE, 7))))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> MARSH_REEDS = FeatureUtils.register("marsh_reeds",
            Feature.FLOWER, new RandomPatchConfiguration(62, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.REEDS.get().
                                    defaultBlockState().setValue(CropBlock.AGE, 7))))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> MARSH_GRASS_TUFT = FeatureUtils.register("marsh_grass_tuft",
            Feature.FLOWER, new RandomPatchConfiguration(46, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            SpecialBlockInit.GRASS_TUFT.get().
                                    defaultBlockState().setValue(CropBlock.AGE, 7))))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> FOREST_GOLD_VEIN_BOULDER = FeatureUtils.register("forest_gold_vein_boulder",
            Feature.FLOWER, new RandomPatchConfiguration(45, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.GOLD_VEIN_BOULDER.get())))));
    //46

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> FLINT = FeatureUtils.register("world_flint",
            Feature.FLOWER, new RandomPatchConfiguration(62, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.WORLD_FLINT.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> SEEDS = FeatureUtils.register("world_seeds",
            Feature.FLOWER, new RandomPatchConfiguration(62, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.WORLD_SEEDS.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> CARROT = FeatureUtils.register("world_carrot",
            Feature.FLOWER, new RandomPatchConfiguration(62, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.WORLD_CARROT.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> MANDRAKE = FeatureUtils.register("world_mandrake",
            Feature.FLOWER, new RandomPatchConfiguration(5, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.WORLD_MANDRAKE.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> PLUGGED_SINKHOLE = FeatureUtils.register("plugged_sinkhole",
            Feature.FLOWER, new RandomPatchConfiguration(7
                    , 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.PLUGGED_SINKHOLE.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> GRAVE = FeatureUtils.register("grave",
            Feature.FLOWER, new RandomPatchConfiguration(5, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.GRAVE.get().defaultBlockState())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> GRAVEYARD_BOULDER = FeatureUtils.register("graveyard_boulder",
            Feature.FLOWER, new RandomPatchConfiguration(36, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.BOULDER.get())))));
    //24

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> GRAVEYARD_GOLD = FeatureUtils.register("graveyard_gold",
            Feature.FLOWER, new RandomPatchConfiguration(30, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.WORLD_GOLD_NUGGET.get())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> GRAVEYARD_GRAVE = FeatureUtils.register("graveyard_grave",
            Feature.FLOWER, new RandomPatchConfiguration(15, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.GRAVE.get().defaultBlockState())))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> GRAVEYARD_GRAVE_SOUTH = FeatureUtils.register("graveyard_grave_south",
            Feature.FLOWER, new RandomPatchConfiguration(20, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.GRAVE.get().defaultBlockState().setValue(GraveBlock.FACING, Direction.SOUTH))))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> GRAVEYARD_GRAVE_NORTH = FeatureUtils.register("graveyard_grave_north",
            Feature.FLOWER, new RandomPatchConfiguration(20, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.GRAVE.get().defaultBlockState().setValue(GraveBlock.FACING, Direction.NORTH))))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> GRAVEYARD_GRAVE_EAST = FeatureUtils.register("graveyard_grave_east",
            Feature.FLOWER, new RandomPatchConfiguration(20, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.GRAVE.get().defaultBlockState().setValue(GraveBlock.FACING, Direction.EAST))))));

    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> GRAVEYARD_GRAVE_WEST = FeatureUtils.register("graveyard_grave_west",
            Feature.FLOWER, new RandomPatchConfiguration(20, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.GRAVE.get().defaultBlockState().setValue(GraveBlock.FACING, Direction.WEST))))));


    public static final Holder<ConfiguredFeature<RandomPatchConfiguration, ?>> HOLLOW_STUMP = FeatureUtils.register("hollow_stump",
            Feature.FLOWER, new RandomPatchConfiguration(46, 16, 16,
                    PlacementUtils.onlyWhenEmpty(Feature.SIMPLE_BLOCK, new
                            SimpleBlockConfiguration(BlockStateProvider.simple(
                            BlockInit.HOLLOW_STUMP.get())))));

}
