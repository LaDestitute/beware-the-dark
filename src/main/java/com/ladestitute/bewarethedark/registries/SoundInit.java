package com.ladestitute.bewarethedark.registries;

import com.ladestitute.bewarethedark.BTDMain;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class SoundInit {
    public static final DeferredRegister<SoundEvent> SOUNDS = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, BTDMain.MOD_ID);

    public static final RegistryObject<SoundEvent> COOK_SIZZLE = SOUNDS.register("block.cook_sizzle",
            () -> new SoundEvent(new ResourceLocation(BTDMain.MOD_ID, "block.cook_sizzle")));

    public static final RegistryObject<SoundEvent> FIRE_OUT = SOUNDS.register("block.fire_out",
            () -> new SoundEvent(new ResourceLocation(BTDMain.MOD_ID, "block.fire_out")));

    public static final RegistryObject<SoundEvent> CRAFT_UNLOCK = SOUNDS.register("jingle.craft_unlock",
            () -> new SoundEvent(new ResourceLocation(BTDMain.MOD_ID, "jingle.craft_unlock")));

    public static final RegistryObject<SoundEvent> TUMBLEWEED_BOUNCE = SOUNDS.register("entity.tumbleweed_bounce",
            () -> new SoundEvent(new ResourceLocation(BTDMain.MOD_ID, "entity.tumbleweed_bounce")));

    public static final RegistryObject<SoundEvent> ADD_FUEL = SOUNDS.register("block.add_fuel",
            () -> new SoundEvent(new ResourceLocation(BTDMain.MOD_ID, "block.add_fuel")));

    public static final RegistryObject<SoundEvent> CHARLIE_WARN = SOUNDS.register("entity.charlie_warn",
            () -> new SoundEvent(new ResourceLocation(BTDMain.MOD_ID, "entity.charlie_warn")));

    public static final RegistryObject<SoundEvent> CHARLIE_BITE = SOUNDS.register("entity.charlie_bite",
            () -> new SoundEvent(new ResourceLocation(BTDMain.MOD_ID, "entity.charlie_bite")));

    public static final RegistryObject<SoundEvent> DAWN_JINGLE = SOUNDS.register("jingle.dawn",
            () -> new SoundEvent(new ResourceLocation(BTDMain.MOD_ID, "jingle.dawn")));

    public static final RegistryObject<SoundEvent> DUSK_JINGLE = SOUNDS.register("jingle.dusk",
            () -> new SoundEvent(new ResourceLocation(BTDMain.MOD_ID, "jingle.dusk")));

    public static final RegistryObject<SoundEvent> AUTUMN_WORK = SOUNDS.register("music.autumn_work",
            () -> new SoundEvent(new ResourceLocation(BTDMain.MOD_ID, "music.autumn_work")));

    public static final RegistryObject<SoundEvent> AUTUMN_WORK_DST = SOUNDS.register("music.autumn_work_dst",
            () -> new SoundEvent(new ResourceLocation(BTDMain.MOD_ID, "music.autumn_work_dst")));

    public static final RegistryObject<SoundEvent> DST_TITLE = SOUNDS.register("music.dst_title",
            () -> new SoundEvent(new ResourceLocation(BTDMain.MOD_ID, "music.dst_title")));

    public static final RegistryObject<SoundEvent> CAVE_WORK = SOUNDS.register("music.cave_work",
            () -> new SoundEvent(new ResourceLocation(BTDMain.MOD_ID, "music.cave_work")));
}
