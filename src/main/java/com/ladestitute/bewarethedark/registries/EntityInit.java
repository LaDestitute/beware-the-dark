package com.ladestitute.bewarethedark.registries;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.entities.mobs.hostile.BatiliskEntity;
import com.ladestitute.bewarethedark.entities.mobs.hostile.GhostEntity;
import com.ladestitute.bewarethedark.entities.mobs.hostile.TentacleEntity;
import com.ladestitute.bewarethedark.entities.mobs.neutral.CatcoonEntity;
import com.ladestitute.bewarethedark.entities.mobs.passive.ButterflyEntity;
import com.ladestitute.bewarethedark.entities.mobs.passive.FireflyEntity;
import com.ladestitute.bewarethedark.entities.mobs.passive.JungleButterflyEntity;
import com.ladestitute.bewarethedark.entities.mobs.passive.MandrakeEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.*;
import com.ladestitute.bewarethedark.entities.objects.EntityTumbleweed;
import com.ladestitute.bewarethedark.entities.projectile.DartEntity;
import com.ladestitute.bewarethedark.entities.projectile.ElectricDartEntity;
import com.ladestitute.bewarethedark.entities.projectile.FireDartEntity;
import com.ladestitute.bewarethedark.entities.projectile.PoisonDartEntity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class EntityInit {
    public static final DeferredRegister<EntityType<?>> ENTITIES = DeferredRegister.create(ForgeRegistries.ENTITY_TYPES, BTDMain.MOD_ID);

    public static final RegistryObject<EntityType<EntityTumbleweed>> TUMBLEWEED = ENTITIES.register("tumbleweed",
            () -> EntityType.Builder.<EntityTumbleweed>of(EntityTumbleweed::new,
                    MobCategory.MISC).setTrackingRange(128).setUpdateInterval(30)
                    .setShouldReceiveVelocityUpdates(true)
                    .build("tumbleweed"));

    public static final RegistryObject<EntityType<FireflyEntity>> FIREFLY = ENTITIES.register("firefly",
            () -> EntityType.Builder.<FireflyEntity>of(FireflyEntity::new, MobCategory.CREATURE)
                    .sized(1F, 1F).build("firefly"));

    public static final RegistryObject<EntityType<ButterflyEntity>> BUTTERFLY = ENTITIES.register("butterfly",
            () -> EntityType.Builder.<ButterflyEntity>of(ButterflyEntity::new, MobCategory.CREATURE)
                    .sized(1F, 1F).build("butterfly"));

    public static final RegistryObject<EntityType<JungleButterflyEntity>> JUNGLE_BUTTERFLY = ENTITIES.register("jungle_butterfly",
            () -> EntityType.Builder.<JungleButterflyEntity>of(JungleButterflyEntity::new, MobCategory.CREATURE)
                    .sized(1F, 1F).build("jungle_butterfly"));

    public static final RegistryObject<EntityType<MandrakeEntity>> MANDRAKE = ENTITIES.register("mandrake",
            () -> EntityType.Builder.<MandrakeEntity>of(MandrakeEntity::new, MobCategory.CREATURE)
                    .sized(1F, 1F).build("mandrake"));

    public static final RegistryObject<EntityType<GhostEntity>> GHOST = ENTITIES.register("ghost",
            () -> EntityType.Builder.<GhostEntity>of(GhostEntity::new, MobCategory.MONSTER)
                    .sized(1F, 1F).build("ghost"));

    public static final RegistryObject<EntityType<CatcoonEntity>> CATCOON = ENTITIES.register("catcoon",
            () -> EntityType.Builder.<CatcoonEntity>of(CatcoonEntity::new, MobCategory.CREATURE)
                    .sized(1F, 1F).build("catcoon"));

    public static final RegistryObject<EntityType<MrSkittsEntity>> MR_SKITTS = ENTITIES.register("mr_skitts",
            () -> EntityType.Builder.<MrSkittsEntity>of(MrSkittsEntity::new, MobCategory.CREATURE)
                    .sized(0.1F, 0.1F).build("mr_skitts"));

    public static final RegistryObject<EntityType<NightHandEntity>> NIGHT_HAND = ENTITIES.register("night_hand",
            () -> EntityType.Builder.<NightHandEntity>of(NightHandEntity::new, MobCategory.CREATURE)
                    .sized(0.75F, 0.75F).build("night_hand"));

    public static final RegistryObject<EntityType<ShadowWatcherEntity>> SHADOW_WATCHER = ENTITIES.register("shadow_watcher",
            () -> EntityType.Builder.<ShadowWatcherEntity>of(ShadowWatcherEntity::new, MobCategory.CREATURE)
                    .sized(1F, 1F).build("shadow_watcher"));

    public static final RegistryObject<EntityType<PassiveCrawlingHorrorEntity>> PASSIVE_CRAWLING_HORROR = ENTITIES.register("passive_crawling_horror",
            () -> EntityType.Builder.<PassiveCrawlingHorrorEntity>of(PassiveCrawlingHorrorEntity::new, MobCategory.CREATURE)
                    .sized(0.1F, 0.1F).build("passive_crawling_horror"));

    public static final RegistryObject<EntityType<HostileCrawlingHorrorEntity>> HOSTILE_CRAWLING_HORROR = ENTITIES.register("hostile_crawling_horror",
            () -> EntityType.Builder.<HostileCrawlingHorrorEntity>of(HostileCrawlingHorrorEntity::new, MobCategory.MONSTER)
                    .sized(1.0F, 1.0F).build("hostile_crawling_horror"));

    public static final RegistryObject<EntityType<TerrorbeakEntity>> TERRORBEAK = ENTITIES.register("terrorbeak",
            () -> EntityType.Builder.<TerrorbeakEntity>of(TerrorbeakEntity::new, MobCategory.MONSTER)
                    .sized(1.0F, 1.0F).build("terrorbeak"));

    public static final RegistryObject<EntityType<DartEntity>> DART = ENTITIES.register("dart",
            () -> EntityType.Builder.<DartEntity>of(DartEntity::new, MobCategory.MISC)
                    .sized(0.5F, 0.5F).build("dart"));

    public static final RegistryObject<EntityType<FireDartEntity>> FIRE_DART = ENTITIES.register("fire_dart",
            () -> EntityType.Builder.<FireDartEntity>of(FireDartEntity::new, MobCategory.MISC)
                    .sized(0.5F, 0.5F).build("fire_dart"));

    public static final RegistryObject<EntityType<ElectricDartEntity>> ELECTRIC_DART = ENTITIES.register("electric_dart",
            () -> EntityType.Builder.<ElectricDartEntity>of(ElectricDartEntity::new, MobCategory.MISC)
                    .sized(0.5F, 0.5F).build("electric_dart"));

    public static final RegistryObject<EntityType<PoisonDartEntity>> POISON_DART = ENTITIES.register("poison_dart",
            () -> EntityType.Builder.<PoisonDartEntity>of(PoisonDartEntity::new, MobCategory.MISC)
                    .sized(0.5F, 0.5F).build("poison_dart"));

    public static final RegistryObject<EntityType<TentacleEntity>> TENTACLE = ENTITIES.register("tentacle",
            () -> EntityType.Builder.<TentacleEntity>of(TentacleEntity::new, MobCategory.MONSTER)
                    .sized(1.1F, 1.1F).build("tentacle"));

    public static final RegistryObject<EntityType<BatiliskEntity>> BATILISK = ENTITIES.register("batilisk",
            () -> EntityType.Builder.<BatiliskEntity>of(BatiliskEntity::new, MobCategory.MONSTER)
                    .sized(1F, 1F).build("batilisk"));


}
