package com.ladestitute.bewarethedark.registries;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.items.food.CrockPotFood;
import com.ladestitute.bewarethedark.items.food.fish.CookedFishItem;
import com.ladestitute.bewarethedark.items.food.fish.FishItem;
import com.ladestitute.bewarethedark.items.food.fruit.BerriesItem;
import com.ladestitute.bewarethedark.items.food.fruit.RoastedBerriesItem;
import com.ladestitute.bewarethedark.items.food.meat.*;
import com.ladestitute.bewarethedark.items.food.mobdrops.BatiliskWingItem;
import com.ladestitute.bewarethedark.items.food.mobdrops.CookedBatiliskWingItem;
import com.ladestitute.bewarethedark.items.food.mobdrops.CookedEggItem;
import com.ladestitute.bewarethedark.items.food.mobdrops.VenomGlandItem;
import com.ladestitute.bewarethedark.items.food.mushrooms.*;
import com.ladestitute.bewarethedark.items.food.vegetables.RoastedBirchnutItem;
import com.ladestitute.bewarethedark.items.food.vegetables.RoastedCarrotItem;
import com.ladestitute.bewarethedark.items.materials.CoralItem;
import com.ladestitute.bewarethedark.items.materials.FoilageItem;
import com.ladestitute.bewarethedark.items.materials.LightBulbItem;
import com.ladestitute.bewarethedark.items.materials.PetalsItem;
import com.ladestitute.bewarethedark.items.utility.AntiVenomItem;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.food.FoodProperties;
import net.minecraft.world.item.Item;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class FoodInit {

    public static final DeferredRegister<Item> FOOD = DeferredRegister.create(ForgeRegistries.ITEMS,
            BTDMain.MOD_ID);

    //Todo: nerf cooked hunger rates to 1/2 value for 0.4

    //Fish
    public static final RegistryObject<Item> FISH = FOOD.register("fish",
            () -> new FishItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(2)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> COOKED_FISH = FOOD.register("cooked_fish",
            () -> new CookedFishItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(2)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));

    //Fruit
    public static final RegistryObject<Item> BERRIES = FOOD.register("berries",
            () -> new BerriesItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(1)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> ROASTED_BERRIES = FOOD.register("roasted_berries",
            () -> new RoastedBerriesItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(2)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));

    //Meat
    public static final RegistryObject<Item> MEAT = FOOD.register("meat",
            () -> new MeatItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(4)
                    .saturationMod(0).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> MONSTER_MEAT = FOOD.register("monster_meat",
            () -> new MonsterMeatItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(2)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> COOKED_MEAT = FOOD.register("cooked_meat",
            () -> new CookedMeatItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(4)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> COOKED_MONSTER_MEAT = FOOD.register("cooked_monster_meat",
            () -> new CookedMonsterMeatItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(2)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> MORSEL = FOOD.register("morsel",
            () -> new MorselItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(2)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> COOKED_MORSEL = FOOD.register("cooked_morsel",
            () -> new CookedMorselItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(2)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));

    public static final RegistryObject<Item> SMALL_JERKY = FOOD.register("small_jerky",
            () -> new Item(new Item.Properties().food(new FoodProperties.Builder().nutrition(4)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));

    public static final RegistryObject<Item> JERKY = FOOD.register("jerky",
            () -> new Item(new Item.Properties().food(new FoodProperties.Builder().nutrition(6)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));

    public static final RegistryObject<Item> MONSTER_JERKY = FOOD.register("monster_jerky",
            () -> new Item(new Item.Properties().food(new FoodProperties.Builder().nutrition(3)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));

    public static final RegistryObject<Item> DRIED_SEAWEED = FOOD.register("dried_seaweed",
            () -> new Item(new Item.Properties().food(new FoodProperties.Builder().nutrition(2)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));

    //Vegetables
    public static final RegistryObject<Item> ROASTED_CARROT = FOOD.register("roasted_carrot",
            () -> new RoastedCarrotItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(3)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> ROASTED_BIRCHNUT = FOOD.register("roasted_birchnut",
            () -> new RoastedBirchnutItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(1)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));

    //Mushrooms
    public static final RegistryObject<Item> RED_CAP = FOOD.register("red_cap",
            () -> new RedCapItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(2)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> GREEN_CAP = FOOD.register("green_cap",
            () -> new GreenCapItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(2)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> BLUE_CAP = FOOD.register("blue_cap",
            () -> new BlueCapItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(2)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> COOKED_RED_CAP = FOOD.register("cooked_red_cap",
            () -> new CookedRedCapItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(0)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> COOKED_GREEN_CAP = FOOD.register("cooked_green_cap",
            () -> new CookedGreenCapItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(0)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> COOKED_BLUE_CAP = FOOD.register("cooked_blue_cap",
            () -> new CookedBlueCapItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(0)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> BATILISK_WING = FOOD.register("batilisk_wing",
            () -> new BatiliskWingItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(2)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> COOKED_BATILISK_WING = FOOD.register("cooked_batilisk_wing",
            () -> new CookedBatiliskWingItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(3)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));

    //Mob drops
    public static final RegistryObject<Item> COOKED_EGG = FOOD.register("cooked_egg",
            () -> new CookedEggItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(2)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> HONEY = FOOD.register("honey",
            () -> new Item(new Item.Properties().food(new FoodProperties.Builder().nutrition(1)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> BUTTERFLY_WINGS = FOOD.register("butterfly_wings",
            () -> new Item(new Item.Properties().food(new FoodProperties.Builder().nutrition(1)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> JUNGLE_BUTTERFLY_WINGS = FOOD.register("jungle_butterfly_wings",
            () -> new Item(new Item.Properties().food(new FoodProperties.Builder().nutrition(1)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));
    public static final RegistryObject<Item> BUTTER = FOOD.register("butter",
            () -> new Item(new Item.Properties().food(new FoodProperties.Builder().nutrition(3)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));

    public static final RegistryObject<Item> MANDRAKE = FOOD.register("mandrake",
            () -> new Item(new Item.Properties().food(new FoodProperties.Builder().nutrition(10)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));

    public static final RegistryObject<Item> COOKED_MANDRAKE = FOOD.register("cooked_mandrake",
            () -> new Item(new Item.Properties().food(new FoodProperties.Builder().nutrition(20)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.FOOD_TAB)));

    public static final RegistryObject<Item> VENOM_GLAND = FOOD.register("venom_gland",
            () -> new VenomGlandItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(0)
                    .saturationMod(0f).alwaysEat().effect(new MobEffectInstance(MobEffects.POISON, 130, 2), 1.0F)
                    .build()).tab(BTDMain.MATERIALS_TAB)));
    //130

    public static final RegistryObject<Item> ANTI_VENOM = FOOD.register("anti_venom",
            () -> new AntiVenomItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(0)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.SURVIVAL_TAB)));

    public static final RegistryObject<Item> LIGHT_BULB = FOOD.register("light_bulb",
            () -> new LightBulbItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(0)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.MATERIALS_TAB)));

    public static final RegistryObject<Item> PETALS = FOOD.register("petals",
            () -> new PetalsItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(0)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.MATERIALS_TAB)));

    public static final RegistryObject<Item> FOILAGE = FOOD.register("foilage",
            () -> new FoilageItem(new Item.Properties().food(new FoodProperties.Builder().nutrition(0)
                    .saturationMod(0f).alwaysEat()
                    .build()).tab(BTDMain.MATERIALS_TAB)));

    //Meals
    public static final RegistryObject<Item> WET_GOOP =
            FOOD.register("wet_goop", () ->
                    CrockPotFood.builder().nutrition(0).saturationMod(0F).heal(0F).build());
    public static final RegistryObject<Item> BACON_AND_EGGS =
            FOOD.register("bacon_and_eggs", () ->
                    CrockPotFood.builder().nutrition(10).saturationMod(0F).heal(2.7F).build());
    public static final RegistryObject<Item> BUNNY_STEW =
            FOOD.register("bunny_stew", () ->
                    CrockPotFood.builder().nutrition(5).saturationMod(0F).heal(2.7F).build());
    public static final RegistryObject<Item> CALIFORNIA_ROLL =
            FOOD.register("california_roll", () ->
                    CrockPotFood.builder().nutrition(5).saturationMod(0F).heal(2.7F).build());
    public static final RegistryObject<Item> CEVICHE =
            FOOD.register("ceviche", () ->
                    CrockPotFood.builder().nutrition(3).saturationMod(0F).heal(2.7F).build());
    public static final RegistryObject<Item> FANCY_SPIRAL_TUBERS =
            FOOD.register("fancy_spiraled_tubers", () ->
                    CrockPotFood.builder().nutrition(5).saturationMod(0F).heal(0.5F).build());
    public static final RegistryObject<Item> FISHSTICKS =
            FOOD.register("fishsticks", () ->
                    CrockPotFood.builder().nutrition(5).saturationMod(0F).heal(5.3F).build());
    public static final RegistryObject<Item> FIST_FULL_OF_JAM =
            FOOD.register("fist_full_of_jam", () ->
                    CrockPotFood.builder().nutrition(5).saturationMod(0F).heal(0.5F).build());
    public static final RegistryObject<Item> FRUIT_MEDLEY =
            FOOD.register("fruit_medley", () ->
                    CrockPotFood.builder().nutrition(3).saturationMod(0F).heal(2.7F).build());
    public static final RegistryObject<Item> HONEY_HAM =
            FOOD.register("honey_ham", () ->
                    CrockPotFood.builder().nutrition(10).saturationMod(0F).heal(4F).build());
    public static final RegistryObject<Item> HONEY_NUGGETS =
            FOOD.register("honey_nuggets", () ->
                    CrockPotFood.builder().nutrition(5).saturationMod(0F).heal(2.7F).build());
    public static final RegistryObject<Item> ICE_CREAM =
            FOOD.register("ice_cream", () ->
                    CrockPotFood.builder().nutrition(3).saturationMod(0F).heal(0F).build());
    public static final RegistryObject<Item> KABOBS =
            FOOD.register("kabobs", () ->
                    CrockPotFood.builder().nutrition(5).saturationMod(0F).heal(0.5F).build());
    public static final RegistryObject<Item> MEATBALLS =
            FOOD.register("meatballs", () ->
                    CrockPotFood.builder().nutrition(8).saturationMod(0F).heal(0.5F).build());
    public static final RegistryObject<Item> MEATY_STEW =
            FOOD.register("meaty_stew", () ->
                    CrockPotFood.builder().nutrition(20).saturationMod(0F).heal(1.6F).build());
    public static final RegistryObject<Item> MELONSICLE =
            FOOD.register("melonsicle", () ->
                    CrockPotFood.builder().nutrition(2).saturationMod(0F).heal(0.5F).build());
    public static final RegistryObject<Item> PIEROGI =
            FOOD.register("pierogi", () ->
                    CrockPotFood.builder().nutrition(5).saturationMod(0F).heal(5.3F).build());
    public static final RegistryObject<Item> PUMPKIN_COOKIE =
            FOOD.register("pumpkin_cookie", () ->
                    CrockPotFood.builder().nutrition(5).saturationMod(0F).heal(0F).build());
    public static final RegistryObject<Item>  RATATOUILLE =
            FOOD.register("ratatouille", () ->
                    CrockPotFood.builder().nutrition(3).saturationMod(0F).heal(0.5F).build());
    public static final RegistryObject<Item>  TAFFY =
            FOOD.register("taffy", () ->
                    CrockPotFood.builder().nutrition(3).saturationMod(0F).damage(DamageSource.GENERIC, 0.5f).build());
    public static final RegistryObject<Item>  TRAIL_MIX =
            FOOD.register("trail_mix", () ->
                    CrockPotFood.builder().nutrition(2).saturationMod(0F).heal(4F).build());
    public static final RegistryObject<Item>  BUTTER_MUFFIN =
            FOOD.register("butter_muffin", () ->
                    CrockPotFood.builder().nutrition(5).saturationMod(0F).heal(2.6F).build());
    public static final RegistryObject<Item>  WAFFLES =
            FOOD.register("waffles", () ->
                    CrockPotFood.builder().nutrition(5).saturationMod(0F).heal(8F).build());
    public static final RegistryObject<Item>  MANDRAKE_SOUP =
            FOOD.register("mandrake_soup", () ->
                    CrockPotFood.builder().nutrition(20).saturationMod(0F).heal(13.3F).build());
    public static final RegistryObject<Item> MONSTER_LASAGNA =
            FOOD.register("monster_lasagna", () ->
                    CrockPotFood.builder().nutrition(5).saturationMod(0F).build());

}
