package com.ladestitute.bewarethedark.registries;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.util.recipes.CrockPotRecipe;
import com.ladestitute.bewarethedark.util.recipes.DryingRackRecipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class RecipeTypeInit {
    public static final DeferredRegister<RecipeSerializer<?>> SERIALIZERS =
            DeferredRegister.create(ForgeRegistries.RECIPE_SERIALIZERS, BTDMain.MOD_ID);

    public static final DeferredRegister<RecipeType<?>> RECIPE_TYPES =
            DeferredRegister.create(ForgeRegistries.RECIPE_TYPES, BTDMain.MOD_ID);

    public static final RegistryObject<RecipeSerializer<DryingRackRecipe>> DRYING_RACK_SERIALIZER =
            SERIALIZERS.register("drying_rack", () -> DryingRackRecipe.Serializer.INSTANCE);

    public static final RegistryObject<RecipeSerializer<CrockPotRecipe>> CROCK_POT_SERIALIZER =
            SERIALIZERS.register("crock_pot", () -> CrockPotRecipe.Serializer.INSTANCE);


}
