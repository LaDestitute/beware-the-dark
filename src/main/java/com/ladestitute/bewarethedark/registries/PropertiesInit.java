package com.ladestitute.bewarethedark.registries;

import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.material.Material;

public class PropertiesInit {
    //First parameter in .strength is hardness, second is blast resistance
    public static final BlockBehaviour.Properties TUMBLEWEEDSPAWNER = BlockBehaviour.Properties.of(Material.AIR)
            .strength(500f, 500f)
            .air()
            .noCollission()
            .noLootTable();

    public static final BlockBehaviour.Properties FIREFLYSPAWNER = BlockBehaviour.Properties.of(Material.AIR)
            .strength(500f, 500f)
            .air()
            .noCollission()
            .noOcclusion()
            .lightLevel(state -> state.getValue(BlockStateProperties.LIT) ? 7 : 0)
            .noLootTable();

    public static final BlockBehaviour.Properties NIGHTHANDSPAWNER = BlockBehaviour.Properties.of(Material.AIR)
            .strength(500f, 500f)
            .air()
            .noCollission()
            .noOcclusion()
            .noLootTable();

    public static final BlockBehaviour.Properties SPAWNER = BlockBehaviour.Properties.of(Material.WOOL)
            .strength(500f, 500f)
            .noLootTable();

    public static final BlockBehaviour.Properties ROCKYTURF = BlockBehaviour.Properties.of(Material.STONE)
            .strength(1f, 3.25f)
            .sound(SoundType.STONE)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties GRASSTURF = BlockBehaviour.Properties.of(Material.GRASS)
            .strength(0.6f, 0.6f)
            .sound(SoundType.GRASS)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties SANDYTURF = BlockBehaviour.Properties.of(Material.SAND)
            .strength(0.6f, 0.6f)
            .sound(SoundType.SAND)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties MARSHTURF = BlockBehaviour.Properties.of(Material.CLAY)
            .strength(0.6f, 0.6f)
            .sound(SoundType.SOUL_SOIL)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties GUANOTURF = BlockBehaviour.Properties.of(Material.STONE)
            .strength(0.6f, 0.6f)
            .sound(SoundType.STONE)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties FUNGAL_TURF = BlockBehaviour.Properties.of(Material.GRASS)
            .strength(0.6f, 0.6f)
            .sound(SoundType.NYLIUM)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties BOULDER = BlockBehaviour.Properties.of(Material.STONE)
            .strength(3.0f, 15f)
            .sound(SoundType.STONE)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties GRAVE = BlockBehaviour.Properties.of(Material.STONE)
            .strength(5.0f, 3.5f)
            .sound(SoundType.STONE)
            .randomTicks()
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties WORLDROCK = BlockBehaviour.Properties.of(Material.STONE)
            .strength(0f, 0f)
            .sound(SoundType.STONE)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties WOODFLOOR = BlockBehaviour.Properties.of(Material.WOOD)
            .strength(2F, 3f)
            .sound(SoundType.WOOD)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties DRYING_RACK = BlockBehaviour.Properties.of(Material.WOOD)
            .strength(500F, 3f)
            .sound(SoundType.WOOD)
            .noOcclusion()
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties PLUGGED_SINKHOLE = BlockBehaviour.Properties.of(Material.STONE)
            .strength(0.75F, 3f)
            .sound(SoundType.STONE)
            .noOcclusion()
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties SINKHOLE = BlockBehaviour.Properties.of(Material.STONE)
            .strength(500F, 4f)
            .sound(SoundType.STONE)
            .lightLevel(state -> state.getValue(BlockStateProperties.LIT) ? 7 : 0)
            .noOcclusion()
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties MUSHROOM_SPAWNER = BlockBehaviour.Properties.of(Material.DIRT)
            .strength(0.5F, 4f)
            .sound(SoundType.GRAVEL)
            .noOcclusion()
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties CROCK_POT = BlockBehaviour.Properties.of(Material.STONE)
            .strength(500F, 3f)
            .sound(SoundType.WOOD)
            .lightLevel(state -> state.getValue(BlockStateProperties.LIT) ? 7 : 0)
            .noOcclusion()
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties PUMPKIN_LANTERN = BlockBehaviour.Properties.of(Material.VEGETABLE)
            .strength(1F, 1f)
            .lightLevel(state -> state.getValue(BlockStateProperties.LIT) ? 7 : 0)
            .noOcclusion()
            .sound(SoundType.WOOD);

    public static final BlockBehaviour.Properties LANTERN = BlockBehaviour.Properties.of(Material.WOOD)
            .strength(1F, 1f)
            .lightLevel(state -> state.getValue(BlockStateProperties.LIT) ? 10 : 0)
            .noOcclusion()
            .sound(SoundType.WOOD);

    public static final BlockBehaviour.Properties STONEFLOOR = BlockBehaviour.Properties.of(Material.STONE)
            .strength(2F, 6f)
            .sound(SoundType.STONE)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties SOFTFLOOR = BlockBehaviour.Properties.of(Material.WOOL)
            .strength(1F, 1.5f)
            .sound(SoundType.WOOL)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties HAMMERABLESCIENCEMACHINE = BlockBehaviour.Properties.of(Material.STONE)
            .strength(500f, 15f)
            .sound(SoundType.STONE)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties HAMMERABLEFARM = BlockBehaviour.Properties.of(Material.DIRT)
            .strength(500f, 3f)
            .sound(SoundType.WOOD)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties HAMMERABLEFIREPIT = BlockBehaviour.Properties.of(Material.STONE)
            .strength(500f, 3f)
            .lightLevel(state -> state.getValue(BlockStateProperties.LIT) ? 13 : 0)
            .sound(SoundType.WOOD)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties HAMMERABLEUNLITFIREPIT = BlockBehaviour.Properties.of(Material.STONE)
            .strength(500f, 3f)
            .sound(SoundType.WOOD)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties LUMPY_EVERGREEN = BlockBehaviour.Properties.of(Material.WOOD)
            .strength(2f, 2f)
            .sound(SoundType.WOOD)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties MUSHTREE_CAP = BlockBehaviour.Properties.of(Material.GRASS)
            .strength(0f, 0f)
            .sound(SoundType.GRASS)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties MUSHTREE = BlockBehaviour.Properties.of(Material.WOOD)
            .strength(1f, 1f)
            .sound(SoundType.WOOD)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties BURNT_EVERGREEN = BlockBehaviour.Properties.of(Material.WOOD)
            .strength(0.5f, 0.5f)
            .sound(SoundType.WOOD)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties STUMP = BlockBehaviour.Properties.of(Material.WOOD)
            .strength(500f, 2f)
            .sound(SoundType.WOOD)
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties LEAVES = BlockBehaviour.Properties.of(Material.LEAVES)
            .strength(0.2f, 1f)
            .sound(SoundType.GRASS)
            .randomTicks()
            .noOcclusion()
            .requiresCorrectToolForDrops();

    public static final BlockBehaviour.Properties BERRY_BUSH = BlockBehaviour.Properties.of(Material.PLANT)
            .strength(500f, 0f)
            .sound(SoundType.CROP)
            .randomTicks()
            .noCollission()
            .noOcclusion();

    public static final BlockBehaviour.Properties WORLD_PLANT = BlockBehaviour.Properties.of(Material.PLANT)
            .strength(0f, 0f)
            .sound(SoundType.CROP)
            .noCollission()
            .noOcclusion();

    public static final BlockBehaviour.Properties MARSH_POND = BlockBehaviour.Properties.of(Material.GRASS)
            .strength(0f, 1f)
            .sound(SoundType.SOUL_SOIL);

    public static final BlockBehaviour.Properties MARSH_POND_PLANT = BlockBehaviour.Properties.of(Material.PLANT)
            .strength(0f, 1f)
            .sound(SoundType.GRASS)
            .noOcclusion()
            .noCollission();

    public static final BlockBehaviour.Properties FLORA = BlockBehaviour.Properties.of(Material.PLANT)
            .strength(0f, 0f)
            .sound(SoundType.CROP)
            .noCollission()
            .noOcclusion();

    public static final BlockBehaviour.Properties FERN = BlockBehaviour.Properties.of(Material.PLANT)
            .strength(0f, 0f)
            .sound(SoundType.CROP)
            .noCollission()
            .noOcclusion();
}
