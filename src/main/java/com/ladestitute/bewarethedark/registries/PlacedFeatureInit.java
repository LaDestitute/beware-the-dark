package com.ladestitute.bewarethedark.registries;

import com.ladestitute.bewarethedark.BTDMain;
import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.data.worldgen.placement.VegetationPlacements;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.placement.BiomeFilter;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.RarityFilter;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.RegistryObject;

import java.util.List;

@SuppressWarnings("ALL")
public class PlacedFeatureInit {
    public static final DeferredRegister<PlacedFeature> PLACED_FEATURES =
            DeferredRegister.create(Registry.PLACED_FEATURE_REGISTRY, BTDMain.MOD_ID);

    static final RegistryObject<PlacedFeature> TUMBLEWEED_PLACED = PLACED_FEATURES.register("tumbleweed_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.TUMBLEWEED, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> FIREFLIES_PLACED = PLACED_FEATURES.register("fireflies_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.FIREFLIES, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> FIREFLIES_DECIDUOUS_PLACED = PLACED_FEATURES.register("fireflies_deciduous_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.FIREFLIES_DECIDUOUS, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> NIGHTHAND_PLACED = PLACED_FEATURES.register("nighthand_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.NIGHTHAND, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> MR_SKITTS_PLACED = PLACED_FEATURES.register("mr_skitts_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.MR_SKITTS, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> SHADOW_WATCHER_PLACED = PLACED_FEATURES.register("shadow_watcher_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.SHADOW_WATCHER, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> CRAWLING_HORROR_PLACED = PLACED_FEATURES.register("crawling_horror_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.CRAWLING_HORROR, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> TERRORBEAK_PLACED = PLACED_FEATURES.register("terrorbeak_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.TERRORBEAK, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> TENTACLE_PLACED = PLACED_FEATURES.register("tentacle_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.TENTACLE, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    ////

    public static final RegistryObject<PlacedFeature> SAPLING_PLACED = PLACED_FEATURES.register("sapling_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.SAPLING, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    public static final RegistryObject<PlacedFeature> GRASS_TUFT_PLACED = PLACED_FEATURES.register("grass_tuft_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.GRASS_TUFT, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    public static final RegistryObject<PlacedFeature> SAVANNA_GRASS_TUFT_PLACED = PLACED_FEATURES.register("savanna_grass_tuft_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.SAVANNA_GRASS_TUFT, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    public static final RegistryObject<PlacedFeature> BERRY_BUSH_PLACED = PLACED_FEATURES.register("berry_bush_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.BERRY_BUSH, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    public static final RegistryObject<PlacedFeature> SAVANNA_BOULDER_PLACED = PLACED_FEATURES.register("savanna_boulder_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.SAVANNA_BOULDER, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    public static final RegistryObject<PlacedFeature> ROCKYLAND_BOULDER_PLACED = PLACED_FEATURES.register("rockyland_boulder_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.ROCKYLAND_BOULDER, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> ROCKYLAND_GOLD_VEIN_BOULDER_PLACED = PLACED_FEATURES.register("rockyland_gold_vein_boulder_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.ROCKYLAND_GOLD_VEIN_BOULDER, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> ROCKYLAND_FLINT_PLACED = PLACED_FEATURES.register("rockyland_flint_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.ROCKYLAND_FLINT, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> ROCKYLAND_ROCKS_PLACED = PLACED_FEATURES.register("rockyland_rocks_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.ROCKYLAND_ROCKS, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> DECIDUOUS_SAPLING_PLACED = PLACED_FEATURES.register("deciduous_sapling_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.DECIDUOUS_SAPLING, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> DECIDUOUS_BERRY_BUSH_PLACED = PLACED_FEATURES.register("deciduous_berry_bush_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.DECIDUOUS_BERRY_BUSH, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> DECIDUOUS_GRASS_TUFT_PLACED = PLACED_FEATURES.register("deciduous_grass_tuft_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.DECIDUOUS_GRASS_TUFT, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> DECIDUOUS_BOULDER_PLACED = PLACED_FEATURES.register("deciduous_boulder_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.DECIDUOUS_BOULDER, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> MARSH_SPIKY_BUSH = PLACED_FEATURES.register("marsh_spiky_bush_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.MARSH_SPIKY_BUSH, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> MARSH_REEDS = PLACED_FEATURES.register("marsh_reeds_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.MARSH_REEDS, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> MARSH_GRASS_TUFT = PLACED_FEATURES.register("marsh_grass_tuft_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.MARSH_GRASS_TUFT, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> FOREST_GOLD_VEIN_BOULDER = PLACED_FEATURES.register("forest_gold_vein_boulder_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.FOREST_GOLD_VEIN_BOULDER, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> WORLD_FLINT = PLACED_FEATURES.register("world_flint_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.FLINT, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> WORLD_SEEDS = PLACED_FEATURES.register("world_seeds_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.SEEDS, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> WORLD_CARROT = PLACED_FEATURES.register("world_carrot_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.CARROT, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> WORLD_MANDRAKE = PLACED_FEATURES.register("world_mandrake_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.MANDRAKE, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> PLUGGED_SINKHOLE = PLACED_FEATURES.register("plugged_sinkhole_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.PLUGGED_SINKHOLE, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> GRAVE = PLACED_FEATURES.register("grave_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.GRAVE, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> GRAVEYARD_BOULDER = PLACED_FEATURES.register("graveyard_boulder_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.GRAVEYARD_BOULDER, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> GRAVEYARD_GOLD = PLACED_FEATURES.register("gold_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.GRAVEYARD_GOLD, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> GRAVEYARD_GRAVE_EAST = PLACED_FEATURES.register("graveyard_grave_east_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.GRAVEYARD_GRAVE_EAST, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> GRAVEYARD_GRAVE_WEST = PLACED_FEATURES.register("graveyard_grave_west_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.GRAVEYARD_GRAVE_WEST, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> GRAVEYARD_GRAVE_NORTH = PLACED_FEATURES.register("graveyard_grave_north_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.GRAVEYARD_GRAVE_NORTH, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> GRAVEYARD_GRAVE_SOUTH = PLACED_FEATURES.register("graveyard_grave_south_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.GRAVEYARD_GRAVE_SOUTH, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));

    static final RegistryObject<PlacedFeature> HOLLOW_STUMP_PLACED = PLACED_FEATURES.register("hollow_stump_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.HOLLOW_STUMP, List.of(RarityFilter.onAverageOnceEvery(16),
                    InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome())));


    // Trees

    public static final RegistryObject<PlacedFeature> ORANGE_BIRCHNUT_PLACED = PLACED_FEATURES.register("orange_birchnut_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.ORANGE_BIRCHNUT, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(1), SpecialBlockInit.BIRCHNUT_SAPLING_ORANGE.get())));

    public static final RegistryObject<PlacedFeature> RED_BIRCHNUT_PLACED = PLACED_FEATURES.register("red_birchnut_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.RED_BIRCHNUT, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(1), SpecialBlockInit.BIRCHNUT_SAPLING_RED.get())));

    public static final RegistryObject<PlacedFeature> YELLOW_BIRCHNUT_PLACED = PLACED_FEATURES.register("yellow_birchnut_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.YELLOW_BIRCHNUT, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(1), SpecialBlockInit.BIRCHNUT_SAPLING_YELLOW.get())));

    public static final RegistryObject<PlacedFeature> GREEN_BIRCHNUT_PLACED = PLACED_FEATURES.register("green_birchnut_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.GREEN_BIRCHNUT, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(1), SpecialBlockInit.BIRCHNUT_SAPLING_GREEN.get())));

    public static final RegistryObject<PlacedFeature> EVERGREEN_PLACED = PLACED_FEATURES.register("evergreen_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.EVERGREEN, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(1), BlockInit.EVERGREEN_SAPLING.get())));

    public static final RegistryObject<PlacedFeature> LUMPY_EVERGREEN_PLACED = PLACED_FEATURES.register("lumpy_evergreen_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.LUMPY_EVERGREEN, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(2), BlockInit.EVERGREEN_SAPLING.get())));

    public static final RegistryObject<PlacedFeature> EVERGREEN_TWO_PLACED = PLACED_FEATURES.register("evergreen_two_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.EVERGREEN, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(1), BlockInit.EVERGREEN_SAPLING.get())));

    public static final RegistryObject<PlacedFeature> LUMPY_EVERGREEN_TWO_PLACED = PLACED_FEATURES.register("lumpy_evergreen_two_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.LUMPY_EVERGREEN, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(2), BlockInit.EVERGREEN_SAPLING.get())));

    public static final RegistryObject<PlacedFeature> EVERGREEN_THREE_PLACED = PLACED_FEATURES.register("evergreen_three_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.EVERGREEN, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(1), BlockInit.EVERGREEN_SAPLING.get())));

    public static final RegistryObject<PlacedFeature> LUMPY_EVERGREEN_THREE_PLACED = PLACED_FEATURES.register("lumpy_evergreen_three_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.LUMPY_EVERGREEN, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(2), BlockInit.EVERGREEN_SAPLING.get())));

    static final RegistryObject<PlacedFeature> EVERGREEN_FOUR_PLACED = PLACED_FEATURES.register("evergreen_four_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.EVERGREEN, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(1), BlockInit.EVERGREEN_SAPLING.get())));

    static final RegistryObject<PlacedFeature> EVERGREEN_FIVE_PLACED = PLACED_FEATURES.register("evergreen_five_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.EVERGREEN, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(1), BlockInit.EVERGREEN_SAPLING.get())));

    static final RegistryObject<PlacedFeature> EVERGREEN_SIX_PLACED = PLACED_FEATURES.register("evergreen_six_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.EVERGREEN, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(1), BlockInit.EVERGREEN_SAPLING.get())));

    static final RegistryObject<PlacedFeature> EVERGREEN_SEVEN_PLACED = PLACED_FEATURES.register("evergreen_seven_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.EVERGREEN, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(1), BlockInit.EVERGREEN_SAPLING.get())));

    static final RegistryObject<PlacedFeature> EVERGREEN_EIGHT_PLACED = PLACED_FEATURES.register("evergreen_eight_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.EVERGREEN, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(1), BlockInit.EVERGREEN_SAPLING.get())));

    public static final RegistryObject<PlacedFeature> LUMPY_EVERGREEN_FOUR_PLACED = PLACED_FEATURES.register("lumpy_evergreen_four_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.LUMPY_EVERGREEN, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(2), BlockInit.EVERGREEN_SAPLING.get())));


    public static final RegistryObject<PlacedFeature> LUMPY_EVERGREEN_FIVE_PLACED = PLACED_FEATURES.register("lumpy_evergreen_five_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.LUMPY_EVERGREEN, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(2), BlockInit.EVERGREEN_SAPLING.get())));

    public static final RegistryObject<PlacedFeature> LUMPY_EVERGREEN_SIX_PLACED = PLACED_FEATURES.register("lumpy_evergreen_six_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.LUMPY_EVERGREEN, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(2), BlockInit.EVERGREEN_SAPLING.get())));

    public static final RegistryObject<PlacedFeature> SPIKY_TREE = PLACED_FEATURES.register("spiky_tree_placed",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.SPIKY_TREE, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(16), BlockInit.EVERGREEN_SAPLING.get())));

    public static final RegistryObject<PlacedFeature> GRAVEYARD_EVERGREEN = PLACED_FEATURES.register("graveyard_evergreen",
            () -> new PlacedFeature((Holder<ConfiguredFeature<?,?>>)(Holder<? extends ConfiguredFeature<?,?>>)
                    FeatureInit.EVERGREEN, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(1), BlockInit.EVERGREEN_SAPLING.get())));

    public static void register(IEventBus eventBus) {
        PLACED_FEATURES.register(eventBus);
    }



}
