package com.ladestitute.bewarethedark.registries;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.blocks.entity.*;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class BlockEntityInit {
    public static final DeferredRegister<BlockEntityType<?>> BLOCK_ENTITIES =
            DeferredRegister.create(ForgeRegistries.BLOCK_ENTITY_TYPES, BTDMain.MOD_ID);

    public static final RegistryObject<BlockEntityType<FirePitTileEntity>> FIRE_PIT =
            BLOCK_ENTITIES.register("fire_pit", () ->
                    BlockEntityType.Builder.of(FirePitTileEntity::new,
                            SpecialBlockInit.FIRE_PIT.get()).build(null));

    public static final RegistryObject<BlockEntityType<CampFireTileEntity>> CAMP_FIRE =
            BLOCK_ENTITIES.register("camp_fire", () ->
                    BlockEntityType.Builder.of(CampFireTileEntity::new,
                            SpecialBlockInit.CAMP_FIRE.get()).build(null));

    public static final RegistryObject<BlockEntityType<DryingRackBlockEntity>> DRYING_RACK =
            BLOCK_ENTITIES.register("drying_rack", () ->
                    BlockEntityType.Builder.of(DryingRackBlockEntity::new,
                            BlockInit.DRYING_RACK.get()).build(null));

    public static final RegistryObject<BlockEntityType<CrockPotBlockEntity>> CROCK_POT =
            BLOCK_ENTITIES.register("crock_pot", () ->
                    BlockEntityType.Builder.of(CrockPotBlockEntity::new,
                            BlockInit.CROCK_POT.get()).build(null));

    public static final RegistryObject<BlockEntityType<FirefliesSpawnerBlockEntity>> FIREFLIES =
            BLOCK_ENTITIES.register("fireflies", () ->
                    BlockEntityType.Builder.of(FirefliesSpawnerBlockEntity::new,
                            SpecialBlockInit.FIREFLIES_SPAWNER.get()).build(null));

    public static final RegistryObject<BlockEntityType<NightHandSpawnerBlockEntity>> NIGHTHAND_SPAWNER =
            BLOCK_ENTITIES.register("nighthand_spawner", () ->
                    BlockEntityType.Builder.of(NightHandSpawnerBlockEntity::new,
                            SpecialBlockInit.NIGHTHAND_SPAWNER.get()).build(null));

    public static final RegistryObject<BlockEntityType<MrSkittsSpawnerBlockEntity>> MR_SKITTS_SPAWNER =
            BLOCK_ENTITIES.register("mr_skitts_spawner", () ->
                    BlockEntityType.Builder.of(MrSkittsSpawnerBlockEntity::new,
                            SpecialBlockInit.MR_SKITTS_SPAWNER.get()).build(null));

    public static final RegistryObject<BlockEntityType<ShadowWatcherSpawnerBlockEntity>> SHADOW_WATCHER_SPAWNER =
            BLOCK_ENTITIES.register("shadow_watcher_spawner", () ->
                    BlockEntityType.Builder.of(ShadowWatcherSpawnerBlockEntity::new,
                            SpecialBlockInit.SHADOW_WATCHER_SPAWNER.get()).build(null));

    public static final RegistryObject<BlockEntityType<CrawlingHorrorSpawnerBlockEntity>> CRAWLING_HORROR_SPAWNER =
            BLOCK_ENTITIES.register("crawling_horror_spawner", () ->
                    BlockEntityType.Builder.of(CrawlingHorrorSpawnerBlockEntity::new,
                            SpecialBlockInit.CRAWLING_HORROR_SPAWNER.get()).build(null));

    public static final RegistryObject<BlockEntityType<TerrorbeakSpawnerBlockEntity>> TERRORBEAK_SPAWNER =
            BLOCK_ENTITIES.register("terrorbeak_spawner", () ->
                    BlockEntityType.Builder.of(TerrorbeakSpawnerBlockEntity::new,
                            SpecialBlockInit.TERRORBEAK_SPAWNER.get()).build(null));

    public static final RegistryObject<BlockEntityType<PumpkinLanternBlockEntity>> PUMPKIN_LANTERN =
            BLOCK_ENTITIES.register("pumpkin_lantern", () ->
                    BlockEntityType.Builder.of(PumpkinLanternBlockEntity::new,
                            BlockInit.PUMPKIN_LANTERN.get()).build(null));

    public static final RegistryObject<BlockEntityType<HollowStumpBlockEntity>> HOLLOW_STUMP =
            BLOCK_ENTITIES.register("hollow_stump", () ->
                    BlockEntityType.Builder.of(HollowStumpBlockEntity::new,
                            BlockInit.HOLLOW_STUMP.get()).build(null));

    public static final RegistryObject<BlockEntityType<SinkholeBlockEntity>> SINKHOLE =
            BLOCK_ENTITIES.register("sinkhole", () ->
                    BlockEntityType.Builder.of(SinkholeBlockEntity::new,
                            BlockInit.SINKHOLE.get()).build(null));

    public static final RegistryObject<BlockEntityType<RedMushroomSpawnerBlockEntity>> RED_MUSHROOM_SPAWNER =
            BLOCK_ENTITIES.register("red_mushroom_spawner", () ->
                    BlockEntityType.Builder.of(RedMushroomSpawnerBlockEntity::new,
                            SpecialBlockInit.RED_MUSHROOM_SPAWNER.get()).build(null));

    public static final RegistryObject<BlockEntityType<GreenMushroomSpawnerBlockEntity>> GREEN_MUSHROOM_SPAWNER =
            BLOCK_ENTITIES.register("green_mushroom_spawner", () ->
                    BlockEntityType.Builder.of(GreenMushroomSpawnerBlockEntity::new,
                            SpecialBlockInit.GREEN_MUSHROOM_SPAWNER.get()).build(null));

    public static final RegistryObject<BlockEntityType<BlueMushroomSpawnerBlockEntity>> BLUE_MUSHROOM_SPAWNER =
            BLOCK_ENTITIES.register("blue_mushroom_spawner", () ->
                    BlockEntityType.Builder.of(BlueMushroomSpawnerBlockEntity::new,
                            SpecialBlockInit.BLUE_MUSHROOM_SPAWNER.get()).build(null));

    public static final RegistryObject<BlockEntityType<LightShaftBlockEntity>> LIGHT_SHAFT =
            BLOCK_ENTITIES.register("light_shaft", () ->
                    BlockEntityType.Builder.of(LightShaftBlockEntity::new,
                            SpecialBlockInit.LIGHT_SHAFT.get()).build(null));

    public static final RegistryObject<BlockEntityType<TentacleSpawnerBlockEntity>> TENTACLE_SPAWNER =
            BLOCK_ENTITIES.register("tentacle_spawner", () ->
                    BlockEntityType.Builder.of(TentacleSpawnerBlockEntity::new,
                            SpecialBlockInit.TENTACLE_SPAWNER.get()).build(null));

    public static final RegistryObject<BlockEntityType<WoodLanternBlockEntity>> WOOD_LANTERN =
            BLOCK_ENTITIES.register("wood_lantern", () ->
                    BlockEntityType.Builder.of(WoodLanternBlockEntity::new,
                            BlockInit.WOOD_LANTERN.get()).build(null));

    public static void register(IEventBus eventBus) {
        BLOCK_ENTITIES.register(eventBus);
    }
}
