package com.ladestitute.bewarethedark.registries;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.containers.BackpackContainer;
import net.minecraft.world.inventory.MenuType;
import net.minecraftforge.common.extensions.IForgeMenuType;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;

public class ContainerInit {
    public static final DeferredRegister<MenuType<?>> CONTAINER_TYPES = DeferredRegister.create(
            ForgeRegistries.MENU_TYPES, BTDMain.MOD_ID);


    public static final RegistryObject<MenuType<BackpackContainer>> BACKPACK =
            CONTAINER_TYPES.register("sb_container", () -> IForgeMenuType.create(BackpackContainer::fromNetwork));

}

