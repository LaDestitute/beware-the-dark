package com.ladestitute.bewarethedark.events;

import com.ladestitute.bewarethedark.blocks.entity.CampFireTileEntity;
import com.ladestitute.bewarethedark.blocks.entity.TentacleSpawnerBlockEntity;
import com.ladestitute.bewarethedark.btdcapabilities.sanity.PlayerSanityProvider;
import com.ladestitute.bewarethedark.entities.mobs.hostile.TentacleEntity;
import com.ladestitute.bewarethedark.entities.mobs.neutral.CatcoonEntity;
import com.ladestitute.bewarethedark.entities.mobs.passive.ButterflyEntity;
import com.ladestitute.bewarethedark.entities.mobs.passive.JungleButterflyEntity;
import com.ladestitute.bewarethedark.entities.mobs.passive.MandrakeEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.HostileCrawlingHorrorEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.TerrorbeakEntity;
import com.ladestitute.bewarethedark.registries.FoodInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import com.ladestitute.bewarethedark.registries.SoundInit;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import net.minecraft.world.entity.animal.*;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.monster.CaveSpider;
import net.minecraft.world.entity.monster.Spider;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraftforge.event.entity.living.LivingDeathEvent;
import net.minecraftforge.event.entity.living.LivingDropsEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.Random;

public class BTDMobDropsHandler {

    @SubscribeEvent
    public void shadowcreaturesanitydrop(LivingDeathEvent event)
    {
     if(event.getEntity() instanceof HostileCrawlingHorrorEntity && event.getSource().getEntity() instanceof Player)
     {
         event.getSource().getEntity().getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
             sanity.addSanity(2);
         });

     }
        if(event.getEntity() instanceof TerrorbeakEntity && event.getSource().getEntity() instanceof Player)
        {
            event.getSource().getEntity().getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
                sanity.addSanity(4);
            });

        }
    }

    @SubscribeEvent
    public void onMobDrops(LivingDropsEvent event) {
        if (event.getEntity() instanceof Spider) {
            event.getDrops().clear();

            Random rand1 = new Random();

            int dropchance = rand1.nextInt(100);
            int specialdropchance = rand1.nextInt(100);
            int rollforloot = rand1.nextInt(5);

            ItemStack stack1 = new ItemStack(ItemInit.SPIDER_GLAND.get());
            ItemEntity gland = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, stack1);
            ItemStack stack2 = new ItemStack(Items.SPIDER_EYE);
            ItemEntity spidereye = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, stack2);
            ItemStack stack3 = new ItemStack(ItemInit.SILK.get());
            ItemEntity silk = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, stack3);
            ItemStack stack4 = new ItemStack(FoodInit.MONSTER_MEAT.get());
            ItemEntity monstermeat = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, stack4);
            ItemStack stack5 = new ItemStack(FoodInit.COOKED_MONSTER_MEAT.get());
            ItemEntity cookedmonstermeat = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, stack5);

            if (dropchance <= 24) {
                if (rollforloot < 1) {
                    if (event.getEntity().isOnFire()) {
                        event.getDrops().add(cookedmonstermeat);
                    } else event.getDrops().add(monstermeat);
                }
                if (rollforloot == 2) {
                    event.getDrops().add(spidereye);
                }
                if (rollforloot == 3) {
                    event.getDrops().add(gland);
                }
                if (rollforloot == 4) {
                    event.getDrops().add(silk);
                }
            }

        }

        if (event.getEntity() instanceof CaveSpider) {
            event.getDrops().clear();

            Random rand1 = new Random();

            int dropchance = rand1.nextInt(100);
            int rollforloot = rand1.nextInt(5);
            int venomdropchance = rand1.nextInt(101);

            ItemStack stack1 = new ItemStack(ItemInit.SPIDER_GLAND.get());
            ItemEntity gland = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, stack1);
            ItemStack stack2 = new ItemStack(Items.SPIDER_EYE);
            ItemEntity spidereye = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, stack2);
            ItemStack stack3 = new ItemStack(ItemInit.SILK.get());
            ItemEntity silk = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, stack3);
            ItemStack stack4 = new ItemStack(FoodInit.MONSTER_MEAT.get());
            ItemEntity monstermeat = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, stack4);
            ItemStack stack5 = new ItemStack(FoodInit.COOKED_MONSTER_MEAT.get());
            ItemEntity cookedmonstermeat = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, stack5);
            ItemStack stack6 = new ItemStack(FoodInit.VENOM_GLAND.get());
            ItemEntity venomgland = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, stack6);

            if (dropchance <= 49)
            {
                if (rollforloot < 1) {
                    if (event.getEntity().isOnFire()) {
                        event.getDrops().add(cookedmonstermeat);
                    } else event.getDrops().add(monstermeat);
                }
                if (rollforloot == 2) {
                    event.getDrops().add(spidereye);
                }
                if (rollforloot == 3) {
                    event.getDrops().add(gland);
                }
                if (rollforloot == 4) {
                    event.getDrops().add(silk);
                }
            }
            if(venomdropchance <= 25)
            {
                event.getDrops().add(venomgland);
            }
        }
        //
        if (event.getEntity() instanceof Sheep) {
            if (BTDConfig.getInstance().mobsdropgenericmeat()) {
                event.getDrops().clear();
                {

                    ItemStack item1 = new ItemStack(FoodInit.MEAT.get());
                    ItemEntity meat = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item1);
                    ItemStack item4 = new ItemStack(FoodInit.COOKED_MEAT.get());
                    ItemEntity cookedmeat = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item4);
                    if(!((Sheep) event.getEntity()).isBaby()) {
                        if (event.getEntity().isOnFire()) {
                            event.getDrops().add(cookedmeat);
                        } else
                            event.getDrops().add(meat);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.BLACK)
                    {
                        ItemStack bwooli = new ItemStack(Items.BLACK_WOOL);
                        ItemEntity bwool = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, bwooli);
                        event.getDrops().add(bwool);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.BLUE)
                    {
                        ItemStack bluwooli = new ItemStack(Items.BLUE_WOOL);
                        ItemEntity bluwool = new ItemEntity(event.getEntity().level, event.getEntity().position().x,
                                event.getEntity().position().y, event.getEntity().position().z, bluwooli);
                        event.getDrops().add(bluwool);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.BROWN)
                    {
                        ItemStack brwooli = new ItemStack(Items.BROWN_WOOL);
                        ItemEntity brwool = new ItemEntity(event.getEntity().level, event.getEntity().position().x,
                                event.getEntity().position().y, event.getEntity().position().z, brwooli);
                        event.getDrops().add(brwool);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.CYAN)
                    {
                        ItemStack cywooli = new ItemStack(Items.BLUE_WOOL);
                        ItemEntity cywool = new ItemEntity(event.getEntity().level, event.getEntity().position().x,
                                event.getEntity().position().y, event.getEntity().position().z, cywooli);
                        event.getDrops().add(cywool);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.GRAY)
                    {
                        ItemStack grwooli = new ItemStack(Items.GRAY_WOOL);
                        ItemEntity grwool = new ItemEntity(event.getEntity().level, event.getEntity().position().x,
                                event.getEntity().position().y, event.getEntity().position().z, grwooli);
                        event.getDrops().add(grwool);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.GREEN)
                    {
                        ItemStack grewooli = new ItemStack(Items.GREEN_WOOL);
                        ItemEntity grewool = new ItemEntity(event.getEntity().level, event.getEntity().position().x,
                                event.getEntity().position().y, event.getEntity().position().z, grewooli);
                        event.getDrops().add(grewool);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.LIGHT_BLUE)
                    {
                        ItemStack lbwooli = new ItemStack(Items.LIGHT_BLUE_WOOL);
                        ItemEntity lbwool = new ItemEntity(event.getEntity().level, event.getEntity().position().x,
                                event.getEntity().position().y, event.getEntity().position().z, lbwooli);
                        event.getDrops().add(lbwool);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.LIGHT_GRAY)
                    {
                        ItemStack lgwooli = new ItemStack(Items.LIGHT_GRAY_WOOL);
                        ItemEntity lgwool = new ItemEntity(event.getEntity().level, event.getEntity().position().x,
                                event.getEntity().position().y, event.getEntity().position().z, lgwooli);
                        event.getDrops().add(lgwool);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.LIME)
                    {
                        ItemStack limwooli = new ItemStack(Items.LIME_WOOL);
                        ItemEntity limwool = new ItemEntity(event.getEntity().level, event.getEntity().position().x,
                                event.getEntity().position().y, event.getEntity().position().z, limwooli);
                        event.getDrops().add(limwool);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.MAGENTA)
                    {
                        ItemStack magwooli = new ItemStack(Items.MAGENTA_WOOL);
                        ItemEntity magwool = new ItemEntity(event.getEntity().level, event.getEntity().position().x,
                                event.getEntity().position().y, event.getEntity().position().z, magwooli);
                        event.getDrops().add(magwool);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.ORANGE)
                    {
                        ItemStack orwooli = new ItemStack(Items.ORANGE_WOOL);
                        ItemEntity orwool = new ItemEntity(event.getEntity().level, event.getEntity().position().x,
                                event.getEntity().position().y, event.getEntity().position().z, orwooli);
                        event.getDrops().add(orwool);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.PINK)
                    {
                        ItemStack pwooli = new ItemStack(Items.PINK_WOOL);
                        ItemEntity pwool = new ItemEntity(event.getEntity().level, event.getEntity().position().x,
                                event.getEntity().position().y, event.getEntity().position().z, pwooli);
                        event.getDrops().add(pwool);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.PURPLE)
                    {
                        ItemStack pUwooli = new ItemStack(Items.PURPLE_WOOL);
                        ItemEntity pUwool = new ItemEntity(event.getEntity().level, event.getEntity().position().x,
                                event.getEntity().position().y, event.getEntity().position().z, pUwooli);
                        event.getDrops().add(pUwool);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.RED)
                    {
                        ItemStack rwooli = new ItemStack(Items.RED_WOOL);
                        ItemEntity rwool = new ItemEntity(event.getEntity().level, event.getEntity().position().x,
                                event.getEntity().position().y, event.getEntity().position().z, rwooli);
                        event.getDrops().add(rwool);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.YELLOW)
                    {
                        ItemStack ywooli = new ItemStack(Items.YELLOW_WOOL);
                        ItemEntity ywool = new ItemEntity(event.getEntity().level, event.getEntity().position().x,
                                event.getEntity().position().y, event.getEntity().position().z, ywooli);
                        event.getDrops().add(ywool);
                    }
                    if(((Sheep) event.getEntity()).getColor() == DyeColor.WHITE)
                    {
                        ItemStack wwooli = new ItemStack(Items.WHITE_WOOL);
                        ItemEntity wwool = new ItemEntity(event.getEntity().level, event.getEntity().position().x,
                                event.getEntity().position().y, event.getEntity().position().z, wwooli);
                        event.getDrops().add(wwool);
                    }
                }
                //

            }

        }
        //
        if (event.getEntity() instanceof Pig) {
            if (BTDConfig.getInstance().mobsdropgenericmeat()) {
                event.getDrops().clear();

                ItemStack item1 = new ItemStack(FoodInit.MEAT.get());
                ItemEntity meat = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item1);
                ItemStack item4 = new ItemStack(FoodInit.COOKED_MEAT.get());
                ItemEntity cookedmeat = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item4);

                if(!((Pig) event.getEntity()).isBaby()) {
                    if (event.getEntity().isOnFire()) {
                        event.getDrops().add(cookedmeat);
                    } else event.getDrops().add(meat);
                }
                //

            }

        }
        //
        if (event.getEntity() instanceof Chicken) {
            if (BTDConfig.getInstance().mobsdropgenericmeat()) {
                event.getDrops().clear();
                Random rand1 = new Random();

                int featherchance = rand1.nextInt(100);
                int extrafeatherchance = rand1.nextInt(100);

                ItemStack item1 = new ItemStack(FoodInit.MORSEL.get());
                ItemEntity morsel = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item1);
                ItemStack item2 = new ItemStack(Items.FEATHER);
                ItemEntity feather = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item2);
                ItemStack item4 = new ItemStack(FoodInit.COOKED_MORSEL.get());
                ItemEntity cookedmorsel = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item4);

                if(!((Chicken) event.getEntity()).isBaby()) {
                    if (event.getEntity().isOnFire()) {
                        event.getDrops().add(cookedmorsel);
                    } else event.getDrops().add(morsel);
                }
                //
                if (featherchance <= 49 && !((Chicken) event.getEntity()).isBaby()) {
                    event.getDrops().add(feather);
                    if (extrafeatherchance <= 49 && !((Chicken) event.getEntity()).isBaby())
                    {
                        event.getDrops().add(feather);
                    }
                }

            }

        }
        //
        if (event.getEntity() instanceof Cow) {
            if (BTDConfig.getInstance().mobsdropgenericmeat()) {
                event.getDrops().clear();
                Random rand1 = new Random();

                int hidechance = rand1.nextInt(100);
                int extrahidechance = rand1.nextInt(100);

                ItemStack item1 = new ItemStack(FoodInit.MEAT.get());
                ItemEntity meat = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item1);
                ItemStack item2 = new ItemStack(Items.LEATHER);
                ItemEntity hide = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item2);
                ItemStack item4 = new ItemStack(FoodInit.COOKED_MEAT.get());
                ItemEntity cookedmeat = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item4);

                if(!((Cow) event.getEntity()).isBaby()) {
                    if (event.getEntity().isOnFire()) {
                        event.getDrops().add(cookedmeat);
                    } else event.getDrops().add(meat);
                }
                //
                if (hidechance <= 49 && !((Cow) event.getEntity()).isBaby()) {
                    event.getDrops().add(hide);
                    if (extrahidechance <= 49 && !((Cow) event.getEntity()).isBaby())
                    {
                        event.getDrops().add(hide);
                    }
                }

            }

        }
        //
        if (event.getEntity() instanceof Rabbit) {
            if (BTDConfig.getInstance().mobsdropgenericmeat()) {
                event.getDrops().clear();
                Random rand1 = new Random();

                int hidechance = rand1.nextInt(100);
                int footchance = rand1.nextInt(100);

                ItemStack item1 = new ItemStack(FoodInit.MORSEL.get());
                ItemEntity morsel = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item1);
                ItemStack item2 = new ItemStack(Items.RABBIT_HIDE);
                ItemEntity hide = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item2);
                ItemStack item3 = new ItemStack(Items.RABBIT_FOOT);
                ItemEntity foot = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item3);
                ItemStack item4 = new ItemStack(FoodInit.COOKED_MORSEL.get());
                ItemEntity cookedmorsel = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item4);

                if(!((Rabbit) event.getEntity()).isBaby()) {
                    if (event.getEntity().isOnFire()) {
                        event.getDrops().add(cookedmorsel);
                    } else event.getDrops().add(morsel);
                }

                if (hidechance <= 49 && !((Rabbit) event.getEntity()).isBaby()) {
                    event.getDrops().add(hide);
                }

                if (footchance <= 9 && !((Rabbit) event.getEntity()).isBaby()) {
                    event.getDrops().add(foot);
                }

            }

        }
        //
        if (event.getEntity() instanceof ButterflyEntity) {
                Random rand1 = new Random();

                int butterchance = rand1.nextInt(101);

                ItemStack item1 = new ItemStack(FoodInit.BUTTERFLY_WINGS.get());
                ItemEntity wings = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item1);
                ItemStack item2 = new ItemStack(FoodInit.BUTTER.get());
                ItemEntity butter = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item2);

                if (butterchance <= 1) {
                    event.getDrops().add(butter);
                }
                else event.getDrops().add(wings);
        }
        //
        if (event.getEntity() instanceof JungleButterflyEntity) {
            Random rand1 = new Random();

            int butterchance = rand1.nextInt(101);

            ItemStack item1 = new ItemStack(FoodInit.JUNGLE_BUTTERFLY_WINGS.get());
            ItemEntity wings = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item1);
            ItemStack item2 = new ItemStack(FoodInit.BUTTER.get());
            ItemEntity butter = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item2);

            if (butterchance <= 1) {
                event.getDrops().add(butter);
            }
            else event.getDrops().add(wings);
        }
        //
        if (event.getEntity() instanceof MandrakeEntity) {
            ItemStack item2 = new ItemStack(FoodInit.COOKED_MANDRAKE.get());
            ItemEntity cookedmandrake = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item2);
            ItemStack item1 = new ItemStack(FoodInit.MANDRAKE.get());
            ItemEntity mandrake = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item1);
            if (event.getEntity().isOnFire()) {
                event.getDrops().add(cookedmandrake);
            } else event.getDrops().add(mandrake);
        }
        if (event.getEntity() instanceof CatcoonEntity) {
            Random rand1 = new Random();

            int tailchance = rand1.nextInt(101);

            ItemStack item3 = new ItemStack(FoodInit.COOKED_MEAT.get());
            ItemEntity cookedmeat = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item3);
            ItemStack item2 = new ItemStack(ItemInit.CAT_TAIL.get());
            ItemEntity tail = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item2);
            ItemStack item1 = new ItemStack(FoodInit.MEAT.get());
            ItemEntity meat = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item1);
            if (event.getEntity().isOnFire()) {
                event.getDrops().add(cookedmeat);
            } else event.getDrops().add(meat);
            if (tailchance <= 33) {
                event.getDrops().add(tail);
            }
        }
        if (event.getEntity() instanceof TentacleEntity) {
            BlockEntity tileentity = event.getEntity().level.getBlockEntity(event.getEntity().blockPosition());
            assert tileentity != null;
            ((TentacleSpawnerBlockEntity)tileentity).spawncooldown = 6000;

            ItemStack item1 = new ItemStack(FoodInit.MONSTER_MEAT.get());
            ItemEntity meat1 = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item1);
            event.getDrops().add(meat1);
            ItemStack item2 = new ItemStack(FoodInit.MONSTER_MEAT.get());
            ItemEntity meat2 = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y, event.getEntity().position().z, item2);
            event.getDrops().add(meat2);

            Random rand1 = new Random();
            ItemStack spikedrop = new ItemStack(ItemInit.TENTACLE_SPIKE.get());
            ItemEntity spike = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y,
                    event.getEntity().position().z, spikedrop);
            ItemStack spotsdrop = new ItemStack(ItemInit.TENTACLE_SPOTS.get());
            ItemEntity spots = new ItemEntity(event.getEntity().level, event.getEntity().position().x, event.getEntity().position().y,
                    event.getEntity().position().z, spotsdrop);

            int spikechance = rand1.nextInt(101);
            int spotschance = rand1.nextInt(101);
            if (spikechance <= 49) {
                event.getDrops().add(spike);
            }
            if (spotschance <= 19) {
                event.getDrops().add(spots);
            }
        }
    }

}
