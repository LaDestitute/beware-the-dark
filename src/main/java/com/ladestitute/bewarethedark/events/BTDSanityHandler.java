package com.ladestitute.bewarethedark.events;

import com.ladestitute.bewarethedark.btdcapabilities.sanity.PlayerSanityProvider;
import com.ladestitute.bewarethedark.entities.mobs.hostile.GhostEntity;
import com.ladestitute.bewarethedark.entities.mobs.hostile.TentacleEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.HostileCrawlingHorrorEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.MrSkittsEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.NightHandEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.TerrorbeakEntity;
import com.ladestitute.bewarethedark.network.ClientboundPlayerSanityUpdateMessage;
import com.ladestitute.bewarethedark.network.NetworkingHandler;
import com.ladestitute.bewarethedark.registries.BlockInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.server.commands.GameModeCommand;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.level.ServerPlayerGameMode;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.monster.CaveSpider;
import net.minecraft.world.entity.monster.Spider;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.GameType;
import net.minecraft.world.level.LightLayer;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.lighting.LayerLightEventListener;
import net.minecraft.world.phys.AABB;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;

import java.util.List;

public class BTDSanityHandler {

    @SubscribeEvent
    public void onPlayerTick(TickEvent.PlayerTickEvent event) {
        if (event.side == LogicalSide.SERVER) {
            event.player.getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
                //Sanity drain if in rain

                BlockPos pos = event.player.blockPosition();
                BlockState oneblocksupx = event.player.level.getBlockState(pos.above());
                BlockState twoblocksupx = event.player.level.getBlockState(pos.above().above());
                BlockState threeblocksupx = event.player.level.getBlockState(pos.above().above().above());
                BlockState fourblocksupx = event.player.level.getBlockState(pos.above().above().above().above());
                BlockState fiveblocksupx = event.player.level.getBlockState(pos.above().above().above().above().above());
                Block oneblocksup = oneblocksupx.getBlock();
                Block twoblocksup = twoblocksupx.getBlock();
                Block threeblocksup = threeblocksupx.getBlock();
                Block fourblocksup = fourblocksupx.getBlock();
                Block fiveblocksup = fiveblocksupx.getBlock();

                NetworkingHandler.sendToPlayer(new ClientboundPlayerSanityUpdateMessage(sanity.getIsFirstSpawn(), sanity.getSanity(), sanity.getNightTimer(),
                        sanity.getRainTimer(), sanity.getSpiderAuraTimer(), sanity.getDarknessTimer(),
                        sanity.getGhostAuraTimer(), sanity.getShadowCreatureTimer(), sanity.getPoisonTimer(), sanity.getGarlandTimer()), ((ServerPlayer) event.player));

                ItemStack headstack = event.player.getItemBySlot(EquipmentSlot.HEAD);
                if(((ServerPlayer) event.player).gameMode.isSurvival() && BTDConfig.getInstance().enable_sanity()) {
                    if (headstack.getItem() == ItemInit.GARLAND.get()) {
                        sanity.addGarlandTimer(1);
                    }
                    if (headstack.getItem() != ItemInit.GARLAND.get()) {
                        sanity.setGarlandTimer(0);
                    }
                    if (sanity.getGarlandTimer() >= 6000) {
                        sanity.addSanity(1);
                        sanity.setGarlandTimer(0);
                    }
                    if (event.player.level.isRainingAt(event.player.blockPosition()) && event.player.level.canSeeSky(event.player.blockPosition())) {
                        sanity.addRainTimer(1);
                    }
                    if (headstack.isEmpty() && sanity.getSanity() > 0
                            && sanity.getRainTimer() > 1000) {
                        sanity.subSanity(1);
                        sanity.setRainTimer(0);
                    }
                    if (!headstack.isEmpty() && sanity.getSanity() > 0
                            && sanity.getRainTimer() > 1500) {
                        sanity.subSanity(1);
                        sanity.setRainTimer(0);
                    }
                    //
                    if (event.player.hasEffect(MobEffects.POISON)) {
                        sanity.addPoisonTimer(1);
                    }
                    if (sanity.getSanity() > 0
                            && sanity.getPoisonTimer() > 1000) {
                        sanity.subSanity(2);
                        sanity.setPoisonTimer(0);
                    }
                    //Sanity drain from dusk onwards
                    if (event.player.level.isNight()) {
                        sanity.addNightTimer(1);
                    }
                    if (event.player.level.isNight()
                            && sanity.getSanity() > 0
                            && sanity.getNightTimer() > 3146) {
                        sanity.subSanity(1);
                        sanity.setNightTimer(0);

                    }
                }
                //
                //Sanity loss from spider/ghost insanity auras
                double radius = 1;
                AABB axisalignedbb = new AABB(event.player.blockPosition()).inflate(radius).expandTowards(0.0D, event.player.level.getMaxBuildHeight(), 0.0D);
                List<LivingEntity> list = event.player.level.getEntitiesOfClass(LivingEntity.class, axisalignedbb);
                for (LivingEntity aura : list) {
                    if(aura instanceof TentacleEntity ||aura instanceof GhostEntity||aura instanceof NightHandEntity)
                    {
                        if(((ServerPlayer) event.player).gameMode.isSurvival() && BTDConfig.getInstance().enable_sanity()) {
                            sanity.addGhostAuraTimer(1);
                        }
                    }
                    if(aura instanceof HostileCrawlingHorrorEntity ||aura instanceof TerrorbeakEntity)
                    {
                        if(((ServerPlayer) event.player).gameMode.isSurvival() && BTDConfig.getInstance().enable_sanity()) {
                            sanity.addShadowCreatureTimer(1);
                        }
                    }
                    if(aura instanceof CaveSpider)
                    {
                        if(((ServerPlayer) event.player).gameMode.isSurvival() && BTDConfig.getInstance().enable_sanity()) {
                            sanity.addSpiderAuraTimer(1);
                        }
                    }
                    else if(aura instanceof Spider)
                    {
                        if(((ServerPlayer) event.player).gameMode.isSurvival() && BTDConfig.getInstance().enable_sanity()) {
                            sanity.addSpiderAuraTimer(1);
                        }
                    }
                }
                //Linebreak
                if (sanity.getSanity() > 0
                        && sanity.getSpiderAuraTimer() > 1000) {
                    if(((ServerPlayer) event.player).gameMode.isSurvival() && BTDConfig.getInstance().enable_sanity()) {
                        sanity.subSanity(1);
                        sanity.setSpiderAuraTimer(0);
                    }
                }
                if (sanity.getSanity() > 0
                        && sanity.getGhostAuraTimer() > 1000) {
                    if(((ServerPlayer) event.player).gameMode.isSurvival() && BTDConfig.getInstance().enable_sanity()) {
                        sanity.subSanity(2);
                        sanity.setGhostAuraTimer(0);
                    }
                }
                if (sanity.getSanity() > 0
                        && sanity.getShadowCreatureTimer() > 1000) {
                    if(((ServerPlayer) event.player).gameMode.isSurvival() && BTDConfig.getInstance().enable_sanity()) {
                        sanity.subSanity(4);
                        sanity.setShadowCreatureTimer(0);
                    }
                }
                //Sanity drain from pure darkness
                LayerLightEventListener blockLightingLayer = event.player.level.getLightEngine().getLayerListener(LightLayer.BLOCK);
                if (!event.player.isUnderWater() && event.player.level.isNight() && blockLightingLayer.getLightValue(event.player.blockPosition()) <= 0||
                        !event.player.isUnderWater() && !event.player.level.canSeeSky(event.player.blockPosition()) && blockLightingLayer.getLightValue(event.player.blockPosition()) <= 0)
                {
                    if(((ServerPlayer) event.player).gameMode.isSurvival() && BTDConfig.getInstance().enable_sanity()) {
                        if(oneblocksup != Blocks.ACACIA_LEAVES ||
                                oneblocksup != Blocks.BIRCH_LEAVES ||
                                oneblocksup != Blocks.DARK_OAK_LEAVES ||
                                oneblocksup != Blocks.JUNGLE_LEAVES ||
                                oneblocksup != Blocks.OAK_LEAVES ||
                                oneblocksup != Blocks.SPRUCE_LEAVES ||
                                oneblocksup != BlockInit.EVERGREEN_LEAVES.get() ||
                                oneblocksup != BlockInit.LUMPY_EVERGREEN_LEAVES.get() ||
                                oneblocksup != BlockInit.GREEN_DECIDUOUS_LEAVES.get() ||
                                oneblocksup != BlockInit.ORANGE_DECIDUOUS_LEAVES.get() ||
                                oneblocksup != BlockInit.RED_DECIDUOUS_LEAVES.get() ||
                                oneblocksup != BlockInit.YELLOW_DECIDUOUS_LEAVES.get() ||
                                twoblocksup != Blocks.ACACIA_LEAVES ||
                                twoblocksup != Blocks.BIRCH_LEAVES ||
                                twoblocksup != Blocks.DARK_OAK_LEAVES ||
                                twoblocksup != Blocks.JUNGLE_LEAVES ||
                                twoblocksup != Blocks.OAK_LEAVES ||
                                twoblocksup != Blocks.SPRUCE_LEAVES ||
                                twoblocksup != BlockInit.EVERGREEN_LEAVES.get() ||
                                twoblocksup != BlockInit.LUMPY_EVERGREEN_LEAVES.get() ||
                                twoblocksup != BlockInit.GREEN_DECIDUOUS_LEAVES.get() ||
                                twoblocksup != BlockInit.ORANGE_DECIDUOUS_LEAVES.get() ||
                                twoblocksup != BlockInit.RED_DECIDUOUS_LEAVES.get() ||
                                twoblocksup != BlockInit.YELLOW_DECIDUOUS_LEAVES.get() ||
                                threeblocksup != Blocks.ACACIA_LEAVES ||
                                threeblocksup != Blocks.BIRCH_LEAVES ||
                                threeblocksup != Blocks.DARK_OAK_LEAVES ||
                                threeblocksup != Blocks.JUNGLE_LEAVES ||
                                threeblocksup != Blocks.OAK_LEAVES ||
                                threeblocksup != Blocks.SPRUCE_LEAVES ||
                                threeblocksup != BlockInit.EVERGREEN_LEAVES.get() ||
                                threeblocksup != BlockInit.LUMPY_EVERGREEN_LEAVES.get() ||
                                threeblocksup != BlockInit.GREEN_DECIDUOUS_LEAVES.get() ||
                                threeblocksup != BlockInit.ORANGE_DECIDUOUS_LEAVES.get() ||
                                threeblocksup != BlockInit.RED_DECIDUOUS_LEAVES.get() ||
                                threeblocksup != BlockInit.YELLOW_DECIDUOUS_LEAVES.get() ||
                                fourblocksup != Blocks.ACACIA_LEAVES ||
                                fourblocksup != Blocks.BIRCH_LEAVES ||
                                fourblocksup != Blocks.DARK_OAK_LEAVES ||
                                fourblocksup != Blocks.JUNGLE_LEAVES ||
                                fourblocksup != Blocks.OAK_LEAVES ||
                                fourblocksup != Blocks.SPRUCE_LEAVES ||
                                fourblocksup != BlockInit.EVERGREEN_LEAVES.get() ||
                                fourblocksup != BlockInit.LUMPY_EVERGREEN_LEAVES.get() ||
                                fourblocksup != BlockInit.GREEN_DECIDUOUS_LEAVES.get() ||
                                fourblocksup != BlockInit.ORANGE_DECIDUOUS_LEAVES.get() ||
                                fourblocksup != BlockInit.RED_DECIDUOUS_LEAVES.get() ||
                                fourblocksup != BlockInit.YELLOW_DECIDUOUS_LEAVES.get() ||
                                fiveblocksup != Blocks.ACACIA_LEAVES ||
                                fiveblocksup != Blocks.BIRCH_LEAVES ||
                                fiveblocksup != Blocks.DARK_OAK_LEAVES ||
                                fiveblocksup != Blocks.JUNGLE_LEAVES ||
                                fiveblocksup != Blocks.OAK_LEAVES ||
                                fiveblocksup != Blocks.SPRUCE_LEAVES ||
                                fiveblocksup != BlockInit.EVERGREEN_LEAVES.get() ||
                                fiveblocksup != BlockInit.LUMPY_EVERGREEN_LEAVES.get() ||
                                fiveblocksup != BlockInit.GREEN_DECIDUOUS_LEAVES.get() ||
                                fiveblocksup != BlockInit.ORANGE_DECIDUOUS_LEAVES.get() ||
                                fiveblocksup != BlockInit.RED_DECIDUOUS_LEAVES.get() ||
                                fiveblocksup != BlockInit.YELLOW_DECIDUOUS_LEAVES.get()) {
                            sanity.addDarknessTimer(1);
                        }
                    }
                }
                if (sanity.getSanity() > 0
                        && sanity.getDarknessTimer() > 1000) {
                    if(((ServerPlayer) event.player).gameMode.isSurvival() && BTDConfig.getInstance().enable_sanity()) {
                        sanity.subSanity(2);
                        sanity.setDarknessTimer(0);
                    }
                }
            });
        }
    }
}
