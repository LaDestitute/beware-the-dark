package com.ladestitute.bewarethedark.events;

import com.ladestitute.bewarethedark.entities.mobs.passive.ButterflyEntity;
import com.ladestitute.bewarethedark.entities.mobs.passive.JungleButterflyEntity;
import com.ladestitute.bewarethedark.registries.EntityInit;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.tags.BiomeTags;
import net.minecraft.world.entity.MobSpawnType;
import net.minecraft.world.entity.monster.Phantom;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class BTDMobSpawningHandler {
    int spawntimer = 0;

    @SubscribeEvent(priority = EventPriority.HIGH)
    public void onSpecialSpawn(LivingSpawnEvent.SpecialSpawn event) {
        if (!event.getLevel().isClientSide() && (event.getLevel()).dimensionType().bedWorks() && event.getEntity() instanceof Phantom && !(event.getSpawnReason() == MobSpawnType.SPAWN_EGG) && !(event.getSpawnReason() == MobSpawnType.COMMAND)) {
            if (!BTDConfig.getInstance().allowphantomstospawn()) {
               event.getEntity().discard();
            }
        }
    }

    @SubscribeEvent
    public void butterflyspawner(TickEvent.PlayerTickEvent event) {
        int searchRadius = 2;
        int radius = (int) (Math.floor(1.5) + searchRadius);


        for (int x = -radius; x <= radius; ++x) {
            for (int y = -1; y <= 1; ++y) {
                for (int z = -radius; z <= radius; ++z) {
                    BlockPos shrinePos = new BlockPos(event.player.blockPosition().getX() + x, event.player.blockPosition().getY() + y, event.player.blockPosition().getZ() + z);
                    BlockState blockState = event.player.level.getBlockState(shrinePos);
                    Block block = blockState.getBlock();
                    if (block == Blocks.ALLIUM || block == Blocks.CORNFLOWER ||
                            block == Blocks.DANDELION || block == Blocks.LILAC ||
                            block == Blocks.LILY_OF_THE_VALLEY || block == Blocks.ORANGE_TULIP ||
                            block == Blocks.OXEYE_DAISY || block == Blocks.PEONY ||
                            block == Blocks.PINK_TULIP || block == Blocks.POPPY ||
                            block == Blocks.RED_TULIP || block == Blocks.ROSE_BUSH
                            || block == Blocks.WHITE_TULIP || block == Blocks.SUNFLOWER ||
                            block == Blocks.WITHER_ROSE || block == Blocks.BLUE_ORCHID ||
                    block == Blocks.AZURE_BLUET) {
                        spawntimer++;
                        if(event.player.level.getBiome(event.player.blockPosition()).is(BiomeTags.IS_JUNGLE))
                        {
                            if(event.player.level.isDay() && spawntimer >= 760)
                            {
                                JungleButterflyEntity butterfly = new JungleButterflyEntity(EntityInit.JUNGLE_BUTTERFLY.get(), event.player.level);
                                butterfly.setPos(shrinePos.getX() + 0.5, shrinePos.getY(), shrinePos.getZ() + 0.5);
                                event.player.level.addFreshEntity(butterfly);
                                spawntimer=0;
                            }
                            if(event.player.level.isDay() &&  spawntimer >= 760)
                            {
                                spawntimer=0;
                            }
                        }
                        else if(event.player.level.isDay() && spawntimer >= 760)
                        {
                                ButterflyEntity butterfly = new ButterflyEntity(EntityInit.BUTTERFLY.get(), event.player.level);
                                butterfly.setPos(shrinePos.getX() + 0.5, shrinePos.getY(), shrinePos.getZ() + 0.5);
                                event.player.level.addFreshEntity(butterfly);
                                spawntimer=0;
                        }
                        if(event.player.level.isDay() &&  spawntimer >= 760)
                        {
                            spawntimer=0;
                        }
                    }

                }
            }
        }
    }
}
