package com.ladestitute.bewarethedark.events;

import com.ladestitute.bewarethedark.blocks.foodutil.BasicFarmBlock;
import com.ladestitute.bewarethedark.blocks.foodutil.CrockPotBlock;
import com.ladestitute.bewarethedark.blocks.foodutil.DryingRackBlock;
import com.ladestitute.bewarethedark.blocks.foodutil.ImprovedFarmBlock;
import com.ladestitute.bewarethedark.blocks.helper.FirefliesSpawnerBlock;
import com.ladestitute.bewarethedark.blocks.natural.plant.GrassTuftBlock;
import com.ladestitute.bewarethedark.blocks.natural.plant.BerryBushBlock;
import com.ladestitute.bewarethedark.blocks.natural.plant.SaplingPlantBlock;
import com.ladestitute.bewarethedark.blocks.natural.plant.SpikyBushBlock;
import com.ladestitute.bewarethedark.blocks.natural.plant.StumpBlock;
import com.ladestitute.bewarethedark.blocks.natural.plant.logs.BirchnutStumpBlock;
import com.ladestitute.bewarethedark.blocks.turf.DeciduousTurfBlock;
import com.ladestitute.bewarethedark.blocks.turf.ForestTurfBlock;
import com.ladestitute.bewarethedark.blocks.turf.MarshTurfBlock;
import com.ladestitute.bewarethedark.blocks.utility.BTDCampfireBlock;
import com.ladestitute.bewarethedark.blocks.utility.FirePitBlock;
import com.ladestitute.bewarethedark.blocks.utility.ScienceMachineBlock;
import com.ladestitute.bewarethedark.blocks.utility.UnlitFirepitBlock;
import com.ladestitute.bewarethedark.entities.mobs.neutral.CatcoonEntity;
import com.ladestitute.bewarethedark.entities.mobs.passive.FireflyEntity;
import com.ladestitute.bewarethedark.registries.BlockInit;
import com.ladestitute.bewarethedark.registries.EntityInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import com.ladestitute.bewarethedark.registries.SpecialBlockInit;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.animal.Bee;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.GrassBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

import java.util.Random;

public class BTDBlockInteractionsHandler {

    @SubscribeEvent
    //Event for removing saplings and stumps with mod shovels
    public void shovelutility(PlayerInteractEvent.RightClickBlock event) {
        BlockPos shovelhitpos = event.getPos();
        BlockState state = event.getLevel().getBlockState(shovelhitpos);
        if (event.getLevel().isClientSide) {
            return;
        }

        ItemStack logstack1 = new ItemStack(ItemInit.SPRUCE_LOG.get());
        ItemEntity log1 = new ItemEntity(event.getEntity().level, shovelhitpos.getX(), shovelhitpos.getY(), shovelhitpos.getZ(), logstack1);
        ItemStack pinestack = new ItemStack(ItemInit.PINE_CONE.get());
        ItemEntity pine = new ItemEntity(event.getEntity().level, shovelhitpos.getX(), shovelhitpos.getY(), shovelhitpos.getZ(), pinestack);
        ItemStack birchnutstack = new ItemStack(ItemInit.BIRCHNUT.get());
        ItemEntity birchnut = new ItemEntity(event.getEntity().level, shovelhitpos.getX(), shovelhitpos.getY(), shovelhitpos.getZ(), birchnutstack);
        ItemStack berrybushstack = new ItemStack(ItemInit.BERRY_BUSH_ITEM.get());
        ItemEntity berrybush = new ItemEntity(event.getEntity().level, shovelhitpos.getX(), shovelhitpos.getY(), shovelhitpos.getZ(), berrybushstack);
        ItemStack saplingstack = new ItemStack(ItemInit.SAPLING_PLANT_ITEM.get());
        ItemEntity sapling = new ItemEntity(event.getEntity().level, shovelhitpos.getX(), shovelhitpos.getY(), shovelhitpos.getZ(), saplingstack);
        ItemStack grasstuftstack = new ItemStack(ItemInit.GRASS_TUFT_ITEM.get());
        ItemEntity grasstuft = new ItemEntity(event.getEntity().level, shovelhitpos.getX(), shovelhitpos.getY(), shovelhitpos.getZ(), grasstuftstack);
        ItemStack spikybushstack = new ItemStack(ItemInit.SPIKY_BUSH_ITEM.get());
        ItemEntity spiky = new ItemEntity(event.getEntity().level, shovelhitpos.getX(), shovelhitpos.getY(), shovelhitpos.getZ(), spikybushstack);
        ItemStack twigsstack = new ItemStack(ItemInit.TWIGS.get());
        ItemEntity twigs = new ItemEntity(event.getEntity().level, shovelhitpos.getX(), shovelhitpos.getY(), shovelhitpos.getZ(), twigsstack);

        if (event.getItemStack().getItem() == ItemInit.FLINT_SHOVEL.get()) {
            if (state.getBlock() instanceof StumpBlock || state.getBlock() instanceof BirchnutStumpBlock)
            {
                ItemStack shovel = ItemInit.FLINT_SHOVEL.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.FLINT_SHOVEL.get()) {
                            event.getEntity().level.destroyBlock(shovelhitpos, false);
                            event.getEntity().level.addFreshEntity(log1);
                            held.hurtAndBreak(4, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.REGAL_SHOVEL.get()) {
            if (state.getBlock() instanceof StumpBlock || state.getBlock() instanceof BirchnutStumpBlock)
            {
                ItemStack shovel = ItemInit.REGAL_SHOVEL.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.REGAL_SHOVEL.get()) {
                            event.getEntity().level.destroyBlock(shovelhitpos, false);
                            event.getEntity().level.addFreshEntity(log1);
                            held.hurtAndBreak(16, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.FLINT_SHOVEL.get()) {
            if (state.getBlock() == BlockInit.EVERGREEN_SAPLING.get())
            {
                ItemStack shovel = ItemInit.FLINT_SHOVEL.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.FLINT_SHOVEL.get()) {
                            event.getEntity().level.addFreshEntity(pine);
                            event.getEntity().level.removeBlock(shovelhitpos, false);
                            held.hurtAndBreak(4, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.REGAL_SHOVEL.get()) {
            if (state.getBlock() == BlockInit.EVERGREEN_SAPLING.get())
            {
                ItemStack shovel = ItemInit.REGAL_SHOVEL.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.REGAL_SHOVEL.get()) {
                            event.getEntity().level.addFreshEntity(pine);
                            event.getEntity().level.removeBlock(shovelhitpos, false);
                            held.hurtAndBreak(16, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.FLINT_SHOVEL.get()) {
            //ADD birch saplings
            if (state.getBlock() == BlockInit.EVERGREEN_SAPLING.get())
            {
                ItemStack shovel = ItemInit.FLINT_SHOVEL.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.FLINT_SHOVEL.get()) {
                            event.getEntity().level.addFreshEntity(birchnut);
                            event.getEntity().level.removeBlock(shovelhitpos, false);
                            held.hurtAndBreak(4, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.REGAL_SHOVEL.get()) {
            if (state.getBlock() == BlockInit.EVERGREEN_SAPLING.get())
            {
                ItemStack shovel = ItemInit.REGAL_SHOVEL.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.REGAL_SHOVEL.get()) {
                            event.getEntity().level.addFreshEntity(birchnut);
                            event.getEntity().level.getBiome(event.getEntity().blockPosition()).get();
                            event.getEntity().level.removeBlock(shovelhitpos, false);
                            held.hurtAndBreak(16, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.FLINT_SHOVEL.get()) {
            if (state.getBlock() instanceof BerryBushBlock && state.getValue(BerryBushBlock.AGE) < 7)
            {
                ItemStack shovel = ItemInit.FLINT_SHOVEL.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.FLINT_SHOVEL.get()) {
                            event.getEntity().level.addFreshEntity(berrybush);
                            event.getEntity().level.setBlockAndUpdate(shovelhitpos, Blocks.AIR.defaultBlockState());
                            held.hurtAndBreak(4, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.REGAL_SHOVEL.get()) {
            if (state.getBlock() instanceof BerryBushBlock && state.getValue(BerryBushBlock.AGE) < 7)
            {
                ItemStack shovel = ItemInit.REGAL_SHOVEL.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.REGAL_SHOVEL.get()) {
                            event.getEntity().level.addFreshEntity(berrybush);
                            event.getEntity().level.removeBlock(shovelhitpos, false);
                            held.hurtAndBreak(16, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.FLINT_SHOVEL.get()) {
            if (state.getBlock() instanceof GrassTuftBlock && state.getValue(GrassTuftBlock.AGE) < 7)
            {
                ItemStack shovel = ItemInit.FLINT_SHOVEL.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.FLINT_SHOVEL.get()) {
                            event.getEntity().level.addFreshEntity(grasstuft);
                            event.getEntity().level.removeBlock(shovelhitpos, false);
                            held.hurtAndBreak(4, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.REGAL_SHOVEL.get()) {
            if (state.getBlock() instanceof GrassTuftBlock && state.getValue(GrassTuftBlock.AGE) < 7)
            {
                ItemStack shovel = ItemInit.REGAL_SHOVEL.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.REGAL_SHOVEL.get()) {
                            event.getEntity().level.addFreshEntity(grasstuft);
                            event.getEntity().level.removeBlock(shovelhitpos, false);
                            held.hurtAndBreak(16, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.FLINT_SHOVEL.get()) {
            if (state.getBlock() instanceof SaplingPlantBlock && state.getValue(SaplingPlantBlock.AGE) < 7)
            {
                ItemStack shovel = ItemInit.FLINT_SHOVEL.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.FLINT_SHOVEL.get()) {
                            event.getEntity().level.addFreshEntity(sapling);
                            event.getEntity().level.removeBlock(shovelhitpos, false);
                            held.hurtAndBreak(4, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.REGAL_SHOVEL.get()) {
            if (state.getBlock() instanceof SaplingPlantBlock && state.getValue(SaplingPlantBlock.AGE) < 7)
            {
                ItemStack shovel = ItemInit.REGAL_SHOVEL.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.REGAL_SHOVEL.get()) {
                            event.getEntity().level.addFreshEntity(sapling);
                            event.getEntity().level.removeBlock(shovelhitpos, false);
                            held.hurtAndBreak(16, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.FLINT_SHOVEL.get()) {
            if (state.getBlock() instanceof SpikyBushBlock && state.getValue(SpikyBushBlock.AGE) < 7)
            {
                ItemStack shovel = ItemInit.FLINT_SHOVEL.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.FLINT_SHOVEL.get()) {
                            event.getEntity().level.addFreshEntity(spiky);
                            event.getEntity().level.addFreshEntity(twigs);
                            event.getEntity().level.removeBlock(shovelhitpos, false);
                            held.hurtAndBreak(4, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.REGAL_SHOVEL.get()) {
            if (state.getBlock() instanceof SpikyBushBlock && state.getValue(SpikyBushBlock.AGE) < 7)
            {
                ItemStack shovel = ItemInit.REGAL_SHOVEL.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.REGAL_SHOVEL.get()) {
                            event.getEntity().level.addFreshEntity(spiky);
                            event.getEntity().level.addFreshEntity(twigs);
                            event.getEntity().level.removeBlock(shovelhitpos, false);
                            held.hurtAndBreak(16, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
    }

    @SubscribeEvent
    //Subscribe event for breaking down utility/single-block structures with the hammer, which removes said block and returns 50% of its materials used to craft it
    public void hammerutility(PlayerInteractEvent.RightClickBlock event) {
        BlockPos hammerhitpos = event.getPos();
        BlockState state = event.getLevel().getBlockState(hammerhitpos);
        if (event.getLevel().isClientSide) {
            return;
        }

        ItemStack rocksstack1 = new ItemStack(ItemInit.ROCKS.get());
        ItemEntity rocks1 = new ItemEntity(event.getEntity().level, hammerhitpos.getX(), hammerhitpos.getY(), hammerhitpos.getZ(), rocksstack1);
        ItemStack rocksstack2 = new ItemStack(ItemInit.ROCKS.get());
        ItemEntity rocks2 = new ItemEntity(event.getEntity().level, hammerhitpos.getX(), hammerhitpos.getY(), hammerhitpos.getZ(), rocksstack2);
        ItemStack rocksstack3 = new ItemStack(ItemInit.ROCKS.get());
        ItemEntity rocks3 = new ItemEntity(event.getEntity().level, hammerhitpos.getX(), hammerhitpos.getY(), hammerhitpos.getZ(), rocksstack3);
        ItemStack rocksstack4 = new ItemStack(ItemInit.ROCKS.get());
        ItemEntity rocks4 = new ItemEntity(event.getEntity().level, hammerhitpos.getX(), hammerhitpos.getY(), hammerhitpos.getZ(), rocksstack4);
        ItemStack logstack1 = new ItemStack(ItemInit.SPRUCE_LOG.get());
        ItemEntity log1 = new ItemEntity(event.getEntity().level, hammerhitpos.getX(), hammerhitpos.getY(), hammerhitpos.getZ(), logstack1);
        ItemStack logstack2 = new ItemStack(ItemInit.SPRUCE_LOG.get());
        ItemEntity log2 = new ItemEntity(event.getEntity().level, hammerhitpos.getX(), hammerhitpos.getY(), hammerhitpos.getZ(), logstack2);
        ItemStack grassstack1 = new ItemStack(ItemInit.CUT_GRASS.get());
        ItemEntity grass1 = new ItemEntity(event.getEntity().level, hammerhitpos.getX(), hammerhitpos.getY(), hammerhitpos.getZ(), grassstack1);
        ItemStack manurestack1 = new ItemStack(ItemInit.MANURE.get());
        ItemEntity manure1 = new ItemEntity(event.getEntity().level, hammerhitpos.getX(), hammerhitpos.getY(), hammerhitpos.getZ(), manurestack1);
        ItemStack charcoalstack1 = new ItemStack(Items.CHARCOAL);
        ItemEntity charcoal1 = new ItemEntity(event.getEntity().level, hammerhitpos.getX(), hammerhitpos.getY(), hammerhitpos.getZ(), charcoalstack1);
        ItemStack twigsstack1 = new ItemStack(ItemInit.TWIGS.get());
        ItemEntity twigs1 = new ItemEntity(event.getEntity().level, hammerhitpos.getX(), hammerhitpos.getY(), hammerhitpos.getZ(), twigsstack1);
        ItemStack ropestack1 = new ItemStack(ItemInit.ROPE.get());
        ItemEntity rope1 = new ItemEntity(event.getEntity().level, hammerhitpos.getX(), hammerhitpos.getY(), hammerhitpos.getZ(), ropestack1);
        ItemStack twigsstack2 = new ItemStack(ItemInit.TWIGS.get());
        ItemEntity twigs2 = new ItemEntity(event.getEntity().level, hammerhitpos.getX(), hammerhitpos.getY(), hammerhitpos.getZ(), twigsstack2);
        ItemStack cutstonestack1 = new ItemStack(ItemInit.CUT_STONE.get());
        ItemEntity cutstone1 = new ItemEntity(event.getEntity().level, hammerhitpos.getX(), hammerhitpos.getY(), hammerhitpos.getZ(), cutstonestack1);

        if (event.getItemStack().getItem() == ItemInit.HAMMER.get()) {
            if (state.getBlock() instanceof ScienceMachineBlock)
            {
                ItemStack hammer = ItemInit.HAMMER.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(hammer))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.HAMMER.get()) {
                            event.getEntity().level.destroyBlock(hammerhitpos, false);
                            event.getEntity().level.addFreshEntity(rocks1);
                            event.getEntity().level.addFreshEntity(rocks2);
                            event.getEntity().level.addFreshEntity(log1);
                            event.getEntity().level.addFreshEntity(log2);
                            held.hurtAndBreak(1, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.HAMMER.get()) {
            if (state.getBlock() instanceof BTDCampfireBlock)
            {
                ItemStack hammer = ItemInit.HAMMER.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(hammer))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.HAMMER.get()) {
                            event.getEntity().level.destroyBlock(hammerhitpos, false);
                            event.getEntity().level.addFreshEntity(log1);
                            event.getEntity().level.addFreshEntity(grass1);
                            held.hurtAndBreak(1, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.HAMMER.get()) {
            if (state.getBlock() instanceof FirePitBlock||
                    state.getBlock() instanceof UnlitFirepitBlock)
            {
                ItemStack hammer = ItemInit.HAMMER.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(hammer))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.HAMMER.get()) {
                            event.getEntity().level.destroyBlock(hammerhitpos, false);
                            event.getEntity().level.addFreshEntity(rocks1);
                            event.getEntity().level.addFreshEntity(rocks2);
                            event.getEntity().level.addFreshEntity(rocks3);
                            event.getEntity().level.addFreshEntity(rocks4);
                            event.getEntity().level.addFreshEntity(log1);
                            held.hurtAndBreak(1, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.HAMMER.get()) {
            if (state.getBlock() instanceof BasicFarmBlock)
            {
                ItemStack hammer = ItemInit.HAMMER.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(hammer))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.HAMMER.get()) {
                            event.getEntity().level.destroyBlock(hammerhitpos, false);
                            event.getEntity().level.addFreshEntity(log1);
                            event.getEntity().level.addFreshEntity(grass1);
                            event.getEntity().level.addFreshEntity(manure1);
                            held.hurtAndBreak(1, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.HAMMER.get()) {
            if (state.getBlock() instanceof ImprovedFarmBlock)
            {
                ItemStack hammer = ItemInit.HAMMER.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(hammer))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.HAMMER.get()) {
                            event.getEntity().level.destroyBlock(hammerhitpos, false);
                            event.getEntity().level.addFreshEntity(rocks1);
                            event.getEntity().level.addFreshEntity(grass1);
                            event.getEntity().level.addFreshEntity(manure1);
                            held.hurtAndBreak(1, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.HAMMER.get()) {
            if (state.getBlock() instanceof DryingRackBlock)
            {
                ItemStack hammer = ItemInit.HAMMER.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(hammer))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.HAMMER.get()) {
                            event.getEntity().level.destroyBlock(hammerhitpos, false);
                            event.getEntity().level.addFreshEntity(charcoal1);
                            event.getEntity().level.addFreshEntity(twigs1);
                            event.getEntity().level.addFreshEntity(rope1);
                            held.hurtAndBreak(1, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.HAMMER.get()) {
            if (state.getBlock() instanceof CrockPotBlock)
            {
                ItemStack hammer = ItemInit.HAMMER.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(hammer))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.HAMMER.get()) {
                            event.getEntity().level.destroyBlock(hammerhitpos, false);
                            event.getEntity().level.addFreshEntity(charcoal1);
                            event.getEntity().level.addFreshEntity(twigs1);
                            event.getEntity().level.addFreshEntity(twigs2);
                            event.getEntity().level.addFreshEntity(cutstone1);
                            held.hurtAndBreak(1, event.getEntity(), (p_220045_0_) -> {
                                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
                            });
                        }
            }
        }
    }

    @SubscribeEvent
    //Subscribe event for planting pine cones (they're used to plant saplings instead)
    public void plantingpinecones(PlayerInteractEvent.RightClickBlock event) {
        BlockPos blockpos = event.getPos();
        BlockState state = event.getLevel().getBlockState(blockpos);
        if (event.getLevel().isClientSide) {
            return;
        }
        if (event.getItemStack().getItem() == ItemInit.PINE_CONE.get()) {
            if (state.getBlock() instanceof GrassBlock ||
                    state.getBlock() instanceof DeciduousTurfBlock ||
                    state.getBlock() instanceof ForestTurfBlock ||
                    state.getBlock() instanceof MarshTurfBlock)
            {
                ItemStack item = ItemInit.PINE_CONE.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(item))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.PINE_CONE.get()) {
                            event.getEntity().level.setBlock(blockpos.above(), BlockInit.EVERGREEN_SAPLING.get().defaultBlockState(), 2);
                            if(!event.getEntity().isCreative()) {
                                held.shrink(1);
                            }
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.FIREFLIES.get()) {
                ItemStack item = ItemInit.FIREFLIES.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(item))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.FIREFLIES.get()) {
                            FireflyEntity bug = new FireflyEntity(EntityInit.FIREFLY.get(), event.getLevel());
                            bug.setPos(blockpos.getX()+0.5, blockpos.getY()+1, blockpos.getZ()+0.5);
                            event.getLevel().addFreshEntity(bug);
                            if(!event.getEntity().isCreative()) {
                                held.shrink(1);
                            }
                        }

        }
        //
        if (event.getItemStack().getItem() == ItemInit.BEE.get()) {
            ItemStack item = ItemInit.BEE.get().getDefaultInstance();
            if (event.getEntity().getInventory().contains(item))
                for (ItemStack held : event.getEntity().getInventory().items)
                    if (held.getItem() == ItemInit.BEE.get()) {
                        Bee bug = new Bee(EntityType.BEE, event.getLevel());
                        bug.setPos(blockpos.getX()+0.5, blockpos.getY()+1, blockpos.getZ()+0.5);
                        event.getLevel().addFreshEntity(bug);
                        if(!event.getEntity().isCreative()) {
                            held.shrink(1);
                        }
                    }

        }
        //
            if (event.getItemStack().getItem() == ItemInit.BIRCHNUT.get()) {
                if (state.getBlock() instanceof GrassBlock ||
                        state.getBlock() instanceof DeciduousTurfBlock ||
                        state.getBlock() instanceof ForestTurfBlock ||
                        state.getBlock() instanceof MarshTurfBlock)
                {
                    ItemStack item = ItemInit.BIRCHNUT.get().getDefaultInstance();
                    if (event.getEntity().getInventory().contains(item))
                        for (ItemStack held : event.getEntity().getInventory().items)
                            if (held.getItem() == ItemInit.BIRCHNUT.get()) {
                                Random rand = new Random();
                                int birchnut = rand.nextInt(4);
                                if(birchnut == 0)
                                {
                                    event.getEntity().level.setBlock(blockpos.above(),
                                            SpecialBlockInit.BIRCHNUT_SAPLING_ORANGE.get().defaultBlockState(), 2);
                                }
                                if(birchnut == 1)
                                {
                                    event.getEntity().level.setBlock(blockpos.above(),
                                            SpecialBlockInit.BIRCHNUT_SAPLING_RED.get().defaultBlockState(), 2);
                                }
                                if(birchnut == 2)
                                {
                                    event.getEntity().level.setBlock(blockpos.above(),
                                            SpecialBlockInit.BIRCHNUT_SAPLING_YELLOW.get().defaultBlockState(), 2);
                                }
                                if(birchnut == 3)
                                {
                                    event.getEntity().level.setBlock(blockpos.above(),
                                            SpecialBlockInit.BIRCHNUT_SAPLING_GREEN.get().defaultBlockState(), 2);
                                }
                                if(!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                }
                            }
                }
            }
        if (event.getItemStack().getItem() == ItemInit.BERRY_BUSH_ITEM.get()) {
            if (state.getBlock() instanceof GrassBlock ||
                    state.getBlock() instanceof DeciduousTurfBlock ||
                    state.getBlock() instanceof ForestTurfBlock ||
                    state.getBlock() instanceof MarshTurfBlock)
            {
                ItemStack item = ItemInit.BERRY_BUSH_ITEM.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(item))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.BERRY_BUSH_ITEM.get()) {
                            event.getEntity().level.setBlock(blockpos.above(), SpecialBlockInit.BERRY_BUSH.get().defaultBlockState(), 2);
                            if(!event.getEntity().isCreative()) {
                                held.shrink(1);
                            }
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.GRASS_TUFT_ITEM.get()) {
            if (state.getBlock() instanceof GrassBlock ||
                    state.getBlock() instanceof DeciduousTurfBlock ||
                    state.getBlock() instanceof ForestTurfBlock ||
                    state.getBlock() instanceof MarshTurfBlock)
            {
                ItemStack item = ItemInit.GRASS_TUFT_ITEM.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(item))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.GRASS_TUFT_ITEM.get()) {
                            event.getEntity().level.setBlock(blockpos.above(), SpecialBlockInit.GRASS_TUFT.get().defaultBlockState(), 2);
                            if(!event.getEntity().isCreative()) {
                                held.shrink(1);
                            }
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.FIREFLIES.get()) {

                ItemStack item = ItemInit.FIREFLIES.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(item))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.FIREFLIES.get()) {
                         //   event.getEntity().level.setBlock(blockpos.above(), SpecialBlockInit.FIREFLIES_SPAWNER.get().defaultBlockState(), 2);
                            if(!event.getEntity().isCreative()) {
                         //       held.shrink(1);
                            }
                        }

        }
        //
        if (event.getItemStack().getItem() == ItemInit.SAPLING_PLANT_ITEM.get()) {
            if (state.getBlock() instanceof GrassBlock ||
                    state.getBlock() instanceof DeciduousTurfBlock ||
                    state.getBlock() instanceof ForestTurfBlock ||
                    state.getBlock() instanceof MarshTurfBlock)
            {
                ItemStack item = ItemInit.SAPLING_PLANT_ITEM.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(item))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.SAPLING_PLANT_ITEM.get()) {
                            event.getEntity().level.setBlock(blockpos.above(), SpecialBlockInit.SAPLING_PLANT.get().defaultBlockState(), 2);
                            if(!event.getEntity().isCreative()) {
                                held.shrink(1);
                            }
                        }
            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.SPIKY_BUSH_ITEM.get()) {
            if (state.getBlock() instanceof GrassBlock ||
                    state.getBlock() instanceof DeciduousTurfBlock ||
                    state.getBlock() instanceof ForestTurfBlock ||
                    state.getBlock() instanceof MarshTurfBlock)
            {
                ItemStack item = ItemInit.SPIKY_BUSH_ITEM.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(item))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.SPIKY_BUSH_ITEM.get()) {
                            event.getEntity().level.setBlock(blockpos.above(), SpecialBlockInit.SPIKY_BUSH.get().defaultBlockState(), 2);
                            if(!event.getEntity().isCreative()) {
                                held.shrink(1);
                            }
                        }
            }
        }
    }

    @SubscribeEvent
    //Butterfly usage
    public void plantingbutterflies(PlayerInteractEvent.RightClickBlock event) {
        Random rand = new Random();
        BlockPos blockpos = event.getPos();
        BlockState state = event.getLevel().getBlockState(blockpos);
        if (event.getLevel().isClientSide) {
            return;
        }
        if (event.getItemStack().getItem() == ItemInit.BUTTERFLY.get()) {
            if (state.getBlock() instanceof GrassBlock ||
                    state.getBlock() instanceof DeciduousTurfBlock ||
                    state.getBlock() instanceof ForestTurfBlock ||
                    state.getBlock() instanceof MarshTurfBlock)
            {
                ItemStack item = ItemInit.BUTTERFLY.get().getDefaultInstance();
                int flowertype = rand.nextInt(12);
                if (event.getEntity().getInventory().contains(item))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.BUTTERFLY.get())
                        {
                            if(flowertype == 0)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.DANDELION.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 1)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.POPPY.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 2)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.BLUE_ORCHID.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 3)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.ALLIUM.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 4)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.AZURE_BLUET.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 5)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.ORANGE_TULIP.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 6)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.PINK_TULIP.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 7)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.RED_TULIP.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 8)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.WHITE_TULIP.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 9)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.OXEYE_DAISY.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 10)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.LILY_OF_THE_VALLEY.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 11)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.CORNFLOWER.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                        }

                        //

            }
        }
        //
        if (event.getItemStack().getItem() == ItemInit.JUNGLE_BUTTERFLY.get()) {
            if (state.getBlock() instanceof GrassBlock ||
                    state.getBlock() instanceof DeciduousTurfBlock ||
                    state.getBlock() instanceof ForestTurfBlock ||
                    state.getBlock() instanceof MarshTurfBlock)
            {
                ItemStack item = ItemInit.JUNGLE_BUTTERFLY.get().getDefaultInstance();
                int flowertype = rand.nextInt(12);
                if (event.getEntity().getInventory().contains(item))
                    for (ItemStack held : event.getEntity().getInventory().items)
                        if (held.getItem() == ItemInit.JUNGLE_BUTTERFLY.get())
                        {
                            if(flowertype == 0)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.DANDELION.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 1)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.POPPY.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 2)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.BLUE_ORCHID.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 3)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.ALLIUM.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 4)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.AZURE_BLUET.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 5)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.ORANGE_TULIP.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 6)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.PINK_TULIP.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 7)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.RED_TULIP.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 8)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.WHITE_TULIP.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 9)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.OXEYE_DAISY.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 10)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.LILY_OF_THE_VALLEY.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                            if(flowertype == 11)
                            {
                                event.getEntity().level.setBlock(blockpos.above(), Blocks.CORNFLOWER.defaultBlockState(), 2);
                                if (!event.getEntity().isCreative()) {
                                    held.shrink(1);
                                    break;
                                }
                            }
                        }

                //

            }
        }
    }


}
