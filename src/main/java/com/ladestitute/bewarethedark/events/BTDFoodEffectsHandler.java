package com.ladestitute.bewarethedark.events;

import com.ladestitute.bewarethedark.btdcapabilities.customplayerdata.CustomPlayerDataProvider;
import com.ladestitute.bewarethedark.btdcapabilities.sanity.PlayerSanityProvider;
import com.ladestitute.bewarethedark.network.ClientboundPlayerSanityUpdateMessage;
import com.ladestitute.bewarethedark.network.NetworkingHandler;
import com.ladestitute.bewarethedark.registries.EffectInit;
import com.ladestitute.bewarethedark.registries.FoodInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Items;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class BTDFoodEffectsHandler {

    @SubscribeEvent
    public void applyeffectsandcooldown(LivingEntityUseItemEvent.Finish event)
    {
        if (!(event.getEntity() instanceof Player))
        {
            return;
        }
        Player player = (Player) event.getEntity();
        if (event.getEntity() instanceof Player)
        {

            if(BTDConfig.getInstance().dont_starve_style_hunger())
            {
                if (event.getItem().getItem().isEdible())
                {
                    player.getFoodData().setSaturation(0F);
                }
                if(event.getItem().getItem() == FoodInit.FANCY_SPIRAL_TUBERS.get()||
                        event.getItem().getItem() == FoodInit.FIST_FULL_OF_JAM.get()||
                        event.getItem().getItem() == FoodInit.PUMPKIN_COOKIE.get()||
                        event.getItem().getItem() == FoodInit.BUTTER_MUFFIN.get()||
                        event.getItem().getItem() == FoodInit.WAFFLES.get()||
                        event.getItem().getItem() == FoodInit.CALIFORNIA_ROLL.get()||
                        event.getItem().getItem() == Items.BAKED_POTATO||
                        event.getItem().getItem() == Items.BREAD||
                        event.getItem().getItem() == Items.HONEY_BOTTLE||
                        event.getItem().getItem() == Items.SUSPICIOUS_STEW||
                        event.getItem().getItem() == Items.MUSHROOM_STEW||
                        event.getItem().getItem() == Items.BEETROOT_SOUP)


                {
                    if(player.getFoodData().getFoodLevel() >= 19) {
                        player.removeEffect(EffectInit.WELL_FED.get());
                        player.addEffect(new MobEffectInstance(EffectInit.WELL_FED.get(), 1500, 0));
                    }
                }
                if(event.getItem().getItem() == FoodInit.JERKY.get()||
                        event.getItem().getItem() == FoodInit.BUNNY_STEW.get()||
                        event.getItem().getItem() == FoodInit.FISHSTICKS.get()||
                        event.getItem().getItem() == FoodInit.HONEY_NUGGETS.get()||
                        event.getItem().getItem() == FoodInit.KABOBS.get()||
                        event.getItem().getItem() == FoodInit.PIEROGI.get()||
                        event.getItem().getItem() == FoodInit.MONSTER_LASAGNA.get()||
                        event.getItem().getItem() == Items.COOKED_CHICKEN||
                        event.getItem().getItem() == Items.COOKED_COD||
                        event.getItem().getItem() == Items.COOKED_MUTTON||
                        event.getItem().getItem() == Items.COOKED_RABBIT||
                        event.getItem().getItem() == Items.COOKED_SALMON||
                        event.getItem().getItem() == Items.PUMPKIN_PIE) {
                    if(player.getFoodData().getFoodLevel() >= 19) {
                        player.removeEffect(EffectInit.WELL_FED.get());
                        player.addEffect(new MobEffectInstance(EffectInit.WELL_FED.get(), 3000, 0));
                    }
                }
                if(event.getItem().getItem() == FoodInit.BACON_AND_EGGS.get()||
                        event.getItem().getItem() == FoodInit.HONEY_HAM.get()||
                        event.getItem().getItem() == Items.COOKED_BEEF||
                        event.getItem().getItem() == Items.COOKED_PORKCHOP||
                        event.getItem().getItem() == Items.RABBIT_STEW)
                {
                    if(player.getFoodData().getFoodLevel() >= 10) {
                        player.removeEffect(EffectInit.WELL_FED.get());
                        player.addEffect(new MobEffectInstance(EffectInit.WELL_FED.get(), 6000, 0));
                    }
                }
                if(event.getItem().getItem() == FoodInit.MANDRAKE_SOUP.get()||
                        event.getItem().getItem() == FoodInit.MEATY_STEW.get()||
                        event.getItem().getItem() == FoodInit.COOKED_MANDRAKE.get())
                {
                        player.removeEffect(EffectInit.WELL_FED.get());
                        player.addEffect(new MobEffectInstance(EffectInit.WELL_FED.get(), 12000, 0));

                }
            }
            if (event.getItem().getItem() == FoodInit.MONSTER_MEAT.get())
            {
                        int health = (int) (player.getHealth() - 3);
                        if (player.level.isClientSide)
                        {
                            return;
                        }
                        if (health > 20)
                        {
                                player.setHealth(20);
                        }
                        {
                        player.setHealth(health);
                        }
                    }
            }
            // // //
        if (event.getItem().getItem() == FoodInit.COOKED_MONSTER_MEAT.get()||
                event.getItem().getItem() == FoodInit.MONSTER_JERKY.get())
        {
            int health = (int) (player.getHealth() - 1);
            if (player.level.isClientSide)
            {
                return;
            }
            if (health > 20)
            {
                player.setHealth(20);
            }
            {
                player.setHealth(health);
            }
        }
        ///
        if (event.getItem().getItem() == FoodInit.RED_CAP.get()||
                event.getItem().getItem() == FoodInit.MONSTER_LASAGNA.get())
        {
            int health = (int) (player.getHealth() - 2.6);
            if (player.level.isClientSide)
            {
                return;
            }
            if (health > 20)
            {
                player.setHealth(20);
            }
            {
                player.setHealth(health);
            }
        }
    // // //
        if (event.getItem().getItem() == FoodInit.BLUE_CAP.get())
        {
            int health = (int) (player.getHealth() + 2.6);
            if (player.level.isClientSide)
            {
                return;
            }
            if (health > 20)
            {
                player.setHealth(20);
            }
            {
                player.setHealth(health);
            }
            player.getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
                sanity.subSanity(2);
            });
        }
        ///
        if (event.getItem().getItem() == FoodInit.COOKED_RED_CAP.get()||
                event.getItem().getItem() == FoodInit.COOKED_BATILISK_WING.get())
        {
            int health = (int) (player.getHealth() + 1);
            if (player.level.isClientSide)
            {
                return;
            }
            if (health > 20)
            {
                player.setHealth(20);
            }
            {
                player.setHealth(health);
            }
            player.getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
                sanity.subSanity(1);
            });
        }
        ///
        if (event.getItem().getItem() == FoodInit.COOKED_GREEN_CAP.get())
        {
            int health = (int) (player.getHealth() - 1);
            if (player.level.isClientSide)
            {
                return;
            }
            if (health > 20)
            {
                player.setHealth(20);
            }
            {
                player.setHealth(health);
            }
            player.getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
                sanity.addSanity(2);
            });
        }
        ///
        if (event.getItem().getItem() == FoodInit.COOKED_BLUE_CAP.get())
        {
            int health = (int) (player.getHealth() - 1);
            if (player.level.isClientSide)
            {
                return;
            }
            if (health > 20)
            {
                player.setHealth(20);
            }
            {
                player.setHealth(health);
            }
            player.getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
                sanity.addSanity(1);
            });
        }
        ///
        if (event.getItem().getItem() == FoodInit.FISH.get()||
                event.getItem().getItem() == FoodInit.COOKED_FISH.get()||
                event.getItem().getItem() == FoodInit.ROASTED_BERRIES.get()||
                event.getItem().getItem() == FoodInit.ROASTED_CARROT.get()||
                event.getItem().getItem() == FoodInit.ROASTED_BIRCHNUT.get()||
                event.getItem().getItem() == FoodInit.MEAT.get()||
                event.getItem().getItem() == FoodInit.COOKED_MEAT.get()||
                event.getItem().getItem() == FoodInit.COOKED_MORSEL.get())
        {
            int health = (int) (player.getHealth() + 2);
            if (player.level.isClientSide)
            {
                return;
            }
            if (health > 20)
            {
                player.setHealth(20);
            }
            {
                player.setHealth(health);
            }
        }
        ///
        if (event.getItem().getItem() == FoodInit.BUTTERFLY_WINGS.get()||
                event.getItem().getItem() == FoodInit.JUNGLE_BUTTERFLY_WINGS.get())
        {
            int health = (int) (player.getHealth() + 2);
            if (player.level.isClientSide)
            {
                return;
            }
            if (health > 20)
            {
                player.setHealth(20);
            }
            {
                player.setHealth(health);
            }
        }
        //
        if (event.getItem().getItem() == FoodInit.MANDRAKE.get())
        {
            player.removeEffect(EffectInit.WELL_FED.get());
            player.addEffect(new MobEffectInstance(EffectInit.WELL_FED.get(), 6000, 0));
            int health = (int) (player.getHealth() + 8);
            if (player.level.isClientSide)
            {
                return;
            }
            if (health > 20)
            {
                player.setHealth(20);
            }
            {
                player.setHealth(health);
            }
            if (!event.getEntity().level.isClientSide)
            {
                event.getEntity().startSleeping(event.getEntity().blockPosition().above());
                if(event.getEntity().level instanceof ServerLevel)
                {
                    ((ServerLevel) event.getEntity().level).setDayTime(23999);
                }
            }
        }
        //
        if (event.getItem().getItem() == FoodInit.COOKED_MANDRAKE.get())
        {
            int health = (int) (player.getHealth() + 13.3);
            if (player.level.isClientSide)
            {
                return;
            }
            if (health > 20)
            {
                player.setHealth(20);
            }
            {
                player.setHealth(health);
            }
            if (!event.getEntity().level.isClientSide)
            {
                event.getEntity().startSleeping(event.getEntity().blockPosition().above());
                if(event.getEntity().level instanceof ServerLevel)
                {
                    ((ServerLevel) event.getEntity().level).setDayTime(23999);
                }
            }
        }
        //
        if (event.getItem().getItem() == FoodInit.BUTTER.get())
        {
            int health = (int) (player.getHealth() + 5);
            if (player.level.isClientSide)
            {
                return;
            }
            if (health > 20)
            {
                player.setHealth(20);
            }
            {
                player.setHealth(health);
            }
        }
        ///
        if (player.getMainHandItem().getItem() == FoodInit.PETALS.get()||
                player.getMainHandItem().getItem() == FoodInit.FOILAGE.get()||
                event.getItem().getItem() == FoodInit.BATILISK_WING.get()||
                event.getItem().getItem() == FoodInit.LIGHT_BULB.get())
        {
            int health = (int) (player.getHealth() + 1);
            if (player.level.isClientSide)
            {
                return;
            }
            if (health > 20)
            {
                player.setHealth(20);
            }
            {
                player.setHealth(health);
            }
        }
        ///
        if (event.getItem().getItem() == FoodInit.ICE_CREAM.get()) {
            player.getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
                sanity.addSanity(6);
            });
        }
        if (event.getItem().getItem() == FoodInit.MELONSICLE.get()||
                event.getItem().getItem() == FoodInit.TAFFY.get()||
                event.getItem().getItem() == FoodInit.PUMPKIN_COOKIE.get()||
                event.getItem().getItem() == FoodInit.JERKY.get()||
                event.getItem().getItem() == FoodInit.FANCY_SPIRAL_TUBERS.get()) {
            player.getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
                sanity.addSanity(3);
            });
        }
        if (event.getItem().getItem() == FoodInit.SMALL_JERKY.get()||
                event.getItem().getItem() == FoodInit.CALIFORNIA_ROLL.get()||
                event.getItem().getItem() == FoodInit.DRIED_SEAWEED.get()||
                event.getItem().getItem() == Items.DRIED_KELP) {
            player.getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
                sanity.addSanity(2);
            });
        }
        if (event.getItem().getItem() == FoodInit.WAFFLES.get()||
                event.getItem().getItem() == FoodInit.TRAIL_MIX.get()||
                event.getItem().getItem() == FoodInit.RATATOUILLE.get()||
                event.getItem().getItem() == FoodInit.PIEROGI.get()||
                event.getItem().getItem() == FoodInit.MEATBALLS.get()||
                event.getItem().getItem() == FoodInit.MEATY_STEW.get()||
                event.getItem().getItem() == FoodInit.MANDRAKE_SOUP.get()||
                event.getItem().getItem() == FoodInit.KABOBS.get()||
                event.getItem().getItem() == FoodInit.HONEY_NUGGETS.get()||
                event.getItem().getItem() == FoodInit.HONEY_HAM.get()||
                event.getItem().getItem() == FoodInit.FRUIT_MEDLEY.get()||
                event.getItem().getItem() == FoodInit.FIST_FULL_OF_JAM.get()||
                event.getItem().getItem() == FoodInit.FISHSTICKS.get()||
                event.getItem().getItem() == FoodInit.CEVICHE.get()||
                event.getItem().getItem() == FoodInit.BUTTER_MUFFIN.get()||
                event.getItem().getItem() == FoodInit.BUNNY_STEW.get()||
                event.getItem().getItem() == FoodInit.BACON_AND_EGGS.get()||
                event.getItem().getItem() == Items.MELON_SLICE) {
            player.getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
                sanity.addSanity(1);
            });
        }
        if (event.getItem().getItem() == FoodInit.MEAT.get()||
                event.getItem().getItem() == FoodInit.MORSEL.get()||
                event.getItem().getItem() == FoodInit.COOKED_MONSTER_MEAT.get()||
                event.getItem().getItem() == FoodInit.MONSTER_JERKY.get()||
                event.getItem().getItem() == Items.KELP||
                event.getItem().getItem() == Items.POTATO||
        event.getItem().getItem() == FoodInit.BATILISK_WING.get()) {
            player.getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
                sanity.subSanity(1);
            });
        }
        if (event.getItem().getItem() == FoodInit.MONSTER_MEAT.get()||
                event.getItem().getItem() == FoodInit.MONSTER_LASAGNA.get()) {
            player.getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
                sanity.subSanity(2);
            });
        }
        //
        if (event.getItem().getItem() == FoodInit.ANTI_VENOM.get()) {
            if(player.hasEffect(MobEffects.POISON))
            {
                player.removeEffect(MobEffects.POISON);
                player.getCapability(CustomPlayerDataProvider.CUSTOM_PLAYER_DATA).ifPresent(cpd -> {
                    cpd.setPoisonImmunityTick(24000);
                });
            }
        }
        //
        if (event.getItem().getItem() == FoodInit.VENOM_GLAND.get()) {
            if(BTDConfig.getInstance().dont_starve_style_poison())
            {
                player.addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, 130, 0));
                player.addEffect(new MobEffectInstance(MobEffects.WEAKNESS, 130, 0));
            }
        }
        }
        //

    }



