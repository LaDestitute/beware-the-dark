package com.ladestitute.bewarethedark.events;

import com.ladestitute.bewarethedark.btdcapabilities.sanity.PlayerSanityProvider;
import com.ladestitute.bewarethedark.registries.BlockInit;
import com.ladestitute.bewarethedark.registries.SoundInit;
import net.minecraft.core.BlockPos;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.Difficulty;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.level.LightLayer;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraft.world.level.lighting.LayerLightEventListener;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class BTDCharlieHandler {

    /**
     * Subscribe event for Charlie attacks, working mostly fine now
     * Further improvements will include including Blind to simulate darkness when attacks start and setting a grace-period before the first attack
     * It's a little janky though, either needs fine tuning or a delay before attackings
     * Charlie also doesn't attack immediently when you die or from torches being put away
     */
    private int charlietimer;
    @SubscribeEvent(priority = EventPriority.NORMAL, receiveCanceled = true)
    public void charlieattack(TickEvent.PlayerTickEvent event) {
        if (event.phase == TickEvent.Phase.START && !event.player.level.isClientSide) {
            if (!event.player.isCreative() && event.player.level.getMoonPhase() != 0.0F) {
                LevelChunk chunk = event.player.level.getChunk(event.player.chunkPosition().x, event.player.chunkPosition().z);
                BlockPos pos = event.player.blockPosition();
                BlockState oneblocksupx = event.player.level.getBlockState(pos.above());
                BlockState twoblocksupx = event.player.level.getBlockState(pos.above().above());
                BlockState threeblocksupx = event.player.level.getBlockState(pos.above().above().above());
                BlockState fourblocksupx = event.player.level.getBlockState(pos.above().above().above().above());
                BlockState fiveblocksupx = event.player.level.getBlockState(pos.above().above().above().above().above());
                Block oneblocksup = oneblocksupx.getBlock();
                Block twoblocksup = twoblocksupx.getBlock();
                Block threeblocksup = threeblocksupx.getBlock();
                Block fourblocksup = fourblocksupx.getBlock();
                Block fiveblocksup = fiveblocksupx.getBlock();
                if(!event.player.level.isNight()) {
                    if (oneblocksup != Blocks.ACACIA_LEAVES ||
                            oneblocksup != Blocks.BIRCH_LEAVES ||
                            oneblocksup != Blocks.DARK_OAK_LEAVES ||
                            oneblocksup != Blocks.JUNGLE_LEAVES ||
                            oneblocksup != Blocks.OAK_LEAVES ||
                            oneblocksup != Blocks.SPRUCE_LEAVES ||
                            oneblocksup != BlockInit.EVERGREEN_LEAVES.get() ||
                            oneblocksup != BlockInit.LUMPY_EVERGREEN_LEAVES.get() ||
                            oneblocksup != BlockInit.GREEN_DECIDUOUS_LEAVES.get() ||
                            oneblocksup != BlockInit.ORANGE_DECIDUOUS_LEAVES.get() ||
                            oneblocksup != BlockInit.RED_DECIDUOUS_LEAVES.get() ||
                            oneblocksup != BlockInit.YELLOW_DECIDUOUS_LEAVES.get() ||
                            twoblocksup != Blocks.ACACIA_LEAVES ||
                            twoblocksup != Blocks.BIRCH_LEAVES ||
                            twoblocksup != Blocks.DARK_OAK_LEAVES ||
                            twoblocksup != Blocks.JUNGLE_LEAVES ||
                            twoblocksup != Blocks.OAK_LEAVES ||
                            twoblocksup != Blocks.SPRUCE_LEAVES ||
                            twoblocksup != BlockInit.EVERGREEN_LEAVES.get() ||
                            twoblocksup != BlockInit.LUMPY_EVERGREEN_LEAVES.get() ||
                            twoblocksup != BlockInit.GREEN_DECIDUOUS_LEAVES.get() ||
                            twoblocksup != BlockInit.ORANGE_DECIDUOUS_LEAVES.get() ||
                            twoblocksup != BlockInit.RED_DECIDUOUS_LEAVES.get() ||
                            twoblocksup != BlockInit.YELLOW_DECIDUOUS_LEAVES.get() ||
                            threeblocksup != Blocks.ACACIA_LEAVES ||
                            threeblocksup != Blocks.BIRCH_LEAVES ||
                            threeblocksup != Blocks.DARK_OAK_LEAVES ||
                            threeblocksup != Blocks.JUNGLE_LEAVES ||
                            threeblocksup != Blocks.OAK_LEAVES ||
                            threeblocksup != Blocks.SPRUCE_LEAVES ||
                            threeblocksup != BlockInit.EVERGREEN_LEAVES.get() ||
                            threeblocksup != BlockInit.LUMPY_EVERGREEN_LEAVES.get() ||
                            threeblocksup != BlockInit.GREEN_DECIDUOUS_LEAVES.get() ||
                            threeblocksup != BlockInit.ORANGE_DECIDUOUS_LEAVES.get() ||
                            threeblocksup != BlockInit.RED_DECIDUOUS_LEAVES.get() ||
                            threeblocksup != BlockInit.YELLOW_DECIDUOUS_LEAVES.get() ||
                            fourblocksup != Blocks.ACACIA_LEAVES ||
                            fourblocksup != Blocks.BIRCH_LEAVES ||
                            fourblocksup != Blocks.DARK_OAK_LEAVES ||
                            fourblocksup != Blocks.JUNGLE_LEAVES ||
                            fourblocksup != Blocks.OAK_LEAVES ||
                            fourblocksup != Blocks.SPRUCE_LEAVES ||
                            fourblocksup != BlockInit.EVERGREEN_LEAVES.get() ||
                            fourblocksup != BlockInit.LUMPY_EVERGREEN_LEAVES.get() ||
                            fourblocksup != BlockInit.GREEN_DECIDUOUS_LEAVES.get() ||
                            fourblocksup != BlockInit.ORANGE_DECIDUOUS_LEAVES.get() ||
                            fourblocksup != BlockInit.RED_DECIDUOUS_LEAVES.get() ||
                            fourblocksup != BlockInit.YELLOW_DECIDUOUS_LEAVES.get() ||
                            fiveblocksup != Blocks.ACACIA_LEAVES ||
                            fiveblocksup != Blocks.BIRCH_LEAVES ||
                            fiveblocksup != Blocks.DARK_OAK_LEAVES ||
                            fiveblocksup != Blocks.JUNGLE_LEAVES ||
                            fiveblocksup != Blocks.OAK_LEAVES ||
                            fiveblocksup != Blocks.SPRUCE_LEAVES ||
                            fiveblocksup != BlockInit.EVERGREEN_LEAVES.get() ||
                            fiveblocksup != BlockInit.LUMPY_EVERGREEN_LEAVES.get() ||
                            fiveblocksup != BlockInit.GREEN_DECIDUOUS_LEAVES.get() ||
                            fiveblocksup != BlockInit.ORANGE_DECIDUOUS_LEAVES.get() ||
                            fiveblocksup != BlockInit.RED_DECIDUOUS_LEAVES.get() ||
                            fiveblocksup != BlockInit.YELLOW_DECIDUOUS_LEAVES.get()) {
                        charlietimer = 0;
                    }
                }

                BlockPos playerPos = event.player.blockPosition();
                LayerLightEventListener blockLightingLayer = event.player.level.getLightEngine().getLayerListener(LightLayer.BLOCK);
                LayerLightEventListener skyLightingLayer = event.player.level.getLightEngine().getLayerListener(LightLayer.SKY);
                if(event.player.level.isNight() && event.player.level.canSeeSky(event.player.blockPosition()))
                {
                    if(event.player.level.getDifficulty() == Difficulty.PEACEFUL)
                    {
                        charlietimer = 0;
                    }
                    if (blockLightingLayer.getLightValue(playerPos) >= 1)
                    {
                        charlietimer = 0;
                    }
                    if (this.charlietimer == 100)
                    {
                        event.player.playNotifySound(SoundInit.CHARLIE_WARN.get(), SoundSource.AMBIENT, 2F, 1.0F);
                    }
                    if (blockLightingLayer.getLightValue(playerPos) == 0)
                    {
                        charlietimer++;

                        if (this.charlietimer == 200)
                        {
                            event.player.playNotifySound(SoundInit.CHARLIE_BITE.get(), SoundSource.AMBIENT, 2F, 1.0F);
                            event.player.hurt(DamageSource.GENERIC, 14.5F);
                            event.player.getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
                                sanity.subSanity(2);
                            });
                            this.charlietimer = 0;
                        }
                    }
                }
                if(!event.player.level.isNight() && event.player.level.canSeeSky(event.player.blockPosition()))
                {
                    if(event.player.level.getDifficulty() == Difficulty.PEACEFUL)
                    {
                        charlietimer = 0;
                    }
                    if (skyLightingLayer.getLightValue(playerPos) >= 1)
                    {
                        charlietimer = 0;
                    }
                    if (this.charlietimer == 100)
                    {
                        event.player.playNotifySound(SoundInit.CHARLIE_WARN.get(), SoundSource.AMBIENT, 2F, 1.0F);
                    }
                    if (skyLightingLayer.getLightValue(playerPos) == 0)
                    {
                        charlietimer++;

                        if (this.charlietimer == 200)
                        {
                            event.player.playNotifySound(SoundInit.CHARLIE_BITE.get(), SoundSource.AMBIENT, 2F, 1.0F);
                            event.player.hurt(DamageSource.GENERIC, 14.5F);
                            this.charlietimer = 0;
                        }
                    }
                }
            }
        }
    }





    @SubscribeEvent(priority = EventPriority.NORMAL, receiveCanceled = true)
    public void charlieattack2(TickEvent.PlayerTickEvent event) {

        if (event.phase == TickEvent.Phase.START && !event.player.level.isClientSide) {
            if (!event.player.isCreative() && event.player.level.getMoonPhase() != 0.0F) {
                BlockPos playerPos = event.player.blockPosition();
                LayerLightEventListener blockLightingLayer = event.player.level.getLightEngine().getLayerListener(LightLayer.BLOCK);
                if (!event.player.level.canSeeSky(event.player.blockPosition())) {
                    BlockPos pos = event.player.blockPosition();
                    BlockState oneblocksupx = event.player.level.getBlockState(pos.above());
                    BlockState twoblocksupx = event.player.level.getBlockState(pos.above().above());
                    BlockState threeblocksupx = event.player.level.getBlockState(pos.above().above().above());
                    BlockState fourblocksupx = event.player.level.getBlockState(pos.above().above().above().above());
                    BlockState fiveblocksupx = event.player.level.getBlockState(pos.above().above().above().above().above());
                    Block oneblocksup = oneblocksupx.getBlock();
                    Block twoblocksup = twoblocksupx.getBlock();
                    Block threeblocksup = threeblocksupx.getBlock();
                    Block fourblocksup = fourblocksupx.getBlock();
                    Block fiveblocksup = fiveblocksupx.getBlock();
                    if(!event.player.level.isNight()) {
                        if (oneblocksup != Blocks.ACACIA_LEAVES ||
                                oneblocksup != Blocks.BIRCH_LEAVES ||
                                oneblocksup != Blocks.DARK_OAK_LEAVES ||
                                oneblocksup != Blocks.JUNGLE_LEAVES ||
                                oneblocksup != Blocks.OAK_LEAVES ||
                                oneblocksup != Blocks.SPRUCE_LEAVES ||
                                oneblocksup != BlockInit.EVERGREEN_LEAVES.get() ||
                                oneblocksup != BlockInit.LUMPY_EVERGREEN_LEAVES.get() ||
                                oneblocksup != BlockInit.GREEN_DECIDUOUS_LEAVES.get() ||
                                oneblocksup != BlockInit.ORANGE_DECIDUOUS_LEAVES.get() ||
                                oneblocksup != BlockInit.RED_DECIDUOUS_LEAVES.get() ||
                                oneblocksup != BlockInit.YELLOW_DECIDUOUS_LEAVES.get() ||
                                twoblocksup != Blocks.ACACIA_LEAVES ||
                                twoblocksup != Blocks.BIRCH_LEAVES ||
                                twoblocksup != Blocks.DARK_OAK_LEAVES ||
                                twoblocksup != Blocks.JUNGLE_LEAVES ||
                                twoblocksup != Blocks.OAK_LEAVES ||
                                twoblocksup != Blocks.SPRUCE_LEAVES ||
                                twoblocksup != BlockInit.EVERGREEN_LEAVES.get() ||
                                twoblocksup != BlockInit.LUMPY_EVERGREEN_LEAVES.get() ||
                                twoblocksup != BlockInit.GREEN_DECIDUOUS_LEAVES.get() ||
                                twoblocksup != BlockInit.ORANGE_DECIDUOUS_LEAVES.get() ||
                                twoblocksup != BlockInit.RED_DECIDUOUS_LEAVES.get() ||
                                twoblocksup != BlockInit.YELLOW_DECIDUOUS_LEAVES.get() ||
                                threeblocksup != Blocks.ACACIA_LEAVES ||
                                threeblocksup != Blocks.BIRCH_LEAVES ||
                                threeblocksup != Blocks.DARK_OAK_LEAVES ||
                                threeblocksup != Blocks.JUNGLE_LEAVES ||
                                threeblocksup != Blocks.OAK_LEAVES ||
                                threeblocksup != Blocks.SPRUCE_LEAVES ||
                                threeblocksup != BlockInit.EVERGREEN_LEAVES.get() ||
                                threeblocksup != BlockInit.LUMPY_EVERGREEN_LEAVES.get() ||
                                threeblocksup != BlockInit.GREEN_DECIDUOUS_LEAVES.get() ||
                                threeblocksup != BlockInit.ORANGE_DECIDUOUS_LEAVES.get() ||
                                threeblocksup != BlockInit.RED_DECIDUOUS_LEAVES.get() ||
                                threeblocksup != BlockInit.YELLOW_DECIDUOUS_LEAVES.get() ||
                                fourblocksup != Blocks.ACACIA_LEAVES ||
                                fourblocksup != Blocks.BIRCH_LEAVES ||
                                fourblocksup != Blocks.DARK_OAK_LEAVES ||
                                fourblocksup != Blocks.JUNGLE_LEAVES ||
                                fourblocksup != Blocks.OAK_LEAVES ||
                                fourblocksup != Blocks.SPRUCE_LEAVES ||
                                fourblocksup != BlockInit.EVERGREEN_LEAVES.get() ||
                                fourblocksup != BlockInit.LUMPY_EVERGREEN_LEAVES.get() ||
                                fourblocksup != BlockInit.GREEN_DECIDUOUS_LEAVES.get() ||
                                fourblocksup != BlockInit.ORANGE_DECIDUOUS_LEAVES.get() ||
                                fourblocksup != BlockInit.RED_DECIDUOUS_LEAVES.get() ||
                                fourblocksup != BlockInit.YELLOW_DECIDUOUS_LEAVES.get() ||
                                fiveblocksup != Blocks.ACACIA_LEAVES ||
                                fiveblocksup != Blocks.BIRCH_LEAVES ||
                                fiveblocksup != Blocks.DARK_OAK_LEAVES ||
                                fiveblocksup != Blocks.JUNGLE_LEAVES ||
                                fiveblocksup != Blocks.OAK_LEAVES ||
                                fiveblocksup != Blocks.SPRUCE_LEAVES ||
                                fiveblocksup != BlockInit.EVERGREEN_LEAVES.get() ||
                                fiveblocksup != BlockInit.LUMPY_EVERGREEN_LEAVES.get() ||
                                fiveblocksup != BlockInit.GREEN_DECIDUOUS_LEAVES.get() ||
                                fiveblocksup != BlockInit.ORANGE_DECIDUOUS_LEAVES.get() ||
                                fiveblocksup != BlockInit.RED_DECIDUOUS_LEAVES.get() ||
                                fiveblocksup != BlockInit.YELLOW_DECIDUOUS_LEAVES.get()) {
                            charlietimer = 0;
                        }
                    }
                    if (event.player.level.getDifficulty() == Difficulty.PEACEFUL) {
                        charlietimer = 0;
                    }
                    if (blockLightingLayer.getLightValue(playerPos) >= 1) {
                        charlietimer = 0;
                    }
                    if (this.charlietimer == 100) {
                        event.player.playNotifySound(SoundInit.CHARLIE_BITE.get(), SoundSource.AMBIENT, 2F, 1.0F);
                    }
                    if (blockLightingLayer.getLightValue(playerPos) == 0) {
                        charlietimer++;

                        if (this.charlietimer == 200) {
                            event.player.playNotifySound(SoundInit.CHARLIE_BITE.get(), SoundSource.AMBIENT, 2F, 1.0F);
                            event.player.hurt(DamageSource.GENERIC, 14.5F);
                            this.charlietimer = 0;
                        }
                    }
                }
            }
        }
    }


}
