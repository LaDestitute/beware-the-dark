package com.ladestitute.bewarethedark.events;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.btdcapabilities.customplayerdata.CustomPlayerDataProvider;
import com.ladestitute.bewarethedark.btdcapabilities.sanity.PlayerSanityProvider;
import com.ladestitute.bewarethedark.network.ClientboundPlayerSanityUpdateMessage;
import com.ladestitute.bewarethedark.network.NetworkingHandler;
import com.ladestitute.bewarethedark.registries.ItemInit;
import com.ladestitute.bewarethedark.registries.SoundInit;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.level.GameRules;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class BTDMainHandler {

    @SubscribeEvent
    public void setdefaulthandheldlanternfuel(PlayerEvent.ItemCraftedEvent event)
    {
        if(event.getCrafting().getItem() == ItemInit.WOOD_LANTERN.get())
        {
            event.getEntity().getCapability(CustomPlayerDataProvider.CUSTOM_PLAYER_DATA).ifPresent(cpd -> {
                cpd.setLanternFuel(9120);
            });

        }
    }

    @SubscribeEvent
    public void jingles(TickEvent.PlayerTickEvent event)
    {
        event.player.getCapability(CustomPlayerDataProvider.CUSTOM_PLAYER_DATA).ifPresent(cpd -> {
            if(cpd.getPoisonImmunityTick() >= 1)
            {
                cpd.subPoisonImmunityTick(1);
            }
            if(cpd.getPoisonImmunityTick() >= 1 && event.player.hasEffect(MobEffects.POISON))
            {
                event.player.removeEffect(MobEffects.POISON);
            }
//            if(event.player.hasEffect(MobEffects.POISON) && BTDConfig.getInstance().dont_starve_style_poison())
//            {
//                event.player.addEffect(new MobEffectInstance(MobEffects.MOVEMENT_SLOWDOWN, event.player.getEffect(MobEffects.POISON).getDuration(), 0));
//                event.player.addEffect(new MobEffectInstance(MobEffects.WEAKNESS, event.player.getEffect(MobEffects.POISON).getDuration(), 0));
//            }
        });
        if(!event.player.level.dimension().equals(BTDMain.CAVES_DIMENSION)) {
            if (event.player.level.getDayTime() == 1) {
                event.player.playNotifySound(SoundInit.DAWN_JINGLE.get(), SoundSource.AMBIENT, 0.25F, 1.0F);
            }
            if (event.player.level.getDayTime() == 11415) {
                event.player.playNotifySound(SoundInit.DUSK_JINGLE.get(), SoundSource.AMBIENT, 0.25F, 1.0F);
            }
        }
    }

    @SubscribeEvent
    public  void onPlayerLoggin(PlayerEvent.PlayerLoggedInEvent event) {

        /**
         * ResourceLocation "starter" must match the total amount of recipes to be unlocked (count from zero!), lest we throw an array out of index error
         */
        ResourceLocation[] recipe = new ResourceLocation[17];
        recipe[0] = new ResourceLocation("minecraft", "torch");
        recipe[1] = new ResourceLocation(BTDMain.MOD_ID, "light/campfire");
        recipe[2] = new ResourceLocation(BTDMain.MOD_ID, "science/science_machine");
        recipe[3] = new ResourceLocation(BTDMain.MOD_ID, "tools/portableaxe");
        recipe[4] = new ResourceLocation(BTDMain.MOD_ID, "tools/portablepickaxe");
        recipe[5] = new ResourceLocation(BTDMain.MOD_ID, "tools/hammer");
        recipe[6] = new ResourceLocation("minecraft", "crafting_table");
        recipe[7] = new ResourceLocation(BTDMain.MOD_ID, "tools/flint_axe");
        recipe[8] = new ResourceLocation(BTDMain.MOD_ID, "tools/flint_pickaxe");
        recipe[9] = new ResourceLocation(BTDMain.MOD_ID, "light/fire_pit");
        recipe[10] = new ResourceLocation(BTDMain.MOD_ID, "survival/garland");
        recipe[11] = new ResourceLocation(BTDMain.MOD_ID, "survival/kindling_set");
        recipe[12] = new ResourceLocation(BTDMain.MOD_ID, "structures/cobblestone_to_rocks");
        recipe[13] = new ResourceLocation(BTDMain.MOD_ID, "structures/spruce_log_to_planks");
        recipe[14] = new ResourceLocation(BTDMain.MOD_ID, "structures/rocks_furnace");
        recipe[15] = new ResourceLocation(BTDMain.MOD_ID, "structures/spruce_log_smoker");
        recipe[16] = new ResourceLocation(BTDMain.MOD_ID, "structures/spruce_log_charcoal");
        event.getEntity().awardRecipesByKey(recipe);
        if(BTDConfig.getInstance().enablerecipeprototyping()) {
            event.getEntity().level.getGameRules().getRule(GameRules.RULE_LIMITED_CRAFTING).set(true, event.getEntity().level.getServer());
        }
        if(!BTDConfig.getInstance().enablerecipeprototyping()) {
            event.getEntity().level.getGameRules().getRule(GameRules.RULE_LIMITED_CRAFTING).set(false, event.getEntity().level.getServer());
        }
        event.getEntity().getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
            if(sanity.getIsFirstSpawn() == 0) {
                sanity.setSanity(20);
                sanity.setHasSpawnedOnce(1);
            }

        });



    }

}
