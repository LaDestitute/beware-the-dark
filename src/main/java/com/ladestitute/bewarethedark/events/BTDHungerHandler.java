package com.ladestitute.bewarethedark.events;

import com.ladestitute.bewarethedark.btdcapabilities.customplayerdata.CustomPlayerDataProvider;
import com.ladestitute.bewarethedark.registries.EffectInit;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.stats.Stats;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.LogicalSide;

public class BTDHungerHandler {
    @SubscribeEvent
    public void hungermanager(TickEvent.PlayerTickEvent event)
    {
        if (event.side == LogicalSide.SERVER)
        {
            if (BTDConfig.getInstance().dont_starve_style_hunger() && ((ServerPlayer) event.player).gameMode.isSurvival() &&
            !event.player.hasEffect(EffectInit.WELL_FED.get()))
            {
                event.player.getFoodData().setSaturation(0);
                event.player.getFoodData().setExhaustion(0);
                event.player.getCapability(CustomPlayerDataProvider.CUSTOM_PLAYER_DATA).ifPresent(h ->
                {
                    h.addHungerTick(1);
                    if (h.getHungerTick() >= BTDConfig.getInstance().hunger_tick()) {
                        event.player.getFoodData().setFoodLevel(event.player.getFoodData().getFoodLevel() - 1);
                        h.setHungerTick(0);
                    }
                });
            }
            if(BTDConfig.getInstance().dont_starve_style_hunger() && ((ServerPlayer) event.player).gameMode.isSurvival() &&
                    event.player.hasEffect(EffectInit.WELL_FED.get()) && event.player.isSprinting()) {
                event.player.getFoodData().setExhaustion(0);
            }
        }
    }
}
