package com.ladestitute.bewarethedark.events;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.registries.SpecialBlockInit;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class BTDUnlockRecipeHandler {

    @SubscribeEvent
    public void sciencemachineradius(TickEvent.PlayerTickEvent event) {
        BlockPos blockPos = event.player.blockPosition();
        int searchRadius = 1;
        int radius = (int) (Math.floor(1) + searchRadius);

        for (int x = -radius; x <= radius; ++x) {
            for (int y = -1; y <= 1; ++y) {
                for (int z = -radius; z <= radius; ++z) {
                    BlockPos shrinePos = new BlockPos(blockPos.getX() + x, blockPos.getY() + y, blockPos.getZ() + z);
                    BlockState blockState = event.player.level.getBlockState(shrinePos);
                    Block block = blockState.getBlock();
                    if (block == SpecialBlockInit.SCIENCE_MACHINE.get())
                    {
                        ResourceLocation[] recipe = new ResourceLocation[33];
                        recipe[0] = new ResourceLocation(BTDMain.MOD_ID, "tools/flint_shovel");
                        recipe[1] = new ResourceLocation(BTDMain.MOD_ID, "tools/pitchfork");
                        recipe[2] = new ResourceLocation(BTDMain.MOD_ID, "tools/compass");
                        recipe[3] = new ResourceLocation(BTDMain.MOD_ID, "survival/healing_salve");
                        recipe[4] = new ResourceLocation(BTDMain.MOD_ID, "food/basic_farm");
                        recipe[5] = new ResourceLocation(BTDMain.MOD_ID, "fight/spear");
                        recipe[6] = new ResourceLocation(BTDMain.MOD_ID, "fight/log_suit");
                        recipe[7] = new ResourceLocation("minecraft", "chest");
                        recipe[8] = new ResourceLocation(BTDMain.MOD_ID, "structures/sign");
                        recipe[9] = new ResourceLocation(BTDMain.MOD_ID, "refine/boards");
                        recipe[10] = new ResourceLocation(BTDMain.MOD_ID, "refine/rope");
                        recipe[11] = new ResourceLocation(BTDMain.MOD_ID, "refine/cut_stone");
                        recipe[12] = new ResourceLocation("minecraft", "jack_o_lantern");
                        recipe[13] = new ResourceLocation(BTDMain.MOD_ID, "structures/cobblestones");
                        recipe[14] = new ResourceLocation(BTDMain.MOD_ID, "structures/wooden_flooring");
                        recipe[15] = new ResourceLocation(BTDMain.MOD_ID, "tools/opulent_pickaxe");
                        recipe[16] = new ResourceLocation(BTDMain.MOD_ID, "tools/luxury_axe");
                        recipe[17] = new ResourceLocation(BTDMain.MOD_ID, "tools/regal_shovel");
                        recipe[18] = new ResourceLocation("minecraft", "paper");
                        recipe[19] = new ResourceLocation(BTDMain.MOD_ID, "structures/carpeted_flooring");
                        recipe[20] = new ResourceLocation("minecraft", "spruce_door");
                        recipe[21] = new ResourceLocation("minecraft", "spruce_trapdoor");
                        recipe[22] = new ResourceLocation("minecraft", "ladder");
                        recipe[23] = new ResourceLocation(BTDMain.MOD_ID, "food/drying_rack");
                        recipe[24] = new ResourceLocation(BTDMain.MOD_ID, "food/crock_pot");
                        recipe[25] = new ResourceLocation(BTDMain.MOD_ID, "food/honeyblock_to_honey");
                        recipe[26] = new ResourceLocation(BTDMain.MOD_ID, "survival/bug_net");
                        recipe[27] = new ResourceLocation(BTDMain.MOD_ID, "survival/backpack");
                        recipe[28] = new ResourceLocation(BTDMain.MOD_ID, "survival/straw_roll");
                        recipe[29] = new ResourceLocation(BTDMain.MOD_ID, "fight/dart");
                        recipe[30] = new ResourceLocation(BTDMain.MOD_ID, "fight/fire_dart");
                        recipe[31] = new ResourceLocation(BTDMain.MOD_ID, "fight/electric_dart");
                        recipe[32] = new ResourceLocation(BTDMain.MOD_ID, "structures/guano_turf");
                        event.player.awardRecipesByKey(recipe);
                    }
                }
            }
        }
    }

//    @SubscribeEvent
//    public void crafted(PlayerEvent.ItemCraftedEvent event)
//    {
//        if(!event.getEntity().level.isClientSide)
//            event.getEntity().getCapability(BTDCustomPlayerdataCapabilityProvider.BTDPLAYERDATA).ifPresent(h ->
//            {
//            if (h.getisshovelrecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.FLINT_SHOVEL.get())
//            {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockshovelrecipe(1);
//            }
//            if (h.getispitchforkrecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.PITCHFORK.get())
//            {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockpitchforkrecipe(1);
//            }
//            if (h.getiscompassrecipeunlocked() == 0 && event.getCrafting().getItem() == Items.COMPASS)
//            {
//                event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                h.unlockcompassrecipe(1);
//            }
//                if (h.getishealingsalverecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.HEALING_SALVE.get())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockhealingsalverecipe(1);
//                }
//                if (h.getisfishingrodrecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.FISHING_ROD.get())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockfishingrodrecipe(1);
//                }
//                if (h.getisbasicfarmrecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.BASIC_FARM_BLOCK.get())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockbasicfarmrecipe(1);
//                }
//                if (h.getisimprovedfarmrecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.IMPROVED_FARM_BLOCK.get())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockimprovedfarmrecipe(1);
//                }
//                if (h.getisspearrecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.SPEAR.get())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockspearrecipe(1);
//                }
//                if (h.getislogsuitrecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.LOG_SUIT.get())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlocklogsuitrecipe(1);
//                }
//                if (h.getischestrecipeunlocked() == 0 && event.getCrafting().getItem() == Blocks.CHEST.asItem())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockchestrecipe(1);
//                }
//                if (h.getissignrecipeunlocked() == 0 && event.getCrafting().getItem() == Blocks.SPRUCE_SIGN.asItem())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlocksignrecipe(1);
//                }
//
//                if (h.getisboardsrecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.BOARDS.get())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockboardsrecipe(1);
//                }
//                if (h.getisroperecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.ROPE.get())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockroperecipe(1);
//                }
//                if (h.getiscutstonerecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.CUT_STONE.get())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockcutstonerecipe(1);
//                }
//                if (h.getisopulentpickaxerecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.OPULENT_PICKAXE.get())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockopulentpickaxerecipe(1);
//                }
//                if (h.getisluxuryaxerecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.LUXURY_AXE.get())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockluxuryaxerecipe(1);
//                }
//                if (h.getisregalshovelrecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.REGAL_SHOVEL.get())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockregalshovelrecipe(1);
//                }
//                if (h.getissignrecipeunlocked() == 0 && event.getCrafting().getItem() == Blocks.JACK_O_LANTERN.asItem())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockpumpkinlanternrecipe(1);
//                }
//                if (h.getiscobblestonesrecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.COBBLESTONES_BLOCK.get())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockcobblestonesrecipe(1);
//                }
//                if (h.getiswoodenflooringrecipeunlocked() == 0 && event.getCrafting().getItem() == ItemInit.WOODEN_FLOORING_BLOCK.get())
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockwoodenflooringrecipe(1);
//                }
//                if (h.getispaperrecipeunlocked() == 0
//                        && event.getCrafting().getItem() == Items.PAPER)
//                {
//                    event.getEntity().playNotifySound(SoundInit.CRAFT_UNLOCK.get(), SoundSource.AMBIENT,  0.5f, 1f);
//                    h.unlockpaperrecipe(1);
//                }
//            });
//    }
    }
