package com.ladestitute.bewarethedark.events;

import com.ladestitute.bewarethedark.blocks.entity.WoodLanternBlockEntity;
import com.ladestitute.bewarethedark.btdcapabilities.customplayerdata.CustomPlayerDataProvider;
import com.ladestitute.bewarethedark.btdcapabilities.sanity.PlayerSanityProvider;
import com.ladestitute.bewarethedark.entities.mobs.passive.FireflyEntity;
import com.ladestitute.bewarethedark.registries.BlockInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.AirBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.LightBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class DynamicLightHandler {
    //Todo: add checking tags for light values, for now this is just for reimplementing crucial v0.4 handheld torch/lantern functionality

    @SubscribeEvent
    public void dynamiclight(TickEvent.PlayerTickEvent event) {
        BlockPos blockPos = event.player.blockPosition();
        int lightsearchRadius = 10;
        int radius = (int) (Math.floor(2.0) + lightsearchRadius);


        for (int x = -radius; x <= radius; ++x) {
            for (int y = -1; y <= 1; ++y) {
                for (int z = -radius; z <= radius; ++z) {
                    BlockPos shrinePos = new BlockPos(blockPos.getX() + x, blockPos.getY() + y, blockPos.getZ() + z);
                    BlockState blockState = event.player.level.getBlockState(shrinePos);
                    Block block = blockState.getBlock();
                    if (block instanceof LightBlock && shrinePos != event.player.blockPosition().above()) {
                        event.player.level.removeBlock(shrinePos, false);
                    }
                }

            }
        }

        if(BTDConfig.getInstance().allowdynamiclight())
        {
                ItemStack handstack = event.player.getItemBySlot(EquipmentSlot.MAINHAND);
                ItemStack offstack = event.player.getItemBySlot(EquipmentSlot.OFFHAND);
                ItemStack headstack = event.player.getItemBySlot(EquipmentSlot.HEAD);

                BlockPos playerPos = event.player.blockPosition().above();
                BlockPos lightPos = new BlockPos(playerPos.getX(), playerPos.getY(), playerPos.getZ());
                BlockState lightblockState = event.player.level.getBlockState(lightPos);
                Block block = lightblockState.getBlock();
                if(block instanceof AirBlock) {
                    event.player.getCapability(CustomPlayerDataProvider.CUSTOM_PLAYER_DATA).ifPresent(cpd -> {
                        if (cpd.getLanternFuel() >= 1 && handstack.getItem() == BlockInit.WOOD_LANTERN.get().asItem() ||
                                cpd.getLanternFuel() >= 1 && offstack.getItem() == BlockInit.WOOD_LANTERN.get().asItem()) {
                            cpd.subLanternFuel(1);
                            event.player.level.setBlockAndUpdate(playerPos, Blocks.LIGHT.defaultBlockState().setValue(LightBlock.LEVEL, 11));
                        }
                    });
                    if (handstack.getItem() == ItemInit.TORCH.get() ||
                            offstack.getItem() == ItemInit.TORCH.get()) {
                        event.player.level.setBlockAndUpdate(playerPos, Blocks.LIGHT.defaultBlockState().setValue(LightBlock.LEVEL, 11));
                    }
                    if (headstack.getItem() == ItemInit.MINER_HAT.get()) {
                        event.player.level.setBlockAndUpdate(playerPos, Blocks.LIGHT.defaultBlockState().setValue(LightBlock.LEVEL, 11));
                    }
                }
        }
    }

    @SubscribeEvent
    public void entitylight(LivingEvent.LivingTickEvent event)
    {
            if(event.getEntity() instanceof FireflyEntity)
            {
                if(event.getEntity().isAlive()) {
                    if (BTDConfig.getInstance().allowdynamiclight())
                    {
                        event.getEntity().level.setBlockAndUpdate(event.getEntity().blockPosition().above(), Blocks.LIGHT.defaultBlockState().setValue(LightBlock.LEVEL, 7));
                    }
                }
                else event.getEntity().level.setBlockAndUpdate(event.getEntity().blockPosition().above(), Blocks.AIR.defaultBlockState());
            }

    }
}
