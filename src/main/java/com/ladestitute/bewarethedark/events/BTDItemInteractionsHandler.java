package com.ladestitute.bewarethedark.events;

import com.ladestitute.bewarethedark.blocks.building.CarpetedFlooringBlock;
import com.ladestitute.bewarethedark.blocks.building.WoodenFlooringBlock;
import com.ladestitute.bewarethedark.blocks.natural.plant.*;
import com.ladestitute.bewarethedark.blocks.natural.plant.logs.EvergreenLogBlock;
import com.ladestitute.bewarethedark.blocks.natural.plant.logs.LumpyEvergreenLogBlock;
import com.ladestitute.bewarethedark.blocks.natural.plant.logs.SpikyLogBlock;
import com.ladestitute.bewarethedark.blocks.turf.DeciduousTurfBlock;
import com.ladestitute.bewarethedark.blocks.turf.ForestTurfBlock;
import com.ladestitute.bewarethedark.blocks.turf.MarshTurfBlock;
import com.ladestitute.bewarethedark.containers.BackpackContainer;
import com.ladestitute.bewarethedark.util.backpack.BackpackData;
import com.ladestitute.bewarethedark.items.utility.BackpackItem;
import com.ladestitute.bewarethedark.registries.BlockInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import com.ladestitute.bewarethedark.util.BTDKeyboardUtil;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.SimpleMenuProvider;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.event.entity.living.LivingHurtEvent;
import net.minecraftforge.event.entity.player.PlayerInteractEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.network.NetworkHooks;

public class BTDItemInteractionsHandler {

    @SubscribeEvent
    public void torchfire(LivingHurtEvent event) {
        Entity sourceEntity = event.getSource().getEntity();

        if (sourceEntity instanceof Player) {
            for (ItemStack held : event.getSource().getEntity().getHandSlots()) {
                if (held.getItem() == ItemInit.TORCH.get())
                {
                   event.getEntity().setSecondsOnFire(3);
                }
            }
        }
    }

    @SubscribeEvent
    //Event for setting things on fire
    public void lightingshitonfire(PlayerInteractEvent.RightClickBlock event) {
        BlockPos blockpos = event.getPos();
        BlockState state = event.getLevel().getBlockState(blockpos);
        if (event.getLevel().isClientSide) {
            return;
        }

        if (event.getItemStack().getItem() == ItemInit.TORCH.get()) {
            if (state.getBlock() instanceof GrassBlock ||
                    state.getBlock() instanceof ForestTurfBlock ||
                    state.getBlock() instanceof DeciduousTurfBlock ||
                    state.getBlock() instanceof MarshTurfBlock ||
                    state.getBlock() instanceof DirtPathBlock ||
                    state.getBlock() instanceof MyceliumBlock ||
                    state.getBlock() instanceof CarpetedFlooringBlock ||
                    state.getBlock() instanceof WoodenFlooringBlock) {
                ItemStack shovel = ItemInit.TORCH.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    event.getEntity().level.setBlock(blockpos.above(),
                            Blocks.FIRE.defaultBlockState(), 2);
            }
            if (state.getBlock() instanceof TallGrassBlock ||
                    state.getBlock() instanceof BerryBushBlock ||
                    state.getBlock() instanceof GrassTuftBlock ||
                    state.getBlock() instanceof MarshPondPlantBlock ||
                    state.getBlock() instanceof ReedsBlock ||
                    state.getBlock() instanceof SaplingPlantBlock ||
                    state.getBlock() instanceof SpikyBushBlock ||
                    state.getBlock() instanceof BushBlock ||
                    state.getBlock() instanceof DeadBushBlock ||
                    state.getBlock() instanceof FlowerBlock ||
                    state.getBlock() instanceof VineBlock ||
                    state.getBlock() instanceof SweetBerryBushBlock ||
                    state.getBlock() instanceof WebBlock) {
                ItemStack shovel = ItemInit.TORCH.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel))
                    event.getEntity().level.setBlock(blockpos,
                            Blocks.FIRE.defaultBlockState(), 2);
            }
            if (state.getBlock() instanceof EvergreenLogBlock ||
                    state.getBlock() instanceof LumpyEvergreenLogBlock ||
                    state.getBlock() instanceof SpikyLogBlock ||
                    state.getBlock() == Blocks.OAK_LOG||
                    state.getBlock() == Blocks.BIRCH_LOG||
                    state.getBlock() == Blocks.JUNGLE_LOG||
                    state.getBlock() == Blocks.MANGROVE_LOG||
                    state.getBlock() == Blocks.SPRUCE_LOG||
                    state.getBlock() == Blocks.ACACIA_LOG||
                    state.getBlock() == Blocks.DARK_OAK_LOG) {

                ItemStack shovel = ItemInit.TORCH.get().getDefaultInstance();
                if (event.getEntity().getInventory().contains(shovel)) {
                    event.getEntity().level.setBlock(blockpos.south(),
                            Blocks.FIRE.defaultBlockState(), 2);
                    event.getEntity().level.setBlock(blockpos,
                            BlockInit.BURNT_LOG.get().defaultBlockState(), 2);
                }
            }
        }


    }

    @SubscribeEvent
    public void backpackopen(TickEvent.PlayerTickEvent event)
    {
        ItemStack cheststack = event.player.getItemBySlot(EquipmentSlot.CHEST);
        if(event.player instanceof ServerPlayer serverPlayer && !event.player.level.isClientSide && cheststack.getItem() == ItemInit.BACKPACK.get()
                && BTDKeyboardUtil.OPEN_BACKPACK.consumeClick())
        {
            //ItemStack backpack = event.player.
            BackpackData data = BackpackItem.getData(cheststack);
            NetworkHooks.openScreen(((ServerPlayer) event.player), new SimpleMenuProvider( (windowId, playerInventory, playerEntity) ->
                    new BackpackContainer(windowId, playerInventory, data.getHandler()), cheststack.getHoverName()));

        }
    }
}
