package com.ladestitute.bewarethedark.blocks.utility;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.registries.*;
import com.ladestitute.bewarethedark.blocks.entity.CampFireTileEntity;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.EntityBlock;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.items.ItemHandlerHelper;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class BTDCampfireBlock extends HorizontalDirectionalBlock implements EntityBlock {
    int fueleffiency = 1;

    public BTDCampfireBlock(Properties properties) {
        super(properties.lightLevel((state) -> {
            return 15;
        }));
        runCalculation(SHAPE.orElse(Shapes.block()));
    }

    protected void runCalculation(VoxelShape shape) {
        for (Direction direction : Direction.values())
            SHAPES.put(direction, BTDMain.calculateShapes(direction, shape));
    }

    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level level, BlockState state,
                                                                  BlockEntityType<T> type) {
        return level.isClientSide ? null
                : (level0, pos, state0, blockEntity) -> ((CampFireTileEntity) blockEntity).tick();
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
        return BlockEntityInit.CAMP_FIRE.get().create(pos, state);
    }

    private static final Map<Direction, VoxelShape> SHAPES = new EnumMap<>(Direction.class);

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(FACING);
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context) {
        return SHAPES.get(state.getValue(FACING));
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
    }

    private static final Optional<VoxelShape> SHAPE = Stream
            .of(Block.box(6, -1, 0, 10, 3, 16),
                    Block.box(0, 0, 6, 16, 4, 10),
                    Block.box(5, 0, 3, 11, 1, 13),
                    Block.box(3, 0, 5, 13, 1, 11))
            .reduce((v1, v2) -> Shapes.join(v1, v2, BooleanOp.OR));

    @Override
    public void appendHoverText (ItemStack stack, BlockGetter worldIn, List< Component > tooltip, TooltipFlag
            flagIn){
        tooltip.add(Component.literal("Provides light while burning."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public void stepOn(Level p_152431_, BlockPos p_152432_, BlockState p_152433_, Entity p_152434_) {
        p_152434_.setSecondsOnFire(3);
        super.stepOn(p_152431_, p_152432_, p_152433_, p_152434_);
    }

    @Override
    public InteractionResult use(BlockState state, Level worldIn, BlockPos pos, Player player, InteractionHand p_225533_5_, BlockHitResult p_225533_6_) {
        if(player.getMainHandItem().getItem() == ItemInit.BIRCHNUT.get())
        {
            player.getMainHandItem().shrink(1);
            player.playSound(SoundInit.COOK_SIZZLE.get(), 0.5f, 1f);
            ItemHandlerHelper.giveItemToPlayer(player, FoodInit.ROASTED_BIRCHNUT.get().getDefaultInstance());
        }
        if(player.getMainHandItem().getItem() == FoodInit.BERRIES.get())
        {
            player.getMainHandItem().shrink(1);
            player.playSound(SoundInit.COOK_SIZZLE.get(), 0.5f, 1f);
            ItemHandlerHelper.giveItemToPlayer(player, FoodInit.ROASTED_BERRIES.get().getDefaultInstance());
        }
        if(player.getMainHandItem().getItem() == Items.EGG)
        {
            player.getMainHandItem().shrink(1);
            player.playSound(SoundInit.COOK_SIZZLE.get(), 0.5f, 1f);
            ItemHandlerHelper.giveItemToPlayer(player, FoodInit.COOKED_EGG.get().getDefaultInstance());
        }
        if(player.getMainHandItem().getItem() == Items.CARROT)
        {
            player.getMainHandItem().shrink(1);
            player.playSound(SoundInit.COOK_SIZZLE.get(), 0.5f, 1f);
            ItemHandlerHelper.giveItemToPlayer(player, FoodInit.ROASTED_CARROT.get().getDefaultInstance());
        }
        if(player.getMainHandItem().getItem() == FoodInit.MONSTER_MEAT.get())
        {
            player.getMainHandItem().shrink(1);
            player.playSound(SoundInit.COOK_SIZZLE.get(), 0.5f, 1f);
            ItemHandlerHelper.giveItemToPlayer(player, FoodInit.COOKED_MONSTER_MEAT.get().getDefaultInstance());
        }
        if(player.getMainHandItem().getItem() == FoodInit.FISH.get()||
                player.getMainHandItem().getItem() == Items.SALMON||
                player.getMainHandItem().getItem() == Items.COD||
                player.getMainHandItem().getItem() == Items.TROPICAL_FISH)
        {
            player.getMainHandItem().shrink(1);
            player.playSound(SoundInit.COOK_SIZZLE.get(), 0.5f, 1f);
            ItemHandlerHelper.giveItemToPlayer(player, FoodInit.COOKED_FISH.get().getDefaultInstance());
        }
        if(player.getMainHandItem().getItem() == FoodInit.MORSEL.get())
        {
            player.getMainHandItem().shrink(1);
            player.playSound(SoundInit.COOK_SIZZLE.get(), 0.5f, 1f);
            ItemHandlerHelper.giveItemToPlayer(player, FoodInit.COOKED_MORSEL.get().getDefaultInstance());
        }
        if(player.getMainHandItem().getItem() == FoodInit.MEAT.get())
        {
            player.getMainHandItem().shrink(1);
            player.playSound(SoundInit.COOK_SIZZLE.get(), 0.5f, 1f);
            ItemHandlerHelper.giveItemToPlayer(player, FoodInit.COOKED_MEAT.get().getDefaultInstance());
        }
        if(player.getMainHandItem().getItem() == FoodInit.RED_CAP.get())
        {
            player.getMainHandItem().shrink(1);
            player.playSound(SoundInit.COOK_SIZZLE.get(), 0.5f, 1f);
            ItemHandlerHelper.giveItemToPlayer(player, FoodInit.COOKED_RED_CAP.get().getDefaultInstance());
        }
        if(player.getMainHandItem().getItem() == FoodInit.GREEN_CAP.get())
        {
            player.getMainHandItem().shrink(1);
            player.playSound(SoundInit.COOK_SIZZLE.get(), 0.5f, 1f);
            ItemHandlerHelper.giveItemToPlayer(player, FoodInit.COOKED_GREEN_CAP.get().getDefaultInstance());
        }
        if(player.getMainHandItem().getItem() == FoodInit.BLUE_CAP.get())
        {
            player.getMainHandItem().shrink(1);
            player.playSound(SoundInit.COOK_SIZZLE.get(), 0.5f, 1f);
            ItemHandlerHelper.giveItemToPlayer(player, FoodInit.COOKED_BLUE_CAP.get().getDefaultInstance());
        }
        if(player.getMainHandItem().getItem() == FoodInit.BATILISK_WING.get())
        {
            player.getMainHandItem().shrink(1);
            player.playSound(SoundInit.COOK_SIZZLE.get(), 0.5f, 1f);
            ItemHandlerHelper.giveItemToPlayer(player, FoodInit.COOKED_BATILISK_WING.get().getDefaultInstance());
        }
        if(player.getMainHandItem().getItem() == FoodInit.MANDRAKE.get())
        {
            player.startSleeping(player.blockPosition().above());
            if(worldIn instanceof ServerLevel)
            {
                ((ServerLevel) worldIn).setDayTime(23999);
            }
            player.getMainHandItem().shrink(1);
            player.playSound(SoundInit.COOK_SIZZLE.get(), 0.5f, 1f);
            ItemHandlerHelper.giveItemToPlayer(player, FoodInit.COOKED_MANDRAKE.get().getDefaultInstance());
        }
        if(player.getMainHandItem().getItem() == Items.DEAD_BUSH||
                player.getMainHandItem().getItem() == Items.TALL_GRASS||
                player.getMainHandItem().getItem() == Items.FERN||
                player.getMainHandItem().getItem() == Items.VINE||
                player.getMainHandItem().getItem() == Items.DANDELION||
                player.getMainHandItem().getItem() == Items.POPPY||
                player.getMainHandItem().getItem() == Items.BLUE_ORCHID||
                player.getMainHandItem().getItem() == Items.ALLIUM||
                player.getMainHandItem().getItem() == Items.AZURE_BLUET||
                player.getMainHandItem().getItem() == Items.ORANGE_TULIP||
                player.getMainHandItem().getItem() == Items.PINK_TULIP||
                player.getMainHandItem().getItem() == Items.RED_TULIP||
                player.getMainHandItem().getItem() == Items.WHITE_TULIP||
                player.getMainHandItem().getItem() == Items.OXEYE_DAISY||
                player.getMainHandItem().getItem() == Items.CORNFLOWER||
                player.getMainHandItem().getItem() == Items.LILY_OF_THE_VALLEY)
        {
            player.getMainHandItem().shrink(1);
            BlockEntity tileentity = worldIn.getBlockEntity(pos);
            player.playSound(SoundInit.ADD_FUEL.get(), 0.5f, 1f);
            ((CampFireTileEntity)tileentity).fuel = ((CampFireTileEntity) tileentity).fuel + 360*fueleffiency;

        }
        if(player.getMainHandItem().getItem() == ItemInit.CUT_GRASS.get()||
                player.getMainHandItem().getItem() == ItemInit.TENTACLE_SPOTS.get()||
                player.getMainHandItem().getItem() == ItemInit.PINE_CONE.get()||
                player.getMainHandItem().getItem() == ItemInit.TWIGS.get()||
                player.getMainHandItem().getItem() == Items.PAPER||
                player.getMainHandItem().getItem() == Items.SUGAR_CANE||
                player.getMainHandItem().getItem() == Items.ACACIA_FENCE||
                player.getMainHandItem().getItem() == Items.BIRCH_FENCE||
                player.getMainHandItem().getItem() == Items.CRIMSON_FENCE||
                player.getMainHandItem().getItem() == Items.DARK_OAK_FENCE||
                player.getMainHandItem().getItem() == Items.OAK_FENCE||
                player.getMainHandItem().getItem() == Items.JUNGLE_FENCE||
                player.getMainHandItem().getItem() == Items.SPRUCE_FENCE||
                player.getMainHandItem().getItem() == Items.WARPED_FENCE||
                player.getMainHandItem().getItem() == Items.ACACIA_FENCE_GATE||
                player.getMainHandItem().getItem() == Items.BIRCH_FENCE_GATE||
                player.getMainHandItem().getItem() == Items.CRIMSON_FENCE_GATE||
                player.getMainHandItem().getItem() == Items.DARK_OAK_FENCE_GATE||
                player.getMainHandItem().getItem() == Items.OAK_FENCE_GATE||
                player.getMainHandItem().getItem() == Items.JUNGLE_FENCE_GATE||
                player.getMainHandItem().getItem() == Items.SPRUCE_FENCE_GATE||
                player.getMainHandItem().getItem() == Items.WARPED_FENCE_GATE)
        {
            player.getMainHandItem().shrink(1);
            BlockEntity tileentity = worldIn.getBlockEntity(pos);
            player.playSound(SoundInit.ADD_FUEL.get(), 0.5f, 1f);
            ((CampFireTileEntity)tileentity).fuel = ((CampFireTileEntity) tileentity).fuel + 750*fueleffiency;

        }
        if(player.getMainHandItem().getItem() == FoodInit.COOKED_RED_CAP.get()||
                player.getMainHandItem().getItem() == FoodInit.COOKED_GREEN_CAP.get()||
                player.getMainHandItem().getItem() == FoodInit.COOKED_BLUE_CAP.get()||
                player.getMainHandItem().getItem() == FoodInit.PETALS.get()||
                player.getMainHandItem().getItem() == FoodInit.FOILAGE.get())
        {
            player.getMainHandItem().shrink(1);
            BlockEntity tileentity = worldIn.getBlockEntity(pos);
            player.playSound(SoundInit.ADD_FUEL.get(), 0.5f, 1f);
            ((CampFireTileEntity)tileentity).fuel = ((CampFireTileEntity) tileentity).fuel + 375*fueleffiency;

        }
        if(player.getMainHandItem().getItem() == ItemInit.SPRUCE_LOG.get()||
                player.getMainHandItem().getItem() == ItemInit.MANURE.get()||
                player.getMainHandItem().getItem() == ItemInit.GUANO.get()||
                player.getMainHandItem().getItem() == Items.CHARCOAL||
                player.getMainHandItem().getItem() == Items.COAL||
                player.getMainHandItem().getItem() == ItemInit.ROPE.get()||
                player.getMainHandItem().getItem() == BlockInit.DECIDUOUS_TURF.get().asItem()||
                player.getMainHandItem().getItem() == BlockInit.FOREST_TURF.get().asItem()||
                player.getMainHandItem().getItem() == BlockInit.MARSH_TURF.get().asItem()||
                player.getMainHandItem().getItem() == BlockInit.CARPETED_FLOORING.get().asItem()||
                player.getMainHandItem().getItem() == BlockInit.WOODEN_FLOORING.get().asItem()||
                player.getMainHandItem().getItem() == BlockInit.COBBLESTONES.get().asItem()||
                player.getMainHandItem().getItem() == BlockInit.ROCKY_TURF.get().asItem()||
                player.getMainHandItem().getItem() == BlockInit.BLUE_FUNGAL_TURF.get().asItem()||
                player.getMainHandItem().getItem() == BlockInit.GREEN_FUNGAL_TURF.get().asItem()||
                player.getMainHandItem().getItem() == BlockInit.RED_FUNGAL_TURF.get().asItem()||
                player.getMainHandItem().getItem() == BlockInit.GUANO_TURF.get().asItem()||
                player.getMainHandItem().getItem() == Items.ACACIA_SIGN||
                player.getMainHandItem().getItem() == Items.BIRCH_SIGN||
                player.getMainHandItem().getItem() == Items.CRIMSON_SIGN||
                player.getMainHandItem().getItem() == Items.DARK_OAK_SIGN||
                player.getMainHandItem().getItem() == Items.OAK_SIGN||
                player.getMainHandItem().getItem() == Items.JUNGLE_SIGN||
                player.getMainHandItem().getItem() == Items.SPRUCE_SIGN||
                player.getMainHandItem().getItem() == Items.WARPED_SIGN)
        {
            player.getMainHandItem().shrink(1);
            BlockEntity tileentity = worldIn.getBlockEntity(pos);
            player.playSound(SoundInit.ADD_FUEL.get(), 0.5f, 1f);
            ((CampFireTileEntity)tileentity).fuel = ((CampFireTileEntity) tileentity).fuel + 2250*fueleffiency;

        }
        if(player.getMainHandItem().getItem() == ItemInit.BERRY_BUSH_ITEM.get()||
                player.getMainHandItem().getItem() == ItemInit.SPIKY_BUSH_ITEM.get()||
                player.getMainHandItem().getItem() == ItemInit.SAPLING_PLANT_ITEM.get()||
                player.getMainHandItem().getItem() == ItemInit.GRASS_TUFT_ITEM.get()||
                player.getMainHandItem().getItem() == ItemInit.BOARDS.get()||
                player.getMainHandItem().getItem() == ItemInit.LOG_SUIT.get()||
                player.getMainHandItem().getItem() == ItemInit.STRAW_ROLL.get())
        {
            player.getMainHandItem().shrink(1);
            BlockEntity tileentity = worldIn.getBlockEntity(pos);
            player.playSound(SoundInit.ADD_FUEL.get(), 0.5f, 1f);
            ((CampFireTileEntity)tileentity).fuel = ((CampFireTileEntity) tileentity).fuel + 9000*fueleffiency;

        }
        return super.use(state, worldIn, pos, player, p_225533_5_, p_225533_6_);
    }
}

