package com.ladestitute.bewarethedark.blocks.utility;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;

import java.util.List;

public class ScienceMachineBlock extends Block {
    public ScienceMachineBlock(Properties properties) {
        super(properties);
    }

    @Override
    public void appendHoverText (ItemStack stack, BlockGetter worldIn, List<Component> tooltip, TooltipFlag
            flagIn){
        tooltip.add(Component.literal("Unlock new crafting recipes."));
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
    }

    @Override
    public boolean isFlammable(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return true;
    }

    @Override
    public int getFlammability(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 52;
    }

    @Override
    public int getFireSpreadSpeed(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 32;
    }
}
