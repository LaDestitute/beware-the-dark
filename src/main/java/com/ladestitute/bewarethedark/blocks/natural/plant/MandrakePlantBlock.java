package com.ladestitute.bewarethedark.blocks.natural.plant;

import com.ladestitute.bewarethedark.entities.mobs.passive.MandrakeEntity;
import com.ladestitute.bewarethedark.registries.BlockInit;
import com.ladestitute.bewarethedark.registries.EntityInit;
import com.ladestitute.bewarethedark.registries.FoodInit;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DirectionProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraftforge.items.ItemHandlerHelper;

public class MandrakePlantBlock extends Block {
    public static final DirectionProperty FACING = BlockStateProperties.HORIZONTAL_FACING;

    public MandrakePlantBlock(Properties properties) {
        super(properties);
        this.registerDefaultState(this.getStateDefinition().any().setValue(FACING, Direction.NORTH));
    }

    protected boolean mayPlaceOn(BlockState p_200014_1_, BlockGetter p_200014_2_, BlockPos p_200014_3_) {
        return p_200014_1_.is(Blocks.GRASS_BLOCK) ||
                p_200014_1_.is(BlockInit.FOREST_TURF.get())||
                p_200014_1_.is(BlockInit.DECIDUOUS_TURF.get())||
                p_200014_1_.is(Blocks.MUD)||
                p_200014_1_.is(BlockInit.MARSH_TURF.get());
    }

    @Override
    public boolean canSurvive(BlockState p_51028_, LevelReader p_51029_, BlockPos p_51030_) {
        BlockPos blockpos = p_51030_.below();
        return this.mayPlaceOn(p_51029_.getBlockState(blockpos), p_51029_, blockpos);
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext pContext) {
        return this.defaultBlockState().setValue(FACING, pContext.getHorizontalDirection().getOpposite());
    }

    @Override
    public BlockState rotate(BlockState pState, Rotation pRotation) {
        return pState.setValue(FACING, pRotation.rotate(pState.getValue(FACING)));
    }

    @Override
    public BlockState mirror(BlockState pState, Mirror pMirror) {
        return pState.rotate(pMirror.getRotation(pState.getValue(FACING)));
    }

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> pBuilder) {
        pBuilder.add(FACING);
    }

    @Override
    public RenderShape getRenderShape(BlockState pState) {
        return RenderShape.MODEL;
    }

    @Override
    public boolean onDestroyedByPlayer(BlockState state, Level level, BlockPos pos, Player player, boolean willHarvest, FluidState fluid) {
        if(!level.isDay())
        {
            MandrakeEntity firefly = new MandrakeEntity(EntityInit.MANDRAKE.get(), level);
            firefly.setPos(pos.getX()+0.5, pos.getY(), pos.getZ()+0.5);
            level.addFreshEntity(firefly);
        }
        if(level.isDay())
        {
            ItemHandlerHelper.giveItemToPlayer(player, FoodInit.MANDRAKE.get().getDefaultInstance());
        }
        return super.onDestroyedByPlayer(state, level, pos, player, willHarvest, fluid);
    }

}

