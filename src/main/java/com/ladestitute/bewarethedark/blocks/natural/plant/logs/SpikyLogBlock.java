package com.ladestitute.bewarethedark.blocks.natural.plant.logs;

import com.ladestitute.bewarethedark.registries.ItemInit;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.FluidState;

import java.util.Random;

public class SpikyLogBlock extends RotatedPillarBlock {
    public SpikyLogBlock(Properties properties) {
        super(properties);
    }


    @Override
    public boolean onDestroyedByPlayer(BlockState state, Level world, BlockPos pos, Player player, boolean willHarvest, FluidState fluid) {
        ItemStack logstack = new ItemStack(ItemInit.SPRUCE_LOG.get());
        ItemEntity log = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), logstack);
        ItemStack twigsstack = new ItemStack(ItemInit.TWIGS.get());
        ItemEntity twigs = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), twigsstack);

        Random rand = new Random();
        int logdrop = rand.nextInt(100);

        if(!player.isCreative()) {
            world.addFreshEntity(twigs);
            if (logdrop <= 19) {
                world.addFreshEntity(log);
            }
        }
        return super.onDestroyedByPlayer(state, world, pos, player, willHarvest, fluid);
    }

    @Override
    public boolean isFlammable(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return true;
    }

    @Override
    public int getFlammability(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 5;
    }

    @Override
    public int getFireSpreadSpeed(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 5;
    }

}
