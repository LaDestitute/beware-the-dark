package com.ladestitute.bewarethedark.blocks.natural.plant;

import com.ladestitute.bewarethedark.registries.BlockInit;
import com.ladestitute.bewarethedark.registries.FoodInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.FluidState;

public class GreenMushtreeCapBlock extends Block {
    public GreenMushtreeCapBlock(Properties properties) {
        super(properties);
    }

    protected boolean mayPlaceOn(BlockState p_200014_1_, BlockGetter p_200014_2_, BlockPos p_200014_3_) {
        return p_200014_1_.is(Blocks.GRASS_BLOCK) ||
                p_200014_1_.is(BlockInit.FOREST_TURF.get())||
                p_200014_1_.is(BlockInit.DECIDUOUS_TURF.get())||
                p_200014_1_.is(Blocks.MUD)||
                p_200014_1_.is(BlockInit.MUSHTREE.get())||
                p_200014_1_.is(BlockInit.MARSH_TURF.get())||
                p_200014_1_.is(BlockInit.RED_FUNGAL_TURF.get())||
                p_200014_1_.is(BlockInit.BLUE_FUNGAL_TURF.get())||
                p_200014_1_.is(BlockInit.GREEN_FUNGAL_TURF.get());
    }

    @Override
    public boolean canSurvive(BlockState p_51028_, LevelReader p_51029_, BlockPos p_51030_) {
        BlockPos blockpos = p_51030_.below();
        return this.mayPlaceOn(p_51029_.getBlockState(blockpos), p_51029_, blockpos);
    }

    @Override
    public boolean onDestroyedByPlayer(BlockState state, Level world, BlockPos pos, Player player, boolean willHarvest, FluidState fluid) {
        ItemStack logstack = new ItemStack(FoodInit.GREEN_CAP.get());
        ItemEntity log = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), logstack);
        ItemStack pineconestack = new ItemStack(FoodInit.GREEN_CAP.get());
        ItemEntity pinecone = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), pineconestack);

        if(!player.isCreative()) {
            world.addFreshEntity(log);
            world.addFreshEntity(pinecone);
        }
        return super.onDestroyedByPlayer(state, world, pos, player, willHarvest, fluid);
    }

    @Override
    public boolean isFlammable(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return true;
    }

    @Override
    public int getFlammability(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 30;
    }

    @Override
    public int getFireSpreadSpeed(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 60;
    }
}

