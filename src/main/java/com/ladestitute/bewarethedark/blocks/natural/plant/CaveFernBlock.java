package com.ladestitute.bewarethedark.blocks.natural.plant;

import com.ladestitute.bewarethedark.registries.BlockInit;
import com.ladestitute.bewarethedark.registries.FoodInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.FluidState;

import java.util.Random;

public class CaveFernBlock extends Block {

    public CaveFernBlock(Properties properties) {
        super(properties);
    }

    protected boolean mayPlaceOn(BlockState p_200014_1_, BlockGetter p_200014_2_, BlockPos p_200014_3_) {
        return p_200014_1_.is(BlockInit.FOREST_TURF.get())||
                p_200014_1_.is(Blocks.GRASS)||
                p_200014_1_.is(Blocks.MUD)||
                p_200014_1_.is(BlockInit.RED_FUNGAL_TURF.get())||
                p_200014_1_.is(BlockInit.GREEN_FUNGAL_TURF.get())||
                p_200014_1_.is(BlockInit.BLUE_FUNGAL_TURF.get());
    }

    @Override
    public boolean canSurvive(BlockState p_51028_, LevelReader p_51029_, BlockPos p_51030_) {
        BlockPos blockpos = p_51030_.below();
        return this.mayPlaceOn(p_51029_.getBlockState(blockpos), p_51029_, blockpos);
    }

    @Override
    public boolean isFlammable(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return true;
    }

    @Override
    public int getFlammability(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 100;
    }

    @Override
    public int getFireSpreadSpeed(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 60;
    }

    @Override
    public boolean onDestroyedByPlayer(BlockState state, Level world, BlockPos pos, Player player, boolean willHarvest, FluidState fluid) {
        ItemStack rockstack1 = new ItemStack(FoodInit.FOILAGE.get());
        ItemEntity rock1 = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), rockstack1);
            world.addFreshEntity(rock1);
        return super.onDestroyedByPlayer(state, world, pos, player, willHarvest, fluid);
    }

}
