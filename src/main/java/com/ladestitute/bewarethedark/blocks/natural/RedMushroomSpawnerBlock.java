package com.ladestitute.bewarethedark.blocks.natural;

import com.ladestitute.bewarethedark.blocks.entity.RedMushroomSpawnerBlockEntity;
import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import com.ladestitute.bewarethedark.registries.FoodInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseEntityBlock;
import net.minecraft.world.level.block.RenderShape;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityTicker;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.FluidState;
import net.minecraftforge.common.IPlantable;

import java.util.Random;

public class RedMushroomSpawnerBlock extends BaseEntityBlock {

    public RedMushroomSpawnerBlock(Properties properties) {
        super(properties);
    }

    @Override
    public boolean isFertile(BlockState state, BlockGetter world, BlockPos pos) {
        return true;
    }

    @Override
    public boolean canSustainPlant(BlockState state, BlockGetter world, BlockPos pos, Direction facing, IPlantable plantable) {
        return true;
    }


    @Override
    public RenderShape getRenderShape(BlockState state) {
        return RenderShape.MODEL;
    }

    @javax.annotation.Nullable
    @Override
    public BlockEntity newBlockEntity(BlockPos pPos, BlockState pState) {
        return new RedMushroomSpawnerBlockEntity(pPos, pState);
    }

    @javax.annotation.Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(Level pLevel, BlockState pState, BlockEntityType<T> pBlockEntityType) {
        return createTickerHelper(pBlockEntityType, BlockEntityInit.RED_MUSHROOM_SPAWNER.get(), RedMushroomSpawnerBlockEntity::tick);
    }

    @Override
    public boolean onDestroyedByPlayer(BlockState state, Level world, BlockPos pos, Player player, boolean willHarvest, FluidState fluid) {
        ItemStack pickaxestack = player.getItemBySlot(EquipmentSlot.MAINHAND);
        Random rand = new Random();
        ItemStack rockstack1 = new ItemStack(FoodInit.RED_CAP.get());
        ItemEntity rock1 = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), rockstack1);
        ItemStack rockstack2 = new ItemStack(FoodInit.RED_CAP.get());
        ItemEntity rock2 = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), rockstack2);

        if(pickaxestack.getItem() == ItemInit.FLINT_SHOVEL.get() ||
                pickaxestack.getItem() == ItemInit.REGAL_SHOVEL.get())
        {
            world.addFreshEntity(rock1);
            world.addFreshEntity(rock2);
        }
        return super.onDestroyedByPlayer(state, world, pos, player, willHarvest, fluid);
    }

}
