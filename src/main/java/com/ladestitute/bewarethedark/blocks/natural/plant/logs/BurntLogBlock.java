package com.ladestitute.bewarethedark.blocks.natural.plant.logs;

import com.ladestitute.bewarethedark.registries.BlockInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.FluidState;

import java.util.Random;

public class BurntLogBlock extends RotatedPillarBlock {
    public BurntLogBlock(Properties properties) {
        super(properties);
    }

    protected boolean mayPlaceOn(BlockState p_200014_1_, BlockGetter p_200014_2_, BlockPos p_200014_3_) {
        return p_200014_1_.is(Blocks.GRASS_BLOCK) ||
                p_200014_1_.is(BlockInit.FOREST_TURF.get())||
                p_200014_1_.is(BlockInit.DECIDUOUS_TURF.get())||
                p_200014_1_.is(Blocks.MUD)||
                p_200014_1_.is(BlockInit.MARSH_TURF.get());
    }

    @Override
    public boolean canSurvive(BlockState p_51028_, LevelReader p_51029_, BlockPos p_51030_) {
        BlockPos blockpos = p_51030_.below();
        return this.mayPlaceOn(p_51029_.getBlockState(blockpos), p_51029_, blockpos);
    }

    @Override
    public boolean onDestroyedByPlayer(BlockState state, Level world, BlockPos pos, Player player, boolean willHarvest, FluidState fluid) {
        ItemStack logstack = new ItemStack(Items.CHARCOAL);
        ItemEntity log = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), logstack);
        ItemStack logstack2 = new ItemStack(Items.CHARCOAL);
        ItemEntity log2 = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), logstack2);
        ItemStack pineconestack = new ItemStack(ItemInit.PINE_CONE.get());
        ItemEntity pinecone = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), pineconestack);
        ItemStack pineconestack2 = new ItemStack(ItemInit.PINE_CONE.get());
        ItemEntity pinecone2 = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), pineconestack2);

        Random rand = new Random();
        //next int counts from zero and ignores the last number so you will only get possible results of 0-4 so keep this in mind
        int pineconechance = rand.nextInt(2);
        int extracharchance = rand.nextInt(2);
        int extrapineconechance = rand.nextInt(2);

        if(!player.isCreative()) {
            world.addFreshEntity(log);
            if(pineconechance == 0)
            {
                world.addFreshEntity(pinecone);
                if(extrapineconechance == 0)
                {
                    world.addFreshEntity(pinecone2);
                }
            }
            if(pineconechance == 1)
            {
                if(extracharchance == 0)
                {
                    world.addFreshEntity(log2);
                }
            }
        }
        return super.onDestroyedByPlayer(state, world, pos, player, willHarvest, fluid);
    }

    @Override
    public boolean isFlammable(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return false;
    }

    @Override
    public int getFlammability(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 5;
    }

    @Override
    public int getFireSpreadSpeed(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 5;
    }
}
