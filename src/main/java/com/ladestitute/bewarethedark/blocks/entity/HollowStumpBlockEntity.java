package com.ladestitute.bewarethedark.blocks.entity;

import com.ladestitute.bewarethedark.blocks.helper.FirefliesSpawnerBlock;
import com.ladestitute.bewarethedark.entities.mobs.neutral.CatcoonEntity;
import com.ladestitute.bewarethedark.entities.mobs.passive.FireflyEntity;
import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import com.ladestitute.bewarethedark.registries.EntityInit;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

import javax.annotation.Nullable;

public class HollowStumpBlockEntity extends BlockEntity {
    public int catcoonspawntimer;
    public int stumplives = 9;
    public int regeneratelivestimer;
    public int firstspawn;

    protected final ContainerData data;

    public HollowStumpBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityInit.HOLLOW_STUMP.get(), pos, state);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return HollowStumpBlockEntity.this.catcoonspawntimer;
                    case 1: return HollowStumpBlockEntity.this.stumplives;
                    case 2: return HollowStumpBlockEntity.this.regeneratelivestimer;
                    case 3: return HollowStumpBlockEntity.this.firstspawn;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: HollowStumpBlockEntity.this.catcoonspawntimer = value;
                    case 1: HollowStumpBlockEntity.this.stumplives = value;
                    case 2: HollowStumpBlockEntity.this.regeneratelivestimer = value;
                    break;
                }
            }

            public int getCount() {
                return 3;
            }
        };
    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, HollowStumpBlockEntity entity) {
        if(entity.stumplives >=1 && entity.firstspawn == 0 && !pLevel.isRaining())
        {
            CatcoonEntity catcoon = new CatcoonEntity(EntityInit.CATCOON.get(), pLevel);
            catcoon.setPos(pPos.getX() + 1.5, pPos.getY(), pPos.getZ() + 1.5);
            pLevel.addFreshEntity(catcoon);
            entity.stumplives = entity.stumplives - 1;
            entity.firstspawn=1;
        }
        if(entity.stumplives >=1 && entity.firstspawn == 1)
        {
            entity.catcoonspawntimer++;
            //8 real-time minutes (40% of a full in-game day)
            if (!pLevel.isRaining() && entity.catcoonspawntimer >= 9600)
            {
                CatcoonEntity catcoon = new CatcoonEntity(EntityInit.CATCOON.get(), pLevel);
                catcoon.setPos(pPos.getX() + 1.5, pPos.getY(), pPos.getZ() + 1.5);
                pLevel.addFreshEntity(catcoon);
                entity.stumplives = entity.stumplives - 1;
                entity.catcoonspawntimer = 0;
            }
        }
        else entity.regeneratelivestimer++;
        //4 real-time hours and 35 minutes (13.75 days in-game days)
        if(entity.regeneratelivestimer >= 330000)
        {
            entity.stumplives=9;
            entity.regeneratelivestimer=0;
        }
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        catcoonspawntimer = tag.getInt("catcoonspawntimer");
        stumplives = tag.getInt("stumplives");
        regeneratelivestimer = tag.getInt("regeneratelivestimer");
        firstspawn = tag.getInt("firstspawn");
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.putInt("catcoonspawntimer", catcoonspawntimer);
        tag.putInt("stumplives", stumplives);
        tag.putInt("regeneratelivestimer", regeneratelivestimer);
        tag.putInt("firstspawn", firstspawn);
    }

    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = new CompoundTag();
        this.saveAdditional(tag);
        return tag;
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

}
