package com.ladestitute.bewarethedark.blocks.entity;

import com.ladestitute.bewarethedark.client.ClientSanityData;
import com.ladestitute.bewarethedark.entities.mobs.shadow.ShadowWatcherEntity;
import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import com.ladestitute.bewarethedark.registries.EntityInit;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

import javax.annotation.Nullable;

public class ShadowWatcherSpawnerBlockEntity extends BlockEntity {
    public int hasshadowwatcher = 0;
    public int spawnedshadowwatcher = 0;

    protected final ContainerData data;

    public ShadowWatcherSpawnerBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityInit.SHADOW_WATCHER_SPAWNER.get(), pos, state);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return ShadowWatcherSpawnerBlockEntity.this.hasshadowwatcher;
                    case 1: return ShadowWatcherSpawnerBlockEntity.this.spawnedshadowwatcher;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: ShadowWatcherSpawnerBlockEntity.this.hasshadowwatcher = value; break;
                    case 1: ShadowWatcherSpawnerBlockEntity.this.spawnedshadowwatcher = value; break;
                }
            }

            public int getCount() {
                return 2;
            }
        };
    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, ShadowWatcherSpawnerBlockEntity entity) {
        entity.hasshadowwatcher=1;
        ShadowWatcherEntity shadowwatcher = new ShadowWatcherEntity(EntityInit.SHADOW_WATCHER.get(), pLevel);
        if(entity.spawnedshadowwatcher == 0 && entity.level.isNight() && ClientSanityData.getPlayerSanity() <= 9)
        {
            shadowwatcher.setPos(pPos.getX()+0.5, pPos.getY(), pPos.getZ()+0.5);
            pLevel.addFreshEntity(shadowwatcher);
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
            entity.spawnedshadowwatcher=1;
        }
        //Remove nighthands upon day or regaining sanity
        if(entity.spawnedshadowwatcher == 1 && !entity.level.isNight()||
                entity.spawnedshadowwatcher == 1 && ClientSanityData.getPlayerSanity() >= 10)
        {
            shadowwatcher.discard();
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
            entity.spawnedshadowwatcher=0;
        }
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        hasshadowwatcher = tag.getInt("hasshadowwatcher");
        spawnedshadowwatcher = tag.getInt("spawnedshadowwatcher");
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.putInt("hasshadowwatcher", hasshadowwatcher);
        tag.putInt("spawnedshadowwatcher", spawnedshadowwatcher);
    }

    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = new CompoundTag();
        this.saveAdditional(tag);
        return tag;
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

}

