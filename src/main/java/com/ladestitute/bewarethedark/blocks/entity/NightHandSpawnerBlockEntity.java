package com.ladestitute.bewarethedark.blocks.entity;

import com.ladestitute.bewarethedark.blocks.helper.FirefliesSpawnerBlock;
import com.ladestitute.bewarethedark.client.ClientSanityData;
import com.ladestitute.bewarethedark.entities.mobs.passive.FireflyEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.NightHandEntity;
import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import com.ladestitute.bewarethedark.registries.EntityInit;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

import javax.annotation.Nullable;

public class NightHandSpawnerBlockEntity extends BlockEntity {
    public int hasnighthand = 0;
    public int spawnednighthand = 0;

    protected final ContainerData data;

    public NightHandSpawnerBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityInit.NIGHTHAND_SPAWNER.get(), pos, state);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return NightHandSpawnerBlockEntity.this.hasnighthand;
                    case 1: return NightHandSpawnerBlockEntity.this.spawnednighthand;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: NightHandSpawnerBlockEntity.this.hasnighthand = value; break;
                    case 1: NightHandSpawnerBlockEntity.this.spawnednighthand = value; break;
                }
            }

            public int getCount() {
                return 2;
            }
        };
    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, NightHandSpawnerBlockEntity entity) {
        entity.hasnighthand=1;
        NightHandEntity nighthand = new NightHandEntity(EntityInit.NIGHT_HAND.get(), pLevel);
        if(entity.spawnednighthand == 0 && entity.level.isNight() && ClientSanityData.getPlayerSanity() <= 14)
        {
            nighthand.setPos(pPos.getX()+0.5, pPos.getY(), pPos.getZ()+0.5);
            pLevel.addFreshEntity(nighthand);
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
            entity.spawnednighthand=1;
        }
        //Remove nighthands upon day or regaining sanity
        if(entity.spawnednighthand == 1 && !entity.level.isNight()||
                entity.spawnednighthand == 1 && ClientSanityData.getPlayerSanity() >= 15)
        {
            nighthand.discard();
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
            entity.spawnednighthand=0;
        }
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        hasnighthand = tag.getInt("hasnighthand");
        spawnednighthand = tag.getInt("spawnednighthand");
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.putInt("hasnighthand", hasnighthand);
        tag.putInt("spawnednighthand", spawnednighthand);
    }

    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = new CompoundTag();
        this.saveAdditional(tag);
        return tag;
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

}
