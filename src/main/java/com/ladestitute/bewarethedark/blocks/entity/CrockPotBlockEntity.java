package com.ladestitute.bewarethedark.blocks.entity;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.blocks.foodutil.CrockPotBlock;
import com.ladestitute.bewarethedark.blocks.helper.FirefliesSpawnerBlock;
import com.ladestitute.bewarethedark.client.screen.CrockPotMenu;
import com.ladestitute.bewarethedark.network.CrockPotStackRenderUpdatePacket;
import com.ladestitute.bewarethedark.network.NetworkingHandler;
import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import com.ladestitute.bewarethedark.util.recipes.CrockPotRecipe;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Registry;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.TagKey;
import net.minecraft.world.MenuProvider;
import net.minecraft.world.SimpleContainer;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.ForgeCapabilities;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.wrapper.RangedWrapper;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Optional;

public class CrockPotBlockEntity extends BlockEntity implements MenuProvider {
    private static final TagKey<Item> ingredient = TagKey.create(Registry.ITEM_REGISTRY, new ResourceLocation(BTDMain.MOD_ID, "crockpot_inputs"));

    //Item-renderer stuff
    public void setHandler(ItemStackHandler itemStackHandler) {
        for (int i = 0; i < itemStackHandler.getSlots(); i++) {
            itemHandler.setStackInSlot(i, itemStackHandler.getStackInSlot(i));
        }
    }


    //Item-renderer stuff
    //Possible todo: render all ingredients in pot during cooking?
    public ItemStack getRenderStack() {
        ItemStack stack;
        stack = itemHandler.getStackInSlot(4);
        return stack;
    }

    private final ItemStackHandler itemHandler = new ItemStackHandler(5) {

        @Override
        protected int getStackLimit(int slot, @NotNull ItemStack stack) {
            return 1;
        }

        @Override
        protected void onContentsChanged(int slot) {
            setChanged();
            //Item-renderer stuff
            if(!level.isClientSide()) {
                NetworkingHandler.sendToClients(new CrockPotStackRenderUpdatePacket(this, worldPosition));
            }
        }

        //Input-filtering stuff
        @Override
        public boolean isItemValid(int slot, @Nonnull ItemStack stack) {
            return canPut(stack);
        }

        //Input-filtering stuff
        private boolean canPut(ItemStack stack) {
            return stack.getTags().anyMatch(i -> (i == ingredient));
        }

        //Input-filtering stuff
        @Nonnull
        @Override
        public ItemStack insertItem(int slot, @Nonnull ItemStack stack, boolean simulate) {
            if (!canPut(stack)) {
                return stack;
            }
            return super.insertItem(slot, stack, simulate);
        }

    };

    private final RangedWrapper itemHandlerInput = new RangedWrapper(itemHandler, 0, 4);
    private final RangedWrapper itemHandlerOutput = new RangedWrapper(itemHandler, 5, 6);

    private ItemStack result = ItemStack.EMPTY;

    public int progress = 0;
    //Test progress, to be changed to json-arguments with the recipe
    private int maxProgress;

    protected final ContainerData data;

    public CrockPotBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityInit.CROCK_POT.get(), pos, state);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return CrockPotBlockEntity.this.progress;
                    case 1: return CrockPotBlockEntity.this.maxProgress;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: CrockPotBlockEntity.this.progress = value; break;
                    case 1: CrockPotBlockEntity.this.maxProgress = value; break;
                }
            }

            public int getCount() {
                return 2;
            }
        };
    }

    @Override
    public Component getDisplayName() {
        return Component.literal("Crock Pot");
    }

    @Nullable
    @Override
    public AbstractContainerMenu createMenu(int pContainerId, Inventory pInventory, Player pPlayer) {
        return new CrockPotMenu(pContainerId, pInventory, this, this.data);
    }

    private static boolean hasRecipe(CrockPotBlockEntity entity) {
        Level level = entity.level;
        SimpleContainer inventory = new SimpleContainer(entity.itemHandler.getSlots());
        for (int i = 0; i < entity.itemHandler.getSlots(); i++) {
            inventory.setItem(i, entity.itemHandler.getStackInSlot(i));
        }

        Optional<CrockPotRecipe> match = level.getRecipeManager()
                .getRecipeFor(CrockPotRecipe.Type.INSTANCE, inventory, level);

        return match.isPresent() && canInsertAmountIntoOutputSlot(inventory)
                && canInsertItemIntoOutputSlot(inventory, match.get().getResultItem());
    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, CrockPotBlockEntity pBlockEntity) {
        //Item-renderer stuff
        if(!pLevel.isClientSide())
        {
            NetworkingHandler.sendToClients(new CrockPotStackRenderUpdatePacket(pBlockEntity.itemHandler,
                    pBlockEntity.worldPosition));
        }

        SimpleContainer container = new SimpleContainer(pBlockEntity.itemHandler.getSlots());
        for(int i = 0; i < pBlockEntity.itemHandler.getSlots(); i++) {
            container.setItem(i, pBlockEntity.itemHandler.getStackInSlot(i));
        }
        Optional<CrockPotRecipe> recipe = pLevel.getRecipeManager().getRecipeFor(CrockPotRecipe.Type.INSTANCE, container, pLevel);
        recipe.ifPresent(crockpotRecipe -> pBlockEntity.maxProgress = crockpotRecipe.getTime());

        if(hasRecipe(pBlockEntity)) {
            setChanged(pLevel, pPos, pState);
                pBlockEntity.progress++;
                if(pBlockEntity.progress > pBlockEntity.maxProgress) {

                    craftItem(pBlockEntity);

                }
        } else {
            setChanged(pLevel, pPos, pState);
        }
        setChanged(pLevel, pPos, pState);

        if(pBlockEntity.progress > 0)
        {
            pState = pState.setValue(CrockPotBlock.LIT, true);
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
        }
        if(pBlockEntity.progress == 0)
        {
            pState = pState.setValue(CrockPotBlock.LIT, false);
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
        }
    }

    public ItemStackHandler getItemHandler() {
        return itemHandler;
    }

    public ItemStack getResult() {
        return result;
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        itemHandler.deserializeNBT(tag.getCompound("ItemHandler"));
        progress = tag.getInt("progress");
        maxProgress = tag.getInt("max");
        result.deserializeNBT(tag.getCompound("Result"));
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.put("ItemHandler", itemHandler.serializeNBT());
        tag.putInt("progress", progress);
        tag.putInt("max", maxProgress);
        tag.put("Result", result.serializeNBT());
    }

    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = new CompoundTag();
        this.saveAdditional(tag);
        return tag;
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    private final LazyOptional<IItemHandler> itemHandlerCap = LazyOptional.of(() -> itemHandler);
    private final LazyOptional<IItemHandler> itemHandlerInputCap = LazyOptional.of(() -> itemHandlerInput);
    private final LazyOptional<IItemHandler> itemHandlerOutputCap = LazyOptional.of(() -> itemHandlerOutput);

    @Nonnull
    @Override
    public <T> LazyOptional<T> getCapability(@Nonnull Capability<T> cap, @Nullable Direction side) {
        if (cap == ForgeCapabilities.ITEM_HANDLER) {
            if (side == null) {
                return itemHandlerCap.cast();
            }
            return switch (side) {
                case UP -> itemHandlerInputCap.cast();
                case DOWN -> itemHandlerOutputCap.cast();
                default -> itemHandlerInputCap.cast();
            };
        }
        return super.getCapability(cap, side);
    }

    private static void craftItem(CrockPotBlockEntity entity) {
        Level level = entity.level;
        SimpleContainer inventory = new SimpleContainer(entity.itemHandler.getSlots());
        for (int i = 0; i < entity.itemHandler.getSlots(); i++) {
            inventory.setItem(i, entity.itemHandler.getStackInSlot(i));
        }

        // !!!
        Optional<CrockPotRecipe> match = level.getRecipeManager()
                .getRecipeFor(CrockPotRecipe.Type.INSTANCE, inventory, level);

        if(match.isPresent()) {
            entity.itemHandler.extractItem(0,1, false);
            entity.itemHandler.extractItem(1,1, false);
            entity.itemHandler.extractItem(2,1, false);
            entity.itemHandler.extractItem(3,1, false);
            entity.itemHandler.setStackInSlot(4, new ItemStack(match.get().getResultItem().getItem(),
                    entity.itemHandler.getStackInSlot(4).getCount() + 1));

            entity.resetProgress();
        }

    }

    private void resetProgress() {
        this.progress = 0;
    }

    private static boolean canInsertItemIntoOutputSlot(SimpleContainer inventory, ItemStack output) {
        return inventory.getItem(4).getItem() == output.getItem() || inventory.getItem(4).isEmpty();
    }

    private static boolean canInsertAmountIntoOutputSlot(SimpleContainer inventory) {
        return inventory.getItem(4).getMaxStackSize() > inventory.getItem(4).getCount();
    }

}
