package com.ladestitute.bewarethedark.blocks.entity;

import com.ladestitute.bewarethedark.entities.mobs.hostile.TentacleEntity;
import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import com.ladestitute.bewarethedark.registries.EntityInit;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.monster.*;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.entity.npc.WanderingTrader;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.AABB;

import javax.annotation.Nullable;
import java.util.List;

public class TentacleSpawnerBlockEntity extends BlockEntity {
    public int hastentacle = 0;
    public int spawnedtentacle = 0;
    public int spawncooldown = 0;

    protected final ContainerData data;

    public TentacleSpawnerBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityInit.TENTACLE_SPAWNER.get(), pos, state);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0:
                        return TentacleSpawnerBlockEntity.this.hastentacle;
                    case 1:
                        return TentacleSpawnerBlockEntity.this.spawnedtentacle;
                    case 2:
                        return TentacleSpawnerBlockEntity.this.spawncooldown;
                    default:
                        return 0;
                }
            }

            public void set(int index, int value) {
                switch (index) {
                    case 0:
                        TentacleSpawnerBlockEntity.this.hastentacle = value;
                        break;
                    case 1:
                        TentacleSpawnerBlockEntity.this.spawnedtentacle = value;
                    case 2:
                        TentacleSpawnerBlockEntity.this.spawncooldown = value;
                        break;
                }
            }

            public int getCount() {
                return 3;
            }
        };
    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, TentacleSpawnerBlockEntity entity) {
        if(entity.spawncooldown >= 1) {
            entity.spawncooldown--;
        }
        double xPos = (double) entity.getBlockPos().getX() + 0.5;
        double yPos = (double) entity.getBlockPos().getY() + 0.2;
        double zPos = (double) entity.getBlockPos().getZ() + 0.5;
        double xOffset = Mth.nextDouble(entity.level.random, -0.15, 0.15);
        double zOffset = Mth.nextDouble(entity.level.random, -0.15, 0.15);
        double bubbleradius = 3;
        double spawnradius = 2;
        AABB bubbleaabb = new AABB(entity.worldPosition).inflate(bubbleradius).expandTowards(0.0D, entity.level.getMaxBuildHeight(), 0.0D);
        List<LivingEntity> bubblelist = entity.level.getEntitiesOfClass(LivingEntity.class, bubbleaabb);
        AABB spawnaabb = new AABB(entity.worldPosition).inflate(spawnradius).expandTowards(0.0D, entity.getBlockPos().getY()+7, 0.0D);
        List<TentacleEntity> tentacles = entity.level.getEntitiesOfClass(TentacleEntity.class, spawnaabb);

        for (LivingEntity targets : bubblelist) {
            if (targets instanceof LivingEntity && tentacles.isEmpty()) {
                entity.level.addParticle(ParticleTypes.BUBBLE, xPos+0.2 + xOffset, yPos + 0.2,
                        zPos + zOffset, 0.0, 0.0, 0.0);
                entity.level.addParticle(ParticleTypes.BUBBLE, xPos + xOffset, yPos + 0.2,
                        zPos + zOffset, 0.0, 0.0, 0.0);
                entity.level.addParticle(ParticleTypes.BUBBLE, xPos + xOffset, yPos + 0.2,
                        zPos+0.2 + zOffset, 0.0, 0.0, 0.0);
            }
        }

        TentacleEntity tentacle = new TentacleEntity(EntityInit.TENTACLE.get(), pLevel);
        List<Player> players = entity.level.getEntitiesOfClass(Player.class, spawnaabb);
        List<Animal> animals = entity.level.getEntitiesOfClass(Animal.class, spawnaabb);
        List<Villager> villager = entity.level.getEntitiesOfClass(Villager.class, spawnaabb);
        List<WanderingTrader> trader = entity.level.getEntitiesOfClass(WanderingTrader.class, spawnaabb);
        List<Spider> spider = entity.level.getEntitiesOfClass(Spider.class, spawnaabb);
        List<Zombie> zombie = entity.level.getEntitiesOfClass(Zombie.class, spawnaabb);
        List<Skeleton> skeleton = entity.level.getEntitiesOfClass(Skeleton.class, spawnaabb);
        List<Creeper> creeper = entity.level.getEntitiesOfClass(Creeper.class, spawnaabb);
        List<Slime> slime = entity.level.getEntitiesOfClass(Slime.class, spawnaabb);
        List<EnderMan> enderman = entity.level.getEntitiesOfClass(EnderMan.class, spawnaabb);
        List<Witch> witch = entity.level.getEntitiesOfClass(Witch.class, spawnaabb);
        //Spawn if any of the entities above walk into its spawn range as long as one has no spawned already within the range
        if(!players.isEmpty() && tentacles.isEmpty()||
                !animals.isEmpty() && tentacles.isEmpty()||
                !villager.isEmpty() && tentacles.isEmpty()||
                !trader.isEmpty() && tentacles.isEmpty()||
                !spider.isEmpty() && tentacles.isEmpty()||
                !zombie.isEmpty() && tentacles.isEmpty()||
                !skeleton.isEmpty() && tentacles.isEmpty()||
                !creeper.isEmpty() && tentacles.isEmpty()||
                !slime.isEmpty() && tentacles.isEmpty()||
                !enderman.isEmpty() && tentacles.isEmpty()||
                !witch.isEmpty() && tentacles.isEmpty()) {
            if(entity.spawncooldown == 0) {
                tentacle.setPos(pPos.getX() + 0.5, pPos.getY(), pPos.getZ() + 0.5);
                pLevel.addFreshEntity(tentacle);
                pLevel.setBlockAndUpdate(pPos, pState);
                setChanged(pLevel, pPos, pState);
            }
        }
        //Check that there are no mobs around tentacles for them to despawn
                if(players.isEmpty() && animals.isEmpty() && villager.isEmpty() && trader.isEmpty() && spider.isEmpty()
                && zombie.isEmpty() && skeleton.isEmpty() && creeper.isEmpty()
                && slime.isEmpty() && enderman.isEmpty() && witch.isEmpty())
                {
                    for(TentacleEntity tenta : tentacles) {
                        tenta.discard();
                        pLevel.setBlockAndUpdate(pPos, pState);
                        setChanged(pLevel, pPos, pState);
                    }
                }
        }

        @Override
        public void load (CompoundTag tag){
            super.load(tag);
            hastentacle = tag.getInt("hastentacle");
            spawnedtentacle = tag.getInt("spawnedtentacle");
            spawncooldown = tag.getInt("spawncooldown");
        }

        @Override
        protected void saveAdditional (CompoundTag tag){
            super.saveAdditional(tag);
            tag.putInt("hastentacle", hastentacle);
            tag.putInt("spawnedtentacle", spawnedtentacle);
            tag.putInt("spawncooldown", spawncooldown);
        }

        @Override
        public CompoundTag getUpdateTag () {
            CompoundTag tag = new CompoundTag();
            this.saveAdditional(tag);
            return tag;
        }

        @Nullable
        @Override
        public Packet<ClientGamePacketListener> getUpdatePacket () {
            return ClientboundBlockEntityDataPacket.create(this);
        }

    }

