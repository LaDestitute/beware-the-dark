package com.ladestitute.bewarethedark.blocks.entity;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.blocks.world.SinkholeBlock;
import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class LightShaftBlockEntity extends BlockEntity {

    public LightShaftBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityInit.LIGHT_SHAFT.get(), pos, state);
    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, LightShaftBlockEntity entity)
    {
        if(entity.level.dimension().equals(BTDMain.CAVES_DIMENSION) && entity.level.isDay())
        {
            pState = pState.setValue(SinkholeBlock.LIT, true);
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
        }
        if(entity.level.dimension().equals(BTDMain.CAVES_DIMENSION) && !entity.level.isDay())
        {
            pState = pState.setValue(SinkholeBlock.LIT, false);
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
        }
    }
}
