package com.ladestitute.bewarethedark.blocks.entity;

import com.ladestitute.bewarethedark.blocks.helper.FirefliesSpawnerBlock;
import com.ladestitute.bewarethedark.blocks.light.PumpkinLanternBlock;
import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

import javax.annotation.Nullable;

public class PumpkinLanternBlockEntity extends BlockEntity {
    public int age;

    protected final ContainerData data;

    public PumpkinLanternBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityInit.PUMPKIN_LANTERN.get(), pos, state);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return PumpkinLanternBlockEntity.this.age;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: PumpkinLanternBlockEntity.this.age = value; break;
                }
            }

            public int getCount() {
                return 1;
            }
        };
    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, PumpkinLanternBlockEntity entity) {
        entity.age++;
        if(BTDConfig.getInstance().pumpkin_lantern_long_perish_time())
        {
            if(entity.age >= 170700)
            {
                pLevel.setBlockAndUpdate(pPos, Blocks.AIR.defaultBlockState());
            }
        }
        else if(entity.age >= 32000)
        {
            pLevel.setBlockAndUpdate(pPos, Blocks.AIR.defaultBlockState());
        }
        if(entity.level.isNight())
        {
            pState = pState.setValue(PumpkinLanternBlock.LIT, true);
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
        }
        //Turn off pumpkin lantern during day
        if(!entity.level.isNight())
        {
            pState = pState.setValue(PumpkinLanternBlock.LIT, false);
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
        }
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        age = tag.getInt("age");
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.putInt("age", age);
    }

    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = new CompoundTag();
        this.saveAdditional(tag);
        return tag;
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

}

