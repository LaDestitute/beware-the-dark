package com.ladestitute.bewarethedark.blocks.entity;

import com.ladestitute.bewarethedark.client.ClientSanityData;
import com.ladestitute.bewarethedark.entities.mobs.shadow.HostileCrawlingHorrorEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.PassiveCrawlingHorrorEntity;
import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import com.ladestitute.bewarethedark.registries.EntityInit;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

import javax.annotation.Nullable;

public class CrawlingHorrorSpawnerBlockEntity extends BlockEntity {
    public int hascrawlinghorror = 0;
    public int spawnedcrawlinghorror = 0;

    protected final ContainerData data;

    public CrawlingHorrorSpawnerBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityInit.CRAWLING_HORROR_SPAWNER.get(), pos, state);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return CrawlingHorrorSpawnerBlockEntity.this.hascrawlinghorror;
                    case 1: return CrawlingHorrorSpawnerBlockEntity.this.spawnedcrawlinghorror;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: CrawlingHorrorSpawnerBlockEntity.this.hascrawlinghorror = value; break;
                    case 1: CrawlingHorrorSpawnerBlockEntity.this.spawnedcrawlinghorror = value; break;
                }
            }

            public int getCount() {
                return 2;
            }
        };
    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, CrawlingHorrorSpawnerBlockEntity entity) {
        entity.hascrawlinghorror=1;
        PassiveCrawlingHorrorEntity crawlinghorror = new PassiveCrawlingHorrorEntity(EntityInit.PASSIVE_CRAWLING_HORROR.get(),
                pLevel);
        HostileCrawlingHorrorEntity hostilecrawlinghorror = new HostileCrawlingHorrorEntity(EntityInit.HOSTILE_CRAWLING_HORROR.get(),
                pLevel);
        if(entity.spawnedcrawlinghorror == 0 && ClientSanityData.getPlayerSanity() <= 9 && ClientSanityData.getPlayerSanity() > 3)
        {
            crawlinghorror.setPos(pPos.getX()+0.5, pPos.getY(), pPos.getZ()+0.5);
            pLevel.addFreshEntity(crawlinghorror);
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
            entity.spawnedcrawlinghorror=1;
        }
        if(entity.spawnedcrawlinghorror == 0 && ClientSanityData.getPlayerSanity() <= 3)
        {
            hostilecrawlinghorror.setPos(pPos.getX()+0.5, pPos.getY(), pPos.getZ()+0.5);
            pLevel.addFreshEntity(hostilecrawlinghorror);
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
            entity.spawnedcrawlinghorror=1;
        }
        //Remove nighthands upon day or regaining sanity
        if(entity.spawnedcrawlinghorror == 1 && ClientSanityData.getPlayerSanity() >= 10)
        {
            crawlinghorror.discard();
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
            entity.spawnedcrawlinghorror=0;
        }
        if(entity.spawnedcrawlinghorror == 1 && ClientSanityData.getPlayerSanity() >= 4)
        {
            hostilecrawlinghorror.discard();
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
            entity.spawnedcrawlinghorror=0;
        }
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        hascrawlinghorror = tag.getInt("hascrawlinghorror");
        spawnedcrawlinghorror = tag.getInt("spawnedcrawlinghorror");
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.putInt("hascrawlinghorror", hascrawlinghorror);
        tag.putInt("spawnedcrawlinghorror", spawnedcrawlinghorror);
    }

    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = new CompoundTag();
        this.saveAdditional(tag);
        return tag;
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

}
