package com.ladestitute.bewarethedark.blocks.entity;

import com.ladestitute.bewarethedark.blocks.helper.FirefliesSpawnerBlock;
import com.ladestitute.bewarethedark.blocks.light.WoodLanternBlock;
import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import com.ladestitute.bewarethedark.util.config.BTDConfig;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;

import javax.annotation.Nullable;

public class WoodLanternBlockEntity extends BlockEntity {
    public int fuel=9120;

    protected final ContainerData data;

    public WoodLanternBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityInit.WOOD_LANTERN.get(), pos, state);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return WoodLanternBlockEntity.this.fuel;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: WoodLanternBlockEntity.this.fuel = value; break;
                }
            }

            public int getCount() {
                return 1;
            }
        };
    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, WoodLanternBlockEntity entity) {
        if(entity.fuel < 0) {
            entity.fuel = 0;
        }
        if(entity.fuel > 24000) {
            entity.fuel = 24000;
        }
        if(pState.getValue(BlockStateProperties.LIT) && entity.fuel >= 1) {
            entity.fuel--;
        }
        if(entity.fuel == 0)
        {
            pState = pState.setValue(WoodLanternBlock.LIT, false);
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
        }
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        fuel = tag.getInt("fuel");
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.putInt("fuel", fuel);
    }

    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = new CompoundTag();
        this.saveAdditional(tag);
        return tag;
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

}


