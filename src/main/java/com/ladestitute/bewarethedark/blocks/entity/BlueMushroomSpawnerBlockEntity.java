package com.ladestitute.bewarethedark.blocks.entity;

import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import com.ladestitute.bewarethedark.registries.BlockInit;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

import javax.annotation.Nullable;

public class BlueMushroomSpawnerBlockEntity extends BlockEntity {
    public int hasmushroom;
    public int mushroompicked;
    public int regrowthtimer;

    protected final ContainerData data;

    public BlueMushroomSpawnerBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityInit.BLUE_MUSHROOM_SPAWNER.get(), pos, state);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return BlueMushroomSpawnerBlockEntity.this.hasmushroom;
                    case 1: return BlueMushroomSpawnerBlockEntity.this.regrowthtimer;
                    case 2: return BlueMushroomSpawnerBlockEntity.this.mushroompicked;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: BlueMushroomSpawnerBlockEntity.this.hasmushroom = value; break;
                    case 1: BlueMushroomSpawnerBlockEntity.this.regrowthtimer = value; break;
                    case 2: BlueMushroomSpawnerBlockEntity.this.mushroompicked = value; break;
                }
            }

            public int getCount() {
                return 3;
            }
        };
    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, BlueMushroomSpawnerBlockEntity entity) {
        BlockPos posAbove = pPos.above();
        BlockState blockStateAbove = pLevel.getBlockState(posAbove);
        Block above = blockStateAbove.getBlock();
        if(entity.regrowthtimer >= 1 && !pLevel.isRainingAt(pPos.above()))
        {
            entity.regrowthtimer--;
        }
        if(entity.regrowthtimer >= 1 && !pLevel.isRainingAt(pPos.above()))
        {
            entity.regrowthtimer--;
            entity.regrowthtimer--;
        }

        if(pLevel.getDayTime() >= 13000 && entity.hasmushroom == 0 && entity.mushroompicked ==0)
        {
            pLevel.setBlockAndUpdate(pPos.above(), BlockInit.BLUE_MUSHROOM.get().defaultBlockState());
            entity.hasmushroom=1;
        }
        if(pLevel.getDayTime() >= 13000 && entity.hasmushroom == 1 && above == Blocks.AIR)
        {
            entity.regrowthtimer=12000;
            entity.mushroompicked=1;
            entity.hasmushroom=0;
        }
        if(pLevel.getDayTime() >= 13000 && entity.mushroompicked == 1 && above == Blocks.AIR && entity.regrowthtimer==0)
        {
            pLevel.setBlockAndUpdate(pPos.above(), BlockInit.BLUE_MUSHROOM.get().defaultBlockState());
            entity.mushroompicked=0;
            entity.hasmushroom=1;
        }
        if(pLevel.getDayTime() < 13000)
        {
            pLevel.setBlockAndUpdate(pPos.above(), Blocks.AIR.defaultBlockState());
        }
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        hasmushroom = tag.getInt("hasmushroom");
        regrowthtimer = tag.getInt("regrowthtimer");
        mushroompicked = tag.getInt("mushroompicked");
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.putInt("hasmushroom", hasmushroom);
        tag.putInt("regrowthtimer", regrowthtimer);
        tag.putInt("mushroompicked", mushroompicked);
    }

    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = new CompoundTag();
        this.saveAdditional(tag);
        return tag;
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

}

