package com.ladestitute.bewarethedark.blocks.entity;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.blocks.world.SinkholeBlock;
import com.ladestitute.bewarethedark.entities.mobs.hostile.BatiliskEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.ShadowWatcherEntity;
import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import com.ladestitute.bewarethedark.registries.EntityInit;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

import javax.annotation.Nullable;

public class SinkholeBlockEntity extends BlockEntity {
    public int batiliskcap;
    public int batiliskspawntimer=262;

    protected final ContainerData data;

    public SinkholeBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityInit.SINKHOLE.get(), pos, state);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return SinkholeBlockEntity.this.batiliskcap;
                    case 1: return SinkholeBlockEntity.this.batiliskspawntimer;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: SinkholeBlockEntity.this.batiliskcap = value; break;
                    case 1: SinkholeBlockEntity.this.batiliskspawntimer = value; break;
                }
            }

            public int getCount() {
                return 2;
            }
        };
    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, SinkholeBlockEntity entity)
    {
        BatiliskEntity batilisk = new BatiliskEntity(EntityInit.BATILISK.get(), pLevel);
        if(entity.level.dimension().equals(BTDMain.CAVES_DIMENSION))
        {
            pState = pState.setValue(SinkholeBlock.LIT, true);
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
        }
        if(!entity.level.dimension().equals(BTDMain.CAVES_DIMENSION))
        {
            pState = pState.setValue(SinkholeBlock.LIT, false);
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
            if(entity.level.getBiome(entity.getBlockPos()).get().getBaseTemperature() <= 0.0)
            {
                if(pLevel.getDayTime() <= 11413 && pLevel.getDayTime() >= 0)
                {
                    entity.batiliskcap=0;
                    entity.batiliskspawntimer=0;
                }
                if(entity.batiliskspawntimer == 0 && pLevel.getDayTime() >= 11414 && entity.batiliskcap < 3)
                {
                    batilisk.setPos(pPos.getX()+0.5, pPos.getY(), pPos.getZ()+0.5);
                    pLevel.addFreshEntity(batilisk);
                    entity.batiliskcap= entity.batiliskcap+1;
                    entity.batiliskspawntimer=1048;
                }
                if(entity.batiliskspawntimer >= 1 && pLevel.getDayTime() >= 11414)
                {
                    entity.batiliskspawntimer--;
                }
            }
            if(entity.level.getBiome(entity.getBlockPos()).get().getBaseTemperature() >= 0.1) {
                if(pLevel.getDayTime() <= 11413 && pLevel.getDayTime() >= 0)
                {
                    entity.batiliskcap=0;
                    entity.batiliskspawntimer=0;
                }
                if (entity.batiliskspawntimer == 0 && pLevel.getDayTime() >= 11414 && entity.batiliskcap < 6) {
                    batilisk.setPos(pPos.getX() + 0.5, pPos.getY(), pPos.getZ() + 0.5);
                    pLevel.addFreshEntity(batilisk);
                    entity.batiliskcap = entity.batiliskcap + 1;
                    entity.batiliskspawntimer = 1048;
                }
                if (entity.batiliskspawntimer >= 1 && pLevel.getDayTime() >= 11414) {
                    entity.batiliskspawntimer--;
                }
            }
        }
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        batiliskcap = tag.getInt("batiliskcap");
        batiliskspawntimer = tag.getInt("batiliskspawntimer");
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.putInt("batiliskcap", batiliskcap);
        tag.putInt("batiliskspawntimer", batiliskspawntimer);
    }

    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = new CompoundTag();
        this.saveAdditional(tag);
        return tag;
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }
}
