package com.ladestitute.bewarethedark.blocks.entity;

import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import com.ladestitute.bewarethedark.registries.SoundInit;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Connection;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.sounds.SoundSource;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class CampFireTileEntity extends BlockEntity {
    public int fuel;
    public int burnout;
    public int maxfuel;
    public int timesincelastfuel;
    private Player Player;

    public CampFireTileEntity(BlockPos pos, BlockState state) {
        super(BlockEntityInit.CAMP_FIRE.get(), pos, state);

        /**
         * We start off with 100 fuel (remember, this is in ticks!) so we have a grace-period of five seconds when the campfire is placed
         * Times to be adjusted, still testing but the fuel-times of the Fire Pit will be more than the Campfire to make up for it costing more in materials
         */
        this.fuel = 0;
        /**
         * 9000 is four minutes and 30 seconds
         */
        this.burnout = 6750;
        /**
         * 18000 is 15 minutes
         */
        this.maxfuel = 13500;
        /**
         * This int only increases when fuel is zero and will make a campfire despawn when timesincelastfuel=burnout
         * Fire Pits retain state successfully now, but there's a little quirk of them flashing 'on' if out of fuel on re-log
         * This is probably due to this int, since it's used to set the state instead of fuel being zero doing it
         * As we want a grace period before they go out, whether it's max fuel or the general time they have before going out upon being spawned and not fueled period
         */
        this.timesincelastfuel = 0;
    }

    @Override
    public ClientboundBlockEntityDataPacket getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

    @Override
    public CompoundTag getUpdateTag() {
        return saveWithFullMetadata();
    }

    @Override
    public void handleUpdateTag(CompoundTag tag) {
        load(tag);
    }

    @Override
    public void load(CompoundTag nbt) {
        super.load(nbt);
        this.fuel = nbt.getInt("Fuel");
        this.burnout = nbt.getInt("Burnout");
        this.maxfuel = nbt.getInt("Max Fuel");
        this.timesincelastfuel = nbt.getInt("Time Since Last Fuel");

    }

    @Override
    public void onDataPacket(Connection net, ClientboundBlockEntityDataPacket pkt) {
        load(pkt.getTag());
    }

    @Override
    public void onLoad() {
        super.onLoad();
    }

    @Override
    protected void saveAdditional(CompoundTag nbt) {
        nbt.putInt("Fuel", this.fuel);
        nbt.putInt("Burnout", this.burnout);
        nbt.putInt("Max Fuel", this.maxfuel);
        nbt.putInt("Time Since Last Fuel", this.timesincelastfuel);
        super.saveAdditional(nbt);
    }

    public void tick() {
        Player player = Player;
        if(fuel > maxfuel)
        {
            fuel = maxfuel;
        }
        /**
         * To be changed, this was just to make sure it would go out correctly AND retain state on relog
         */
        if(timesincelastfuel >= burnout)
        {
            ItemStack ashstack = new ItemStack(ItemInit.ASHES.get());
            ItemEntity ashes = new ItemEntity(this.level, this.worldPosition.getX(), this.worldPosition.getY(), this.worldPosition.getZ(),ashstack);
            this.level.playSound(player, this.getBlockPos(), SoundInit.FIRE_OUT.get(), SoundSource.BLOCKS, 1F, 1F);
            this.level.setBlock(this.worldPosition, Blocks.AIR.defaultBlockState(), 2);
            this.level.addFreshEntity(ashes);
        }
        if(fuel > 0)
        {
            if(this.level.isRainingAt(this.getBlockPos())) {
                fuel--;
                fuel--;
                fuel--;
            }
            else
                fuel--;
        }
        /**
         * This is a temp-mechanic until we can get variable-light levels working based on the fuel
         */
        if(fuel == 0)
        {
            timesincelastfuel++;
        }

    }
}

