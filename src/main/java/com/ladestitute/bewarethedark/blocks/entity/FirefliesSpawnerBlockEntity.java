package com.ladestitute.bewarethedark.blocks.entity;

import com.ladestitute.bewarethedark.blocks.helper.FirefliesSpawnerBlock;
import com.ladestitute.bewarethedark.entities.mobs.passive.FireflyEntity;
import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import com.ladestitute.bewarethedark.registries.EntityInit;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

import javax.annotation.Nullable;

public class FirefliesSpawnerBlockEntity extends BlockEntity {
    public int hasfirefly = 0;
    public int spawnedfirefly = 0;

    protected final ContainerData data;

    public FirefliesSpawnerBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityInit.FIREFLIES.get(), pos, state);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return FirefliesSpawnerBlockEntity.this.hasfirefly;
                    case 1: return FirefliesSpawnerBlockEntity.this.spawnedfirefly;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: FirefliesSpawnerBlockEntity.this.hasfirefly = value; break;
                    case 1: FirefliesSpawnerBlockEntity.this.spawnedfirefly = value; break;
                }
            }

            public int getCount() {
                return 2;
            }
        };
    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, FirefliesSpawnerBlockEntity entity) {
        entity.hasfirefly=1;
        FireflyEntity firefly = new FireflyEntity(EntityInit.FIREFLY.get(), pLevel);
      if(entity.spawnedfirefly == 0 && entity.level.isNight())
      {
          firefly.setPos(pPos.getX()+0.5, pPos.getY(), pPos.getZ()+0.5);
          pLevel.addFreshEntity(firefly);
      //    pState = pState.setValue(FirefliesSpawnerBlock.LIT, true);
       //   pLevel.setBlockAndUpdate(pPos, pState);
          setChanged(pLevel, pPos, pState);
          entity.spawnedfirefly=1;
      }
      //Remove fireflies upon day
        if(entity.spawnedfirefly == 1 && !entity.level.isNight())
        {
            firefly.discard();
         //   pState = pState.setValue(FirefliesSpawnerBlock.LIT, false);
          //  pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
            entity.spawnedfirefly=0;
        }
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        hasfirefly = tag.getInt("hasfirefly");
        spawnedfirefly = tag.getInt("spawnedfirefly");
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.putInt("hasfirefly", hasfirefly);
        tag.putInt("spawnedfirefly", spawnedfirefly);
    }

    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = new CompoundTag();
        this.saveAdditional(tag);
        return tag;
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

}
