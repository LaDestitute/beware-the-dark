package com.ladestitute.bewarethedark.blocks.entity;

import com.ladestitute.bewarethedark.client.ClientSanityData;
import com.ladestitute.bewarethedark.entities.mobs.shadow.ShadowWatcherEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.TerrorbeakEntity;
import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import com.ladestitute.bewarethedark.registries.EntityInit;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

import javax.annotation.Nullable;

public class TerrorbeakSpawnerBlockEntity extends BlockEntity {
    public int hasterrorbeak = 0;
    public int spawnedterrorbeak = 0;

    protected final ContainerData data;

    public TerrorbeakSpawnerBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityInit.TERRORBEAK_SPAWNER.get(), pos, state);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return TerrorbeakSpawnerBlockEntity.this.hasterrorbeak;
                    case 1: return TerrorbeakSpawnerBlockEntity.this.spawnedterrorbeak;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: TerrorbeakSpawnerBlockEntity.this.hasterrorbeak = value; break;
                    case 1: TerrorbeakSpawnerBlockEntity.this.spawnedterrorbeak = value; break;
                }
            }

            public int getCount() {
                return 2;
            }
        };
    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, TerrorbeakSpawnerBlockEntity entity) {
        entity.hasterrorbeak=1;
        TerrorbeakEntity terrorbeak = new TerrorbeakEntity(EntityInit.TERRORBEAK.get(), pLevel);
        if(entity.spawnedterrorbeak == 0 && ClientSanityData.getPlayerSanity() <= 3)
        {
            terrorbeak.setPos(pPos.getX()+0.5, pPos.getY(), pPos.getZ()+0.5);
            pLevel.addFreshEntity(terrorbeak);
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
            entity.spawnedterrorbeak=1;
        }
        //Remove nighthands upon day or regaining sanity
        if(entity.spawnedterrorbeak == 1 && ClientSanityData.getPlayerSanity() > 3)
        {
            terrorbeak.discard();
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
            entity.spawnedterrorbeak=0;
        }
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        hasterrorbeak = tag.getInt("hasterrorbeak");
        spawnedterrorbeak = tag.getInt("spawnedterrorbeak");
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.putInt("hasterrorbeak", hasterrorbeak);
        tag.putInt("spawnedterrorbeak", spawnedterrorbeak);
    }

    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = new CompoundTag();
        this.saveAdditional(tag);
        return tag;
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

}
