package com.ladestitute.bewarethedark.blocks.entity;

import com.ladestitute.bewarethedark.blocks.helper.FirefliesSpawnerBlock;
import com.ladestitute.bewarethedark.client.ClientSanityData;
import com.ladestitute.bewarethedark.entities.mobs.passive.FireflyEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.MrSkittsEntity;
import com.ladestitute.bewarethedark.entities.mobs.shadow.NightHandEntity;
import com.ladestitute.bewarethedark.registries.BlockEntityInit;
import com.ladestitute.bewarethedark.registries.EntityInit;
import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.protocol.Packet;
import net.minecraft.network.protocol.game.ClientGamePacketListener;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.inventory.ContainerData;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

import javax.annotation.Nullable;

public class MrSkittsSpawnerBlockEntity extends BlockEntity {
    public int hasmrskitts = 0;
    public int spawnedmrskitts = 0;

    protected final ContainerData data;

    public MrSkittsSpawnerBlockEntity(BlockPos pos, BlockState state) {
        super(BlockEntityInit.MR_SKITTS_SPAWNER.get(), pos, state);
        this.data = new ContainerData() {
            public int get(int index) {
                switch (index) {
                    case 0: return MrSkittsSpawnerBlockEntity.this.hasmrskitts;
                    case 1: return MrSkittsSpawnerBlockEntity.this.spawnedmrskitts;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: MrSkittsSpawnerBlockEntity.this.hasmrskitts = value; break;
                    case 1: MrSkittsSpawnerBlockEntity.this.spawnedmrskitts = value; break;
                }
            }

            public int getCount() {
                return 2;
            }
        };
    }

    public static void tick(Level pLevel, BlockPos pPos, BlockState pState, MrSkittsSpawnerBlockEntity entity) {
        entity.hasmrskitts=1;
        MrSkittsEntity mrskitts = new MrSkittsEntity(EntityInit.MR_SKITTS.get(), pLevel);
        if(entity.spawnedmrskitts == 0 && ClientSanityData.getPlayerSanity() <= 15)
        {
            mrskitts.setPos(pPos.getX()+0.5, pPos.getY(), pPos.getZ()+0.5);
            pLevel.addFreshEntity(mrskitts);
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
            entity.spawnedmrskitts=1;
        }
        //Remove nighthands upon day or regaining sanity
        if(entity.spawnedmrskitts == 1 && ClientSanityData.getPlayerSanity() >= 16)
        {
            mrskitts.discard();
            pLevel.setBlockAndUpdate(pPos, pState);
            setChanged(pLevel, pPos, pState);
            entity.spawnedmrskitts=0;
        }
    }

    @Override
    public void load(CompoundTag tag) {
        super.load(tag);
        hasmrskitts = tag.getInt("hasmrskitts");
        spawnedmrskitts = tag.getInt("spawnedmrskitts");
    }

    @Override
    protected void saveAdditional(CompoundTag tag) {
        super.saveAdditional(tag);
        tag.putInt("hasmrskitts", hasmrskitts);
        tag.putInt("spawnedmrskitts", spawnedmrskitts);
    }

    @Override
    public CompoundTag getUpdateTag() {
        CompoundTag tag = new CompoundTag();
        this.saveAdditional(tag);
        return tag;
    }

    @Nullable
    @Override
    public Packet<ClientGamePacketListener> getUpdatePacket() {
        return ClientboundBlockEntityDataPacket.create(this);
    }

}
