package com.ladestitute.bewarethedark.blocks.world;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.registries.BlockInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import net.minecraft.core.Direction;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.core.BlockPos;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;

import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

public class GoldVeinBoulderBlock extends HorizontalDirectionalBlock {
    // Boulder with gold veins, drops rocks, flint and gold nuggets
    // Rarer than boulders

    public GoldVeinBoulderBlock(BlockBehaviour.Properties properties) {
        super(properties);
        registerDefaultState(defaultBlockState().setValue(FACING, Direction.NORTH));
        runCalculation(SHAPE.orElse(Shapes.block()));
    }

    protected void runCalculation(VoxelShape shape) {
        for (Direction direction : Direction.values())
            SHAPES.put(direction, BTDMain.calculateShapes(direction, shape));
    }

    protected boolean mayPlaceOn(BlockState p_200014_1_, BlockGetter p_200014_2_, BlockPos p_200014_3_) {
        return p_200014_1_.is(Blocks.GRASS_BLOCK) ||
                p_200014_1_.is(BlockInit.FOREST_TURF.get())||
                p_200014_1_.is(BlockInit.DECIDUOUS_TURF.get())||
                p_200014_1_.is(BlockInit.MARSH_TURF.get())||
                p_200014_1_.is(Blocks.PODZOL)||
                p_200014_1_.is(Blocks.COARSE_DIRT)||
                p_200014_1_.is(BlockInit.ROCKY_TURF.get());
    }

    @Override
    public boolean canSurvive(BlockState p_51028_, LevelReader p_51029_, BlockPos p_51030_) {
        BlockPos blockpos = p_51030_.below();
        return this.mayPlaceOn(p_51029_.getBlockState(blockpos), p_51029_, blockpos);
    }

    private static final Map<Direction, VoxelShape> SHAPES = new EnumMap<>(Direction.class);

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(FACING);
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context) {
        return SHAPES.get(state.getValue(FACING));
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
    }

    private static final Optional<VoxelShape> SHAPE = Stream
            .of(Block.box(4, 0, 3, 5, 4, 4),
                    Block.box(3, 0, 4, 4, 4, 5),
                    Block.box(3, 0, 11, 4, 4, 12),
                    Block.box(4, 0, 12, 5, 4, 13),
                    Block.box(11, 0, 12, 12, 4, 13),
                    Block.box(12, 0, 11, 13, 4, 12),
                    Block.box(12, 0, 4, 13, 4, 5),
                    Block.box(11, 0, 3, 12, 4, 4),
                    Block.box(13, 0, 5, 14, 4, 11),
                    Block.box(2, 0, 5, 3, 4, 11),
                    Block.box(5, 4, 12, 11, 5, 13),
                    Block.box(5, 0, 13, 11, 4, 14),
                    Block.box(5, 0, 2, 11, 4, 3),
                    Block.box(5, 4, 3, 11, 5, 4),
                    Block.box(12, 4, 5, 13, 5, 11),
                    Block.box(3, 4, 5, 4, 5, 11),
                    Block.box(4, 4, 4, 5, 5, 5),
                    Block.box(4, 4, 11, 5, 5, 12),
                    Block.box(11, 4, 11, 12, 5, 12),
                    Block.box(11, 4, 4, 12, 5, 5),
                    Block.box(4, 5, 6, 5, 6, 10),
                    Block.box(11, 5, 6, 12, 6, 10),
                    Block.box(6, 5, 11, 10, 6, 12),
                    Block.box(6, 5, 4, 10, 6, 5),
                    Block.box(4, 5, 5, 5, 6, 6),
                    Block.box(4, 5, 10, 5, 6, 11),
                    Block.box(5, 5, 11, 6, 6, 12),
                    Block.box(5, 5, 4, 6, 6, 5),
                    Block.box(10, 5, 4, 11, 6, 5),
                    Block.box(11, 5, 5, 12, 6, 6),
                    Block.box(11, 5, 10, 12, 6, 11),
                    Block.box(10, 5, 11, 11, 6, 12),
                    Block.box(6, 6, 5, 10, 7, 6),
                    Block.box(6, 6, 10, 10, 7, 11),
                    Block.box(5, 6, 6, 6, 7, 10),
                    Block.box(10, 6, 6, 11, 7, 10),
                    Block.box(10, 5, 10, 11, 6, 11),
                    Block.box(10, 5, 5, 11, 6, 6),
                    Block.box(5, 5, 10, 6, 6, 11),
                    Block.box(5, 5, 5, 6, 6, 6),
                    Block.box(6, 7, 7, 7, 8, 9),
                    Block.box(9, 7, 7, 10, 8, 9),
                    Block.box(7, 7, 9, 9, 8, 10),
                    Block.box(7, 7, 6, 9, 8, 7),
                    Block.box(6, 6, 6, 7, 7, 7),
                    Block.box(6, 6, 9, 7, 7, 10),
                    Block.box(9, 6, 9, 10, 7, 10),
                    Block.box(9, 6, 6, 10, 7, 7),
                    Block.box(7, 8, 7, 9, 9, 9),
                    Block.box(5, 0, 3, 11, 1, 13),
                    Block.box(11, 0, 5, 13, 1, 11),
                    Block.box(3, 0, 5, 5, 1, 11),
                    Block.box(11, 0, 4, 12, 1, 5),
                    Block.box(4, 0, 4, 5, 1, 5),
                    Block.box(11, 0, 11, 12, 1, 12),
                    Block.box(4, 0, 11, 5, 1, 12))
            .reduce((v1, v2) -> Shapes.join(v1, v2, BooleanOp.OR));

    @Override
    public boolean onDestroyedByPlayer(BlockState state, Level world, BlockPos pos, Player player, boolean willHarvest, FluidState fluid) {
        ItemStack pickaxestack = player.getItemBySlot(EquipmentSlot.MAINHAND);
        Random rand = new Random();
        int flintbonusdrop = rand.nextInt(100);
        int nitrebonusdrop = rand.nextInt(100);
        ItemStack rockstack1 = new ItemStack(ItemInit.ROCKS.get());
        ItemEntity rock1 = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), rockstack1);
        ItemStack rockstack2 = new ItemStack(ItemInit.ROCKS.get());
        ItemEntity rock2 = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), rockstack2);
        ItemStack rockstack3 = new ItemStack(ItemInit.ROCKS.get());
        ItemEntity rock3 = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), rockstack3);
        ItemStack flintstack = new ItemStack(Items.FLINT);
        ItemEntity flint = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), flintstack);
        ItemStack bonusflintstack = new ItemStack(Items.FLINT);
        ItemEntity bonusflint = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), bonusflintstack);
        ItemStack goldstack = new ItemStack(Items.GOLD_NUGGET);
        ItemEntity gold = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), goldstack);
        ItemStack bonusgoldstack = new ItemStack(Items.GOLD_NUGGET);
        ItemEntity bonusgold = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), bonusgoldstack);

        if(pickaxestack.getItem() == ItemInit.FLINT_PICKAXE.get() ||
                pickaxestack.getItem() == ItemInit.OPULENT_PICKAXE.get())
        {
            world.addFreshEntity(rock1);
            world.addFreshEntity(rock2);
            world.addFreshEntity(rock3);
            world.addFreshEntity(flint);
            world.addFreshEntity(gold);
            if(flintbonusdrop < 60)
            {
                world.addFreshEntity(bonusflint);
            }
            if(nitrebonusdrop < 25)
            {
                world.addFreshEntity(bonusgold);
            }
        }
        return super.onDestroyedByPlayer(state, world, pos, player, willHarvest, fluid);
    }
}

