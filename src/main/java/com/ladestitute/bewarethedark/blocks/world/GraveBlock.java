package com.ladestitute.bewarethedark.blocks.world;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.btdcapabilities.sanity.PlayerSanityProvider;
import com.ladestitute.bewarethedark.entities.mobs.hostile.GhostEntity;
import com.ladestitute.bewarethedark.registries.BlockInit;
import com.ladestitute.bewarethedark.registries.EntityInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Registry;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.TagKey;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition.Builder;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

public class GraveBlock extends HorizontalDirectionalBlock {

    public GraveBlock(BlockBehaviour.Properties properties) {
        super(properties);
        runCalculation(SHAPE.orElse(Shapes.block()));
    }

    protected void runCalculation(VoxelShape shape) {
        for (Direction direction : Direction.values())
            SHAPES.put(direction, BTDMain.calculateShapes(direction, shape));
    }

    private static final Map<Direction, VoxelShape> SHAPES = new EnumMap<>(Direction.class);

    protected boolean mayPlaceOn(BlockState p_200014_1_, BlockGetter p_200014_2_, BlockPos p_200014_3_) {
        return p_200014_1_.is(Blocks.GRASS_BLOCK) ||
                p_200014_1_.is(BlockInit.FOREST_TURF.get())||
                p_200014_1_.is(BlockInit.DECIDUOUS_TURF.get())||
                p_200014_1_.is(BlockInit.MARSH_TURF.get())||
                p_200014_1_.is(Blocks.PODZOL)||
                p_200014_1_.is(Blocks.COARSE_DIRT)||
                p_200014_1_.is(BlockInit.ROCKY_TURF.get());
    }

    @Override
    public boolean canSurvive(BlockState p_51028_, LevelReader p_51029_, BlockPos p_51030_) {
        BlockPos blockpos = p_51030_.below();
        return this.mayPlaceOn(p_51029_.getBlockState(blockpos), p_51029_, blockpos);
    }

    @Override
    protected void createBlockStateDefinition(Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(FACING);
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context) {
        return SHAPES.get(state.getValue(FACING));
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
    }

    private static final Optional<VoxelShape> SHAPE = Stream
            .of(Block.box(2, 0, 2, 14, 6, 14))
            .reduce((v1, v2) -> Shapes.join(v1, v2, BooleanOp.OR));

    private static final TagKey<Item> gravedrops = TagKey.create(Registry.ITEM_REGISTRY, new ResourceLocation(BTDMain.MOD_ID, "gravedrops"));

    private int ghostspawnchance;

    private int spawndelay;

    @Override
    public void randomTick(BlockState p_222954_, ServerLevel p_222955_, BlockPos p_222956_, RandomSource p_222957_) {
        if(p_222955_.getMoonPhase() == 0 && p_222955_.getDayTime() >= 13000)
        {
            spawndelay++;
            if(spawndelay == 50) {
                GhostEntity ghost = new GhostEntity(EntityInit.GHOST.get(), p_222955_);
                ghost.setPos(p_222956_.getX(), p_222956_.getY() + 1, p_222956_.getZ());
                p_222955_.addFreshEntity(ghost);
                spawndelay=0;
            }
        }
        super.randomTick(p_222954_, p_222955_, p_222956_, p_222957_);
    }

    @Override
    public InteractionResult use(BlockState p_60503_, Level p_60504_, BlockPos p_60505_, Player p_60506_, InteractionHand p_60507_, BlockHitResult p_60508_) {
        ItemStack handstack = p_60506_.getItemBySlot(EquipmentSlot.MAINHAND);
        if(handstack.getItem() == ItemInit.FLINT_SHOVEL.get()||
                handstack.getItem() == ItemInit.REGAL_SHOVEL.get())
        {
            ItemHandlerHelper.giveItemToPlayer(p_60506_, ForgeRegistries.ITEMS.tags().getTag(gravedrops).getRandomElement(p_60504_.getRandom()).get().getDefaultInstance());
            p_60506_.getCapability(PlayerSanityProvider.PLAYER_SANITY).ifPresent(sanity -> {
                sanity.subSanity(1);
            });
            p_60506_.getMainHandItem().hurtAndBreak(4, p_60506_, (p_220045_0_) -> {
                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
            });
            GhostEntity ghost = new GhostEntity(EntityInit.GHOST.get(), p_60504_);
            ghost.setPos(p_60505_.getX(), p_60505_.getY()+1, p_60505_.getZ());
            Random rand = new Random();
            int ghostspawnchance = rand.nextInt(10);
            if(ghostspawnchance == 0)
            {
                p_60504_.addFreshEntity(ghost);
            }
            p_60504_.setBlock(p_60505_, BlockInit.DUG_GRAVE.get().defaultBlockState().setValue(FACING,
                    p_60503_.getValue(FACING)), 2);
        }
        return super.use(p_60503_, p_60504_, p_60505_, p_60506_, p_60507_, p_60508_);
    }
}

