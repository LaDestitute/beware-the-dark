package com.ladestitute.bewarethedark.blocks.world;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.registries.BlockInit;
import net.minecraft.core.Direction;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.core.BlockPos;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;

import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class WorldFlintBlock extends HorizontalDirectionalBlock {

    public WorldFlintBlock(BlockBehaviour.Properties properties) {
        super(properties);
        registerDefaultState(defaultBlockState().setValue(FACING, Direction.NORTH));
        runCalculation(SHAPE.orElse(Shapes.block()));
    }

    protected void runCalculation(VoxelShape shape) {
        for (Direction direction : Direction.values())
            SHAPES.put(direction, BTDMain.calculateShapes(direction, shape));
    }

    protected boolean mayPlaceOn(BlockState p_200014_1_, BlockGetter p_200014_2_, BlockPos p_200014_3_) {
        return p_200014_1_.is(Blocks.GRASS_BLOCK) ||
                p_200014_1_.is(BlockInit.FOREST_TURF.get())||
                p_200014_1_.is(BlockInit.DECIDUOUS_TURF.get())||
                p_200014_1_.is(BlockInit.MARSH_TURF.get())||
                p_200014_1_.is(Blocks.PODZOL)||
                p_200014_1_.is(Blocks.COARSE_DIRT)||
                p_200014_1_.is(BlockInit.ROCKY_TURF.get());
    }

    @Override
    public boolean canSurvive(BlockState p_51028_, LevelReader p_51029_, BlockPos p_51030_) {
        BlockPos blockpos = p_51030_.below();
        return this.mayPlaceOn(p_51029_.getBlockState(blockpos), p_51029_, blockpos);
    }

    private static final Map<Direction, VoxelShape> SHAPES = new EnumMap<>(Direction.class);

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(FACING);
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context) {
        return SHAPES.get(state.getValue(FACING));
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
    }

    private static final Optional<VoxelShape> SHAPE = Stream
            .of(Block.box(4.737500000000001, 0, 8.0375, 5.737500000000001, 1, 9.0375),
                    Block.box(6.737500000000001, 0, 10.0375, 7.737500000000001, 1, 11.0375),
                    Block.box(9.7375, 0, 7.0375, 10.7375, 1, 8.0375),
                    Block.box(7.737500000000001, 0, 5.0375, 8.7375, 1, 6.0375),
                    Block.box(9.7375, 0, 5.0375, 10.7375, 1, 6.0375),
                    Block.box(4.737500000000001, 0, 10.0375, 5.737500000000001, 1, 11.0375),
                    Block.box(6.737500000000001, 0, 6.0375, 7.737500000000001, 1, 7.0375),
                    Block.box(5.737500000000001, 0, 7.0375, 6.737500000000001, 1, 8.0375),
                    Block.box(7.737500000000001, 0, 9.0375, 8.7375, 1, 10.0375),
                    Block.box(8.7375, 0, 8.0375, 9.7375, 1, 9.0375),
                    Block.box(9.7375, 0, 6.0375, 10.7375, 1, 7.0375),
                    Block.box(8.7375, 0, 5.0375, 9.7375, 1, 6.0375),
                    Block.box(5.737500000000001, 0, 10.0375, 6.737500000000001, 1, 11.0375),
                    Block.box(4.737500000000001, 0, 9.0375, 5.737500000000001, 1, 10.0375),
                    Block.box(7.737500000000001, 0, 6.0375, 9.7375, 2, 8.0375),
                    Block.box(5.737500000000001, 0, 8.0375, 7.737500000000001, 2, 10.0375),
                    Block.box(7.737500000000001, 0, 8.0375, 8.7375, 2, 9.0375),
                    Block.box(6.737500000000001, 0, 7.0375, 7.737500000000001, 2, 8.0375))
            .reduce((v1, v2) -> Shapes.join(v1, v2, BooleanOp.OR));


    @Override
    public boolean onDestroyedByPlayer(BlockState state, Level world, BlockPos pos, Player player, boolean willHarvest, FluidState fluid) {
        ItemStack rockstack1 = new ItemStack(Items.FLINT);
        ItemEntity rock1 = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), rockstack1);
        world.addFreshEntity(rock1);
        return super.onDestroyedByPlayer(state, world, pos, player, willHarvest, fluid);
    }
}
