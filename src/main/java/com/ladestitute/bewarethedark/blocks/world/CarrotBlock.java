package com.ladestitute.bewarethedark.blocks.world;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.registries.BlockInit;
import net.minecraft.core.Direction;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.core.BlockPos;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;

import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

public class CarrotBlock extends HorizontalDirectionalBlock {

    public CarrotBlock(BlockBehaviour.Properties properties) {
        super(properties);
        registerDefaultState(defaultBlockState().setValue(FACING, Direction.NORTH));
        runCalculation(SHAPE.orElse(Shapes.block()));
    }

    protected void runCalculation(VoxelShape shape) {
        for (Direction direction : Direction.values())
            SHAPES.put(direction, BTDMain.calculateShapes(direction, shape));
    }

    private static final Map<Direction, VoxelShape> SHAPES = new EnumMap<>(Direction.class);

    @Override
    protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder) {
        super.createBlockStateDefinition(builder);
        builder.add(FACING);
    }

    @Override
    public VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context) {
        return SHAPES.get(state.getValue(FACING));
    }

    @Override
    public BlockState getStateForPlacement(BlockPlaceContext context) {
        return defaultBlockState().setValue(FACING, context.getHorizontalDirection().getOpposite());
    }

    protected boolean mayPlaceOn(BlockState p_200014_1_, BlockGetter p_200014_2_, BlockPos p_200014_3_) {
        return p_200014_1_.is(Blocks.GRASS_BLOCK) ||
                p_200014_1_.is(BlockInit.FOREST_TURF.get())||
                p_200014_1_.is(BlockInit.DECIDUOUS_TURF.get())||
                p_200014_1_.is(BlockInit.MARSH_TURF.get())||
                p_200014_1_.is(Blocks.MUD)||
                p_200014_1_.is(Blocks.PODZOL)||
                p_200014_1_.is(Blocks.COARSE_DIRT)||
                p_200014_1_.is(BlockInit.RED_FUNGAL_TURF.get())||
                p_200014_1_.is(BlockInit.GREEN_FUNGAL_TURF.get())||
                p_200014_1_.is(BlockInit.BLUE_FUNGAL_TURF.get());
    }

    @Override
    public boolean canSurvive(BlockState p_51028_, LevelReader p_51029_, BlockPos p_51030_) {
        BlockPos blockpos = p_51030_.below();
        return this.mayPlaceOn(p_51029_.getBlockState(blockpos), p_51029_, blockpos);
    }

    private static final Optional<VoxelShape> SHAPE = Stream
            .of(Block.box(8.004391111111106, 0, 6.995608888888892, 9.004391111111106, 1, 7.995608888888892),
                    Block.box(8.004391111111106, 0, 5.995608888888892, 9.004391111111106, 0.72, 6.995608888888892),
                    Block.box(7.004391111111107, 0, 5.995608888888892, 8.004391111111106, 0.72, 6.995608888888892),
                    Block.box(6.004391111111107, 0, 6.995608888888892, 7.004391111111107, 0.72, 7.995608888888892),
                    Block.box(6.004391111111107, 0, 7.995608888888892, 7.004391111111107, 0.72, 8.995608888888892),
                    Block.box(7.004391111111107, 0, 8.995608888888892, 8.004391111111106, 0.72, 9.995608888888892),
                    Block.box(8.004391111111106, 0, 8.995608888888892, 9.004391111111106, 0.72, 9.995608888888892),
                    Block.box(9.004391111111106, 0, 7.995608888888892, 10.004391111111106, 0.72, 8.995608888888892),
                    Block.box(9.004391111111106, 0, 6.995608888888892, 10.004391111111106, 0.72, 7.995608888888892),
                    Block.box(9.004391111111106, 0, 5.995608888888892, 10.004391111111106, 0.36, 6.995608888888892),
                    Block.box(6.004391111111107, 0, 5.995608888888892, 7.004391111111107, 0.36, 6.995608888888892),
                    Block.box(6.004391111111107, 0, 8.995608888888892, 7.004391111111107, 0.36, 9.995608888888892),
                    Block.box(9.004391111111106, 0, 8.995608888888892, 10.004391111111106, 0.36, 9.995608888888892),
                    Block.box(8.004391111111106, -1, 6.995608888888892, 9.004391111111106, 0, 7.995608888888892),
                    Block.box(7.004391111111107, 0, 6.995608888888892, 8.004391111111106, 1, 7.995608888888892),
                    Block.box(7.004391111111107, -1, 6.995608888888892, 8.004391111111106, 0, 7.995608888888892),
                    Block.box(7.004391111111107, 0, 7.995608888888892, 8.004391111111106, 1, 8.995608888888892),
                    Block.box(7.004391111111107, -1, 7.995608888888892, 8.004391111111106, 0, 8.995608888888892),
                    Block.box(8.004391111111106, 0, 7.995608888888892, 9.004391111111106, 1, 8.995608888888892),
                    Block.box(8.004391111111106, 1, 7.995608888888892, 9.004391111111106, 2, 8.995608888888892),
                    Block.box(8.004391111111106, 1, 6.995608888888892, 9.004391111111106, 2, 7.995608888888892),
                    Block.box(7.004391111111107, 1, 6.995608888888892, 8.004391111111106, 2, 7.995608888888892),
                    Block.box(7.004391111111107, 2, 6.995608888888892, 8.004391111111106, 3, 7.995608888888892),
                    Block.box(8.004391111111106, 2, 6.995608888888892, 9.004391111111106, 3, 7.995608888888892),
                    Block.box(8.004391111111106, 2, 7.995608888888892, 9.004391111111106, 3, 8.995608888888892),
                    Block.box(8.004391111111106, 3, 7.995608888888892, 9.004391111111106, 4, 8.995608888888892),
                    Block.box(8.004391111111106, 3, 6.995608888888892, 9.004391111111106, 4, 7.995608888888892),
                    Block.box(7.004391111111107, 3, 6.995608888888892, 8.004391111111106, 4, 7.995608888888892),
                    Block.box(7.004391111111107, 4, 6.995608888888892, 8.004391111111106, 4.5, 7.995608888888892),
                    Block.box(6.831591111111106, 2.974400000000002, 6.995608888888892, 7.328391111111106, 3.974400000000002, 7.995608888888892),
                    Block.box(8.004391111111106, 2.9744, 8.671608888888894, 9.004391111111106, 3.9744, 9.168408888888894),
                    Block.box(7.004391111111107, 2, 7.995608888888892, 8.004391111111106, 3, 8.995608888888892),
                    Block.box(7.004391111111107, 3, 7.995608888888892, 8.004391111111106, 4, 8.995608888888892),
                    Block.box(7.004391111111107, 1, 7.995608888888892, 8.004391111111106, 2, 8.995608888888892),
                    Block.box(8.004391111111106, -1, 7.995608888888892, 9.004391111111106, 0, 8.995608888888892),
                    Block.box(6.831591111111106, 2.974400000000002, 7.995608888888892, 7.328391111111106, 3.974400000000002, 8.995608888888892),
                    Block.box(8.831591111111106, 2.974400000000002, 6.995608888888892, 9.328391111111106, 3.974400000000002, 7.995608888888892),
                    Block.box(8.831591111111106, 2.974400000000002, 7.995608888888892, 9.328391111111106, 3.974400000000002, 8.995608888888892),
                    Block.box(7.004391111111106, 2.9744, 8.671608888888894, 8.004391111111106, 3.9744, 9.168408888888894),
                    Block.box(7, 2.9744, 6.671608888888893, 8, 3.9744, 7.168408888888894),
                    Block.box(8.004391111111106, 2.9744, 6.671608888888893, 9.004391111111106, 3.9744, 7.1684088888888935),
                    Block.box(8.004391111111108, 4, 6.995608888888892, 9.004391111111106, 4.5, 7.995608888888892),
                    Block.box(8.004391111111108, 4, 7.995608888888892, 9.004391111111106, 4.5, 8.995608888888892),
                    Block.box(7.004391111111108, 4, 7.995608888888892, 8.004391111111106, 4.5, 8.995608888888892))
            .reduce((v1, v2) -> Shapes.join(v1, v2, BooleanOp.OR));



    @Override
    public boolean onDestroyedByPlayer(BlockState state, Level world, BlockPos pos, Player player, boolean willHarvest, FluidState fluid) {
        ItemStack rockstack1 = new ItemStack(Items.CARROT);
        ItemEntity rock1 = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), rockstack1);
        world.addFreshEntity(rock1);
        return super.onDestroyedByPlayer(state, world, pos, player, willHarvest, fluid);
    }

    @Override
    public boolean isFlammable(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return true;
    }

    @Override
    public int getFlammability(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 100;
    }

    @Override
    public int getFireSpreadSpeed(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 60;
    }
}

