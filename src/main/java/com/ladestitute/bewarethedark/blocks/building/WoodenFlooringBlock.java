package com.ladestitute.bewarethedark.blocks.building;

import com.ladestitute.bewarethedark.registries.BlockInit;
import com.ladestitute.bewarethedark.registries.ItemInit;
import com.ladestitute.bewarethedark.registries.SpecialBlockInit;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.material.FluidState;
import net.minecraftforge.common.IPlantable;

import javax.annotation.Nullable;
import java.util.List;

public class WoodenFlooringBlock extends Block {

    public WoodenFlooringBlock(Properties properties) {
        super(properties);
    }

    @Override
    public void appendHoverText(ItemStack p_49816_, @Nullable BlockGetter p_49817_, List<Component> p_49818_, TooltipFlag p_49819_) {
        p_49818_.add(Component.literal("High-quality laminate floorboards."));
        super.appendHoverText(p_49816_, p_49817_, p_49818_, p_49819_);
    }

    @Override
    public boolean canSustainPlant(BlockState state, BlockGetter world, BlockPos pos, Direction facing, IPlantable plantable) {
        return false;
    }

    @Override
    public boolean isFlammable(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return true;
    }

    @Override
    public int getFlammability(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 20;
    }

    @Override
    public int getFireSpreadSpeed(BlockState state, BlockGetter world, BlockPos pos, Direction face) {
        return 5;
    }

    @Override
    public boolean onDestroyedByPlayer(BlockState state, Level world, BlockPos pos, Player player, boolean willHarvest, FluidState fluid) {
        ItemStack pitchforkstack = player.getItemBySlot(EquipmentSlot.MAINHAND);
        ItemStack turfstack = new ItemStack(BlockInit.WOODEN_FLOORING.get());
        ItemEntity turf = new ItemEntity(world, pos.getX(), pos.getY(), pos.getZ(), turfstack);

        if(pitchforkstack.getItem() == ItemInit.PITCHFORK.get())
        {
            world.addFreshEntity(turf);
            player.getMainHandItem().hurtAndBreak(1, player, (p_220045_0_) -> {
                p_220045_0_.broadcastBreakEvent(EquipmentSlot.MAINHAND);
            });
        }
        return super.onDestroyedByPlayer(state, world, pos, player, willHarvest, fluid);
    }
}
