package com.ladestitute.bewarethedark;

public class Sandbox {

    //Throwaway class for testing bits of code and notes

    // In Don't Starve, the damage for unarmed player attacks is 10
    // So respectively, unarmed damage of players in MC is 1
    // This can be to translate weapon or mob damage to Minecraft
    // by calculating what percentage of weapon/mob damage is of 10 (i.e, a tentacle spike is 510% of unarmed attacks)
    // then inputting what the result is of 1 (5.1 is 510% of 1)

}
