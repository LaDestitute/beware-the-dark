package com.ladestitute.bewarethedark.dimension;

import com.ladestitute.bewarethedark.BTDMain;
import com.ladestitute.bewarethedark.blocks.entity.SinkholeBlockEntity;
import com.ladestitute.bewarethedark.registries.BlockInit;
import com.ladestitute.bewarethedark.registries.SpecialBlockInit;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.common.util.ITeleporter;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.LevelChunk;
import net.minecraftforge.common.util.ITeleporter;

import java.util.Comparator;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Stream;

public class CavesDimensionTeleporter implements ITeleporter {

        private final BlockPos pos;

    public CavesDimensionTeleporter(BlockPos pos) {
            this.pos = pos;
        }

    @Override
    public boolean playTeleportSound(ServerPlayer player, ServerLevel sourceWorld, ServerLevel destWorld) {
        return false;
    }

    @Override
    public Entity placeEntity(Entity entity, ServerLevel currentWorld, ServerLevel destWorld, float yaw, Function<Boolean, Entity> repositionEntity) {
        Entity e = repositionEntity.apply(false);
        if (!(e instanceof ServerPlayer)) {
            return e;
        }
        ServerPlayer player = (ServerPlayer) e;
        LevelChunk chunk = (LevelChunk) destWorld.getChunk(pos);
        BlockPos teleporterPos = findPortalInChunk(chunk);

        if (teleporterPos == null) {
            if (destWorld.dimension().equals(BTDMain.CAVES_DIMENSION)) {
                teleporterPos = placeTeleporterMining(destWorld, chunk);
            } else {
                teleporterPos = placeTeleporterOverworld(destWorld, chunk);
            }
        }
        if (teleporterPos == null) {
            return e;
        }

        player.giveExperienceLevels(0);
        player.teleportTo(teleporterPos.getX() + 0.5D, teleporterPos.getY() + 1D, teleporterPos.getZ() + 0.5D);
        return e;
    }

    public boolean spawnDeep = false;

    private BlockPos findPortalInChunk(LevelChunk chunk) {
        Stream<Map.Entry<BlockPos, BlockEntity>> stream = chunk.getBlockEntities()
                .entrySet()
                .stream()
                .filter(entry -> entry.getValue() instanceof SinkholeBlockEntity);

        BlockPos teleporter;

        if (spawnDeep) {
            teleporter = stream.sorted(Comparator.comparingInt(o -> o.getKey().getY())).map(Map.Entry::getKey).findFirst().orElse(null);
        } else {
            teleporter = stream.sorted((o1, o2) -> o2.getKey().getY() - o1.getKey().getY()).map(Map.Entry::getKey).findFirst().orElse(null);
        }

        if (teleporter != null) {
            if (chunk.getBlockState(teleporter.above()).isAir()) {
                return teleporter;
            }
        }
        return null;
    }

    private BlockPos placeTeleporterMining(ServerLevel world, LevelChunk chunk) {
        boolean deep = false;
        BlockPos.MutableBlockPos pos = new BlockPos.MutableBlockPos();
        int min = world.getMinBuildHeight();
        int max = 85;
        for (int y = deep ? min : max - 1; (deep ? y < max - 1 : y >= min); y = (deep ? y + 1 : y - 1)) {
            for (int x = 0; x < 16; x++) {
                for (int z = 0; z < 16; z++) {
                    pos.set(x, y, z);
                    if (y <= 85 && y >= 15 && isSolid(chunk, pos) && isStone(chunk, pos) && chunk.getBlockState(pos.above(1)).isAir() && chunk.getBlockState(pos.above(2)).isAir()) {
                        BlockPos absolutePos = chunk.getPos().getWorldPosition().offset(pos.getX(), pos.getY(), pos.getZ());
                        world.setBlockAndUpdate(absolutePos, BlockInit.SINKHOLE.get().defaultBlockState());
                        if(world.getBiome(absolutePos).is(new ResourceLocation("bewarethedark", "red_mushtree_forest")))
                        {
                            world.setBlockAndUpdate(absolutePos.below(), BlockInit.RED_FUNGAL_TURF.get().defaultBlockState());
                        }
                        if(world.getBiome(absolutePos).is(new ResourceLocation("bewarethedark", "green_mushtree_forest")))
                        {
                            world.setBlockAndUpdate(absolutePos.below(), BlockInit.GREEN_FUNGAL_TURF.get().defaultBlockState());
                        }
                        if(world.getBiome(absolutePos).is(new ResourceLocation("bewarethedark", "blue_mushtree_forest")))
                        {
                            world.setBlockAndUpdate(absolutePos.below(), BlockInit.BLUE_FUNGAL_TURF.get().defaultBlockState());
                        }
                        if(world.getBiome(absolutePos).is(new ResourceLocation("bewarethedark", "cave_marsh")))
                        {
                            world.setBlockAndUpdate(absolutePos.below(), BlockInit.MARSH_TURF.get().defaultBlockState());
                        }
                        if(world.getBiome(absolutePos).is(new ResourceLocation("bewarethedark", "sunken_forest")) && y <= 31)
                        {
                            world.setBlockAndUpdate(absolutePos.below(), Blocks.MUD.defaultBlockState());
                        }
                        if(world.getBiome(absolutePos).is(new ResourceLocation("bewarethedark", "sunken_forest")) && y >= 32)
                        {
                            world.setBlockAndUpdate(absolutePos.below(), BlockInit.FOREST_TURF.get().defaultBlockState());
                        }
                        return absolutePos;
                    }
                }
            }
        }

        for (int y = deep ? min : max - 1; (deep ? y < max - 1 : y >= min); y = (deep ? y + 1 : y - 1)) {
            for (int x = 0; x < 16; x++) {
                for (int z = 0; z < 16; z++) {
                    pos.set(x, y, z);
                    if (y <= 85 && y >= 15 && isAirOrStone(chunk, pos) && isAirOrStone(chunk, pos.above(1)) && isAirOrStone(chunk, pos.above(2))) {
                        BlockPos absolutePos = chunk.getPos().getWorldPosition().offset(pos.getX(), pos.getY(), pos.getZ());
                        if (isReplaceable(world, absolutePos.above(3)) &&
                                isReplaceable(world, absolutePos.above(1).relative(Direction.NORTH)) &&
                                isReplaceable(world, absolutePos.above(1).relative(Direction.NORTH)) &&
                                isReplaceable(world, absolutePos.above(1).relative(Direction.SOUTH)) &&
                                isReplaceable(world, absolutePos.above(1).relative(Direction.EAST)) &&
                                isReplaceable(world, absolutePos.above(1).relative(Direction.WEST)) &&
                                isReplaceable(world, absolutePos.above(2).relative(Direction.NORTH)) &&
                                isReplaceable(world, absolutePos.above(2).relative(Direction.SOUTH)) &&
                                isReplaceable(world, absolutePos.above(2).relative(Direction.EAST)) &&
                                isReplaceable(world, absolutePos.above(2).relative(Direction.WEST))
                        ) {
                            world.setBlockAndUpdate(absolutePos, BlockInit.SINKHOLE.get().defaultBlockState());
                            world.setBlockAndUpdate(absolutePos.above(1), Blocks.AIR.defaultBlockState());
                            world.setBlockAndUpdate(absolutePos.above(2), Blocks.AIR.defaultBlockState());
                            world.setBlockAndUpdate(absolutePos.above(3), Blocks.STONE.defaultBlockState());
                            world.setBlockAndUpdate(absolutePos.above(1).relative(Direction.NORTH), Blocks.STONE.defaultBlockState());
                            world.setBlockAndUpdate(absolutePos.above(1).relative(Direction.SOUTH), Blocks.STONE.defaultBlockState());
                            world.setBlockAndUpdate(absolutePos.above(1).relative(Direction.EAST), Blocks.STONE.defaultBlockState());
                            world.setBlockAndUpdate(absolutePos.above(1).relative(Direction.WEST), Blocks.STONE.defaultBlockState());
                            world.setBlockAndUpdate(absolutePos.above(2).relative(Direction.NORTH), Blocks.STONE.defaultBlockState());
                            world.setBlockAndUpdate(absolutePos.above(2).relative(Direction.SOUTH), Blocks.STONE.defaultBlockState());
                            world.setBlockAndUpdate(absolutePos.above(2).relative(Direction.EAST), Blocks.STONE.defaultBlockState());
                            world.setBlockAndUpdate(absolutePos.above(2).relative(Direction.WEST), Blocks.STONE.defaultBlockState());
                            return absolutePos;
                        }
                    }
                }
            }
        }

        return null;
    }

    private boolean isAirOrStone(LevelChunk chunk, BlockPos pos) {
        BlockState state = chunk.getBlockState(pos);
        return state.getBlock().equals(Blocks.STONE) || state.isAir();
    }

    private boolean isStone(LevelChunk chunk, BlockPos pos) {
        BlockState state = chunk.getBlockState(pos);
        return state.getBlock().equals(Blocks.STONE);
    }

    private boolean isSolid(LevelChunk chunk, BlockPos pos) {
        BlockState state = chunk.getBlockState(pos);
        return !state.getBlock().equals(Blocks.LAVA)||
                !state.getBlock().equals(Blocks.WATER)||
                !state.getBlock().equals(Blocks.AIR);
    }

    private boolean isReplaceable(Level world, BlockPos pos) {
        BlockState state = world.getBlockState(pos);
        return state.getBlock().equals(Blocks.STONE) ||
                state.getBlock().equals(Blocks.GRANITE) ||
                state.getBlock().equals(Blocks.ANDESITE) ||
                state.getBlock().equals(Blocks.DIORITE) ||
                state.getBlock().equals(Blocks.DIRT) ||
                state.getBlock().equals(Blocks.GRAVEL) ||
                state.getBlock().equals(Blocks.LAVA) ||
                state.isAir();
    }

    private BlockPos placeTeleporterOverworld(ServerLevel world, LevelChunk chunk) {
        BlockPos.MutableBlockPos pos = new BlockPos.MutableBlockPos();
        for (int x = 0; x < 16; x++) {
            for (int z = 0; z < 16; z++) {
                for (int y = world.getSeaLevel(); y < world.getMaxBuildHeight(); y++) {
                    pos.set(x, y, z);
                    if (chunk.getBlockState(pos).isAir() && chunk.getBlockState(pos.above(1)).isAir()) {
                        BlockPos absolutePos = chunk.getPos().getWorldPosition().offset(pos.getX(), pos.getY(), pos.getZ());
                        world.setBlockAndUpdate(absolutePos, BlockInit.SINKHOLE.get().defaultBlockState());
                        return absolutePos;
                    }
                }
            }
        }
        return null;
    }
}
